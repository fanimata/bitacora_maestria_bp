# **Bitácora: Febrero, 2020**


#### *Lunes 3 de febrero de 2020*


#### Continuacion de Pipeline de Cabili


#### Filtro 3: Identificacion de secuencias homologas a la base de datos de Pfam


Se corrio en Mazorka, en linea de comandos el *hmmpress* para preparar la base de datos del *Pfam*, se realizo asi debido a que no se pudo realizar al correr el *script* de *hmmscan* 

    $ cat hmmscan_st_NoN.err

    Error: Failed to open binary auxfiles for Pfam-A.hmm: use hmmpress first

    $ module load hmmer/3.1b2
    $ hmmpress Pfam-A.hmm

    Working...    done.
    Pressed and indexed 16295 HMMs (16295 names and 16295 accessions).
    Models pressed into binary file:   Pfam-A.hmm.h3m
    SSI index for binary model file:   Pfam-A.hmm.h3i
    Profiles (MSV part) pressed into:  Pfam-A.hmm.h3f
    Profiles (remainder) pressed into: Pfam-A.hmm.h3p

Una vez preparada la base de datos se corrio el modulo de *hmmer* para determinar secuencias homologas

   #PBS -N hmmscan_st_NoN
   #PBS -q default
   #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=24:00:00
   #PBS -V
   #PBS -o hmmscan_st_NoN.out
   #PBS -e hmmscan_st_NoN.err

   module load hmmer/3.1b2

   cd $PBS_O_WORKDIR

   hmmscan --cpu 2 --domtblout TrinotatePFAM_st_NoN.out Pfam-A.hmm filter_getorf_st_NoN.pep > pfam_st_NoN.log

---------------------------------

Por otra parte se conto la cantidad de N's presentes en el archivo "filter_CDS_st.fasta" (archivo fasta despues de filtrar los CDS, del cual se removio las secuencias incompletas). Se uso un *script* de *perl* al cual se le llamo findN.pl (https://www.biostars.org/p/19426/)

    #!/usr/bin/env perl

    use strict;
    use warnings;
    use Bio::SeqIO;

    my $usage = "$0 infile\n";
    my $in = shift or die $usage;

    my $seq_in = Bio::SeqIO->new(-file => $in, -format => 'fasta');

    my ($n_count, $total, $seq_ct, $n_ratio, $n_ratio_t, $n_perc, $n_perc_t) = (0, 0, 0, 0, 0, 0, 0);

    print "Seq_ID\tLength\tN_count\tN_ratio\tN_perc\n";

    while(my $seq = $seq_in->next_seq) {
        if ($seq->length > 0) {
            $seq_ct++;
            $total += $seq->length;
            $n_count = ($seq->seq =~ tr/Nn//);
            $n_perc  = sprintf("%.2f",$n_count/$seq->length);
            $n_ratio = sprintf("%.2f",$n_count/($seq->length - $n_count));
            $n_ratio_t += $n_ratio;
            $n_perc_t  += $n_perc; 
            print join("\t",($seq->id,$seq->length,$n_count,$n_ratio,$n_perc)),"\n";
        }
    }

    my $len_ave     = sprintf("%.0f",$total/$seq_ct);
    my $n_ratio_ave = sprintf("%.2f",$n_ratio_t/$seq_ct);
    my $n_perc_ave  = sprintf("%.2f",$n_perc_t/$seq_ct);

    print "\nAverage length, N-ratio and N-percent for $in :   $len_ave\t$n_ratio_ave\t$n_perc_ave\n";

Y se ejecuto de la siguiente manera

    $ perl findN.pl filter_CDS_st.fasta

    Average length, N-ratio and N-percent for filter_CDS_st.fasta :   7970	0.01	0.00

Tambien se aplico sobre el archivo *fasta* que contiene solo las secuencias completas (archivo fasta inicial filtrado)

    $ perl findN.pl filter_CDS_st_NoN.fasta

    Average length, N-ratio and N-percent for filter_CDS_st_NoN.fasta :   6903	0.00	0.00



#### *Martes 4 de febrero de 2020*


Finalizada la corrida del *hmmscan* se reviso el archivo de salida "TrinotatePFAM_st_NoN.out" en donde se encontro que algunos transcritos resultaron homologos a algunos grupos proteicos
como proteinas transmembrana, proteinas de funcion desconocida, proteinas de sistemas de secrecion, cinasas, entre otras.

    $ head TrinotatePFAM_st_NoN.out 

    #                                                                            --- full sequence --- -------------- this domain -------------   hmm coord   ali coord   env coord
    # target name        accession   tlen query name           accession   qlen   E-value  score  bias   #  of  c-Evalue  i-Evalue  score  bias  from    to  from    to  from    to  acc description of target
    #------------------- ---------- ----- -------------------- ---------- ----- --------- ------ ----- --- --- --------- --------- ------ ----- ----- ----- ----- ----- ----- ----- ---- ---------------------
    Spp-24               PF07448.8     64 MSTRG.100.1.p18      -            147      0.09   12.8   1.3   1   1     1e-05      0.16   12.0   1.3    29    53   115   139   106   142 0.92 Secreted phosphoprotein 24 (Spp-24) cystatin-like domain
    Mucin                PF01456.14   143 MSTRG.100.1.p2       -            267   2.2e-06   27.6  21.7   1   1   9.1e-10   3.7e-06   26.8  21.7    12   109    79   174    69   190 0.59 Mucin-like glycoprotein
    FAM163               PF15069.3    166 MSTRG.100.1.p2       -            267     0.011   16.1   6.3   1   1   4.1e-06     0.017   15.5   6.3    16   104    81   173    76   183 0.64 FAM163 family
    Tmemb_cc2            PF10267.6    396 MSTRG.100.1.p2       -            267      0.18   10.8   2.9   1   1   5.6e-05      0.23   10.4   2.9   136   203   101   168    82   193 0.49 Predicted transmembrane and coiled-coil 2 protein
    Lamp                 PF01299.14   313 MSTRG.100.1.p2       -            267      0.25   10.8  18.7   1   1   9.5e-05      0.39   10.1  18.7    50   114   111   172    79   191 0.43 Lysosome-associated membrane glycoprotein (Lamp)
    SelP_N               PF04592.11   233 MSTRG.100.1.p1       -            285       5.9    6.1  13.1   1   2      0.13   2.2e+03   -2.3   0.0    82   102    35    55    31    57 0.82 Selenoprotein P, N terminal region
    SelP_N               PF04592.11   233 MSTRG.100.1.p1       -            285       5.9    6.1  13.1   2   2    0.0012        19    4.4  13.6   186   212   168   194   149   209 0.46 Selenoprotein P, N terminal region


Se extrajo todos aquellos ID's de transcritos que homologos con proteinas de la base de datos del *Pfam*

    $ awk '{print $4}' TrinotatePFAM_st_NoN.out > list_pfam_st_NoN
    $ head list_pfam_st_NoN

    sequence
    accession
    --------------------
    MSTRG.100.1.p18
    MSTRG.100.1.p2
    MSTRG.100.1.p2
    MSTRG.100.1.p2
    MSTRG.100.1.p2
    MSTRG.100.1.p1
    MSTRG.100.1.p1

    $ tail list_pfam_st_NoN 

    (February
    SCAN
    filter_getorf_st_NoN.pep
    Pfam-A.hmm
    hmmscan
    /NFS/LUSTRE/usuario/smata/BBRB/Cabili/hmmscan
    Feb


De la lista se removio las primeras 3 lineas y las ultimas 10

    $ wc -l list_pfam_st_NoN # Son 80 853 lineas
    $ sed 1,3d list_pfam_st_NoN | head -n-10 > list_pfam_st_Noblank
    $ head list_st_No_blank 
    
    MSTRG.100.1.p18
    MSTRG.100.1.p2
    MSTRG.100.1.p2
    MSTRG.100.1.p2
    MSTRG.100.1.p2
    MSTRG.100.1.p1
    MSTRG.100.1.p1
    MSTRG.100.1.p3
    MSTRG.100.1.p3
    MSTRG.100.1.p11
   
    $ tail list_st_No_blank 

    MSTRG.9994.2.p2
    MSTRG.9994.2.p2
    MSTRG.9994.2.p7
    MSTRG.9994.2.p12
    MSTRG.9994.2.p12
    MSTRG.9997.1.p1
    MSTRG.9997.1.p1
    MSTRG.9997.1.p1
    MSTRG.9997.1.p2
    MSTRG.9997.1.p2

    $ wc -l list_st_No_blank # Son 80 840 lineas

Se edito la lista para eliminar las "p#" de los ID's y ID's redundantes.

    $ perl -pe 's/\.p[0-9].*\n/\n/' list_st_No_blank | sort -V | uniq > list_pfam_st_Noblank_nr
    $ head list_st_No_blank_nr

    MSTRG.2.6
    MSTRG.3.1
    MSTRG.3.2
    MSTRG.3.3
    MSTRG.6.1
    MSTRG.8.1
    MSTRG.12.2
    MSTRG.16.1
    MSTRG.16.2
    MSTRG.25.1

    $ wc -l list_st_No_blank_nr # Son 6 695 ID's 

Se procedio a extraer la lista de ID's del archivo bed12 del filtro anterior

    $ 


#### *Miercoles 5 de febrero de 2020*


Los archivos *gtf* de la anotacion de referencia, el ensamble guiado, el ensamble guiado anotado, el ensamble de novo y el ensamble de novo anotado, se indexaron de la siguiente manera


   $ bedtools sort -i Bbraunii_502_v2.1.gene.anot.gtf > Bbraunii_502_v2.1.gene.anot.sort.gtf
   $ bgzip -i Bbraunii_502_v2.1.gene.anot.sort.gtf
   $ tabix -p gff Bbraunii_502_v2.1.gene.anot.sort.gtf.gz
 
   $ bedtools sort -i BBRB_v2_transcriptome.annotated.gtf > BBRB_v2_transcriptome.annotated.sort.gtf
   $ bgzip -i BBRB_v2_transcriptome.annotated.sort.gtf 
   $ tabix -p gff BBRB_v2_transcriptome.annotated.sort.gtf.gz
  
   $ bedtools sort -i BBRB_v2_transcriptome.gtf > BBRB_v2_transcriptome.sort.gtf
   $ bgzip -i BBRB_v2_transcriptome.sort.gtf
   $ tabix -p gff BBRB_v2_transcriptome.sort.gtf.gz

   $ bedtools sort -i BBRB_novo_transcriptome.gff > BBRB_novo_transcriptome.sort.gff
   $ bgzip -i BBRB_novo_transcriptome.sort.gff
   $ tabix -p gff BBRB_novo_transcriptome.sort.gff.gz

   $ bedtools sort -i BBRB_novo_transcriptome_annotated.gtf > BBRB_novo_transcriptome_annotated.sort.gtf 
   $ bgzip -i BBRB_novo_transcriptome_annotated.sort.gtf 
   $ tabix -p gff BBRB_novo_transcriptome_annotated.sort.gtf.gz


NOTA: Se tenia que indexar el genona de la microalga en fasta, pero ya se contaba con el archivo ".fai"

Se creo un archivo llamado "tracks.conf" y se edito con la informacion de cada archivo


    [GENERAL]
    refSeqs=Bbraunii_genome_502_v2.0.fa.fai

    [tracks.Botryococcus braunii genome]
    urlTemplate=Bbraunii_genome_502_v2.0.fa
    storeClass=JBrowse/Store/SeqFeature/IndexedFasta
    type=Sequence
    key=Botryococcus braunii genome

    [tracks.Botryococcus braunii reference annotation]
    urlTemplate=Bbraunii_502_v2.1.gene.anot.sort.gtf.gz
    storeClass=JBrowse/Store/SeqFeature/GFF3Tabix
    type=CanvasFeatures
    key=Botryococcus braunii reference annotation

    [tracks.Botryococcus braunii original guided assembly]
    urlTemplate=BBRB_v2_transcriptome.sort.gtf.gz
    storeClass=JBrowse/Store/SeqFeature/GFF3Tabix
    type=CanvasFeatures
    key=Botryococcus braunii original guided assembly

    [tracks.Botryococcus braunii annotated guided assembly]
    urlTemplate=BBRB_v2_transcriptome.annotated.sort.gtf.gz
    storeClass=JBrowse/Store/SeqFeature/GFF3Tabix
    type=CanvasFeatures
    key=Botryococcus braunii annotated guided assembly

    [tracks.Botryococcus braunii original novo assembly]
    urlTemplate=BBRB_novo_transcriptome.sort.gff.gz
    storeClass=JBrowse/Store/SeqFeature/GFF3Tabix
    type=CanvasFeatures
    key=Botryococcus braunii original novo assembly

    [tracks.Botryococcus braunii annotated novo assembly]
    urlTemplate=BBRB_novo_transcriptome_annotated.sort.gtf.gz
    storeClass=JBrowse/Store/SeqFeature/GFF3Tabix
    type=CanvasFeatures
    key=Botryococcus braunii annotated novo assembly


En el portal de agrogenomica se cargaron los archivos y los tracks para visualizar el genoma y las anotaciones de referencia y de los ensambles

Portal: https://agrogenomica.ddns.net

Enviar a la Dra este link: https://agrogenomica.ddns.net/jbrowse/?data=%2Fgenomes%2FBotryococcus_braunii


#### *Viernes 7 de febrero de 2020*


Revise con la Dra que estaba pasando mi archivo gtf al convertirlo de de gtf a bed12 y de bed12 a fasta
Encontramos que realmente cuando hacia la conversion de bed12 a fasta no usaba la opcion de -split, para poder tener la secuencia de mis transcriptos maduros, por lo que de nuevo se convirtio de bed12 a fasta

    $ /home/fani/gtfToGenePred BBRB_v2_transcriptome.annotated.gtf BBRB_v2_transcriptome.annotated.genePred
    $ /home/fani/genePredToBed BBRB_v2_transcriptome.annotated.genePred BBRB_v2_transcriptome.annotated.bed12
    $ bedtools getfasta -split -fi /home/fani/Documentos/I_Semestre_2019/Tesis/BBRB_analisis_bioinfo/genome_data/genome_v2.1/assembly/Bbraunii_genome_502_v2.0.fa -bed BBRB_v2_transcriptome.annotated.bed12 -name -fo BBRB_v2_transcriptome.annotated.fasta
    $ fold -w 60 BBRB_v2_transcriptome.annotated.fasta > folded_transcritome.fasta
    $ mv folded_transcritome.fasta ./BBRB_v2_transcriptome.annotated.fasta

De ahi revisamos cuantas N's habian en mi archivo fasta

    $ grep N BBRB_v2_transcriptome.annotated.fasta | wc -l # 165 transcritos con N's

Como no eran tantos se tomo la decision de conservar los transcritos y seguir trabajando el archivo bed12 para los filtros

NOTA: Todo esto se realizo dentro de la carpeta del ensamble guiado de *StringTie* y el archivo bed12 de este fasta es el correcto para trabajar!!


#### Pipeline de Cabili

#### Filtro 1: Longitud de secuencias (remover secuencias con tamano mayor o igual a 200 nt)

Primero revisamos el tamano de los transcritos en el archivo fasta que esta en la carpeta del ensamble guiado

    $ infoseq BBRB_v2_transcriptome.annotated.fasta -only -name -type -length | sort -k 3n | head

Name           Type Length 
Bobra.0012s0078.1.v2.1 N    90     
Bobra.0151s0014.1.v2.1 N    96     
Bobra.0758s0002.1.v2.1 N    96     
Bobra.0016s0011.1.v2.1 N    99     
Bobra.0159s0201.1.v2.1 N    102    
Bobra.0675s0001.1.v2.1 N    105    
Bobra.0057s0013.1.v2.1 N    108    
Bobra.0120s0017.1.v2.1 N    108    
Bobra.0151s0048.1.v2.1 N    108   

Con esto se observo que hay transcritos con longitud menor a 200 nt que seran removidos en este filtro.
Para removerlos se uso el comando *awk*, de tal manera que hiciera la resta entre la columna 3 y 2 que corresponden a las coordenadas de inicio y fin de los transcritos y se conservara solo aquellas lineas de transcritos con longitud mayor a 200 nt. Para esto el archivo fasta del transcriptoma se copio a la carpeta de Cabili del ensamble guiado	

    $ cp BBRB_v2_transcriptome.annotated.fasta ../../Cabili/guided_ensam/

Se saco la lista de los transcritos y sus tamanos

    $ infoseq BBRB_v2_transcriptome.annotated.fasta -only -name -type -length | sort -k 3n > transcripts_length
    
Se extrajeron aquellos con tamano menor a 200nt en una lista aparte

    $ awk '$3 < 200 {print}' transcripts_length > transcripts_less_than_200
    $ head transcripts_less_than_200 

Bobra.0012s0078.1.v2.1 N    90     
Bobra.0151s0014.1.v2.1 N    96     
Bobra.0758s0002.1.v2.1 N    96     
Bobra.0016s0011.1.v2.1 N    99     
Bobra.0159s0201.1.v2.1 N    102    
Bobra.0675s0001.1.v2.1 N    105    
Bobra.0057s0013.1.v2.1 N    108    
Bobra.0120s0017.1.v2.1 N    108    
Bobra.0151s0048.1.v2.1 N    108    
Bobra.160_2s0034.1.v2.1 N    108  

    $ tail transcripts_less_than_200

Bobra.0194s0016.1.v2.1 N    198    
Bobra.0195s0013.1.v2.1 N    198    
Bobra.0344s0023.1.v2.1 N    198    
Bobra.0396s0027.1.v2.1 N    198    
Bobra.152_2s0026.1.v2.1 N    198    
Bobra.152_2s0027.1.v2.1 N    198    
Bobra.180_1s0004.1.v2.1 N    198    
Bobra.55_2s0049.1.v2.1 N    198    
Bobra.67_1s0030.1.v2.1 N    198    
Bobra.0052s0020.1.v2.1 N    199 

    $ wc -l transcripts_less_than_200
    315 transcripts_less_than_200

Se extrajeron aquellos con tamano mayor o igual a 200nt en una lista aparte

    $ awk '$3 >= 200 {print}' transcripts_length > transcripts_more_than_200
    $ head transcripts_more_than_200

Name           Type Length 
Bobra.168_1s0032.1.v2.1 N    200    
MSTRG.11131.2  N    200    
MSTRG.11258.1  N    200    
MSTRG.12203.1  N    200    
MSTRG.12646.1  N    200    
MSTRG.13676.1  N    200    
MSTRG.16335.1  N    200    
MSTRG.2365.1   N    200    
MSTRG.3629.1   N    200  

    $ tail transcripts_more_than_200

MSTRG.14861.2  N    16798  
MSTRG.14861.1  N    16803  
Bobra.0054s0027.1.v2.1 N    16817  
Bobra.110_2s0053.4.v2.1 N    16966  
Bobra.110_2s0053.3.v2.1 N    16969  
Bobra.110_2s0053.2.v2.1 N    17545  
Bobra.110_2s0053.1.v2.1 N    17548  
MSTRG.14861.9  N    17755  
MSTRG.14861.10 N    17782  
MSTRG.14861.5  N    18010  

    $ wc -l transcripts_more_than_200
    36673 transcripts_more_than_200   # Menos 1 por el encabezado, entonces serian 36 672 transcritos

Se extrajo solo los ID's de "transcripts_more_than_200", se removio el encabezado y se le agrego un ">" al inicio de cada ID

    $ awk '{print $1}' transcripts_more_than_200 | sed '1d' > ID_list_more_than_200
    $ wc -l ID_list_more_than_200 
    36672 ID_list_more_than_200

Se comparo con *faSomeRecords* la lista de "ID_list_more_than_200" y el archivo fasta original, y todo lo que no coincidia se elimino

    $ faSomeRecords BBRB_v2_transcriptome.annotated.fasta ID_list_more_than_200 filter_200nt_st.fasta
    $ head filter_200nt_st.fasta 

>Bobra.0001s0001.1.v2.1
ATGGAGGGCACTAAGGTGGATAGGGCTGATCTCTTCATGAGAAAAATGCAAGACAAAATC
ACCCTTGCAAGGAAGTGTTTGCGTGCAGCTCAAGACCGTCAGACGGCCTATGCCAACCGG
CATAGGAGGGAGGCCATCTACCAGGAAGGGGACTATGTCCTCCTTTCAACCCGTGCGGCC
CCCACGCTAACTTCAAACAGATTCAGCAAGCTGCAACCCAGGCACGTGGGACCCTTCAGG
GTCATGGCGGTCACGCCCGACAACGTTAACTTCGTCACACTGGATCTTCCGGACCGGCTG
GACATCCACCCCCGCATAAATGTCTGCTATTTGAAGCCATACTGGACACGTGAGGAGGCG
CAGTCGCGTGGATACTGCCCCCCAAAAGTCATCACCAAAGCGGATGGCACCACTCACCTT
GAGTGGGTAGTGGATGCACTAGTCGATCACCGGCTGTTGCACGACAAGGATGGGTTCTCC
TACCATTTTGAGGATGGCACACCCTTTCTCGAGTACCTTGTAAAGTGGGCAGGCCATGGG


Para verificar el archivo se tomo el ID de unos cuantos transcritos de la lista "ID_list_more_than_200":

    Bobra.0758s0002.1.v2.1 # 96 nt
    Bobra.0073s0072.1.v2.1 # 144 nt
    Bobra.0036s0042.1.v2.1 # 177 nt
    Bobra.0052s0020.1.v2.1 # 199 nt

Una vez seleccionado al azar los ID's se busco en el archivo bed12 del filtro

   $ grep 'Bobra.0758s0002.1.v2.1' filter_200nt_st.fasta  
   $ grep 'Bobra.0073s0072.1.v2.1' filter_200nt_st.fasta   
   $ grep 'Bobra.0036s0042.1.v2.1' filter_200nt_st.fasta
   $ grep 'Bobra.0052s0020.1.v2.1' filter_200nt_st.fasta

Ninguno de los ID's anteriores se encontro en el archivo fasta filtrado por tamano

Se realizo de nuevo el calculo de los tamanos usando *infoseq* del fasta del filtro para verificar que las secuencias si conservar la misma longotud que aquellas de la lista de transcritos con tamano mayor o igual a 200nt, esto para ver si no hubo algun error en el proceso

   $ infoseq filter_200nt_st.fasta -only -name -type -length | sort -k 3n > transcripts_200_length
   $ head transcripts_200_length 

Name           Type Length 
Bobra.168_1s0032.1.v2.1 N    200    
MSTRG.11131.2  N    200    
MSTRG.11258.1  N    200    
MSTRG.12203.1  N    200    
MSTRG.12646.1  N    200    
MSTRG.13676.1  N    200    
MSTRG.16335.1  N    200    
MSTRG.2365.1   N    200    
MSTRG.3629.1   N    200 
   
   $ tail transcripts_200_length 

MSTRG.14861.2  N    16798  
MSTRG.14861.1  N    16803  
Bobra.0054s0027.1.v2.1 N    16817  
Bobra.110_2s0053.4.v2.1 N    16966  
Bobra.110_2s0053.3.v2.1 N    16969  
Bobra.110_2s0053.2.v2.1 N    17545  
Bobra.110_2s0053.1.v2.1 N    17548  
MSTRG.14861.9  N    17755  
MSTRG.14861.10 N    17782  
MSTRG.14861.5  N    18010   

   $ wc -l transcripts_200_length
   36673 transcripts_200_length  # Menos 1 por el encabezado

   $ grep '>' filter_200nt_st.fasta | wc -l
   36672

Al final del filtro nos quedamos con 36 672 transcritos con longitud mayor o igual a 200 nt y eliminamos 315 transcritos con un tamano inferior a 200nt


#### *Lunes 10 de febrero de 2020*

#### Filtro 2: Prediccion de ORF con getorf (conservar ID's que no esten en el output de getorf)


Usando *getorf* se predijo marcos abiertos de lectura entre codones de stop en el archivo del filtro por longitud

NOTA: Se usa la opcion -reverse F ya que no queremos traducir el reverso complemento, las librerias son *stranded*

    $ getorf -reverse F -minsize 225 -find 0 filter_200nt_st.fasta ORFs_225_st.fasta
    $ grep '>' ORFs_225_st.fasta | wc -l
    217229 # Total de ORF encontrados en el archivo

Del archivo fasta de ORF's se calculo el tamano de los marcos abiertos de lectura con *infoseq*

    $ infoseq ORFs_225_st.fasta -only -name -type -length | sort -k 3n > ORFs_225_length
    $ head ORFs_225_length 

Name           Type Length 
Bobra.0001s0002.1.v2.1_1 P    75     
Bobra.0001s0013.1.v2.1_2 P    75     
Bobra.0001s0015.1.v2.1_1 P    75     
Bobra.0001s0018.1.v2.1_1 P    75     
Bobra.0002s0013.1.v2.1_1 P    75     
Bobra.0002s0023.1.v2.1_18 P    75     
Bobra.0002s0030.1.v2.1_3 P    75     
Bobra.0002s0043.1.v2.1_5 P    75     
Bobra.0002s0046.1.v2.1_12 P    75          

    $ tail ORFs_225_length 

MSTRG.3883.6_25 P    3890   
Bobra.0156s0026.1.v2.1_25 P    3961   
MSTRG.3883.2_25 P    3962   
MSTRG.16740.2_26 P    4045   
Bobra.0006s0050.1.v2.1_38 P    4107   
Bobra.110_2s0053.4.v2.1_32 P    4631   
Bobra.110_2s0053.3.v2.1_32 P    4632   
MSTRG.833.5_30 P    4801   
Bobra.110_2s0053.2.v2.1_32 P    4824   
Bobra.110_2s0053.1.v2.1_32 P    4825  
  
    $ wc -l ORFs_225_length
    217230 ORFs_225_length # Menos 1 por el encabezado

Tomando en cuenta que este archivo salida del *getorf* contiene ORFs con longitudes mayores a 75 aa, estos transcritos son los que no queremos conservar, ya que buscamos transcritos con longitudes menores a 75 aa

Se extrajo los IDs del archivo fasta para tener en una lista todos aquellos transcritos que queremos eliminar 

    $ grep -E ">" ORFs_225_st.fasta | sed 's/>//g' > ORFs_225_ID_list 
    $ head ORFs_225_ID_list

Bobra.0001s0001.1.v2.1_1 [1 - 669] 
MSTRG.31.2_1 [2 - 463] 
MSTRG.31.2_2 [831 - 1106] 
MSTRG.31.1_1 [2 - 265] 
MSTRG.31.1_2 [85 - 474] 
Bobra.0001s0003.1.v2.1_1 [2 - 691] 
MSTRG.31.5_1 [2 - 259] 
MSTRG.31.5_2 [79 - 513] 
MSTRG.31.4_1 [2 - 457] 
MSTRG.31.4_2 [825 - 1055] 

    $ tail ORFs_225_ID_list

Bobra.9_2s0122.1.v2.1_6 [1199 - 1477] 
MSTRG.17987.1_1 [889 - 1176] 
MSTRG.18021.1_1 [287 - 583] 
MSTRG.18021.1_2 [361 - 612] 
MSTRG.18021.1_3 [620 - 907] 
MSTRG.18034.1_1 [1 - 321] 
MSTRG.18090.1_1 [1 - 372] 
MSTRG.18090.1_2 [209 - 490] 
MSTRG.18091.1_1 [196 - 435] 
MSTRG.18091.1_2 [2 - 436] 

Separamos la lista en dos, una con todos los ID's de "Bobra" y otra con los ID's de "MSTRG"
 
    Para los ID's "MSTRG" hicimos lo siguiente:

    $ grep -E "MSTRG" ORFs_225_ID_list | sed 's/\[[^]]*\]//g' | perl -pe 's/_.*\n/\n/' > ORFs_225_MSTRG_list
    $ wc -l ORFs_225_MSTRG_list
    89683 ORFs_MSTRG_list
    $ sort -V ORFs_225_MSTRG_list | uniq > ORFs_225_MSTRG_list_nr
    $ wc -l ORFs_225_MSTRG_list_nr
    12086 ORFs_225_MSTRG_list_nr # Transcritos con el ID "MSTRG"

    $ head ORFs_225_MSTRG_list_nr 

MSTRG.1.1
MSTRG.2.6
MSTRG.3.1
MSTRG.3.2
MSTRG.3.3
MSTRG.6.1
MSTRG.7.1
MSTRG.10.1
MSTRG.12.2
MSTRG.15.1


    $ tail ORFs_225_MSTRG_list_nr
 
MSTRG.18078.5
MSTRG.18082.1
MSTRG.18082.2
MSTRG.18082.3
MSTRG.18082.4
MSTRG.18086.1
MSTRG.18090.1
MSTRG.18091.1
MSTRG.18092.1
MSTRG.18093.1


    Para los ID's "Bobra" hicimos lo siguiente:

    $ grep -E "Bobra" ORFs_225_ID_list | sed 's/\[[^]]*\]//g' > ORFs_225_Bobra_list
    $ head ORFs_225_Bobra_list 

Bobra.0001s0001.1.v2.1_1  
Bobra.0001s0003.1.v2.1_1  
Bobra.0001s0007.1.v2.1_1  
Bobra.0001s0008.1.v2.1_1  
Bobra.0001s0008.1.v2.1_2  
Bobra.0001s0008.1.v2.1_3  
Bobra.0001s0008.1.v2.1_4  
Bobra.0001s0008.1.v2.1_5  
Bobra.0001s0009.1.v2.1_1  
Bobra.0001s0009.1.v2.1_2 

    $ tail ORFs_225_Bobra_list 

Bobra.9_2s0117.1.v2.1_8  
Bobra.9_2s0120.1.v2.1_1  
Bobra.9_2s0120.1.v2.1_2  
Bobra.9_2s0120.1.v2.1_3  
Bobra.9_2s0122.1.v2.1_1  
Bobra.9_2s0122.1.v2.1_2  
Bobra.9_2s0122.1.v2.1_3  
Bobra.9_2s0122.1.v2.1_4  
Bobra.9_2s0122.1.v2.1_5  
Bobra.9_2s0122.1.v2.1_6  

    $ wc -l ORFs_225_Bobra_list
    127546 ORFs_225_Bobra_list

Se modifico la lista de los ID's de Bobra de tal manera que no se alterara los nombres, ya que observamos que hay dos tipos de ID's

    $ sed 's/v2.1_.*//' ORFs_225_Bobra_list | awk '{print $0"v2.1"}' | sort -V | uniq > ORFs_225_Bobra_list_nr  
    $ wc -l ORFs_225_Bobra_list_nr
    22162 ORFs_225_Bobra_list_nr


    $ head ORFs_225_Bobra_list_nr

Bobra.0001s0001.1.v2.1
Bobra.0001s0002.1.v2.1
Bobra.0001s0003.1.v2.1
Bobra.0001s0004.1.v2.1
Bobra.0001s0005.1.v2.1
Bobra.0001s0007.1.v2.1
Bobra.0001s0008.1.v2.1
Bobra.0001s0009.1.v2.1
Bobra.0001s0010.1.v2.1
Bobra.0001s0012.1.v2.1

    $ tail ORFs_225_Bobra_list_nr

Bobra.0905s0001.2.v2.1
Bobra.0906s0002.1.v2.1
Bobra.0908s0001.1.v2.1
Bobra.0910s0001.1.v2.1
Bobra.0911s0001.1.v2.1
Bobra.0911s0002.1.v2.1
Bobra.0911s0003.1.v2.1
Bobra.0911s0004.1.v2.1
Bobra.0911s0005.1.v2.1
Bobra.0911s0006.1.v2.1


Las litas de los ID's de los ORFs se unieron en una sola

    $ cat ORFs_225_MSTRG_list_nr ORFs_225_Bobra_list_nr | sort -V | uniq > exclude_225_ORFs_list
    $ wc -l exclude_225_ORFs_list 
    34248 exclude_225_ORFs_list

Extraer todos los ID's del archivo "filter_200nt_st.fasta"

    $ grep -E ">" filter_200nt_st.fasta | sed 's/>//g' | sort -V > list_all_200nt_st
    $ wc -l list_all_200nt_st 
    36672 list_all_200nt_st

    $ head list_all_200nt_st 

Bobra.0001s0001.1.v2.1
Bobra.0001s0002.1.v2.1
Bobra.0001s0003.1.v2.1
Bobra.0001s0004.1.v2.1
Bobra.0001s0005.1.v2.1
Bobra.0001s0007.1.v2.1
Bobra.0001s0008.1.v2.1
Bobra.0001s0009.1.v2.1
Bobra.0001s0010.1.v2.1
Bobra.0001s0011.1.v2.1

    $ tail list_all_200nt_st 

MSTRG.9979.1
MSTRG.9984.1
MSTRG.9985.1
MSTRG.9985.2
MSTRG.9985.4
MSTRG.9988.1
MSTRG.9988.2
MSTRG.9990.1
MSTRG.9994.2
MSTRG.9997.1

Comparamos la lista de los ID's con ORF's y la lista de todos los ID's con longitud mayor o igual a 200nt

    $ diff exclude_225_ORFs_list list_all_200nt_st | grep \> | cut -f2 -d' ' | sort -V | uniq > include_225_ORFs_list
    $ wc -l include_225_ORFs_list 
    2424 include_225_ORFs_list # Corresponde a la diferencia de transcritos entre la lista de aquellos con tamano mayor o igual a 200 nt y las lista de los transcritos cuyo ORF fue predicho con *getorf*

NOTA: Hay que sortear las listas, de otro modo no se puede hacer bien la comparacion

Una vez teniendo la lista de ID's que debemos incluir (o conservar) en nuestro archivo *fasta*, se procedio a hacer una comparacion usando *faSomeRecords*

    $ faSomeRecords filter_200nt_st.fasta include_225_ORFs_list filter_225_ORFs_st.fasta
    $ grep -E '>' filter_225_ORFs_st.fasta | wc -l
    2424 # Cantidad de transcritos de nuestro archivo *fasta*

    $ head filter_225_ORFs_st.fasta

>Bobra.0001s0020.1.v2.1
ATGTATGTATGTATGTATGTATGTATGTATGTATGTATGTATGTATGTATGTATGTATGT
ATGTATGTATGTATGTACCATACTCATTGTGGGGTGGACACTTGGACATAAAAACAGTTC
GGTAGACAAATGTAACTGCGCCATCTTGGCCGCACATATTTAAGGTACGCCCTTATTATG
TGAACATCAAGGTACATATACCCTTGACCTCCTGTCGCTCTGACCCTTCTTGGGCTTACA
TAGATCGCTAGCGTCAATGCTTTGTAGCCAGAGTTCACAACCCCCCCC
>MSTRG.9.1
AATGAATTGGTCAATACTGTACTTAGTGACATCCCCTGTCACATGCCATCAACAGCAGTG
TAAACTATAGCCTTTATAGCCCTTTATGAACGGTATAGATCATACTCGTACCTCAATGAC
ATAACACGTGACTTGTACGATATGCGTTCTCAATAGACATTACGTATGGTAGTGCTTTAG


Hicimos una prueba buscando en el archivo *fasta* algunos de los ID's que se encuentra en el "exclude_225_ORFs_list"

    La lista fue la siguiente:
    
    HACER esto

Ninguno de los ID's seleccionados al azar se encontro en el *fasta* del filtro de ORFs 


#### Filtro 3: Homologia con dominios proteicos usando Hmmer/Pfam

Convertir la secuencia de nucletidos a aminoacidos con *getorf*   

   $ getorf -reverse F -minsize 20 -find 0 filter_225_ORFs_st.fasta filter_225_ORFs_protein.fasta
   $ grep '>' filter_225_ORFs_protein.fasta | wc -l
   45065

Luego de convertir a aminoacidos contamos cuantos transcritos se pudieron traducir
   
   $ grep '>' filter_225_ORFs_protein.fasta | sed 's/>//g' > orf_protein
   $ grep 'MSTRG' orf_protein > orf_protein_MSTRG
   $ grep 'Bobra' orf_protein > orf_protein_Bobra
   $ sed 's/\[[^]]*\]//g;s/>//g;s/_.*//' orf_protein_MSTRG | sort -V | uniq > orf_protein_MSTRG_nr
   $ wc -l orf_protein_MSTRG_nr
   1220 orf_protein_MSTRG_nr
   $ sed 's/\[[^]]*\]//g;s/>//g;s/v2.1_.*//' orf_protein_Bobra | awk '{print $0"v2.1"}' | sort -V | uniq > orf_protein_Bobra_nr
   $ wc -l orf_protein_Bobra_nr 
   1204 orf_protein_Bobra_nr
   $ cat orf_protein_MSTRG_nr orf_protein_Bobra_nr | sort -V | uniq > orf_protein_nr
   $ wc -l orf_protein_nr 
   2424 orf_protein_nr

Se corrio el hmmscan con los archivos en formato .pep en mazorka

Error: Failed to open binary auxfiles for Pfam-A.hmm: use hmmpress first

Por lo que se tuvo que preparar la base de datos con *hmmpress*

    $ module load hmmer/3.1b2
    $ hmmpress Pfam-A.hmm 

Posteriormente se corrio el modulo en Mazorka

#PBS -N hmmscan_pfam_BBRB_st
#PBS -q default
#PBS -l nodes=1:ppn=1,vmem=4gb,walltime=20:00:00
#PBS -V
#PBS -o hmmscan_225_st.out
#PBS -e hmmscan_225_st.err

module load hmmer/3.1b2

cd $PBS_O_WORKDIR

hmmscan --cpu 2 --domtblout TrinotatePFAM_225_st.out Pfam-A.hmm filter_225_ORFs_protein.fasta > pfam_225_st.log


    $ awk '{print $4}' TrinotatePFAM_225_st.out > list_pfam_225
    $ head list_pfam_225

sequence
accession
--------------------
Bobra.0001s0020.1.v2.1_1
Bobra.0001s0020.1.v2.1_1
Bobra.0001s0020.1.v2.1_1
Bobra.0001s0020.1.v2.1_2
Bobra.0001s0020.1.v2.1_2
Bobra.0001s0020.1.v2.1_4
Bobra.0001s0011.1.v2.1_4

    $ tail list_pfam_225

(February
SCAN
filter_225_ORFs_protein.fasta
Pfam-A.hmm
hmmscan
/NFS/LUSTRE/usuario/smata/BBRB/Cabili/guided
Feb


    $ sed 1,3d list_pfam_225 | head -n-10 > list_pfam_225_new
    $ head list_pfam_225_new 

Bobra.0001s0020.1.v2.1_1
Bobra.0001s0020.1.v2.1_1
Bobra.0001s0020.1.v2.1_1
Bobra.0001s0020.1.v2.1_2
Bobra.0001s0020.1.v2.1_2
Bobra.0001s0020.1.v2.1_4
Bobra.0001s0011.1.v2.1_4
MSTRG.8.1_18
Bobra.0010s0044.1.v2.1_13
Bobra.0010s0044.1.v2.1_13
 
    $ tail list_pfam_225_new 

Bobra.0098s0034.1.v2.1_37
Bobra.0098s0034.1.v2.1_37
MSTRG.17963.1_9
Bobra.9_2s0066.1.v2.1_11
MSTRG.18087.1_2
Bobra.9_2s0014.1.v2.1_4
MSTRG.17993.1_31
Bobra.9_2s0064.1.v2.1_7
MSTRG.18045.1_12
MSTRG.18064.1_2

Se tuvo que separar de la lista los ID's

    $ grep -E 'MSTRG' list_pfam_225_new > MSTRG_list_pfam
    $ sed 's/_.*//' MSTRG_list_pfam | sort -V | uniq > MSTRG_list_pfam_nr
    $ wc -l MSTRG_list_pfam_nr
    439 MSTRG_list_pfam_nr
    
    $ head MSTRG_list_pfam_nr

MSTRG.8.1
MSTRG.11.1
MSTRG.83.1
MSTRG.89.1
MSTRG.93.1
MSTRG.145.1
MSTRG.154.1
MSTRG.240.1
MSTRG.246.1
MSTRG.295.1

    $ tail MSTRG_list_pfam_nr 

MSTRG.17664.1
MSTRG.17761.1
MSTRG.17783.1
MSTRG.17863.1
MSTRG.17894.2
MSTRG.17963.1
MSTRG.17993.1
MSTRG.18045.1
MSTRG.18064.1
MSTRG.18087.1

    $ grep -E 'Bobra' list_pfam_225_new > Bobra_list_pfam
    $ sed 's/v2.1_.*//' Bobra_list_pfam | awk '{print $0"v2.1"}' | sort -V | uniq > Bobra_list_pfam_nr
    $ wc -l Bobra_list_pfam_nr
    532 Bobra_list_pfam_nr

    $ head Bobra_list_pfam_nr

Bobra.0001s0011.1.v2.1
Bobra.0001s0020.1.v2.1
Bobra.0002s0054.1.v2.1
Bobra.0003s0039.1.v2.1
Bobra.0003s0051.1.v2.1
Bobra.0003s0067.1.v2.1
Bobra.4_1s0029.1.v2.1
Bobra.4_1s0094.1.v2.1
Bobra.4_1s0097.1.v2.1
Bobra.4_1s0122.1.v2.1

     $ tail Bobra_list_pfam_nr 

Bobra.0620s0002.1.v2.1
Bobra.0756s0002.1.v2.1
Bobra.0791s0001.1.v2.1
Bobra.0799s0003.1.v2.1
Bobra.0812s0002.1.v2.1
Bobra.0816s0001.1.v2.1
Bobra.0841s0003.1.v2.1
Bobra.0841s0004.1.v2.1
Bobra.0859s0001.1.v2.1
Bobra.0900s0001.1.v2.1

Ambas listas se combinaron en una sola

    $ cat Bobra_list_pfam_nr MSTRG_list_pfam_nr > exclude_list_pfam
    $ sort -V exclude_list_pfam | uniq > exclude_list_pfam_nr
    $ wc -l exclude_list_pfam_nr 
    971 exclude_list_pfam_nr

    $ head exclude_list_pfam_nr 

Bobra.0001s0011.1.v2.1
Bobra.0001s0020.1.v2.1
Bobra.0002s0054.1.v2.1
Bobra.0003s0039.1.v2.1
Bobra.0003s0051.1.v2.1
Bobra.0003s0067.1.v2.1
Bobra.4_1s0029.1.v2.1
Bobra.4_1s0094.1.v2.1
Bobra.4_1s0097.1.v2.1
Bobra.4_1s0122.1.v2.1

    $ tail exclude_list_pfam_nr 

MSTRG.17664.1
MSTRG.17761.1
MSTRG.17783.1
MSTRG.17863.1
MSTRG.17894.2
MSTRG.17963.1
MSTRG.17993.1
MSTRG.18045.1
MSTRG.18064.1
MSTRG.18087.1
  

En total fueron 971 transcritos en los que se identifico dominios proteicos

Se procedio a sacar la lista de ID's del archivo "filter_225_ORFs_st.fasta"

    $ grep '>' filter_225_ORFs_st.fasta | wc -l
    2424
    $ grep -E ">" filter_225_ORFs_st.fasta | sed 's/>//g' | sort -V | uniq > list_225_ORFs
    $ wc -l list_225_ORFs 
    2424 list_225_ORFs

Se comparo ambas listas para obtener los ID's de los transcritos que queremos mantener

    $ diff exclude_list_pfam_nr list_225_ORFs | grep \> | cut -f2 -d' ' > include_pfam_225
    $ wc -l include_pfam_225 
    1453 include_pfam_225
    
Una vez teniendo la lista de ID's que debemos incluir (o conservar) en nuestro archivo *fasta*, se procedio a hacer una comparacion usando *faSomeRecords*

    $ faSomeRecords filter_225_ORFs_st.fasta include_pfam_225 filter_pfam_225_st.fasta
    $ grep -E '>' filter_pfam_225_st.fasta | wc -l
    1453 # Cantidad de transcritos de nuestro archivo *fasta*



#### Analisis de calidad de transcriptomas con BUSCO 

### Transcritoma guiado

El archivo *gtf* original del transcriptoma guiado de BBRB se convirtio a *bed12* y de este a *fasta* 

    $ /home/fani/gtfToGenePred BBRB_v2_transcriptome.gtf BBRB_v2_transcriptome.genePred
    $ /home/fani/genePredToBed BBRB_v2_transcriptome.genePred BBRB_v2_transcriptome.bed12
    $ bedtools getfasta -split -fi /home/fani/Documentos/I_Semestre_2019/Tesis/BBRB_analisis_bioinfo/genome_data/genome_v2.1/assembly/Bbraunii_genome_502_v2.0.fa -bed BBRB_v2_transcriptome.bed12 -name -fo BBRB_v2_transcriptome.fasta

En Mazorka se corrio la siguiente linea de comandos para analizar la calidad de los ensambles
 
    Para el transcritoma original:

#PBS -q default
#PBS -l nodes=1:ppn=16,mem=32gb,vmem=32gb,walltime=120:00:00
#PBS -N BUSCO_BBRB_st
#PBS -V
#PBS -o busco_BBRB_st.out
#PBS -e busco_BBRB_st.err

module load ncbi-blast+/2.6.0
module load hmmer/3.1b2
module load augustus/3.2.1
module load BUSCO/2.0.1

AUGUSTUS_CONFIG_PATH=/LUSTRE/usuario/smata/augustus-3.2.1/config

export AUGUSTUS_CONFIG_PATH

cd /LUSTRE/usuario/smata/BBRB/BUSCO/guided  

/data/software/busco/BUSCO.py -i BBRB_v2_transcriptome.fasta  -o BBRB_st_BUSCO -l /LUSTRE/usuario/smata/BUSCO_dbs/chlorophyta_odb10 -m tran -c 16 -e 0.0001 -f 


Resultados: Del archivo "short_summary_BBRB_st_BUSCO.txt"

# BUSCO version is: 2.0.1 
# The lineage dataset is: chlorophyta_odb10 (Creation date: 2017-12-01, number of species: 10, number of BUSCOs: 2168)
# To reproduce this run: python /data/software/busco/BUSCO.py -i BBRB_v2_transcriptome.fasta -o BBRB_st_BUSCO -l /LUSTRE/usuario/smata/BUSCO_dbs/chlorophyta_odb10/ -m tran -c 16 -sp chlamydomonas -e 0.0001
#
# Summarized benchmarking in BUSCO notation for file BBRB_v2_transcriptome.fasta
# BUSCO was run in mode: tran

	C:71.5%[S:47.0%,D:24.5%],F:8.8%,M:19.7%,n:2168

	1550	Complete BUSCOs (C)
	1019	Complete and single-copy BUSCOs (S)
	531	Complete and duplicated BUSCOs (D)
	191	Fragmented BUSCOs (F)
	427	Missing BUSCOs (M)
	2168	Total BUSCO groups searched



     Para el transcriptoma anotado:

#PBS -q default
#PBS -l nodes=1:ppn=16,mem=32gb,vmem=32gb,walltime=120:00:00
#PBS -N BUSCO_BBRB_anot
#PBS -V
#PBS -o busco_BBRB_st_anot.out
#PBS -e busco_BBRB_st_anot.err

module load ncbi-blast+/2.6.0
module load hmmer/3.1b2
module load augustus/3.2.1
module load BUSCO/2.0.1

AUGUSTUS_CONFIG_PATH=/LUSTRE/usuario/smata/augustus-3.2.1/config

export AUGUSTUS_CONFIG_PATH

cd /LUSTRE/usuario/smata/BBRB/BUSCO/guided  

/data/software/busco/BUSCO.py -i BBRB_v2_transcriptome.annotated.fasta  -o BBRB_st_anot_BUSCO -l /LUSTRE/usuario/smata/BUSCO_dbs/chlorophyta_odb10 -m tran -c 16 -e 0.0001 -f 


Resultados: Del archivo "short_summary_BBRB_st_anot_BUSCO.txt"

# BUSCO version is: 2.0.1 
# The lineage dataset is: chlorophyta_odb10 (Creation date: 2017-12-01, number of species: 10, number of BUSCOs: 2168)
# To reproduce this run: python /data/software/busco/BUSCO.py -i BBRB_v2_transcriptome.annotated.fasta -o BBRB_st_anot_BUSCO -l /LUSTRE/usuario/smata/BUSCO_dbs/chlorophyta_odb10/ -m tran -c 16 -sp chlamydomonas -e 0.0001
#
# Summarized benchmarking in BUSCO notation for file BBRB_v2_transcriptome.annotated.fasta
# BUSCO was run in mode: tran

	C:71.5%[S:47.0%,D:24.5%],F:8.8%,M:19.7%,n:2168

	1550	Complete BUSCOs (C)
	1019	Complete and single-copy BUSCOs (S)
	531	Complete and duplicated BUSCOs (D)
	191	Fragmented BUSCOs (F)
	427	Missing BUSCOs (M)
	2168	Total BUSCO groups searched


### Transcritoma de novo

El archivo *gtf* original del transcriptoma guiado de BBRB se convirtio a *bed12* y de este a *fasta* 

    $ /home/fani/gtfToGenePred BBRB_novo_transcriptome_annotated.gtf BBRB_novo_transcriptome_annotated.genePred
    $ /home/fani/genePredToBed BBRB_novo_transcriptome_annotated.genePred BBRB_novo_transcriptome_annotated.bed12
    $ bedtools getfasta -split -fi /home/fani/Documentos/I_Semestre_2019/Tesis/BBRB_analisis_bioinfo/genome_data/genome_v2.1/assembly/Bbraunii_genome_502_v2.0.fa -bed BBRB_novo_transcriptome_annotated.bed12 -name -fo BBRB_v2_transcriptome.fasta

En Mazorka se corrio la siguiente linea de comandos para analizar la calidad del ensamble


    Para el ensamble original:

#PBS -q default
#PBS -l nodes=1:ppn=16,mem=32gb,vmem=32gb,walltime=120:00:00
#PBS -N BUSCO_BBRB_novo
#PBS -V
#PBS -o busco_BBRB_novo.out
#PBS -e busco_BBRB_novo.err

module load ncbi-blast+/2.6.0
module load hmmer/3.1b2
module load augustus/3.2.1
module load BUSCO/2.0.1

AUGUSTUS_CONFIG_PATH=/LUSTRE/usuario/smata/augustus-3.2.1/config

export AUGUSTUS_CONFIG_PATH

cd /LUSTRE/usuario/smata/BBRB/BUSCO/novo  

/data/software/busco/BUSCO.py -i /LUSTRE/usuario/smata/BBRB/transcriptome_trinity_2_8_3/trinity_all/BBRB_novo_transcriptome.fasta  -o BBRB_novo_BUSCO -l /LUSTRE/usuario/smata/BUSCO_dbs/chlorophyta_odb10 -m tran -c 16 -e 0.0001 -f 


Resultados:

# BUSCO version is: 2.0.1 
# The lineage dataset is: chlorophyta_odb10 (Creation date: 2017-12-01, number of species: 10, number of BUSCOs: 2168)
# To reproduce this run: python /data/software/busco/BUSCO.py -i /LUSTRE/usuario/smata/BBRB/transcriptome_trinity_2_8_3/trinity_all/BBRB_novo_transcriptome.fasta -o BBRB_novo_BUSCO -l /LUSTRE/usuario/smata/BUSCO_dbs/chlorophyta_odb10/ -m tran -c 16 -sp chlamydomonas -e 0.0001
#
# Summarized benchmarking in BUSCO notation for file /LUSTRE/usuario/smata/BBRB/transcriptome_trinity_2_8_3/trinity_all/BBRB_novo_transcriptome.fasta
# BUSCO was run in mode: tran

	C:90.7%[S:55.2%,D:35.5%],F:3.6%,M:5.7%,n:2168

	1966	Complete BUSCOs (C)
	1197	Complete and single-copy BUSCOs (S)
	769	Complete and duplicated BUSCOs (D)
	78	Fragmented BUSCOs (F)
	124	Missing BUSCOs (M)
	2168	Total BUSCO groups searched


    Para el ensamble anotado:

#PBS -q default
#PBS -l nodes=1:ppn=16,mem=32gb,vmem=32gb,walltime=120:00:00
#PBS -N BUSCO_BBRB_novo_anot
#PBS -V
#PBS -o busco_BBRB_anot.out
#PBS -e busco_BBRB_anot.err

module load ncbi-blast+/2.6.0
module load hmmer/3.1b2
module load augustus/3.2.1
module load BUSCO/2.0.1

AUGUSTUS_CONFIG_PATH=/LUSTRE/usuario/smata/augustus-3.2.1/config

export AUGUSTUS_CONFIG_PATH

cd /LUSTRE/usuario/smata/BBRB/BUSCO/novo  

/data/software/busco/BUSCO.py -i /LUSTRE/usuario/smata/BBRB/transcriptome_trinity_2_8_3/compare_and_counts/BBRB_novo_transcriptome_annotated.fasta -o BBRB_novo_anot_BUSCO -l /LUSTRE/usuario/smata/BUSCO_dbs/chlorophyta_odb10 -m tran -c 16 -e 0.0001 -f 


Resultados: 

# BUSCO version is: 2.0.1 
# The lineage dataset is: chlorophyta_odb10 (Creation date: 2017-12-01, number of species: 10, number of BUSCOs: 2168)
# To reproduce this run: python /data/software/busco/BUSCO.py -i /LUSTRE/usuario/smata/BBRB/transcriptome_trinity_2_8_3/compare_and_counts/BBRB_novo_transcriptome_annotated.fasta -o BBRB_novo_anot_BUSCO -l /LUSTRE/usuario/smata/BUSCO_dbs/chlorophyta_odb10/ -m tran -c 16 -sp chlamydomonas -e 0.0001
#
# Summarized benchmarking in BUSCO notation for file /LUSTRE/usuario/smata/BBRB/transcriptome_trinity_2_8_3/compare_and_counts/BBRB_novo_transcriptome_annotated.fasta
# BUSCO was run in mode: tran

	C:68.1%[S:45.2%,D:22.9%],F:8.8%,M:23.1%,n:2168

	1477	Complete BUSCOs (C)
	981	Complete and single-copy BUSCOs (S)
	496	Complete and duplicated BUSCOs (D)
	190	Fragmented BUSCOs (F)
	501	Missing BUSCOs (M)
	2168	Total BUSCO groups searched


#### CPC1 para identificacion de lncRNAS en el ensamble guiado de BBRB


Se corrio CPC1 en Mazorka con el archivo "filter_225_ORFs_st.fasta" con 2424 transcritos

#PBS -N CPC_BBRB_st
#PBS -q default
#PBS -l nodes=1:ppn=8,vmem=30gb,walltime=99:00:00
#PBS -V
#PBS -o cpc_BBRB_st.out
#PBS -e cpc_BBRB_st.err

module load cpc/0.9.r2
module load ncbi-blast/2.2.26

cd $PBS_O_WORKDIR

run_predict.sh filter_225_ORFs_st.fasta result_in_table cpc result_evidences


Los archivos de salida se copiaron de Mazorka a una carpeta local en la computadora
De la tabla "result_in_table" se extrajeron todos aquellos transcritos que resultaron no codificantes

    $ grep 'noncoding' result_in_table > BBRB_noncoding
    $ head BBRB_noncoding 

Bobra.0001s0020.1.v2.1	288	noncoding	-1.52382
MSTRG.9.1	404	noncoding	-1.4738
Bobra.0001s0011.1.v2.1	216	noncoding	-1.10767
MSTRG.8.1	384	noncoding	-1.4557
MSTRG.59.1	275	noncoding	-1.27725
Bobra.0010s0044.1.v2.1	442	noncoding	-0.367254
MSTRG.11.1	473	noncoding	-1.09921
MSTRG.83.1	533	noncoding	-1.22119
Bobra.0100s0001.1.v2.1	348	noncoding	-1.22866
Bobra.0100s0036.1.v2.1	225	noncoding	-1.24551

    $  tail BBRB_noncoding 

MSTRG.18087.1	263	noncoding	-1.50733
Bobra.9_2s0005.1.v2.1	810	noncoding	-1.41348
Bobra.9_2s0014.1.v2.1	725	noncoding	-1.12626
MSTRG.17993.1	1120	noncoding	-1.44642
Bobra.9_2s0016.2.v2.1	246	noncoding	-1.15284
Bobra.9_2s0036.1.v2.1	276	noncoding	-1.0435
Bobra.9_2s0064.1.v2.1	571	noncoding	-1.28246
Bobra.9_2s0121.1.v2.1	264	noncoding	-1.28755
MSTRG.18045.1	508	noncoding	-1.22314
MSTRG.18064.1	216	noncoding	-1.04938

    $ wc -l BBRB_noncoding 
    2391 BBRB_noncoding

Se extrajeron de la lista de "noncoding" todo aquellos que resultaron ser lncRNAs

    $ awk '{if($2 >= 200){print}}' BBRB_noncoding > BBRB_lncrnas
    $ head BBRB_lncrnas 

Bobra.0001s0020.1.v2.1	288	noncoding	-1.52382
MSTRG.9.1	404	noncoding	-1.4738
Bobra.0001s0011.1.v2.1	216	noncoding	-1.10767
MSTRG.8.1	384	noncoding	-1.4557
MSTRG.59.1	275	noncoding	-1.27725
Bobra.0010s0044.1.v2.1	442	noncoding	-0.367254
MSTRG.11.1	473	noncoding	-1.09921
MSTRG.83.1	533	noncoding	-1.22119
Bobra.0100s0001.1.v2.1	348	noncoding	-1.22866
Bobra.0100s0036.1.v2.1	225	noncoding	-1.24551

    $ tail BBRB_lncrnas 

MSTRG.18087.1	263	noncoding	-1.50733
Bobra.9_2s0005.1.v2.1	810	noncoding	-1.41348
Bobra.9_2s0014.1.v2.1	725	noncoding	-1.12626
MSTRG.17993.1	1120	noncoding	-1.44642
Bobra.9_2s0016.2.v2.1	246	noncoding	-1.15284
Bobra.9_2s0036.1.v2.1	276	noncoding	-1.0435
Bobra.9_2s0064.1.v2.1	571	noncoding	-1.28246
Bobra.9_2s0121.1.v2.1	264	noncoding	-1.28755
MSTRG.18045.1	508	noncoding	-1.22314
MSTRG.18064.1	216	noncoding	-1.04938

    $ wc -l BBRB_lncrnas 
    2391 BBRB_lncrnas


#### CPC2

Se instalo CPC2 en la computadora descargandolo desde este link: http://cpc2.cbi.pku.edu.cn/download.php

    $ /home/fani/CPC2-beta/bin/CPC2.py -i filter_225_ORFs_st.fasta -o cpc2_results_225st.txt

    $ cd $CPC_HOME && python ./bin/CPC2.py -i /home/fani/Documents/Tesis_BP/BBRB_analysis/CPC/guided/cpc2/filter_225_ORFs_st.fasta -o cpc2_results_225st.txt


NOT working!

#### *Lunes 17 de febrero de 2020*


#### Filtro 4: Prediccion de peptido señal usando SignalP

El archivo "filter_pfam_225_st.fasta" se convirtio de nucleotidos a aminoacidos usando *getorf* tal y como con el filtro anterior:

   $ getorf -reverse F -minsize 20 -find 0 filter_pfam_225_st.fasta filter_pfam_225_protein.fasta
   $ grep '>' filter_pfam_225_protein.fasta | wc -l
   23528

Revisamos el archivo fasta de aminoacidos

    $ head filter_pfam_225_protein.fasta

>MSTRG.9.1_1 [6 - 23] 
IGQYCT
>MSTRG.9.1_2 [1 - 60] 
NELVNTVLSDIPCHMPSTAV
>MSTRG.9.1_3 [2 - 67] 
MNWSILYLVTSPVTCHQQQCKL
>MSTRG.9.1_4 [30 - 86] 
HPLSHAINSSVNYSLYSPL
>MSTRG.9.1_5 [99 - 116] 
IILVPQ


    $ tail filter_pfam_225_protein.fasta

>Bobra.9_2s0121.1.v2.1_6 [63 - 176] 
TRAWHLHVSSKVHLDFYMSPARNNWHPSKQEEPVWGRA
>Bobra.9_2s0121.1.v2.1_7 [173 - 241] 
SVTKCGGIVWGGVGSVWELAFPW
>Bobra.9_2s0121.1.v2.1_8 [245 - 262] 
AAVGRD
>Bobra.9_2s0121.1.v2.1_9 [180 - 263] 
QNVEELCGGGWEVSGSSLSHGRRRSGAT
>Bobra.9_2s0121.1.v2.1_10 [118 - 264] 
AQHGTIGTHPNKRSLCGVERDKMWRNCVGGGGKCLGARFPMVGGGRARH


La cantidad de transcritos cuya secuencia fue convertida a aminoacidos fue la siguiente: 

   $ grep '>' filter_pfam_225_protein.fasta | sed 's/>//g' > all_pfam_list
   $ grep 'MSTRG' all_pfam_list > pfam_list_MSTRG
   $ grep 'Bobra' all_pfam_list > pfam_list_Bobra
   $ sed 's/\[[^]]*\]//g;s/>//g;s/_.*//' pfam_list_MSTRG | sort -V | uniq > pfam_list_MSTRG_nr
   $ wc -l pfam_list_MSTRG_nr
   781 pfam_list_MSTRG_nr
   $ sed 's/\[[^]]*\]//g;s/>//g;s/v2.1_.*//' pfam_list_Bobra | awk '{print $0"v2.1"}' | sort -V | uniq > pfam_list_Bobra_nr
   $ wc -l pfam_list_Bobra_nr
   672 pfam_list_Bobra_nr
   $ cat pfam_list_MSTRG_nr pfam_list_Bobra_nr | sort -V | uniq > all_pfam_transcripts
   $ wc -l all_pfam_transcripts 
   1453 all_pfam_transcripts


El archivo "filter_pfam_225_protein.fasta" se subió a Mazorka y se corrio con el modulo de SignalP


#PBS -N signalp_BBRB_225
#PBS -q default
#PBS -l nodes=1:ppn=1,vmem=4gb,walltime=20:00:00
#PBS -V
#PBS -o signalp_225_BBRB.out
#PBS -e signalp_225_BBRB.err

   
module load SignalP/4.1
   

cd $PBS_O_WORKDIR

signalp -f short -n signalp_225.out filter_pfam_225_protein.fasta > signalp_225.log 


Se reviso el archivo de salida de la corrida

   $ head signalp_225.out

##gff-version 2
##sequence-name	source	feature	start	end	score	N/A ?
## -----------------------------------------------------------
MSTRG.298.1_6	SignalP-4.1	SIGNAL	1	11	0.473	.	.	YES
MSTRG.302.2_21	SignalP-4.1	SIGNAL	1	16	0.487	.	.	YES
MSTRG.196.1_5	SignalP-4.1	SIGNAL	1	18	0.736	.	.	YES
MSTRG.234.1_3	SignalP-4.1	SIGNAL	1	12	0.523	.	.	YES
Bobra.104_1s0038.1.v2.1_17	SignalP-4.1	SIGNAL	1	11	0.550	.	.	YES
MSTRG.716.1_5	SignalP-4.1	SIGNAL	1	10	0.492	.	.	YES
Bobra.110_2s0081.1.v2.1_4	SignalP-4.1	SIGNAL	1	24	0.527	.	.	YES


   $ tail signalp_225.out

Bobra.0086s0003.1.v2.1_4	SignalP-4.1	SIGNAL	1	26	0.624	.	.	YES
Bobra.0874s0002.1.v2.1_3	SignalP-4.1	SIGNAL	1	18	0.839	.	.	YES
MSTRG.17648.1_8	SignalP-4.1	SIGNAL	1	10	0.540	.	.	YES
MSTRG.17671.2_8	SignalP-4.1	SIGNAL	1	21	0.480	.	.	YES
MSTRG.17766.5_7	SignalP-4.1	SIGNAL	1	21	0.571	.	.	YES
MSTRG.17766.4_7	SignalP-4.1	SIGNAL	1	21	0.571	.	.	YES
MSTRG.17766.3_7	SignalP-4.1	SIGNAL	1	21	0.571	.	.	YES
Bobra.92_2s0014.1.v2.1_4	SignalP-4.1	SIGNAL	1	21	0.511	.	.	YES
Bobra.0095s0044.1.v2.1_11	SignalP-4.1	SIGNAL	1	26	0.453	.	.	YES
MSTRG.18003.1_3	SignalP-4.1	SIGNAL	1	11	0.595	.	.	YES


Se reviso el valor del *cutoff* para los transcritos, siendo el corte de 0.45

    $ awk '$6 >= 0.45 {print}' signalp_225.out | sed 1d | wc -l
    $ 224 # transcritos con un cutoff >= a 0.45, es decir que tienen señal

Se extrajo la lista de los ID's del archivo 

    $ awk '{print $1}' signalp_225.out | sed '1,3d' > signalp_all_list
    $ head signalp_all_list

MSTRG.298.1_6
MSTRG.302.2_21
MSTRG.196.1_5
MSTRG.234.1_3
Bobra.104_1s0038.1.v2.1_17
MSTRG.716.1_5
Bobra.110_2s0081.1.v2.1_4
Bobra.110_2s0084.1.v2.1_5
MSTRG.858.1_4
MSTRG.974.1_9

    $ tail signalp_all_list 

Bobra.0086s0003.1.v2.1_4
Bobra.0874s0002.1.v2.1_3
MSTRG.17648.1_8
MSTRG.17671.2_8
MSTRG.17766.5_7
MSTRG.17766.4_7
MSTRG.17766.3_7
Bobra.92_2s0014.1.v2.1_4
Bobra.0095s0044.1.v2.1_11
MSTRG.18003.1_3


Hacer una lista con el ID 'MSTRG'

    $ grep 'MSTRG' signalp_all_list > signalp_MSTRG
    $ wc -l signalp_MSTRG
    120 signalp_MSTRG
    $ sed 's/_.*//' signalp_MSTRG | sort -V | uniq > signalp_MSTRG_nr
    $ wc -l signalp_MSTRG_nr 
    97 signalp_MSTRG_nr

    $ head signalp_MSTRG_nr 

MSTRG.196.1
MSTRG.234.1
MSTRG.298.1
MSTRG.302.2
MSTRG.716.1
MSTRG.858.1
MSTRG.974.1
MSTRG.1335.1
MSTRG.2146.1
MSTRG.2456.1

    $ tail signalp_MSTRG_nr 

MSTRG.16669.1
MSTRG.16669.2
MSTRG.16669.3
MSTRG.17455.1
MSTRG.17648.1
MSTRG.17671.2
MSTRG.17766.3
MSTRG.17766.4
MSTRG.17766.5
MSTRG.18003.1


Hacer una lista con el ID 'Bobra'

    $ grep 'Bobra' signalp_all_list > signalp_Bobra
    $ wc -l signalp_Bobra
    104 signalp_Bobra
    $ sed 's/v2.1_.*//' signalp_Bobra | awk '{print $0"v2.1"}' | sort -V | uniq > signalp_Bobra_nr
    $ wc -l signalp_Bobra_nr
    76 signalp_Bobra_nr

    $ head signalp_Bobra_nr 

Bobra.4_1s0132.1.v2.1
Bobra.7_2s0008.1.v2.1
Bobra.7_2s0024.1.v2.1
Bobra.7_2s0086.1.v2.1
Bobra.0015s0086.1.v2.1
Bobra.0016s0033.1.v2.1
Bobra.0018s0039.1.v2.1
Bobra.0022s0136.1.v2.1
Bobra.0024s0035.1.v2.1
Bobra.0025s0029.1.v2.1

    $ tail signalp_Bobra_nr 

Bobra.0249s0088.1.v2.1
Bobra.0307s0020.1.v2.1
Bobra.0310s0018.1.v2.1
Bobra.0330s0001.1.v2.1
Bobra.0339s0016.1.v2.1
Bobra.0355s0023.1.v2.1
Bobra.357_2s0026.1.v2.1
Bobra.0363s0034.1.v2.1
Bobra.0380s0009.1.v2.1
Bobra.0874s0002.1.v2.1


Ambas listas se unieron en una sola

    $ cat signalp_MSTRG_nr signalp_Bobra_nr | sort -V | uniq > signalp_exclude_list
    $ wc -l signalp_exclude_list
    173 signalp_exclude_list

Se extrajo la lista de transcritos del archivo filter_pfam_225_st.fasta

    $ grep '>' filter_pfam_225_st.fasta | sed 's/>//g' | sort -V | uniq > pfam_list
    $  wc -l pfam_list
    1453 pfam_list

Se comparo la lista "pfam_list" con "signalp_exclude_list" para obtener los ID's de los transcritos que queremos mantener

    $ diff signalp_exclude_list pfam_list | grep \> | cut -f2 -d' ' > signalp_include_list
    $ wc -l signalp_include_list 
    1280 signalp_include_list
    
Una vez teniendo la lista de ID's que debemos incluir (o conservar) en nuestro archivo *fasta*, se procedio a hacer una comparacion usando *faSomeRecords*

    $ faSomeRecords filter_pfam_225_st.fasta signalp_include_list filter_signalp_225.fasta
    $ grep -E '>' filter_signalp_225.fasta | wc -l
    1280


#### *Miercoles 19 de febrero de 2020*


#### Filtro 5: Prediccion de helices transmembranales usando TMHMM

El archivo "filter_signalp_225.fasta" se convirtio de nucleotidos a aminoacidos usando *getorf*

   $ getorf -reverse F -minsize 20 -find 0 filter_signalp_225.fasta filter_signalp_225_protein.fasta
   $ grep '>' filter_signalp_225_protein.fasta | wc -l
   20022

   $  head filter_signalp_225_protein.fasta 

>MSTRG.9.1_1 [6 - 23] 
IGQYCT
>MSTRG.9.1_2 [1 - 60] 
NELVNTVLSDIPCHMPSTAV
>MSTRG.9.1_3 [2 - 67] 
MNWSILYLVTSPVTCHQQQCKL
>MSTRG.9.1_4 [30 - 86] 
HPLSHAINSSVNYSLYSPL
>MSTRG.9.1_5 [99 - 116] 
IILVPQ


   $ tail filter_signalp_225_protein.fasta 

>Bobra.9_2s0121.1.v2.1_6 [63 - 176] 
TRAWHLHVSSKVHLDFYMSPARNNWHPSKQEEPVWGRA
>Bobra.9_2s0121.1.v2.1_7 [173 - 241] 
SVTKCGGIVWGGVGSVWELAFPW
>Bobra.9_2s0121.1.v2.1_8 [245 - 262] 
AAVGRD
>Bobra.9_2s0121.1.v2.1_9 [180 - 263] 
QNVEELCGGGWEVSGSSLSHGRRRSGAT
>Bobra.9_2s0121.1.v2.1_10 [118 - 264] 
AQHGTIGTHPNKRSLCGVERDKMWRNCVGGGGKCLGARFPMVGGGRARH


La cantidad de transcritos cuya secuencia fue convertida a aminoacidos fue la siguiente: 

   $ grep '>' filter_signalp_225_protein.fasta | sed 's/>//g' > all_signalp_list
   $ grep 'MSTRG' all_signalp_list > signalP_list_MSTRG
   $ grep 'Bobra' all_signalp_list > signalP_list_Bobra
   $ sed 's/\[[^]]*\]//g;s/>//g;s/_.*//' signalP_list_MSTRG | sort -V | uniq > signalP_list_MSTRG_nr
   $ wc -l signalP_list_MSTRG_nr
   684 signalP_list_MSTRG_nr
   $ sed 's/\[[^]]*\]//g;s/>//g;s/v2.1_.*//' signalP_list_Bobra | awk '{print $0"v2.1"}' | sort -V | uniq > signalP_list_Bobra_nr
   $ wc -l signalP_list_Bobra_nr
   596 signalP_list_Bobra_nr
   $ cat signalP_list_MSTRG_nr signalP_list_Bobra_nr | sort -V | uniq > all_signalp_transcripts
   $ wc -l all_signalp_transcripts 
   1280 all_signalp_transcripts


El archivo "filter_signalp_225_protein.fasta" se copio a mazorka para correr el modulo de TMHMM

#PBS -N tmhmm_BBRB_st
#PBS -q default
#PBS -l nodes=1:ppn=1,vmem=4gb,walltime=120:00:00
#PBS -V
#PBS -o tmhmm_225_BBRB.out
#PBS -e tmhmm_225_BBRB.err

module load tmhmm/2.0c

cd $PBS_O_WORKDIR

tmhmm --short < filter_signalp_225_protein.fasta > tmhmm_225.out 



El archivo tmhmm_225.out se copio a una carpeta local en la computadora y este se reviso

    $ head tmhmm_225.out 

MSTRG.9.1_1	len=6	ExpAA=0.00	First60=0.00	PredHel=0	Topology=i
MSTRG.9.1_2	len=20	ExpAA=0.00	First60=0.00	PredHel=0	Topology=o
MSTRG.9.1_3	len=22	ExpAA=0.00	First60=0.00	PredHel=0	Topology=o
MSTRG.9.1_4	len=19	ExpAA=0.00	First60=0.00	PredHel=0	Topology=o
MSTRG.9.1_5	len=6	ExpAA=0.00	First60=0.00	PredHel=0	Topology=o
MSTRG.9.1_6	len=14	ExpAA=0.00	First60=0.00	PredHel=0	Topology=i
MSTRG.9.1_7	len=30	ExpAA=0.00	First60=0.00	PredHel=0	Topology=o
MSTRG.9.1_8	len=15	ExpAA=0.00	First60=0.00	PredHel=0	Topology=i
MSTRG.9.1_9	len=7	ExpAA=0.00	First60=0.00	PredHel=0	Topology=i
MSTRG.9.1_10	len=41	ExpAA=0.01	First60=0.01	PredHel=0	Topology=o

    $ tail tmhmm_225.out 

Bobra.9_2s0121.1.v2.1_1	len=8	ExpAA=0.00	First60=0.00	PredHel=0	Topology=i
Bobra.9_2s0121.1.v2.1_2	len=11	ExpAA=0.00	First60=0.00	PredHel=0	Topology=i
Bobra.9_2s0121.1.v2.1_3	len=7	ExpAA=0.00	First60=0.00	PredHel=0	Topology=i
Bobra.9_2s0121.1.v2.1_4	len=17	ExpAA=0.00	First60=0.00	PredHel=0	Topology=i
Bobra.9_2s0121.1.v2.1_5	len=56	ExpAA=0.02	First60=0.02	PredHel=0	Topology=i
Bobra.9_2s0121.1.v2.1_6	len=38	ExpAA=0.00	First60=0.00	PredHel=0	Topology=i
Bobra.9_2s0121.1.v2.1_7	len=23	ExpAA=0.88	First60=0.88	PredHel=0	Topology=o
Bobra.9_2s0121.1.v2.1_8	len=6	ExpAA=0.00	First60=0.00	PredHel=0	Topology=i
Bobra.9_2s0121.1.v2.1_9	len=28	ExpAA=0.00	First60=0.00	PredHel=0	Topology=o
Bobra.9_2s0121.1.v2.1_10	len=49	ExpAA=0.01	First60=0.01	PredHel=0	Topology=o


Del archivo se salida se selecciono aquellos transcritos cuya longitud fuera mayor a 18 aa y que ademas se haya predicho 1 o mas helices transmembranales (PredHel>0)
Primero con *perl* reemplazamos los "=" con tabulador en cada columna y por lo tanto generamos columnas nuevas separadas, luego filtramos conservando aquellas lineas cuyo 
valor de "len" fuera mayor a 18 y cuyo valor de "PredHel" fuera diferente de 0.
	
    $ perl -pe 's/\=/\t/g' tmhmm_225.out | awk 'BEGIN{FS="\t"}{if($3>18 && $9!="0"){print $AF}}' > tmhmm_exclude_file
    $ wc -l tmhmm_exclude_file
    297 tmhmm_exclude_file

    $ head tmhmm_exclude_file   

Bobra.101_2s0080.1.v2.1_8	len	24	ExpAA	17.21	First60	17.21	PredHel	1	Topology	o5-22i
Bobra.101_2s0113.1.v2.1_7	len	47	ExpAA	14.22	First60	14.22	PredHel	1	Topology	i7-29o
MSTRG.365.1_3	len	36	ExpAA	21.54	First60	21.54	PredHel	1	Topology	o10-32i
MSTRG.374.1_10	len	31	ExpAA	18.34	First60	18.34	PredHel	1	Topology	i2-24o
MSTRG.374.1_13	len	68	ExpAA	33.62	First60	30.29	PredHel	2	Topology	o15-37i44-66o
MSTRG.446.1_2	len	61	ExpAA	13.51	First60	13.51	PredHel	1	Topology	o11-33i
MSTRG.545.1_8	len	36	ExpAA	18.20	First60	18.20	PredHel	1	Topology	i13-35o
Bobra.106_1s0064.1.v2.1_19	len	40	ExpAA	17.10	First60	17.10	PredHel	1	Topology	o4-22i
MSTRG.645.1_12	len	45	ExpAA	17.86	First60	17.86	PredHel	1	Topology	o10-32i
MSTRG.645.1_30	len	25	ExpAA	18.26	First60	18.26	PredHel	1	Topology	o4-23i


    $  tail tmhmm_exclude_file  

Bobra.85_1s0019.1.v2.1_7	len	36	ExpAA	21.78	First60	21.78	PredHel	1	Topology	i13-35o
MSTRG.17616.1_7	len	66	ExpAA	17.17	First60	17.17	PredHel	1	Topology	i23-42o
MSTRG.17667.1_4	len	41	ExpAA	22.37	First60	22.37	PredHel	1	Topology	i12-34o
Bobra.0091s0067.1.v2.1_5	len	45	ExpAA	15.32	First60	15.32	PredHel	1	Topology	i21-38o
MSTRG.17749.1_4	len	38	ExpAA	19.79	First60	19.79	PredHel	1	Topology	i7-29o
Bobra.0093s0010.1.v2.1_11	len	46	ExpAA	18.48	First60	18.48	PredHel	1	Topology	o11-28i
MSTRG.17961.1_4	len	34	ExpAA	20.54	First60	20.54	PredHel	1	Topology	o4-26i
Bobra.9_2s0016.2.v2.1_8	len	62	ExpAA	15.46	First60	15.46	PredHel	1	Topology	i31-50o
Bobra.9_2s0036.1.v2.1_4	len	44	ExpAA	19.13	First60	19.13	PredHel	1	Topology	o5-23i
Bobra.9_2s0036.1.v2.1_8	len	68	ExpAA	22.38	First60	22.38	PredHel	1	Topology	o25-47i


    $ awk '{print $1}' tmhmm_exclude_file > tmhmm_exclude_list
    
    
    *Primero con el ID 'MSTRG'*

    $ grep 'MSTRG' tmhmm_exclude_list > tmhmm_MSTRG
    $ wc -l tmhmm_MSTRG
    142 tmhmm_MSTRG
    $ sed 's/_.*//' tmhmm_MSTRG | sort -V | uniq > tmhmm_MSTRG_nr
    $ wc -l tmhmm_MSTRG_nr 
    112 tmhmm_MSTRG_nr

    $ head tmhmm_MSTRG_nr 

MSTRG.365.1
MSTRG.374.1
MSTRG.446.1
MSTRG.545.1
MSTRG.645.1
MSTRG.675.1
MSTRG.691.1
MSTRG.700.1
MSTRG.739.1
MSTRG.744.1

    $ tail tmhmm_MSTRG_nr 

MSTRG.16900.1
MSTRG.17118.1
MSTRG.17178.1
MSTRG.17219.8
MSTRG.17261.1
MSTRG.17386.1
MSTRG.17616.1
MSTRG.17667.1
MSTRG.17749.1
MSTRG.17961.1

    *Luego con el ID 'Bobra'*

    $ grep 'Bobra' tmhmm_exclude_list > tmhmm_Bobra
    $ wc -l tmhmm_Bobra
    155 tmhmm_Bobra
    $ sed 's/v2.1_.*//' tmhmm_Bobra | awk '{print $0"v2.1"}' | sort -V | uniq > tmhmm_Bobra_nr
    $ wc -l tmhmm_Bobra_nr
    118 tmhmm_Bobra_nr

    $ head tmhmm_Bobra_nr 

Bobra.4_1s0042.1.v2.1
Bobra.4_3s0019.1.v2.1
Bobra.0006s0012.1.v2.1
Bobra.9_2s0016.2.v2.1
Bobra.9_2s0036.1.v2.1
Bobra.0012s0047.1.v2.1
Bobra.13_2s0015.1.v2.1
Bobra.0014s0123.1.v2.1
Bobra.0018s0077.1.v2.1
Bobra.0022s0003.1.v2.1

    $ tail tmhmm_Bobra_nr 

Bobra.0346s0048.1.v2.1
Bobra.0352s0032.1.v2.1
Bobra.357_2s0021.1.v2.1
Bobra.0365s0044.1.v2.1
Bobra.0367s0087.1.v2.1
Bobra.0386s0030.1.v2.1
Bobra.0492s0001.1.v2.1
Bobra.0503s0001.1.v2.1
Bobra.0686s0002.1.v2.1
Bobra.0757s0003.1.v2.1


Ambas listas se unieron en una sola

    $ cat tmhmm_MSTRG_nr tmhmm_Bobra_nr | sort -V | uniq > tmhmm_exclude_list_nr
    $ wc -l tmhmm_exclude_list_nr
    230 tmhmm_exclude_list_nr

Se extrajo una lista de los transcritos del input de la corrida del *TMHMM* en Mazorka (filter_signalp_225.fasta)

    $ grep '>' filter_signalp_225.fasta | sed 's/>//g' | sort -V | uniq > all_filter_signalp_nr
    $ wc -l all_filter_signalp_nr
    1280 all_filter_signalp_nr

Se comparó ambas listas para obtener todos los transcritos que queremos conservar

    $ diff tmhmm_exclude_list_nr all_filter_signalp_nr | grep \> | cut -f2 -d' ' > tmhmm_include_list_nr
    $ wc  -l tmhmm_include_list_nr 
    1050 tmhmm_include_list_nr  # Se elimino al final 230 transcritos de los 1280 inicilaes y conservaremos 1050
    
Una vez teniendo la lista de ID's que debemos incluir (o conservar) en nuestro archivo *fasta*, se procedio a hacer una comparacion usando *faSomeRecords* contra el archivo del filtro anterior,
el archivo "filter_signalp_225.fasta"

    $ faSomeRecords filter_signalp_225.fasta tmhmm_include_list_nr filter_tmhmm_225.fasta
    $ grep -E '>' filter_tmhmm_225.fasta | wc -l
    1050 # Cantidad de transcritos de nuestro archivo *fasta*

    $ head filter_tmhmm_225.fasta

>MSTRG.9.1
AATGAATTGGTCAATACTGTACTTAGTGACATCCCCTGTCACATGCCATCAACAGCAGTG
TAAACTATAGCCTTTATAGCCCTTTATGAACGGTATAGATCATACTCGTACCTCAATGAC
ATAACACGTGACTTGTACGATATGCGTTCTCAATAGACATTACGTATGGTAGTGCTTTAG
GATCCCTGCAGTCCGAGCCATCCCTGGGCAGCCCAGGAACGGCTCGGTACTTGTCCGAAG
CATGTTGGTATCCTTGACCAACCTCTACTGTTGGAGAGTGCTGTGTGCTGTTCCTGGAAA
CGTTGTAGCGTGGACGGTTTTTCCCACGGGCTACGAACTGACGCTGCCGTCGTGCCATCA
CTGCAGGTGACGCCGCAAGCAGAAATGCTTGAGGTTATGAGCCC
>MSTRG.59.1
CTATGGCCTCGCATGACACGTAATCCGTAGCTTTACATGATTGCCACAGTACCAGCAACT

    $ tail filter_tmhmm_225.fasta 

TATGTCGGTACATCTTTACATTTTGAGCGTTTGAGTGAAAATTTTGTTAACATTTGACAC
ATATAATTGACGAGAGGGCGAGTAGAGCATTTTTTAGACAACCGAGAAGTGAGTGAGTCT
TTGATCATGCAAGGGTTTGGTACTTGAGCTTGGTTAAGGACACTGCTACGTGTTGTTGTG
TAAAGTTTGTTCTGCCTGTCAGTATGGACT
>Bobra.9_2s0121.1.v2.1
TTATAAGAGAGCTCTCAATCCGCAAAGACCTGAGCTAGAGATCTCAGCCAAAAGCCGTCT
GAACGCGGGCTTGGCACTTGCATGTGAGTTCAAAGGTACATTTAGACTTTTATATGAGCC
CAGCACGGAACAATTGGCACCCATCCAAACAAGAGGAGCCTGTGTGGGGTAGAGCGTGAC
AAAATGTGGAGGAATTGTGTGGGGGGGGGTGGGAAGTGTCTGGGAGCTCGCTTTCCCATG
GTAGGCGGCGGTCGGGCGCGACAT



#### Filtro 6: Homologia con secuencias de nucleotidos y aminoacidos usando Blastx y Blastp

#### BLASTX

El archivo filter_tmhmm_225.fasta se copio a Mazorka y se corrio el modulo del ncbi-blast+ para hacer un blast de nucleotidos usando la base de datos no redundante

#PBS -N blastx_BBRB
#PBS -q default
#PBS -l nodes=1:ppn=8,vmem=18gb,walltime=999:00:00
#PBS -o blastx_st.out
#PBS -e blastx_st.err

module load ncbi-blast+/2.6.0

cd $PBS_O_WORKDIR

blastx -query filter_tmhmm_225.fasta -db /data/secuencias/NR/nr -num_threads 8 -max_target_seqs 1 -outfmt 6 -out blastx_BBRB_225.outmt6


El archivo de salida "blastx_BBRB?225.outmt6" se copio a una carpeta local en la computadora y se procedio a hacer los filtros
Se genero una lista de todos aquellos transcritos cuyo *e-value* de alineamiento fuera menor a 1e-3

    $ head blastx_BBRB_225.outmt6

MSTRG.9.1	AKE41865.1	30.508	59	38	1	386	219	16	74	6.7	35.8
Bobra.0100s0001.1.v2.1	XP_014358561.1	53.968	63	29	0	294	106	365	427	4.02e-15	79.3
Bobra.0100s0081.1.v2.1	WP_011044564.1	33.929	56	28	1	458	291	90	136	9.6	35.8
MSTRG.242.1	WP_053509222.1	45.161	31	17	0	116	208	23	53	3.4	35.4
MSTRG.276.1	WP_020998466.1	38.298	47	29	0	331	191	19	65	4.2	36.6
Bobra.101_2s0009.1.v2.1	XP_013038953.1	36.667	60	37	1	373	197	148	207	2.5	37.7
Bobra.101_2s0087.1.v2.1	EKC38957.1	22.623	305	203	9	948	49	12	288	1.19e-11	74.7
Bobra.101_2s0116.1.v2.1	XP_005844644.1	39.394	33	20	0	485	387	11	43	2.1	37.7
MSTRG.209.1	KKF20718.1	30.645	186	119	3	571	17	187	363	3.59e-19	94.7
Bobra.0102s0013.1.v2.1	ESZ25772.1	32.394	71	46	2	5	211	39	109	8.4	33.9

    $ tail blastx_BBRB_225.outmt6 

MSTRG.17886.2	XP_005847413.1	75.949	79	19	0	239	3	1	79	6.10e-26	111
MSTRG.17950.1	WP_024530693.1	34.000	50	33	0	298	149	207	256	7.1	35.4
MSTRG.17954.1	XP_012940039.1	34.884	43	28	0	204	76	77	119	4.5	35.8
MSTRG.17955.1	EFO23984.2	47.368	38	17	2	2	109	45	81	2.2	36.2
MSTRG.17958.1	KNG49934.1	57.692	26	11	0	78	155	397	422	4.3	34.7
Bobra.9_1s0011.1.v2.1	XP_006071991.1	57.692	26	11	0	38	115	176	201	1.2	37.7
MSTRG.17971.1	YP_009106170.1	68.519	54	17	0	38	199	252	305	3.23e-22	72.4
MSTRG.17971.1	YP_009106170.1	65.000	40	14	0	172	291	297	336	3.23e-22	59.3
MSTRG.17978.1	WP_028404646.1	41.071	56	25	2	184	41	198	253	5.9	34.3
Bobra.9_2s0121.1.v2.1	WP_054772969.1	43.590	39	22	0	177	61	279	317	4.0	35.4


La columna correspondiente al *e-value* corresponde a la #11, de esa se realizo la seleccion de transcritos

    $ awk 'BEGIN{FS="\t"}{if($11<(1e-3)){print $AF}}' blastx_BBRB_225.outmt6 > blastx_exclude_file
    $ wc -l blastx_exclude_file 
    245 blastx_exclude_file
    $ head blastx_exclude_file

Bobra.0100s0001.1.v2.1	XP_014358561.1	53.968	63	29	0	294	106	365	427	4.02e-15	79.3
Bobra.101_2s0087.1.v2.1	EKC38957.1	22.623	305	203	9	948	49	12	288	1.19e-11	74.7
MSTRG.209.1	KKF20718.1	30.645	186	119	3	571	17	187	363	3.59e-19	94.7
Bobra.0102s0035.1.v2.1	XP_005648705.1	71.264	87	25	0	306	46	49	135	9.37e-28	107
MSTRG.531.1	XP_009365073.1	55.340	103	46	0	314	6	695	797	1.78e-20	94.4
MSTRG.531.1	XP_009365073.1	53.398	103	48	0	314	6	719	821	2.98e-20	93.6
MSTRG.531.1	XP_009365073.1	52.427	103	49	0	314	6	743	845	3.35e-20	93.6
MSTRG.531.1	XP_009365073.1	53.398	103	48	0	314	6	671	773	1.09e-18	89.0
MSTRG.531.1	XP_009365073.1	53.191	94	44	0	287	6	656	749	9.32e-18	86.3
MSTRG.531.1	XP_009365073.1	45.918	98	51	1	299	6	630	725	1.25e-10	66.2

    $ tail blastx_exclude_file

Bobra.0077s0039.1.v2.1	XP_005644301.1	44.949	198	93	6	575	3	20	208	8.35e-38	146
Bobra.0077s0113.1.v2.1	EKC38957.1	23.214	280	184	8	915	91	20	273	6.70e-13	77.8
Bobra.7_2s0002.1.v2.1	XP_011396059.1	52.239	134	64	0	444	43	1	134	4.12e-34	125
Bobra.0081s0013.1.v2.1	EKC34444.1	28.448	116	69	2	906	559	199	300	2.68e-06	59.3
MSTRG.17345.1	EKC34444.1	36.364	77	47	1	247	17	209	283	1.10e-07	57.0
Bobra.0083s0008.1.v2.1	WP_042847056.1	39.394	66	39	1	317	120	50	114	4.90e-05	48.1
Bobra.0087s0027.1.v2.1	XP_005642825.1	54.867	113	51	0	369	31	1	113	2.27e-30	117
MSTRG.17886.2	XP_005847413.1	75.949	79	19	0	239	3	1	79	6.10e-26	111
MSTRG.17971.1	YP_009106170.1	68.519	54	17	0	38	199	252	305	3.23e-22	72.4
MSTRG.17971.1	YP_009106170.1	65.000	40	14	0	172	291	297	336	3.23e-22	59.3


Una vez teniendo el archivo con todos aquellos transcritos con *e-value* menor a 1e-3 se procedio a extraer la lista de los ID's

    $ awk '{print $1}' blastx_exclude_file | sort -V | uniq > blastx_exclude_list_nr
    $ wc -l blastx_exclude_list_nr
    175 blastx_exclude_list_nr
    $ head blastx_exclude_list_nr 

Bobra.0002s0033.1.v2.1
Bobra.4_1s0072.1.v2.1
Bobra.4_2s0013.1.v2.1
Bobra.4_2s0017.1.v2.1
Bobra.7_2s0002.1.v2.1
Bobra.0014s0010.1.v2.1
Bobra.0022s0075.1.v2.1
Bobra.0022s0114.1.v2.1
Bobra.0025s0107.1.v2.1
Bobra.0026s0016.1.v2.1

    $ tail blastx_exclude_list_nr 

MSTRG.14391.1
MSTRG.14704.1
MSTRG.15409.1
MSTRG.15560.1
MSTRG.15614.1
MSTRG.15872.1
MSTRG.16885.1
MSTRG.17345.1
MSTRG.17886.2
MSTRG.17971.1

Una vez teniendo la lista de ID's a excluir se procedio a sacar la lista de transcritos totales del archivo fasta original (el que se uso como input para correr el blastx)

    $ grep '>' filter_tmhmm_225.fasta | sed 's/>//g' | sort -V | uniq > all_transc_tmhmm_nr
    $ wc -l all_transc_tmhmm_nr 
    1050 all_transc_tmhmm_nr

Ambas listas se compararon para obtener la lista de transcritos que queremos conservar

    $ diff blastx_exclude_list_nr all_transc_tmhmm_nr | grep \> | cut -f2 -d' ' > blastx_include_list_nr
    $ wc -l blastx_include_list_nr
    875 blastx_include_list_nr # Numero total de transcritos a conservar, luego de remover 175 con el filtro del blastx

Una vez teniendo la lista de ID's que debemos incluir (o conservar) en nuestro archivo *fasta*, se procedio a hacer una comparacion usando *faSomeRecords* contra el archivo del filtro anterior

    $ faSomeRecords filter_tmhmm_225.fasta blastx_include_list_nr filter_blastx_225.fasta
    $ grep -E '>' filter_blastx_225.fasta | wc -l
    875 # Cantidad de transcritos de nuestro archivo *fasta*

    $ head filter_blastx_225.fasta

>MSTRG.9.1
AATGAATTGGTCAATACTGTACTTAGTGACATCCCCTGTCACATGCCATCAACAGCAGTG
TAAACTATAGCCTTTATAGCCCTTTATGAACGGTATAGATCATACTCGTACCTCAATGAC
ATAACACGTGACTTGTACGATATGCGTTCTCAATAGACATTACGTATGGTAGTGCTTTAG
GATCCCTGCAGTCCGAGCCATCCCTGGGCAGCCCAGGAACGGCTCGGTACTTGTCCGAAG
CATGTTGGTATCCTTGACCAACCTCTACTGTTGGAGAGTGCTGTGTGCTGTTCCTGGAAA
CGTTGTAGCGTGGACGGTTTTTCCCACGGGCTACGAACTGACGCTGCCGTCGTGCCATCA
CTGCAGGTGACGCCGCAAGCAGAAATGCTTGAGGTTATGAGCCC
>MSTRG.59.1
CTATGGCCTCGCATGACACGTAATCCGTAGCTTTACATGATTGCCACAGTACCAGCAACT

    $ tail filter_blastx_225.fasta 

TATGTCGGTACATCTTTACATTTTGAGCGTTTGAGTGAAAATTTTGTTAACATTTGACAC
ATATAATTGACGAGAGGGCGAGTAGAGCATTTTTTAGACAACCGAGAAGTGAGTGAGTCT
TTGATCATGCAAGGGTTTGGTACTTGAGCTTGGTTAAGGACACTGCTACGTGTTGTTGTG
TAAAGTTTGTTCTGCCTGTCAGTATGGACT
>Bobra.9_2s0121.1.v2.1
TTATAAGAGAGCTCTCAATCCGCAAAGACCTGAGCTAGAGATCTCAGCCAAAAGCCGTCT
GAACGCGGGCTTGGCACTTGCATGTGAGTTCAAAGGTACATTTAGACTTTTATATGAGCC
CAGCACGGAACAATTGGCACCCATCCAAACAAGAGGAGCCTGTGTGGGGTAGAGCGTGAC
AAAATGTGGAGGAATTGTGTGGGGGGGGGTGGGAAGTGTCTGGGAGCTCGCTTTCCCATG
GTAGGCGGCGGTCGGGCGCGACAT



#### *Viernes 21 de febrero de 2020*


#### BLASTP

Se convirtio de nucleotidos a aminoacidos el archivo *fasta* obtenido del filtro del *blastx* (filter_blastx_225.fasta) para correr el blast de proteinas

    $ getorf -reverse F -minsize 20 -find 0 filter_blastx_225.fasta filter_blastx_225_protein.fasta
    $ grep '>' filter_blastx_225_protein.fasta | wc -l
    13109

Sacamos los ORFs mas largos de cada transcrito

   Primero se calculo con infoseq la longitud de cada ORF y se guardo el resultado en un indice

    $ infoseq filter_blastx_225_protein.fasta | awk '{print $3"\t"$6}' > filter_blastx_225_protein.idx

  Se edito manualmente el indice quitando el encabezado del archivo

    $ vi filter_blastx_225_protein.idx 
    $ head filter_blastx_225_protein.idx

MSTRG.9.1_1	6
MSTRG.9.1_2	20
MSTRG.9.1_3	22
MSTRG.9.1_4	19
MSTRG.9.1_5	6
MSTRG.9.1_6	14
MSTRG.9.1_7	30
MSTRG.9.1_8	15
MSTRG.9.1_9	7
MSTRG.9.1_10	41

    $ tail filter_blastx_225_protein.idx 

Bobra.9_2s0121.1.v2.1_1	8
Bobra.9_2s0121.1.v2.1_2	11
Bobra.9_2s0121.1.v2.1_3	7
Bobra.9_2s0121.1.v2.1_4	17
Bobra.9_2s0121.1.v2.1_5	56
Bobra.9_2s0121.1.v2.1_6	38
Bobra.9_2s0121.1.v2.1_7	23
Bobra.9_2s0121.1.v2.1_8	6
Bobra.9_2s0121.1.v2.1_9	28
Bobra.9_2s0121.1.v2.1_10	49


  Se edito el indice para remover el "_#" de cada ID, para hacerlo en una sola linea de comando y no separar los ID's de "MSTRG" y "Bobra", se invertio cada ID con el comando *rev*, se corto con *cut*
al inicio de cada *string* el numero y el caracter "_" en el campo 1 (-f1), se volvio a ordenar los IDs con *rev*, se sortearon y se eliminaron redundantes

    $ cut -f1 filter_blastx_225_protein.idx | rev | cut -d_ -f1 --complement | rev | sort -V | uniq > id_list
    $ wc -l id_list 
    875 id_list

    $ head id_list 

Bobra.0002s0015.1.v2.1
Bobra.0002s0024.1.v2.1
Bobra.0003s0040.1.v2.1
Bobra.0003s0058.1.v2.1
Bobra.4_1s0044.1.v2.1
Bobra.4_1s0096.1.v2.1
Bobra.4_1s0141.1.v2.1
Bobra.4_3s0010.1.v2.1
Bobra.4_3s0011.1.v2.1
Bobra.0005s0043.1.v2.1

    $ tail id_list 

MSTRG.17732.2
MSTRG.17771.1
MSTRG.17782.1
MSTRG.17810.1
MSTRG.17895.1
MSTRG.17950.1
MSTRG.17954.1
MSTRG.17955.1
MSTRG.17958.1
MSTRG.17978.1


   Por ultimo se hizo un ciclo *for* para obtener los ORfs mas largos de cada transcrito. El ciclo *for* se explico de la siguiente manera:

 1) Determinamos una variable llamada *id* la cual se va a encontrar dentro de "id_list"
 2) Ejecutamos un script buscando la variable *id* que contuviera caracteres como "_[0-9]" en "filter_blastx_225_protein.idx"
 3) Luego sorteamos de mayor a menor las longitudes en la columna #2 de "filter_blastx_225_protein.idx" y seleccionamos la mas grande que se coloco en la primera posicion
 4) Cortamos el campo 1 (columna #1) y guardamos en un archivo de salida en forma de tabla con dos columnas, el ID de cada transcrito en una columna y en la otra el ID de los ORFs mas largos por 
 cada transcrito

    $ for id in $(cat id_list); do longest_orf=$(grep ${id}_[0-9] filter_blastx_225_protein.idx | sort -nrk2 | head -n1 | cut -f1); echo -e "$id\t$longest_orf" ; done > longest_orf_blastx

Teniendo el archivo con los ID's de los ORFs mas largos cortamos la columna 2 y generamos una lista de ID's de los ORFs que queremos usar para correr el *blastp*

    $ cut -f2 longest_orf_blastx > longest_orf_list
    $ wc -l longest_orf_list 
    875 longest_orf_list

    $ head longest_orf_list 

Bobra.0002s0015.1.v2.1_14
Bobra.0002s0024.1.v2.1_5
Bobra.0003s0040.1.v2.1_21
Bobra.0003s0058.1.v2.1_8
Bobra.4_1s0044.1.v2.1_6
Bobra.4_1s0096.1.v2.1_16
Bobra.4_1s0141.1.v2.1_4
Bobra.4_3s0010.1.v2.1_7
Bobra.4_3s0011.1.v2.1_20
Bobra.0005s0043.1.v2.1_7

    $ tail longest_orf_list 

MSTRG.17732.2_13
MSTRG.17771.1_19
MSTRG.17782.1_3
MSTRG.17810.1_13
MSTRG.17895.1_7
MSTRG.17950.1_7
MSTRG.17954.1_10
MSTRG.17955.1_6
MSTRG.17958.1_7
MSTRG.17978.1_11


Com paramos esta lista contra el archivo fasta de proteinas del filtro del *blastx* con faSomeRecords

    $ faSomeRecords filter_blastx_225_protein.fasta longest_orf_list filter_blastx_longest_orf.fasta
    $ grep -E '>' filter_blastx_longest_orf.fasta | wc -l
    875 # Cantidad de ID's de nuestro archivo *fasta* de aminoacidos con la secuencia del ORF mas largo por transcrito

    $ head filter_blastx_longest_orf.fasta

head filter_blastx_longest_orf.fasta
>MSTRG.9.1_16 [181 - 402] 
DPCSPSHPWAAQERLGTCPKHVGILDQPLLLESAVCCSWKRCSVDGFSHGLRTDAAVVPS
LQVTPQAEMLEVMS
>MSTRG.59.1_10 [85 - 225] 
QAMQAELVKELRLSREACNTVLARAKILWLVVGSSRIATSAARRRLL
>Bobra.0100s0049.1.v2.1_12 [188 - 325] 
TTEYELACTLAHAQLLRCQQHIWGTIANLPRSRENPDRLPLASLID
>Bobra.0100s0081.1.v2.1_10 [201 - 377] 
NRKSIQTVQEHVAGQSLRAKLANEVNIIAWKSPVPNQHISLNNSGRQTHFFQKVMGQIR
>MSTRG.242.1_6 [2 - 217] 

    $ tail filter_blastx_longest_orf.fasta

>Bobra.9_1s0012.1.v2.1_6 [3 - 191] 
IQGDLRLIVLLRKVGALVRIPLTTHRVYRIWCEDAIRYDYAVLKAVFCRFVPSSVLEWCS
GCT
>MSTRG.17978.1_11 [99 - 221] 
SKDKTTAATDKCLNRRRTTNTCKHSQGFRTGNFALVLHLVE
>Bobra.9_2s0005.1.v2.1_7 [30 - 233] 
VPAVFSPCVECLISSFHLVEASIGVSEVRQRCPCVAQIENLLCECSSFEKLKLCLEFQTV
PRLFELVR
>Bobra.9_2s0121.1.v2.1_5 [2 - 169] 
YKRALNPQRPELEISAKSRLNAGLALACEFKGTFRLLYEPSTEQLAPIQTRGACVG


El archivo *filter_blastx_longest_orf.fasta* se copio a Mazorka en la carpeta de Cabili/guided para correr el blast de proteinas

#PBS -N blastp_BBRB
#PBS -q default
#PBS -l nodes=1:ppn=8,vmem=18gb,walltime=999:00:00
#PBS -o blastp_st.out
#PBS -e blastp_st.err

module load ncbi-blast+/2.6.0

cd $PBS_O_WORKDIR

blastp -query filter_blastx_longest_orf.fasta -db /data/secuencias/NR/nr -num_threads 8 -max_target_seqs 1 -outfmt 6 -out blastp_BBRB_225.outmt6


Se copio el archvio de salida a una carpeta local en la computadora y se reviso este

    $ head blastp_BBRB_225.outmt6 

MSTRG.9.1_16	XP_015010417.1	30.137	73	40	1	10	71	869	941	3.1	35.0
MSTRG.242.1_6	WP_006635162.1	38.462	39	24	0	21	59	83	121	2.8	35.0
MSTRG.276.1_8	XP_011297101.1	35.849	53	33	1	1	52	2	54	0.84	36.2
Bobra.101_2s0009.1.v2.1_6	WP_046111909.1	39.216	51	29	1	20	68	204	254	0.70	36.6
Bobra.101_2s0116.1.v2.1_24	WP_015246818.1	43.137	51	21	2	17	62	155	202	8.9	33.9
Bobra.0102s0030.1.v2.1_5	EQK97599.1	53.571	28	12	1	23	49	79	106	3.7	33.9
MSTRG.348.1_10	EKD32777.1	34.615	52	31	1	4	52	410	461	0.088	38.9
MSTRG.336.1_5	WP_010740924.1	42.500	40	22	1	4	42	897	936	4.4	33.5
MSTRG.346.1_11	WP_025315424.1	31.746	63	42	1	7	69	318	379	0.59	37.0
Bobra.0103s0006.1.v2.1_4	XP_011787887.1	35.417	48	26	1	8	50	35	82	1.3	35.4


    $ tail blastp_BBRB_225.outmt6 

MSTRG.17732.2_13	XP_010701871.1	48.387	31	16	0	32	62	477	507	4.8	33.9
Bobra.92_2s0020.1.v2.1_9	WP_053932174.1	30.556	72	39	3	1	63	103	172	0.54	37.0
MSTRG.17782.1_3	CQB46367.1	33.333	63	39	1	4	63	25	87	7.4	33.5
MSTRG.17895.1_7	XP_002949450.1	50.000	36	18	0	13	48	29	64	4.2	33.5
MSTRG.17954.1_10	XP_003956289.1	50.000	30	12	1	9	35	497	526	3.8	33.9
MSTRG.17955.1_6	EFO23984.2	47.368	38	17	2	1	36	45	81	1.0	36.2
MSTRG.17958.1_7	KNG49934.1	55.172	29	13	0	26	54	397	425	3.3	35.0
Bobra.9_1s0011.1.v2.1_3	XP_006071991.1	57.692	26	11	0	12	37	176	201	0.20	37.7
Bobra.9_1s0012.1.v2.1_6	WP_051434744.1	34.694	49	29	1	12	57	388	436	2.5	35.0
Bobra.9_2s0005.1.v2.1_7	WP_057409099.1	42.857	77	32	5	3	68	998	1073	0.87	36.6


De este archivo se selecciono todos aquellos transcritos cuyo *e-value* del match fuera menor a 1e-3

    $ awk 'BEGIN{FS="\t"}{if($11<(1e-3)){print $AF}}' blastp_BBRB_225.outmt6 > blastp_exclude_file
    $ wc -l blastp_exclude_file 
    29 blastp_exclude_file

    $ head blastp_exclude_file 

MSTRG.934.1_12	EMT17642.1	46.154	65	23	3	8	61	35	98	3.10e-04	45.8
Bobra.113_1s0008.1.v2.1_12	WP_044810544.1	40.323	62	37	0	5	66	658	719	1.64e-04	47.0
Bobra.113_1s0008.1.v2.1_12	WP_044810544.1	37.313	67	42	0	5	71	651	717	5.33e-04	45.8
MSTRG.2260.1_3	EZA47853.1	98.462	65	1	0	4	68	305	369	6.32e-13	70.9
MSTRG.2260.1_3	EZA47853.1	96.923	65	2	0	4	68	307	371	1.13e-12	70.1
MSTRG.6657.1_3	CDQ87324.1	98.529	68	1	0	2	69	302	369	1.40e-14	75.5
MSTRG.6657.1_3	CDQ87324.1	95.588	68	1	1	2	69	306	371	1.56e-12	69.7
MSTRG.6657.1_3	CDQ87324.1	95.588	68	1	1	2	69	308	373	1.56e-12	69.7
MSTRG.6657.1_3	CDQ87324.1	95.588	68	1	1	2	69	310	375	1.56e-12	69.7
MSTRG.6657.1_3	CDQ87324.1	95.588	68	1	1	2	69	312	377	1.56e-12	69.7

    $ tail blastp_exclude_file 

MSTRG.6657.1_3	CDQ87324.1	92.593	54	4	0	2	55	346	399	5.53e-09	59.7
MSTRG.10409.1_3	EZA47853.1	95.588	68	3	0	2	69	306	373	2.92e-15	77.4
MSTRG.10409.1_3	EZA47853.1	100.000	66	0	0	2	67	304	369	3.86e-15	77.0
MSTRG.10409.1_3	EZA47853.1	100.000	65	0	0	3	67	303	367	1.01e-14	75.9
MSTRG.14437.1_3	XP_001742719.1	96.429	56	2	0	17	72	24	79	2.73e-13	72.0
MSTRG.14942.1_3	CDQ60263.1	100.000	53	0	0	2	54	36	88	2.49e-11	65.1
MSTRG.14942.1_3	CDQ60263.1	96.154	52	2	0	3	54	33	84	9.15e-10	60.8
MSTRG.17499.1_3	CDQ72384.1	98.507	67	1	0	2	68	948	1014	6.32e-16	79.3
MSTRG.17499.1_3	CDQ72384.1	97.015	67	2	0	3	69	961	1027	4.48e-15	77.0
MSTRG.17499.1_3	CDQ72384.1	98.485	66	1	0	3	68	955	1020	4.57e-15	77.0

Una vez teniendo el archivo con todos aquellos transcritos con *e-value* menor a 1e-3 se procedio a extraer la lista de los ID's de los transcritos

    $ awk '{print $1}' blastp_exclude_file | sort -V | uniq > blastp_exclude_list_nr
    $ wc -l blastp_exclude_list_nr
    8 blastp_exclude_list_nr

    $ head blastp_exclude_list_nr 

Bobra.113_1s0008.1.v2.1_12
MSTRG.934.1_12
MSTRG.2260.1_3
MSTRG.6657.1_3
MSTRG.10409.1_3
MSTRG.14437.1_3
MSTRG.14942.1_3
MSTRG.17499.1_3

Se edito los ID's removiendo los caracteres "_#"

    $ rev blastp_exclude_list_nr | cut -d_ -f1 --complement | rev | sort -V | uniq > blastp_exclude_list_uniq
    $ head blastp_exclude_list_uniq 

Bobra.113_1s0008.1.v2.1
MSTRG.934.1
MSTRG.2260.1
MSTRG.6657.1
MSTRG.10409.1
MSTRG.14437.1
MSTRG.14942.1
MSTRG.17499.1


Una vez teniendo la lista de ID's a excluir se procedio a sacar la lista de transcritos totales del archivo fasta original (el que se uso como input para correr el blastp)

    $ grep '>' filter_blastx_225.fasta | sed 's/>//g' | sort -V | uniq > all_transc_blastx_nr
    $ wc -l all_transc_blastx_nr
    875 all_transc_blastx_nr

Ambas listas se compararon para obtener la lista de transcritos que queremos conservar

    $ diff blastp_exclude_list_uniq all_transc_blastx_nr | grep \> | cut -f2 -d' ' > blastp_include_list_nr
    $ wc -l blastp_include_list_nr
    867 blastp_include_list_nr # Numero total de transcritos a conservar, luego de remover 8 con el filtro del blastp

Una vez teniendo la lista de ID's que debemos incluir (o conservar) en nuestro archivo *fasta*, se procedio a hacer una comparacion para obtener el *fasta* final del filtro

    $ faSomeRecords filter_blastx_225.fasta blastp_include_list_nr filter_blastp_225.fasta
    $ grep -E '>' filter_blastp_225.fasta | wc -l
    867 # Cantidad de transcritos de nuestro archivo *fasta*

    $ head filter_blastp_225.fasta

>MSTRG.9.1
AATGAATTGGTCAATACTGTACTTAGTGACATCCCCTGTCACATGCCATCAACAGCAGTG
TAAACTATAGCCTTTATAGCCCTTTATGAACGGTATAGATCATACTCGTACCTCAATGAC
ATAACACGTGACTTGTACGATATGCGTTCTCAATAGACATTACGTATGGTAGTGCTTTAG
GATCCCTGCAGTCCGAGCCATCCCTGGGCAGCCCAGGAACGGCTCGGTACTTGTCCGAAG
CATGTTGGTATCCTTGACCAACCTCTACTGTTGGAGAGTGCTGTGTGCTGTTCCTGGAAA
CGTTGTAGCGTGGACGGTTTTTCCCACGGGCTACGAACTGACGCTGCCGTCGTGCCATCA
CTGCAGGTGACGCCGCAAGCAGAAATGCTTGAGGTTATGAGCCC
>MSTRG.59.1
CTATGGCCTCGCATGACACGTAATCCGTAGCTTTACATGATTGCCACAGTACCAGCAACT

    $ tail filter_blastp_225.fasta 

TATGTCGGTACATCTTTACATTTTGAGCGTTTGAGTGAAAATTTTGTTAACATTTGACAC
ATATAATTGACGAGAGGGCGAGTAGAGCATTTTTTAGACAACCGAGAAGTGAGTGAGTCT
TTGATCATGCAAGGGTTTGGTACTTGAGCTTGGTTAAGGACACTGCTACGTGTTGTTGTG
TAAAGTTTGTTCTGCCTGTCAGTATGGACT
>Bobra.9_2s0121.1.v2.1
TTATAAGAGAGCTCTCAATCCGCAAAGACCTGAGCTAGAGATCTCAGCCAAAAGCCGTCT
GAACGCGGGCTTGGCACTTGCATGTGAGTTCAAAGGTACATTTAGACTTTTATATGAGCC
CAGCACGGAACAATTGGCACCCATCCAAACAAGAGGAGCCTGTGTGGGGTAGAGCGTGAC
AAAATGTGGAGGAATTGTGTGGGGGGGGGTGGGAAGTGTCTGGGAGCTCGCTTTCCCATG
GTAGGCGGCGGTCGGGCGCGACAT



#### *Lunes 24 de febrero de 2020*


#### Filtro 7: Homologia con secuencias de tRNA y rRNA de Chlamydomonas reinhardtii


Buscar secuencias de RNAs de transferencia y ribosomales, indexarlos como con bowtie y alinear transcritos contra ese indice

Se descargo de la base de datos de *Silva* (https://www.arb-silva.de/) las secuencias de rRNAs de *Chlamydomonas reinhardtii* en un archivo fasta comprimido sin *gaps* y el archivo se cambio de nombre

    $ mv arb-silva.de_2020-02-24_id787387_tax_silva.fasta ./rRNA_chlamy_silvadb.fasta

De la bas de datos *tRNAdb* (http://trnadb.bioinf.uni-leipzig.de/DataOutput/Welcome) se descargo las secuencias de tRNA de *C. reinhardtii* y de este archivo tambien se cambio el nombre

    $ mv fasta.fst ./tRNA_chlamy_tRNAdb.fasta

Ambos archvios se unieron en un solo archivo fasta

    $ cat tRNA_chlamy_tRNAdb.fasta rRNA_chlamy_silvadb.fasta > tRNA_rRNA_seqs_chlamy.fasta

Este archivo y el archivo filter_blastp_225.fasta, se copiaron a MAzorka para realizar un alineamiento local con *Bowtie2*
Usando el archivo de las secuencias de los tRNAs y rRNAs se creo un indice en Mazorka, en linea de comandos

    $ module load bowtie2/2.3.5.1 
    $ bowtie2-build -f tRNA_rRNA_seqs_chlamy.fasta tRNA_rRNA

Del indice se crearon los siguientes archivos:

    tRNA_rRNA.1.bt2
    tRNA_rRNA.2.bt2
    tRNA_rRNA.3.bt2
    tRNA_rRNA.4.bt2
    tRNA_rRNA.rev.1.bt2
    tRNA_rRNA.rev.2.bt2

Se envio a la cola *default* de Mazorka el alineamiento local con *bowtie2*

#PBS -N bowtie2_BBRB
#PBS -q default
#PBS -l nodes=1:ppn=8,vmem=18gb,walltime=24:00:00
#PBS -o bowtie2_local.out
#PBS -e bowtie2_local.err

module load bowtie2/2.3.5.1

cd /LUSTRE/usuario/smata/BBRB/Cabili/guided

bowtie2 --local -x tRNA_rRNA -f filter_blastp_225.fasta -S local_tRNA_rRNA.sam
    

Se reviso el archivo ".err"

    $ cat bowtie2_local.err 

867 reads; of these:
  867 (100.00%) were unpaired; of these:
    867 (100.00%) aligned 0 times
    0 (0.00%) aligned exactly 1 time
    0 (0.00%) aligned >1 times
0.00% overall alignment rate

    Tampoco hubo alineamiento con las secuencias de tRNA y rRNA de *C. reinhardtii*

A la vez se hizo el alinemiento local con *minimap* usando el nodo maestro de Mazorka (linea de comandos)

    $ module load minimap2/2.12
    $ minimap2 -x asm5 tRNA_rRNA_seqs_chlamy.fasta filter_blastp_225.fasta


[M::mm_idx_gen::0.012*1.16] collected minimizers
[M::mm_idx_gen::0.015*1.43] sorted minimizers
[M::main::0.015*1.43] loaded/built the index for 107 target sequence(s)
[M::mm_mapopt_update::0.015*1.40] mid_occ = 100
[M::mm_idx_stat] kmer size: 19; skip: 19; is_hpc: 0; #seq: 107
[M::mm_idx_stat::0.015*1.38] distinct minimizers: 694 (26.37% are singletons); average occurrences: 22.326; average spacing: 10.527
[M::worker_pipeline::0.026*1.60] mapped 867 sequences
[M::main] Version: 2.12-r847-dirty
[M::main] CMD: minimap2 -x asm5 tRNA_rRNA_seqs_chlamy.fasta filter_blastp_225.fasta
[M::main] Real time: 0.027 sec; CPU: 0.043 sec; Peak RSS: 0.003 GB

    Los transcritos de mi transcritoma guiado no hicieron *match* con las secuencias de tRNA y rRNA de *C. reinhardtii*


Tambien se corrio un alineamiento con el modulo blast del ncbi, primero se creo la base de datos con el fasta de los tRNAs y rRNAs en el nodo maestro

    $ module load ncbi-blast+/2.6.0
    $ makeblastdb -in tRNA_rRNA_seqs_chlamy.fasta -dbtype nucl

Building a new DB, current time: 02/24/2020 15:13:11
New DB name:   /LUSTRE/usuario/smata/BBRB/Cabili/guided/tRNA_rRNA_seqs_chlamy.fasta
New DB title:  tRNA_rRNA_seqs_chlamy.fasta
Sequence type: Nucleotide
Keep MBits: T
Maximum file size: 1000000000B
Adding sequences from FASTA; added 107 sequences in 0.0470791 seconds.


Posteriormente se corrio el blast enviando a la cola *default* el trabajo

#PBS -N blast_BBRB
#PBS -q default
#PBS -l nodes=1:ppn=8,vmem=18gb,walltime=24:00:00
#PBS -o blast_tRNA_rRNA.out
#PBS -e blast_tRNA_rRNA.err

module load ncbi-blast+/2.6.0

cd /LUSTRE/usuario/smata/BBRB/Cabili/guided

blastn -query filter_blastp_225.fasta -db tRNA_rRNA_seqs_chlamy.fasta -num_threads 8 -max_target_seqs 1 -outfmt 6 -out blast_tRNA_rRNA.outmt6

    El archivo de salida "blast_tRNA_rRNA.outmt6" estaba en blanco, nada hizo match n_n' 


Por lo tanto en este ultimo filtro no se eliminaron transcritos y al final nos quedamos con 867 transcritos que potencialmente son lncRNAs


#### *Martes 25 de febrero de 2020*


#### CPC2 para identificacion de lncRNAS en el ensamble guiado de BBRB


Se corrio CPC2 en linea (http://cpc2.cbi.pku.edu.cn/) con las secuencias de los transcritos del archivo "filter_225_ORFs_st.fasta" y el resultado del analisis se registro con el ID 200226134661376 
El archivo se descargo en formato de texto plano (.txt) y se reviso los resultados

    $ head result_cpc2.txt 

#ID	peptide_length	Fickett_score	pI	ORF_integrity	coding_probability	label
Bobra.0001s0020.1.v2.1	53	0.32621	7.52484130859	1	0.0171213	noncoding
MSTRG.9.1	43	0.44189	7.70404052734	1	0.0244452	noncoding
Bobra.0001s0011.1.v2.1	34	0.44115	11.8851928711	1	0.0346098	noncoding
MSTRG.8.1	8	0.46302	5.75299072266	1	0.00289591	noncoding
MSTRG.59.1	46	0.45332	11.6170043945	1	0.0782934	noncoding
Bobra.0010s0044.1.v2.1	31	0.41901	6.48284912109	1	0.0096951	noncoding
MSTRG.11.1	26	0.44795	9.30438232422	1	0.0159925	noncoding
MSTRG.83.1	37	0.33266	9.10235595703	1	0.011376	noncoding
Bobra.0100s0001.1.v2.1	37	0.43143	9.30096435547	1	0.0237472	noncoding

 
    $ tail result_cpc2.txt
 
MSTRG.18087.1	59	0.46216	10.4584350586	-1	0.116553	noncoding
Bobra.9_2s0005.1.v2.1	15	0.36196	6.50689697266	1	0.00265723	noncoding
Bobra.9_2s0014.1.v2.1	52	0.36016	6.52606201172	1	0.0208394	noncoding
MSTRG.17993.1	35	0.3835	6.05499267578	1	0.0102764	noncoding
Bobra.9_2s0016.2.v2.1	39	0.38942	6.49041748047	-1	0.0229999	noncoding
Bobra.9_2s0036.1.v2.1	68	0.37117	9.82977294922	1	0.061364	noncoding
Bobra.9_2s0064.1.v2.1	37	0.33167	10.7046508789	1	0.017691	noncoding
Bobra.9_2s0121.1.v2.1	27	0.38364	11.542175293	-1	0.166101	noncoding
MSTRG.18045.1	27	0.43513	9.99273681641	1	0.0185818	noncoding
MSTRG.18064.1	0	0.3796	0.0	-1	9.53937e-06	noncoding


    $ grep 'noncoding' result_cpc2.txt > cpc2_noncoding
    $ wc -l cpc2_noncoding 
    2424 cpc2_noncoding


Segun los resultados de CPC2 todos mis 2424 transcritos son no codificantes, pero quise realizar una prueba blasteando los transcritos que en CPC1 fueron descartados como codificantes y que en CPC2
fueron reconocidos como no codificantes

    $ grep -w 'coding' result_in_table > cpc1_coding
    $ wc -l cpc1_coding 
    33 cpc1_coding

    $ head cpc1_coding 

MSTRG.246.1	284	coding	0.090232
Bobra.104_1s0038.1.v2.1	450	coding	0.00389141
Bobra.113_1s0008.1.v2.1	216	coding	0.29465
Bobra.0124s0061.1.v2.1	669	coding	0.123008
MSTRG.2045.1	208	coding	0.514495
Bobra.145_2s0094.1.v2.1	201	coding	0.300668
MSTRG.3348.1	448	coding	1.57507
MSTRG.3497.1	444	coding	0.859716
Bobra.152_2s0023.1.v2.1	207	coding	0.840961
MSTRG.4080.1	218	coding	0.102697

    $ tail cpc1_coding 

MSTRG.13181.1	386	coding	0.297525
Bobra.40_1s0101.1.v2.1	411	coding	0.87564
Bobra.43_2s0075.1.v2.1	265	coding	0.606522
MSTRG.14022.1	274	coding	0.353211
MSTRG.14353.1	378	coding	0.0744533
Bobra.53_1s0026.1.v2.1	222	coding	1.54222
MSTRG.15161.1	374	coding	1.10507
Bobra.0075s0002.1.v2.1	225	coding	0.961918
MSTRG.16970.1	290	coding	0.639976
MSTRG.17971.1	293	coding	0.510996

    $ awk '{print $1}' cpc1_coding | sort -V | uniq > cpc1_coding_list
    $ wc -l cpc1_coding_list
    33 cpc1_coding_list

    $ head cpc1_coding_list 

Bobra.0002s0054.1.v2.1
Bobra.40_1s0101.1.v2.1
Bobra.43_2s0075.1.v2.1
Bobra.53_1s0026.1.v2.1
Bobra.0075s0002.1.v2.1
Bobra.104_1s0038.1.v2.1
Bobra.113_1s0008.1.v2.1
Bobra.0124s0061.1.v2.1
Bobra.145_2s0094.1.v2.1
Bobra.152_2s0023.1.v2.1

    $ tail cpc1_coding_list 

MSTRG.4917.1
MSTRG.5380.1
MSTRG.9443.2
MSTRG.10049.3
MSTRG.13181.1
MSTRG.14022.1
MSTRG.14353.1
MSTRG.15161.1
MSTRG.16970.1
MSTRG.17971.1

Los trancritos de la lista de codificantes de CPC1 se corrieron en blastp para verificar si realmente tienen similitud con secuencias codificantes

    $ faSomeRecords /home/fani/Documents/Tesis_BP/BBRB_analysis/Cabili/guided_ensam/filter_225_ORFs_st.fasta cpc1_coding_list cpc1_coding_transcripts.fasta
    $ grep '>' cpc1_coding_transcripts.fasta | wc -l
    33
    
El archivo fasta generado se convirtio a aminoacidos 

    $ getorf -reverse F -minsize 20 -find 0 cpc1_coding_transcripts.fasta cpc1_coding_protein.fasta
    $ head cpc1_coding_protein.fasta 

>MSTRG.246.1_1 [1 - 75] 
RCTWTPRTGTAPSTSWRPSEQSTRS
>MSTRG.246.1_2 [2 - 115] 
GVLGPQGPEQHRVQAGDLRSSLQEADWKGCRVRIPSAG
>MSTRG.246.1_3 [3 - 122] 
VYLDPKDRNSTEYKLETFGAVYKKLTGKDVVFEFPVQAEA
>MSTRG.246.1_4 [119 - 142] 
GLDTVMCL
>MSTRG.246.1_5 [79 - 153] 
LERMSCSNSQCRLRLRHSDVLVGTP

    $ tail cpc1_coding_protein.fasta 

>MSTRG.17971.1_10 [198 - 221] 
RSSSSNTI
>MSTRG.17971.1_11 [215 - 235] 
HHMIFFQ
>MSTRG.17971.1_12 [225 - 251] 
YFSSKPTLP
>MSTRG.17971.1_13 [172 - 291] 
SKPSACWFEGRVQVTPYDIFPVSPLCLRHRLRKDPLESID
>MSTRG.17971.1_14 [239 - 292] 
AHSALDTGLEKIHLNPSI


Se selecciono el orf mas largo para cada transcrito

    $ infoseq cpc1_coding_protein.fasta | awk '{print $3"\t"$6}' > cpc1_coding_protein_idx
    $ head cpc1_coding_protein_idx 

Name	Length
MSTRG.246.1_1	25
MSTRG.246.1_2	38
MSTRG.246.1_3	40
MSTRG.246.1_4	8
MSTRG.246.1_5	25
MSTRG.246.1_6	7
MSTRG.246.1_7	15
MSTRG.246.1_8	11
MSTRG.246.1_9	31

    $ tail cpc1_coding_protein_idx 

MSTRG.17971.1_5	13
MSTRG.17971.1_6	12
MSTRG.17971.1_7	9
MSTRG.17971.1_8	29
MSTRG.17971.1_9	42
MSTRG.17971.1_10	8
MSTRG.17971.1_11	7
MSTRG.17971.1_12	9
MSTRG.17971.1_13	40
MSTRG.17971.1_14	18


  Se removio manualmente el encabezado del archivo del indice y se edito los ID's

    $ cut -f1 cpc1_coding_protein_idx  | rev | cut -d_ -f1 --complement | rev | sort -V | uniq > coding_list
    $ head coding_list 

Bobra.0002s0054.1.v2.1
Bobra.40_1s0101.1.v2.1
Bobra.43_2s0075.1.v2.1
Bobra.53_1s0026.1.v2.1
Bobra.0075s0002.1.v2.1
Bobra.104_1s0038.1.v2.1
Bobra.113_1s0008.1.v2.1
Bobra.0124s0061.1.v2.1
Bobra.145_2s0094.1.v2.1
Bobra.152_2s0023.1.v2.1

    $ tail coding_list 

MSTRG.4917.1
MSTRG.5380.1
MSTRG.9443.2
MSTRG.10049.3
MSTRG.13181.1
MSTRG.14022.1
MSTRG.14353.1
MSTRG.15161.1
MSTRG.16970.1
MSTRG.17971.1

    $ wc -l coding_list 
    33 coding_list

   Hacemos ciclo *for* para obtener los orf mas largos

    $ for id in $(cat coding_list); do longest_orf=$(grep ${id}_[0-9] cpc1_coding_protein_idx | sort -nrk2 | head -n1 | cut -f1); echo -e "$id\t$longest_orf" ; done > longest_orf_cpc1
    $ head longest_orf_cpc1 

Bobra.0002s0054.1.v2.1	Bobra.0002s0054.1.v2.1_8
Bobra.40_1s0101.1.v2.1	Bobra.40_1s0101.1.v2.1_10
Bobra.43_2s0075.1.v2.1	Bobra.43_2s0075.1.v2.1_6
Bobra.53_1s0026.1.v2.1	Bobra.53_1s0026.1.v2.1_5
Bobra.0075s0002.1.v2.1	Bobra.0075s0002.1.v2.1_9
Bobra.104_1s0038.1.v2.1	Bobra.104_1s0038.1.v2.1_14
Bobra.113_1s0008.1.v2.1	Bobra.113_1s0008.1.v2.1_12
Bobra.0124s0061.1.v2.1	Bobra.0124s0061.1.v2.1_25
Bobra.145_2s0094.1.v2.1	Bobra.145_2s0094.1.v2.1_4
Bobra.152_2s0023.1.v2.1	Bobra.152_2s0023.1.v2.1_8

    $ tail longest_orf_cpc1 

MSTRG.4917.1	MSTRG.4917.1_7
MSTRG.5380.1	MSTRG.5380.1_6
MSTRG.9443.2	MSTRG.9443.2_16
MSTRG.10049.3	MSTRG.10049.3_13
MSTRG.13181.1	MSTRG.13181.1_7
MSTRG.14022.1	MSTRG.14022.1_5
MSTRG.14353.1	MSTRG.14353.1_6
MSTRG.15161.1	MSTRG.15161.1_8
MSTRG.16970.1	MSTRG.16970.1_7
MSTRG.17971.1	MSTRG.17971.1_9


Teniendo el archivo con los ID's de los ORFs mas largos cortamos la columna 2 y generamos una lista de ID's de los ORFs que queremos usar para correr el *blastp*

    $ cut -f2 longest_orf_cpc1  > longest_orf_list_cpc1
    $ wc -l longest_orf_list_cpc1
    33 longest_orf_list_cpc1

    $  head longest_orf_list_cpc1 

Bobra.0002s0054.1.v2.1_8
Bobra.40_1s0101.1.v2.1_10
Bobra.43_2s0075.1.v2.1_6
Bobra.53_1s0026.1.v2.1_5
Bobra.0075s0002.1.v2.1_9
Bobra.104_1s0038.1.v2.1_14
Bobra.113_1s0008.1.v2.1_12
Bobra.0124s0061.1.v2.1_25
Bobra.145_2s0094.1.v2.1_4
Bobra.152_2s0023.1.v2.1_8

    $ tail longest_orf_list_cpc1 

MSTRG.4917.1_7
MSTRG.5380.1_6
MSTRG.9443.2_16
MSTRG.10049.3_13
MSTRG.13181.1_7
MSTRG.14022.1_5
MSTRG.14353.1_6
MSTRG.15161.1_8
MSTRG.16970.1_7
MSTRG.17971.1_9


   Comparamos esta lista contra el archivo fasta de proteinas de los transcritos codificantes de cpc1 con faSomeRecords

    $ faSomeRecords cpc1_coding_protein.fasta longest_orf_list_cpc1 longest_cpc1_orf.fasta
    $ grep -E '>' longest_cpc1_orf.fasta | wc -l
    33 # Cantidad de ID's de nuestro archivo *fasta* de aminoacidos con la secuencia del ORF mas largo de los transcritos codificantes de cpc1

    $ head longest_cpc1_orf.fasta 

>MSTRG.246.1_3 [3 - 122] 
VYLDPKDRNSTEYKLETFGAVYKKLTGKDVVFEFPVQAEA
>Bobra.104_1s0038.1.v2.1_14 [151 - 369] 
YSKCRTPLFFQNIQTYATITVDIGVVDFCLKINLWGFKRVVWRKVNLNHKNPSSIRTVWW
PHDCSLPREQFVR
>Bobra.113_1s0008.1.v2.1_12 [1 - 213] 
MRVCRRPSHETKGPSHVSKGPSHKNKGPSHVSKGPSHENKDPPVRRRDPHIRTRDPPMRR
TDPPMRTTDLP
>Bobra.0124s0061.1.v2.1_25 [491 - 667] 
RTPRSIGRGRLLRALRTPRSIGRGRLLCALRTPRSIGRGRLLSALIRRPSRCPSSIGRG

    $ tail longest_cpc1_orf.fasta
 
>MSTRG.15161.1_8 [109 - 297] 
TIFKGQKIMASFIQWVLDWLRSLFFKKEMELSLIGLQNAGKTSLVNVSGDWSVSRRHDSH
SWF
>Bobra.0075s0002.1.v2.1_9 [1 - 222] 
MVGNAAVVEANEVLDKIYKLATLLDTGLDREEVSIIAALIENGVNPEALALVVKELRQAA
GLENRPAAEHLERQ
>MSTRG.16970.1_7 [20 - 154] 
LQLGFDGSIVFPSADELVEDCGKMKLLDRLLQRLTHAGHKVLIFF
>MSTRG.17971.1_9 [86 - 211] 
IDLELRVKISQMCCELGVDGLWGDIVTNRASQALAGLKVEFK


Ahora corremos en Mazorka el blastp usando el archivo *fasta* generado, para ello el archivo se copia al servidor en la carpeta BBRB/CPC/st_RNAs

#PBS -N blastp_BBRB
#PBS -q default
#PBS -l nodes=1:ppn=8,vmem=18gb,walltime=999:00:00
#PBS -o blastp_cpc1.out
#PBS -e blastp_cpc1.err

module load ncbi-blast+/2.6.0

cd $PBS_O_WORKDIR

blastp -query longest_cpc1_orf.fasta -db /data/secuencias/NR/nr -num_threads 8 -max_target_seqs 1 -outfmt 6 -out blastp_cpc1_coding.outmt6


El archivo "blastp_cpc1_coding.outmt6" se copio a la computadora y se reviso el *e-value* de alineamiento

    $ head blastp_cpc1_coding.outmt6 

MSTRG.246.1_3	XP_005643637.1	89.189	37	4	0	1	37	154	190	9.03e-16	74.7
Bobra.104_1s0038.1.v2.1_14	WP_028427032.1	50.000	28	14	0	34	61	292	319	5.4	34.3
Bobra.113_1s0008.1.v2.1_12	WP_044810544.1	40.323	62	37	0	5	66	658	719	1.64e-04	47.0
Bobra.113_1s0008.1.v2.1_12	WP_044810544.1	37.313	67	42	0	5	71	651	717	5.33e-04	45.8
Bobra.0124s0061.1.v2.1_25	XP_012987296.1	42.373	59	29	1	1	59	134	187	0.083	38.9
Bobra.0124s0061.1.v2.1_25	XP_012987296.1	43.182	44	25	0	1	44	149	192	0.31	37.4
MSTRG.2045.1_6	KNA03602.1	55.224	67	29	1	1	66	34	100	6.50e-16	73.9
MSTRG.3348.1_6	EMT18807.1	60.294	68	27	0	2	69	156	223	9.22e-18	84.7
MSTRG.3497.1_7	KDD77087.1	54.545	33	15	0	16	48	3	35	7.54e-04	45.1
Bobra.152_2s0023.1.v2.1_8	XP_001784927.1	61.905	63	24	0	3	65	8	70	1.03e-20	85.5

    $ tail blastp_cpc1_coding.outmt6 

MSTRG.13181.1_7	XP_002954129.1	52.000	50	23	1	20	68	58	107	1.94e-07	53.1
Bobra.40_1s0101.1.v2.1_10	CAQ03888.1	63.077	65	24	0	1	65	33	97	2.70e-18	80.1
Bobra.43_2s0075.1.v2.1_6	XP_012473036.1	71.429	42	12	0	23	64	334	375	1.58e-12	69.3
MSTRG.14022.1_5	BAQ47392.1	98.413	63	1	0	1	63	23	85	5.43e-38	129
MSTRG.14353.1_6	XP_002488950.1	72.727	55	15	0	1	55	4	58	7.40e-19	79.7
Bobra.53_1s0026.1.v2.1_5	XP_005647731.1	60.274	73	29	0	1	73	1	73	1.32e-27	103
MSTRG.15161.1_8	CEP14728.1	79.487	39	8	0	9	47	1	39	3.03e-14	70.5
Bobra.0075s0002.1.v2.1_9	CEL55536.1	51.471	68	32	1	4	70	7	74	4.26e-14	68.9
MSTRG.16970.1_7	XP_003083957.1	73.171	41	11	0	4	44	454	494	7.38e-13	69.3
MSTRG.17971.1_9	YP_009106568.1	78.947	38	8	0	1	38	268	305	3.32e-11	64.3


Seleccionamos aquellos transcritos con un *e-value* menor a 1e-3

    $ awk 'BEGIN{FS="\t"}{if($11<(1e-3)){print $AF}}' blastp_cpc1_coding.outmt6 > blastp_cpc1_list

Sacamos las lista de ID's unicos y los ordenamos

    $ awk '{print $1}' blastp_cpc1_list | cut -f1 | rev | cut -d_ -f1 --complement | rev | sort -V | uniq > cpc1_lower_evalue_nr
    $ wc -l cpc1_lower_evalue_nr
    28 cpc1_lower_evalue_nr

Por lo tanto son 28 transcritos que el hacen *match* con un *e-value* muy pequeno, de los 33 transcritos que el CPC1 descarta como codificantes. Por lo que probablemente los 5 restantes puede
que si sean no codificantes

Por otra parte se sacaron los 5 transcritos restante comparando la lista de los 33 transcritos que se metio a correr en el blastp y los 28 que resultaron descartados del alineamiento

     $ cut -f1 longest_orf_list_cpc1 | rev | cut -d_ -f1 --complement | rev | sort -V | uniq > longest_cpc1_list
     $ wc -l longest_cpc1_list 
     33 longest_cpc1_list

     $ vimdiff longest_cpc1_list cpc1_lower_evalue_nr
 
Bobra.104_1s0038.1.v2.1
Bobra.0124s0061.1.v2.1                                                                              
Bobra.145_2s0094.1.v2.1                                                                              
Bobra.0169s0076.1.v2.1
Bobra.247_3s0038.1.v2.1 

Estos 5 transcritos se buscaron en el archivo final del *pipeline* de Cabili, para ver si entre los 867 transcritos que resultaron ser lncRNAs, estos se encontraban alli

    $ grep 'Bobra.104_1s0038.1.v2.1' /home/fani/Documents/Tesis_BP/BBRB_analysis/Cabili/guided_ensam/filter_blastp_225.fasta
    $ grep 'Bobra.0124s0061.1.v2.1' /home/fani/Documents/Tesis_BP/BBRB_analysis/Cabili/guided_ensam/filter_blastp_225.fasta
    $ grep 'Bobra.145_2s0094.1.v2.1' /home/fani/Documents/Tesis_BP/BBRB_analysis/Cabili/guided_ensam/filter_blastp_225.fasta
    $ grep 'Bobra.0169s0076.1.v2.1' /home/fani/Documents/Tesis_BP/BBRB_analysis/Cabili/guided_ensam/filter_blastp_225.fasta
    $ grep 'Bobra.247_3s0038.1.v2.1' /home/fani/Documents/Tesis_BP/BBRB_analysis/Cabili/guided_ensam/filter_blastp_225.fasta

Ninguno esta en el archivo final!
 

#### Lista de lncRNAs finales: match entre resultados de Cabili y CPC1

Todo se trabajo en una nueva carpeta llamada "cpc_cabili_match"

Se extrajo la lista de transcritos del del archivo filter_blastp_225.fasta

    $ grep '>' /home/fani/Documents/Tesis_BP/BBRB_analysis/Cabili/guided_ensam/filter_blastp_225.fasta | sed 's/>//g' | sort -V | uniq > cabili_lncRNAs_list
    $ wc -l cabili_lncRNAs_list 
    867 cabili_lncRNAs_list

Tambien la lista de transcritos de result_in_table de CPC1

    $ awk '{print $1}' /home/fani/Documents/Tesis_BP/BBRB_analysis/CPC/guided/cpc1/BBRB_lncrnas_st | sort -V | uniq > cpc1_lncRNAs_list
    $ wc -l cpc1_lncRNAs_list 
    2391 cpc1_lncRNAs_list

Se hizo un match entre ambas listas para determinar cuales son los lncRNAs que se comparten entre ambas, y que al final fueron identificados por ambos metodos (Cabili y CPC1)

    $ vimdiff cabili_lncRNAs_list cpc1_lncRNAs_list
    $ grep -wFf cabili_lncRNAs_list cpc1_lncRNAs_list > lncRNAs_match_st
    $ wc -l lncRNAs_match_st 
    867 lncRNAs_match_st


#### Genes codificantes de BBRB

Se reviso en los archivos de anotacion del genoma v2 de BBRB el archivo fasta de proteinas

    $ grep '>' Bbraunii_502_v2.1.protein.fa | wc -l
    23685

Asi tambien se reviso el archivo de anotacion funcional de la microalga

    $ wc -l Bbraunii_502_v2.1.annotation_info.txt 
    23686 Bbraunii_502_v2.1.annotation_info.txt  # Menos 1 por el encabezado

NOTA: Se encontro un total de 23 685 genes que codifican para proteinas en BBRB



#### *Miercoles 26 de febrero de 2020*


#### Clasificacion de lncRNAs segun sus coordenadas genomicas 


la lista de los 876 lncRNAs que hicieron match entre los lncRNAs identificados con el *pipeline* de Cabili y el metodo de CPC1, se uso para obtener un archivo bed12 unicamente con estos transcritos
y clasificarlos con bedtools por sus coordenadas genomicas. La clasificacion se realizo en la carpeta cpc_cabili_match/guided/

Se comparo primeramente la lista de lncRNAs del match con el archivo bed12 del transcriptoma de BBRB y asi obtuvimos el archivo bed12 de los lncRNAs

    $ grep -wFf lncRNAs_match_st /home/fani/Documents/Tesis_BP/BBRB_analysis/ensam/guided/BBRB_v2_transcriptome.annotated.bed12 > lncRNAs_guided_transcriptome.bed12
    $ wc -l lncRNAs_guided_transcriptome.bed12 
    867 lncRNAs_guided_transcriptome.bed12
 
    $ head lncRNAs_guided_transcriptome.bed12 

scaffold_1	266692	267578	MSTRG.9.1	0	+	267578	267578	0	2	366,38,	0,848,
scaffold_10	317636	318008	MSTRG.59.1	0	+	318008	318008	0	2	150,125,	0,247,
scaffold_100	427342	427669	Bobra.0100s0049.1.v2.1	0	-	427669	427669	0	1	327,	0,
scaffold_100	701451	701920	Bobra.0100s0081.1.v2.1	0	-	701920	701920	0	1	469,	0,
scaffold_101_2	518368	518789	MSTRG.242.1	0	+	518789	518789	0	2	150,100,	0,321,
scaffold_101_2	877132	881046	MSTRG.276.1	0	+	881046	881046	0	2	197,166,	0,3748,
scaffold_101_2	34220	34659	Bobra.101_2s0009.1.v2.1	0	-	34659	34659	0	1	439,	0,
scaffold_101_2	987254	988132	Bobra.101_2s0116.1.v2.1	0	-	988132	988132	0	1	878,	0,
scaffold_102	99210	99432	Bobra.0102s0013.1.v2.1	0	+	99432	99432	0	1	222,	0,
scaffold_102	233046	233392	Bobra.0102s0030.1.v2.1	0	+	233392	233392	0	2	174,46,	0,300,

    $  tail lncRNAs_guided_transcriptome.bed12 

scaffold_98	258079	258475	MSTRG.17950.1	0	+	258475	258475	0	2	149,150,	0,246,
scaffold_98	213195	213618	Bobra.0098s0029.1.v2.1	0	-	213618	213618	0	1	423,	0,
scaffold_98	299657	299982	MSTRG.17954.1	0	.	299982	299982	0	1	325,	0,
scaffold_98	321189	321462	MSTRG.17955.1	0	.	321462	321462	0	1	273,	0,
scaffold_99	30222	30440	MSTRG.17958.1	0	.	30440	30440	0	1	218,	0,
scaffold_9_1	130162	130483	Bobra.9_1s0011.1.v2.1	0	-	130483	130483	0	1	321,	0,
scaffold_9_1	144179	147851	Bobra.9_1s0012.1.v2.1	0	-	147851	147851	0	4	224,54,50,38,	0,939,1683,3634,
scaffold_9_1	131930	132152	MSTRG.17978.1	0	.	132152	132152	0	1	222,	0,
scaffold_9_2	28191	34002	Bobra.9_2s0005.1.v2.1	0	-	34002	34002	0	2	282,528,	0,5283,
scaffold_9_2	998769	999033	Bobra.9_2s0121.1.v2.1	0	-	999033	999033	0	1	264,	0,

Para obtener el archivo bed12 de los genes que codifican para proteinas en BBRB se tomo los ID's de todos aquellos genes del archivo "Bbraunii_502_v2.1.protein.fa" y se extrajo las coordenadas de
estos del archivo de anotacion de referencia de la microalga (Bbraunii_502_v2.1.gene.anot.gtf)

    $ grep 'CDS' Bbraunii_502_v2.1.gene.anot.gtf > BBRB_CDS_ref_anot.gtf
    $ head BBRB_CDS_ref_anot.gtf

scaffold_1	phytozomev12	CDS	581	1252	.	+	0	transcript_id "Bobra.0001s0001.1.v2.1"; gene_id "Bobra.0001s0001.v2.1";
scaffold_1	phytozomev12	CDS	44754	44791	.	-	2	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "Bobra.0001s0002.v2.1";
scaffold_1	phytozomev12	CDS	45095	45304	.	-	2	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "Bobra.0001s0002.v2.1";
scaffold_1	phytozomev12	CDS	45727	46044	.	-	2	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "Bobra.0001s0002.v2.1";
scaffold_1	phytozomev12	CDS	46232	46395	.	-	1	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "Bobra.0001s0002.v2.1";
scaffold_1	phytozomev12	CDS	46996	47205	.	-	1	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "Bobra.0001s0002.v2.1";
scaffold_1	phytozomev12	CDS	47585	47727	.	-	0	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "Bobra.0001s0002.v2.1";
scaffold_1	phytozomev12	CDS	47937	48255	.	-	1	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "Bobra.0001s0002.v2.1";
scaffold_1	phytozomev12	CDS	48346	48512	.	-	0	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "Bobra.0001s0002.v2.1";
scaffold_1	phytozomev12	CDS	48870	48950	.	+	0	transcript_id "Bobra.0001s0003.1.v2.1"; gene_id "Bobra.0001s0003.v2.1";

    $ tail BBRB_CDS_ref_anot.gtf 

scaffold_9_2	phytozomev12	CDS	1007846	1007995	.	-	0	transcript_id "Bobra.9_2s0122.1.v2.1"; gene_id "Bobra.9_2s0122.v2.1";
scaffold_9_2	phytozomev12	CDS	1008667	1008811	.	-	1	transcript_id "Bobra.9_2s0122.1.v2.1"; gene_id "Bobra.9_2s0122.v2.1";
scaffold_9_2	phytozomev12	CDS	1008936	1009075	.	-	0	transcript_id "Bobra.9_2s0122.1.v2.1"; gene_id "Bobra.9_2s0122.v2.1";
scaffold_9_2	phytozomev12	CDS	1013367	1014589	.	+	0	transcript_id "Bobra.9_2s0123.1.v2.1"; gene_id "Bobra.9_2s0123.v2.1";
scaffold_9_2	phytozomev12	CDS	1014939	1015381	.	+	1	transcript_id "Bobra.9_2s0123.1.v2.1"; gene_id "Bobra.9_2s0123.v2.1";
scaffold_9_2	phytozomev12	CDS	1015779	1016014	.	+	2	transcript_id "Bobra.9_2s0123.1.v2.1"; gene_id "Bobra.9_2s0123.v2.1";
scaffold_9_2	phytozomev12	CDS	1016976	1017182	.	+	0	transcript_id "Bobra.9_2s0123.1.v2.1"; gene_id "Bobra.9_2s0123.v2.1";
scaffold_9_2	phytozomev12	CDS	1017767	1017901	.	+	0	transcript_id "Bobra.9_2s0123.1.v2.1"; gene_id "Bobra.9_2s0123.v2.1";
scaffold_9_2	phytozomev12	CDS	1018480	1018599	.	+	0	transcript_id "Bobra.9_2s0123.1.v2.1"; gene_id "Bobra.9_2s0123.v2.1";
scaffold_9_2	phytozomev12	CDS	1019372	1019434	.	+	0	transcript_id "Bobra.9_2s0123.1.v2.1"; gene_id "Bobra.9_2s0123.v2.1";

    $ wc -l BBRB_CDS_ref_anot.gtf 
    126319 BBRB_CDS_ref_anot.gtf


El archivo "CDS_list_BBRB.gtf" se convirtio a formato bed12 

   Dentro de la carpeta de la anotacion del genoma v2 de BBRB se convirtio el archivo y luego se copio a la carpeta cpc_cabili_match/guided/

    $ /home/fani/gtfToGenePred BBRB_CDS_ref_anot.gtf BBRB_CDS_ref_anot.genePred
    $ /home/fani/genePredToBed BBRB_CDS_ref_anot.genePred BBRB_CDS_ref_anot.bed12
    $ cp BBRB_CDS_ref_anot.bed12 ../../cpc_cabili_match/guided/

    $ head BBRB_CDS_ref_anot.bed12
 
scaffold_1	580	1252	Bobra.0001s0001.1.v2.1	0	+	580	1252	0	1	672,	0,
scaffold_1	44753	48512	Bobra.0001s0002.1.v2.1	0	-	44753	48512	0	8	38,210,318,164,210,143,319,167,	0,341,973,1478,2242,2831,3183,3592,
scaffold_1	48869	50226	Bobra.0001s0003.1.v2.1	0	+	48869	50226	0	5	81,71,172,180,84,	0,165,413,1000,1273,
scaffold_1	50537	51415	Bobra.0001s0004.1.v2.1	0	-	50537	51415	0	5	80,79,120,120,165,	0,175,322,520,713,
scaffold_1	61441	63131	Bobra.0001s0005.1.v2.1	0	-	61441	63131	0	5	198,204,127,119,108,	0,559,819,1258,1582,
scaffold_1	77591	77774	Bobra.0001s0006.1.v2.1	0	+	77591	77774	0	1	183,	0,
scaffold_1	81430	81820	Bobra.0001s0007.1.v2.1	0	+	81430	81820	0	1	390,	0,
scaffold_1	88336	89785	Bobra.0001s0008.1.v2.1	0	+	88336	89785	0	2	547,281,	0,1168,
scaffold_1	96897	97431	Bobra.0001s0009.1.v2.1	0	+	96897	97431	0	1	534,	0,
scaffold_1	115574	115919	Bobra.0001s0010.1.v2.1	0	-	115574	115919	0	1	345,	0,

    $  tail BBRB_CDS_ref_anot.bed12 

scaffold_9_2	931703	937072	Bobra.9_2s0116.4.v2.1	0	-	931703	937072	0	7	245,109,114,137,142,215,376,	0,553,1071,1847,2493,3904,4993,
scaffold_9_2	931703	938864	Bobra.9_2s0116.2.v2.1	0	-	931703	938864	0	9	245,109,114,137,142,215,333,199,96,	0,553,1071,1847,2493,3904,4993,6417,7065,
scaffold_9_2	943093	949586	Bobra.9_2s0117.1.v2.1	0	-	943093	949586	0	9	141,168,138,192,177,203,87,143,62,	0,1230,1824,2762,3456,4182,5241,6080,6431,
scaffold_9_2	950829	951165	Bobra.9_2s0118.1.v2.1	0	+	950829	951165	0	1	336,	0,
scaffold_9_2	973178	974059	Bobra.9_2s0119.2.v2.1	0	+	973178	974059	0	4	185,161,55,91,	0,303,651,790,
scaffold_9_2	973178	975335	Bobra.9_2s0119.1.v2.1	0	+	973178	975335	0	5	185,161,55,216,37,	0,303,651,1347,2120,
scaffold_9_2	982231	985240	Bobra.9_2s0120.1.v2.1	0	-	982231	985240	0	4	81,132,96,234,	0,1197,2522,2775,
scaffold_9_2	998769	999033	Bobra.9_2s0121.1.v2.1	0	-	998769	999033	0	1	264,	0,
scaffold_9_2	999636	1009075	Bobra.9_2s0122.1.v2.1	0	-	999636	1009075	0	10	337,284,165,98,281,119,141,150,145,140,	0,969,2195,3362,4542,6156,7136,8209,9030,9299,
scaffold_9_2	1013366	1019434	Bobra.9_2s0123.1.v2.1	0	+	1013366	1019434	0	7	1223,443,236,207,135,120,63,	0,1572,2412,3609,4400,5113,6005,

    $ wc -l BBRB_CDS_ref_anot.bed12 
    23685 BBRB_CDS_ref_anot.bed12    # Cantidad de genes codificantes en BBRB

Una vez teniendo el archivo *bed12* de los lncRNAs identificados en el ensamble guiado de BBRB y el archivo *bed12* de los genes codificantes de la microalga, se procedio a clasificar los lncRNAs por
coordenadas genomicas usando *bedtools intersect*


#### Interseccion de coordenadas genomicas con bedtools


Se clasificaron lncRNAs cuyas coordenadas genomicas intersectaran con regiones de genes codificantes en el genoma de la microalga, a partir del archivo de coordenadas de loe genes codificantes de BBRB
 
    $ bedtools intersect -a lncRNAs_guided_transcriptome.bed12 -b BBRB_CDS_ref_anot.bed12 > overlap_lncRNAs_guided.bed12
    $ wc -l overlap_lncRNAs_guided.bed12 
    635 overlap_lncRNAs_guided.bed12

    $  head overlap_lncRNAs_guided.bed12 

scaffold_100	427342	427669	Bobra.0100s0049.1.v2.1	0	-	427669	427669	0	1	327,	0,
scaffold_100	701609	701858	Bobra.0100s0081.1.v2.1	0	-	701920	701920	0	1	469,	0,
scaffold_101_2	518755	518789	MSTRG.242.1	0	+	518789	518789	0	2	150,100,	0,321,
scaffold_101_2	877132	881046	MSTRG.276.1	0	+	881046	881046	0	2	197,166,	0,3748,
scaffold_101_2	34353	34617	Bobra.101_2s0009.1.v2.1	0	-	34659	34659	0	1	439,	0,
scaffold_101_2	987487	987649	Bobra.101_2s0116.1.v2.1	0	-	988132	988132	0	1	878,	0,
scaffold_102	99253	99427	Bobra.0102s0013.1.v2.1	0	+	99432	99432	0	1	222,	0,
scaffold_102	233092	233362	Bobra.0102s0030.1.v2.1	0	+	233392	233392	0	2	174,46,	0,300,
scaffold_103	38204	38711	MSTRG.370.1	0	+	39436	39436	0	2	150,72,	0,1160,
scaffold_103	38836	39051	MSTRG.370.1	0	+	39436	39436	0	2	150,72,	0,1160,

    $ tail overlap_lncRNAs_guided.bed12 

scaffold_93	182320	182626	MSTRG.17782.1	0	+	187316	187316	0	2	317,38,	0,7415,
scaffold_93	224468	224672	Bobra.0093s0023.1.v2.1	0	-	224672	224672	0	1	204,	0,
scaffold_93	414787	416037	MSTRG.17810.1	0	-	416047	416047	0	4	136,81,142,146,	0,255,1201,1419,
scaffold_93	414841	416037	MSTRG.17810.1	0	-	416047	416047	0	4	136,81,142,146,	0,255,1201,1419,
scaffold_98	258427	258475	MSTRG.17950.1	0	+	258475	258475	0	2	149,150,	0,246,
scaffold_98	213195	213618	Bobra.0098s0029.1.v2.1	0	-	213618	213618	0	1	423,	0,
scaffold_9_1	130162	130483	Bobra.9_1s0011.1.v2.1	0	-	130483	130483	0	1	321,	0,
scaffold_9_1	144179	147851	Bobra.9_1s0012.1.v2.1	0	-	147851	147851	0	4	224,54,50,38,	0,939,1683,3634,
scaffold_9_2	28212	33534	Bobra.9_2s0005.1.v2.1	0	-	34002	34002	0	2	282,528,	0,5283,
scaffold_9_2	998769	999033	Bobra.9_2s0121.1.v2.1	0	-	999033	999033	0	1	264,	0,


Se clasificaron lncRNAs cuya transcripcion fuera en sentido, usando el parametro -s 

    $ bedtools intersect -a lncRNAs_guided_transcriptome.bed12 -b BBRB_CDS_ref_anot.bed12 -f 0.1 -wa -s > sense_lncRNAs_guided.bed12
    $ wc -l sense_lncRNAs_guided.bed12 
    412 sense_lncRNAs_guided.bed12

    $ head sense_lncRNAs_guided.bed12 

scaffold_100	427342	427669	Bobra.0100s0049.1.v2.1	0	-	427669	427669	0	1	327,	0,
scaffold_100	701451	701920	Bobra.0100s0081.1.v2.1	0	-	701920	701920	0	1	469,	0,
scaffold_101_2	34220	34659	Bobra.101_2s0009.1.v2.1	0	-	34659	34659	0	1	439,	0,
scaffold_101_2	987254	988132	Bobra.101_2s0116.1.v2.1	0	-	988132	988132	0	1	878,	0,
scaffold_102	99210	99432	Bobra.0102s0013.1.v2.1	0	+	99432	99432	0	1	222,	0,
scaffold_102	233046	233392	Bobra.0102s0030.1.v2.1	0	+	233392	233392	0	2	174,46,	0,300,
scaffold_103	38204	39436	MSTRG.370.1	0	+	39436	39436	0	2	150,72,	0,1160,
scaffold_103	38798	39116	Bobra.0103s0006.1.v2.1	0	+	39116	39116	0	2	115,171,	0,147,
scaffold_103	81839	82358	Bobra.0103s0011.3.v2.1	0	-	82358	82358	0	3	79,70,71,	0,295,448,
scaffold_103	81839	82358	Bobra.0103s0011.3.v2.1	0	-	82358	82358	0	3	79,70,71,	0,295,448,

    $ tail sense_lncRNAs_guided.bed12 

scaffold_91	546526	547169	Bobra.0091s0061.1.v2.1	0	-	547169	547169	0	1	643,	0,
scaffold_92_2	174584	175195	Bobra.92_2s0020.1.v2.1	0	+	175195	175195	0	1	611,	0,
scaffold_93	224468	224672	Bobra.0093s0023.1.v2.1	0	-	224672	224672	0	1	204,	0,
scaffold_93	414482	416047	MSTRG.17810.1	0	-	416047	416047	0	4	136,81,142,146,	0,255,1201,1419,
scaffold_93	414482	416047	MSTRG.17810.1	0	-	416047	416047	0	4	136,81,142,146,	0,255,1201,1419,
scaffold_98	213195	213618	Bobra.0098s0029.1.v2.1	0	-	213618	213618	0	1	423,	0,
scaffold_9_1	130162	130483	Bobra.9_1s0011.1.v2.1	0	-	130483	130483	0	1	321,	0,
scaffold_9_1	144179	147851	Bobra.9_1s0012.1.v2.1	0	-	147851	147851	0	4	224,54,50,38,	0,939,1683,3634,
scaffold_9_2	28191	34002	Bobra.9_2s0005.1.v2.1	0	-	34002	34002	0	2	282,528,	0,5283,
scaffold_9_2	998769	999033	Bobra.9_2s0121.1.v2.1	0	-	999033	999033	0	1	264,	0,


Se clasificaron lncRNAs cuya transcripcion fuera antisentido, usando el parametro -S

    $ bedtools intersect -a lncRNAs_guided_transcriptome.bed12 -b BBRB_CDS_ref_anot.bed12 -f 0.1 -wa -S > antisense_lncRNAs_guided.bed12
    $ wc -l antisense_lncRNAs_guided.bed12 
    85 antisense_lncRNAs_guided.bed12

    $ head antisense_lncRNAs_guided.bed12 

scaffold_101_2	877132	881046	MSTRG.276.1	0	+	881046	881046	0	2	197,166,	0,3748,
scaffold_103	38204	39436	MSTRG.370.1	0	+	39436	39436	0	2	150,72,	0,1160,
scaffold_109	8891	21921	MSTRG.734.1	0	-	21921	21921	0	2	86,595,	0,12435,
scaffold_114_2	588310	593330	MSTRG.1156.1	0	-	593330	593330	0	2	86,163,	0,4857,
scaffold_114_2	588310	593330	MSTRG.1156.1	0	-	593330	593330	0	2	86,163,	0,4857,
scaffold_114_2	590126	595699	MSTRG.1157.3	0	-	595699	595699	0	2	326,365,	0,5208,
scaffold_127	680995	686543	MSTRG.1802.1	0	+	686543	686543	0	2	85,209,	0,5339,
scaffold_127	681288	686543	MSTRG.1802.2	0	+	686543	686543	0	2	150,209,	0,5046,
scaffold_127	146595	149699	MSTRG.1744.2	0	-	149699	149699	0	2	330,147,	0,2957,
scaffold_132	682165	682692	MSTRG.1982.1	0	+	682692	682692	0	2	92,150,	0,377,

    $ tail antisense_lncRNAs_guided.bed12 

scaffold_661	995	9343	MSTRG.15994.1	0	+	9343	9343	0	2	132,467,	0,7881,
scaffold_661	3007	10978	MSTRG.15994.3	0	+	10978	10978	0	2	108,145,	0,7826,
scaffold_74	44520	53040	MSTRG.16628.2	0	+	53040	53040	0	3	27,316,61,	0,121,8459,
scaffold_74	59841	70790	MSTRG.16628.9	0	+	70790	70790	0	2	1210,148,	0,10801,
scaffold_74	59841	78067	MSTRG.16628.8	0	+	78067	78067	0	3	765,109,146,	0,5494,18080,
scaffold_74	54428	70027	MSTRG.16635.2	0	+	70027	70027	0	2	103,149,	0,15450,
scaffold_74	69231	79174	MSTRG.16636.1	0	+	79174	79174	0	2	85,125,	0,9818,
scaffold_77	1041118	1041337	MSTRG.16857.1	0	+	1041337	1041337	0	1	219,	0,
scaffold_780	372	1493	MSTRG.16954.1	0	-	1493	1493	0	3	95,84,150,	0,546,971,
scaffold_98	258079	258475	MSTRG.17950.1	0	+	258475	258475	0	2	149,150,	0,246,


    NOTA: Los lncRNAs sentido y antisentido son aquellos donde el 10% sobrelapa en sentido positivo de 5' a 3' (en sentido) o negativo de 3' a 5' (antisentido) de la region codificante del ADN


Tambien se obtuvieron aquellos lncRNAs que sobrelapan sobre regiones intergenicas con la opcion -v

    $ bedtools intersect -a lncRNAs_guided_transcriptome.bed12 -b BBRB_CDS_ref_anot.bed12 -v > intergenic_lncRNAs_guided.bed12
    $ wc -l lincRNAs_guided.bed12 
    345 lincRNAs_guided.bed12

    $ head lincRNAs_guided.bed12 

scaffold_1	266692	267578	MSTRG.9.1	0	+	267578	267578	0	2	366,38,	0,848,
scaffold_10	317636	318008	MSTRG.59.1	0	+	318008	318008	0	2	150,125,	0,247,
scaffold_102	280980	290008	MSTRG.337.1	0	+	290008	290008	0	2	533,22,	0,9006,
scaffold_102	395076	395795	MSTRG.348.1	0	+	395795	395795	0	2	177,249,	0,470,
scaffold_102	276247	277760	MSTRG.336.1	0	-	277760	277760	0	3	91,86,52,	0,771,1461,
scaffold_102	368128	368573	MSTRG.346.1	0	-	368573	368573	0	2	150,150,	0,295,
scaffold_105_2	202241	202913	MSTRG.481.1	0	.	202913	202913	0	1	672,	0,
scaffold_106_1	60274	60777	MSTRG.508.1	0	-	60777	60777	0	2	164,182,	0,321,
scaffold_106_1	60276	60756	MSTRG.508.3	0	-	60756	60756	0	2	162,150,	0,330,
scaffold_106_1	568176	568477	MSTRG.564.1	0	.	568477	568477	0	1	301,	0,

    $ tail lincRNAs_guided.bed12 

scaffold_89	90517	91002	MSTRG.17599.1	0	+	91002	91002	0	1	485,	0,
scaffold_91	606714	607346	MSTRG.17732.1	0	+	607346	607346	0	2	250,68,	0,564,
scaffold_91	606714	608094	MSTRG.17732.2	0	+	608094	608094	0	2	250,563,	0,817,
scaffold_91	29610	30354	MSTRG.17677.1	0	.	30354	30354	0	1	744,	0,
scaffold_93	46353	47008	MSTRG.17771.1	0	.	47008	47008	0	1	655,	0,
scaffold_97_1	75443	75761	MSTRG.17895.1	0	+	75761	75761	0	2	133,100,	0,218,
scaffold_98	299657	299982	MSTRG.17954.1	0	.	299982	299982	0	1	325,	0,
scaffold_98	321189	321462	MSTRG.17955.1	0	.	321462	321462	0	1	273,	0,
scaffold_99	30222	30440	MSTRG.17958.1	0	.	30440	30440	0	1	218,	0,
scaffold_9_1	131930	132152	MSTRG.17978.1	0	.	132152	132152	0	1	222,	0,

Teniendo la lista de estos genes no codificantes se clasificaron en dos tipos de lncRNAs

1) lncRNAs intergenicos proximales: Aquellos que intersectan dentro de una ventana de 500pb en el extremo 3' o 5' de la region codificante

    $ bedtools window -a lincRNAs_guided.bed12 -b BBRB_CDS_ref_anot.bed12 -w 500 -v > w500_lincRNAs_guided.bed12
    $ wc -l w500_lincRNAs_guided.bed12 
    273 w500_lincRNAs_guided.bed12

    $ head w500_lincRNAs_guided.bed12 

scaffold_1	266692	267578	MSTRG.9.1	0	+	267578	267578	0	2	366,38,	0,848,
scaffold_102	280980	290008	MSTRG.337.1	0	+	290008	290008	0	2	533,22,	0,9006,
scaffold_102	395076	395795	MSTRG.348.1	0	+	395795	395795	0	2	177,249,	0,470,
scaffold_102	276247	277760	MSTRG.336.1	0	-	277760	277760	0	3	91,86,52,	0,771,1461,
scaffold_102	368128	368573	MSTRG.346.1	0	-	368573	368573	0	2	150,150,	0,295,
scaffold_107	451905	452110	MSTRG.639.1	0	.	452110	452110	0	1	205,	0,
scaffold_108	661860	662245	MSTRG.709.1	0	.	662245	662245	0	1	385,	0,
scaffold_109	7706	8827	MSTRG.733.1	0	+	8827	8827	0	2	231,140,	0,981,
scaffold_109	7772	8827	MSTRG.733.4	0	+	8827	8827	0	2	61,140,	0,915,
scaffold_109	139219	139641	MSTRG.745.1	0	.	139641	139641	0	1	422,	0,

    $ tail w500_lincRNAs_guided.bed12 

scaffold_88	53649	53971	MSTRG.17575.1	0	.	53971	53971	0	1	322,	0,
scaffold_89	90517	91002	MSTRG.17599.1	0	+	91002	91002	0	1	485,	0,
scaffold_91	606714	607346	MSTRG.17732.1	0	+	607346	607346	0	2	250,68,	0,564,
scaffold_91	606714	608094	MSTRG.17732.2	0	+	608094	608094	0	2	250,563,	0,817,
scaffold_93	46353	47008	MSTRG.17771.1	0	.	47008	47008	0	1	655,	0,
scaffold_97_1	75443	75761	MSTRG.17895.1	0	+	75761	75761	0	2	133,100,	0,218,
scaffold_98	299657	299982	MSTRG.17954.1	0	.	299982	299982	0	1	325,	0,
scaffold_98	321189	321462	MSTRG.17955.1	0	.	321462	321462	0	1	273,	0,
scaffold_99	30222	30440	MSTRG.17958.1	0	.	30440	30440	0	1	218,	0,
scaffold_9_1	131930	132152	MSTRG.17978.1	0	.	132152	132152	0	1	222,	0,


2) lncRNAs intergenicos: Aquellos que no sobrelapan en la region comprendida por 500 pb en el extremo 3' o el 5' de la region codificante

    $ bedtools window -a lincRNAs_guided.bed12 -b BBRB_CDS_ref_anot.bed12 -w 500 > near500_lincRNAs_guided.bed12
    $ wc -l near500_lincRNAs_guided.bed12 
    82 near500_lincRNAs_guided.bed12

    $ head near500_lincRNAs_guided.bed12 

scaffold_10	317636	318008	MSTRG.59.1	0	+	318008	318008	0	2	150,125,	0,247,	scaffold_10	312509	317470	Bobra.0010s0039.1.v2.1	0	-	312509	317470	0	6	96,132,110,128,83,255,	0,1177,1940,3957,4389,4706,
scaffold_105_2	202241	202913	MSTRG.481.1	0	.	202913	202913	0	1	672,	0,	scaffold_105_2	199568	201974	Bobra.105_2s0023.1.v2.1	0	+	199568	201974	0	3383,92,140,	0,801,2266,
scaffold_106_1	60274	60777	MSTRG.508.1	0	-	60777	60777	0	2	164,182,	0,321,	scaffold_106_1	51748	60197	Bobra.106_1s0006.1.v2.1	0	+	51748	601970	8	120,130,94,37,44,59,126,155,	0,328,670,1497,2499,6552,7556,8294,
scaffold_106_1	60276	60756	MSTRG.508.3	0	-	60756	60756	0	2	162,150,	0,330,	scaffold_106_1	51748	60197	Bobra.106_1s0006.1.v2.1	0	+	51748	601970	8	120,130,94,37,44,59,126,155,	0,328,670,1497,2499,6552,7556,8294,
scaffold_106_1	568176	568477	MSTRG.564.1	0	.	568477	568477	0	1	301,	0,	scaffold_106_1	567667	567889	Bobra.106_1s0070.1.v2.1	0	-	567667	567889	0	1222,	0,
scaffold_109	277193	280717	MSTRG.764.1	0	-	280717	280717	0	2	148,74,	0,3450,	scaffold_109	275411	276917	Bobra.0109s0027.1.v2.1	0	+	275411	276917	0	11506,	0,
scaffold_111	244347	244555	MSTRG.948.1	0	.	244555	244555	0	1	208,	0,	scaffold_111	233235	243978	Bobra.0111s0036.1.v2.1	0	-	233235	243978	0	12	192,204,60,143,136,155,132,205,151,114,56,189,	0,859,2506,3852,4275,4684,5906,7229,7671,9227,10074,10554,
scaffold_113_1	222796	223095	MSTRG.989.1	0	-	223095	223095	0	1	299,	0,	scaffold_113_1	223293	224586	Bobra.113_1s0024.1.v2.1	0	-	223293	224586	0	2242,13,	0,1280,
scaffold_12	694462	694765	MSTRG.1461.1	0	+	694765	694765	0	2	101,137,	0,166,	scaffold_12	695010	703496	Bobra.0012s0091.1.v2.1	0	-	695010	703496	0	10	284,933,247,293,171,117,106,111,1196,178,	0,820,2213,2906,4079,4632,5556,6282,6984,8308,
scaffold_138	367521	367847	MSTRG.2387.1	0	-	367847	367847	0	1	326,	0,	scaffold_138	368036	368360	Bobra.0138s0046.1.v2.1	0	+	368036	368360	0	1324,	0,

    $ tail near500_lincRNAs_guided.bed12 

scaffold_60_2	632121	632341	MSTRG.15503.1	0	+	632341	632341	0	1	220,	0,	scaffold_60_2	632383	632629	Bobra.60_2s0061.1.v2.1	0	+	632383	632629	0	1246,	0,
scaffold_64	122799	123523	MSTRG.15833.1	0	+	123523	123523	0	3	204,49,52,	0,319,672,	scaffold_64	123854	124172	Bobra.0064s0016.1.v2.1	0	+	123854	124172	0	1	318,	0,
scaffold_70	194769	195134	MSTRG.16298.1	0	+	195134	195134	0	1	365,	0,	scaffold_70	193561	194752	Bobra.0070s0029.1.v2.1	0	+	193561	194752	0	2222,342,	0,849,
scaffold_7_2	16721	17184	MSTRG.17063.1	0	.	17184	17184	0	1	463,	0,	scaffold_7_2	17667	22553	Bobra.7_2s0004.1.v2.1	0	-	17667	22553	0	6220,110,128,115,229,44,	0,628,1900,3801,4277,4842,
scaffold_7_2	827700	828065	MSTRG.17137.1	0	.	828065	828065	0	1	365,	0,	scaffold_7_2	826961	827615	Bobra.7_2s0093.1.v2.1	0	-	826961	827615	0	1654,	0,
scaffold_82	727082	727448	MSTRG.17324.1	0	-	727448	727448	0	2	146,184,	0,182,	scaffold_82	725549	726948	Bobra.0082s0083.1.v2.1	0	+	725549	726948	0	3	73,227,240,	0,229,1159,
scaffold_864	1330	1722	MSTRG.17502.1	0	.	1722	1722	0	1	392,	0,	scaffold_864	42	933	Bobra.0864s0001.1.v2.1	0	-	42	933	0	2113,247,	0,644,
scaffold_87	146991	147284	MSTRG.17533.1	0	+	147284	147284	0	1	293,	0,	scaffold_87	146693	146987	Bobra.0087s0019.1.v2.1	0	-	146693	146987	0	1294,	0,
scaffold_87	267391	267855	MSTRG.17551.1	0	-	267855	267855	0	2	113,111,	0,353,	scaffold_87	263271	267295	Bobra.0087s0036.1.v2.1	0	-	263271	267295	0	4	283,181,153,13,	0,1112,2850,4011,
scaffold_91	29610	30354	MSTRG.17677.1	0	.	30354	30354	0	1	744,	0,	scaffold_91	30626	30974	Bobra.0091s0005.1.v2.1	0	-	30626	30974	0	2170,94,	0,254,


Por ultimo se clasificaron lncRNAs que sobrelapan en regiones de intrones del genoma de la microalga. En este caso debido a que se necesitaba el archivo de coordenadas de los intrones de BBRB
se procedio a usar un *script* de *perl* (https://github.com/riverlee/IntronGTF/blob/master/get_intron_gtf_from_bed.pl) para obtener el tamano de los intrones a partir del archivo bed12 de anotacion de BBRB

Primero se obtuvo el archivo bed12 de anotacion de referencia del genoma v2 de BBRB, los comandos se ejecutaron en la carpeta BBRB_analysis/genome_data_v2/annotation

    $ /home/fani/gtfToGenePred Bbraunii_502_v2.1.gene.anot.gtf Bbraunii_502_v2.1.gene.anot.genePred
    $ /home/fani/genePredToBed Bbraunii_502_v2.1.gene.anot.genePred Bbraunii_502_v2.1.gene.anot.bed12
    $ cp Bbraunii_502_v2.1.gene.anot.bed12 ../../cpc_cabili_match/guided/
 
El *script* se ejecuto de la siguiente manera:
 
    $ perl get_intron_gtf_from_bed.pl Bbraunii_502_v2.1.gene.anot.bed12 intron_coordinates_BBRB_v2.gtf
    
    NOTA: Algunos genes fueron excluidos debido a que no presentan intrones por tener solo un exon, solamente se calculo las coordenadas de intrones de aquellos genes con multiples exones

    $ head intron_coordinates_BBRB_v2.gtf 

scaffold_1	intronGTF	exon	44792	45094	0	-	0	gene_id "Bobra.0001s0002.1.v2.1"; transcript_id "Bobra.0001s0002.1.v2.1"; exon_number 7
scaffold_1	intronGTF	exon	45305	45726	0	-	0	gene_id "Bobra.0001s0002.1.v2.1"; transcript_id "Bobra.0001s0002.1.v2.1"; exon_number 6
scaffold_1	intronGTF	exon	46045	46231	0	-	0	gene_id "Bobra.0001s0002.1.v2.1"; transcript_id "Bobra.0001s0002.1.v2.1"; exon_number 5
scaffold_1	intronGTF	exon	46396	46995	0	-	0	gene_id "Bobra.0001s0002.1.v2.1"; transcript_id "Bobra.0001s0002.1.v2.1"; exon_number 4
scaffold_1	intronGTF	exon	47206	47584	0	-	0	gene_id "Bobra.0001s0002.1.v2.1"; transcript_id "Bobra.0001s0002.1.v2.1"; exon_number 3
scaffold_1	intronGTF	exon	47728	47936	0	-	0	gene_id "Bobra.0001s0002.1.v2.1"; transcript_id "Bobra.0001s0002.1.v2.1"; exon_number 2
scaffold_1	intronGTF	exon	48256	48345	0	-	0	gene_id "Bobra.0001s0002.1.v2.1"; transcript_id "Bobra.0001s0002.1.v2.1"; exon_number 1
scaffold_1	intronGTF	exon	48951	49034	0	+	0	gene_id "Bobra.0001s0003.1.v2.1"; transcript_id "Bobra.0001s0003.1.v2.1"; exon_number 1
scaffold_1	intronGTF	exon	49106	49282	0	+	0	gene_id "Bobra.0001s0003.1.v2.1"; transcript_id "Bobra.0001s0003.1.v2.1"; exon_number 2
scaffold_1	intronGTF	exon	49455	49869	0	+	0	gene_id "Bobra.0001s0003.1.v2.1"; transcript_id "Bobra.0001s0003.1.v2.1"; exon_number 3

   $ tail intron_coordinates_BBRB_v2.gtf 

scaffold_9_2	intronGTF	exon	1006914	1007845	0	-	0	gene_id "Bobra.9_2s0122.1.v2.1"; transcript_id "Bobra.9_2s0122.1.v2.1"; exon_number 3
scaffold_9_2	intronGTF	exon	1007996	1008666	0	-	0	gene_id "Bobra.9_2s0122.1.v2.1"; transcript_id "Bobra.9_2s0122.1.v2.1"; exon_number 2
scaffold_9_2	intronGTF	exon	1008812	1008935	0	-	0	gene_id "Bobra.9_2s0122.1.v2.1"; transcript_id "Bobra.9_2s0122.1.v2.1"; exon_number 1
scaffold_9_2	intronGTF	exon	1012547	1013245	0	+	0	gene_id "Bobra.9_2s0123.1.v2.1"; transcript_id "Bobra.9_2s0123.1.v2.1"; exon_number 1
scaffold_9_2	intronGTF	exon	1014590	1014938	0	+	0	gene_id "Bobra.9_2s0123.1.v2.1"; transcript_id "Bobra.9_2s0123.1.v2.1"; exon_number 2
scaffold_9_2	intronGTF	exon	1015382	1015778	0	+	0	gene_id "Bobra.9_2s0123.1.v2.1"; transcript_id "Bobra.9_2s0123.1.v2.1"; exon_number 3
scaffold_9_2	intronGTF	exon	1016015	1016975	0	+	0	gene_id "Bobra.9_2s0123.1.v2.1"; transcript_id "Bobra.9_2s0123.1.v2.1"; exon_number 4
scaffold_9_2	intronGTF	exon	1017183	1017766	0	+	0	gene_id "Bobra.9_2s0123.1.v2.1"; transcript_id "Bobra.9_2s0123.1.v2.1"; exon_number 5
scaffold_9_2	intronGTF	exon	1017902	1018479	0	+	0	gene_id "Bobra.9_2s0123.1.v2.1"; transcript_id "Bobra.9_2s0123.1.v2.1"; exon_number 6
scaffold_9_2	intronGTF	exon	1018600	1019371	0	+	0	gene_id "Bobra.9_2s0123.1.v2.1"; transcript_id "Bobra.9_2s0123.1.v2.1"; exon_number 7

    NOTA: En la columna #3 se representa cada linea como si fuera un exon, cuando en realidad son los intrones cuyas coordenadas se acaban de calcular

El archivo *gtf* se convirtio a formato *bed12*

    $ /home/fani/gtfToGenePred intron_coordinates_BBRB_v2.gtf intron_coordinates_BBRB_v2.genePred
    $ /home/fani/genePredToBed intron_coordinates_BBRB_v2.genePred intron_coordinates_BBRB_v2.bed12

Una vez teniendo el archivo *bed12* con las coordenadas de los intrones se procedio a realizar la interseccion con *bedtools* para clasificar lncRNAs intronicos como aquellos lncRNAs que sobrelapan en
un 100% aquellas regiones intronicas del genoma de la microalga

    $ bedtools intersect -a lncRNAs_guided_transcriptome.bed12 -b intron_coordinates_BBRB_v2.bed12 -f 1 -wa > intronic_lncRNAs_guided.bed12
    $ wc -l intronic_lncRNAs_guided.bed12 
    50 intronic_lncRNAs_guided.bed12

    $ head intronic_lncRNAs_guided.bed12 

scaffold_101_2	877132	881046	MSTRG.276.1	0	+	881046	881046	0	2	197,166,	0,3748,
scaffold_102	99210	99432	Bobra.0102s0013.1.v2.1	0	+	99432	99432	0	1	222,	0,
scaffold_103	38204	39436	MSTRG.370.1	0	+	39436	39436	0	2	150,72,	0,1160,
scaffold_103	38798	39116	Bobra.0103s0006.1.v2.1	0	+	39116	39116	0	2	115,171,	0,147,
scaffold_103	38798	39116	Bobra.0103s0006.1.v2.1	0	+	39116	39116	0	2	115,171,	0,147,
scaffold_11	88715	89385	MSTRG.776.1	0	.	89385	89385	0	1	670,	0,
scaffold_121	163294	163824	MSTRG.1532.5	0	+	163824	163824	0	2	169,111,	0,419,
scaffold_121	163294	163824	MSTRG.1532.5	0	+	163824	163824	0	2	169,111,	0,419,
scaffold_133	716311	718706	MSTRG.2097.1	0	-	718706	718706	0	3	126,210,339,	0,1520,2056,
scaffold_141	264067	264390	MSTRG.2688.1	0	.	264390	264390	0	1	323,	0,

    $ tail intronic_lncRNAs_guided.bed12 

scaffold_598	15834	16107	Bobra.0598s0005.1.v2.1	0	-	16107	16107	0	1	273,	0,
scaffold_62	372184	372460	Bobra.0062s0044.1.v2.1	0	-	372460	372460	0	1	276,	0,
scaffold_67_1	210453	210734	MSTRG.16041.1	0	+	210734	210734	0	1	281,	0,
scaffold_70	577657	578487	MSTRG.16335.1	0	+	578487	578487	0	2	141,59,	0,771,
scaffold_72	661627	661840	MSTRG.16525.1	0	.	661840	661840	0	1	213,	0,
scaffold_72	661627	661840	MSTRG.16525.1	0	.	661840	661840	0	1	213,	0,
scaffold_72	661627	661840	MSTRG.16525.1	0	.	661840	661840	0	1	213,	0,
scaffold_72	661627	661840	MSTRG.16525.1	0	.	661840	661840	0	1	213,	0,
scaffold_77	1041118	1041337	MSTRG.16857.1	0	+	1041337	1041337	0	1	219,	0,
scaffold_784	3534	3748	MSTRG.16958.1	0	.	3748	3748	0	1	214,	0,


Los argumentos usados para cada uno de las intersecciones con bedtools fueron los siguientes:

    -a: Se refiere al archivo "A" que en este caso es en formato "bed12".
    -b: Se refiere al archivo "B" que en este caso es en formato "bed12".
    -f: Indica el minimo sobrelape requerido como una fraccion del archivo "A". El valor por defecto es 1x10^-9.
    -s: Este argumento fuerza la direccionalidad del transcrito, es decir, solo reporta en el archivo "B" "hits" que sobrelapan 
    en el archivo "A", en la misma hebra. Por defecto los sobrelapes son reportados sin que sean respecto a la hebra.
    -S: Indica que se requiere de una direccionalidad diferente, es decir, solo reporta en el archivo "B" "hits" que sobrelapan 
    el archivo "A" en la hebra opuesta. Al igual que con "-s" los sobrelapes son reportados sin que sean respecto a la hebra.
    -v: Le indica al programa que solamente reporte en el archivo "A" aquellas entradas que no tienen sobrelape en el archivo "B" 
    y este es restringido por el argumento "-f" y "-r".
    

NOTA aclaratoria: Para identificar los lncRNAs intronicos no se uso el argumento -r debido a que este indica al programa que la interseccion requiere de una fraccion de sobrelape sea reciproca en el 
archivo "A" y el "B", es decir, si -f es 1 (como en este caso) y se usa el argumento -r, se requiere que el archivo "intron_coordinates_BBRB_v2.bed12" sobrelape en un 100% al archivo 
"lncRNAs_guided_transcriptome.bed12" y viceversa. Al ser tan estricto usando -r no se obtenia resultados de sobrelapes, por lo que se elimino esa opcion. De esta manera los lncRNAs clasificados como
intronicos en este caso, no necesariamente sobrelapan reciprocamente en ambos archivos



