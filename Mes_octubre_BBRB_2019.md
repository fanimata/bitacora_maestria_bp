# **Bit cora: Octubre**


#### *Inicio de analisis bioinformatico de datos de Botryococcus braunii Raza B (Showa)*


#### *Viernes 18 de octubre de 2019*


Se descargaron los datos de secuenciacion de BBRB desde *FileZila* desde Mazorka en una carpeta nueva, usando la siguiente linea de comando:

    $ mkdir BBRB
    $ mkdir fastq
    $ mkdir orig_files
    $ nohup wget --user USUARIO --pasword PASSWORDftp://DIRECCION/* . &
    
NOTA: Los datos de USUARIO y PASSWORD usados fueron los proporcionados por la empresa para la descarga de las secuencias.

El progreso de la descarga fue monitoreado usando el comando:

    $ ls -lh
    
Tambien se reviso el archivo nohup.out como otra manera de monitorear la descarga:

    $ tail nohup.out

Una vez finalizada la descarga se reviso el codigo de cada archivo *fastq* comparandolo con el archivo md5.txt. Se reviso que los condigos coincidieran
de tal manera que esto indicaran que las secuencias se descargaron bien.


Los datos descargados fueron 12 archivos *fastq* comprimidos, un archivo md5.txt y un resumen en formato *xlsx*:

    C1_BBRB_S255_L001_R1_001.fastq.gz
    C1_BBRB_S255_L001_R2_001.fastq.gz
    C2_BBRB_S255_L001_R1_001.fastq.gz
    C2_BBRB_S255_L001_R2_001.fastq.gz
    C3_BBRB_S255_L001_R1_001.fastq.gz
    C3_BBRB_S255_L001_R2_001.fastq.gz
    L1_BBRB_S255_L001_R1_001.fastq.gz
    L1_BBRB_S255_L001_R2_001.fastq.gz
    L2_BBRB_S255_L001_R1_001.fastq.gz
    L2_BBRB_S255_L001_R2_001.fastq.gz
    L3_BBRB_S255_L001_R1_001.fastq.gz
    L3_BBRB_S255_L001_R2_001.fastq.gz
    1735-232-QC summary.xlsx
    md5.txt

NOTA: Cada una de las secuencias presentaba dos replicas biologicas (R1 y R2) por lo que era necesaria cancatenar los archivos R1 y R2 de cada una para
continuar trabajandolos.


#### *Lunes 21 de octubre de 2019*


Se concateno los archivos R1 y R2 de cada secuencia y se movieron a una nueva carpeta que se creo dentro de /fastq:


    $ mkdir cat_files
    $ cat C1_BBRB_S255_L001_R1_001.fastq.gz C1_BBRB_S255_L001_R2_001.fastq.gz > C1_BBRB_S255_L001.fastq.gz


Se creo otra carpeta dentro del directorio principal para llevar a cabo los analisis de calidad con FastQC:


    $ mkdir FASTQC


Se realizo un analisis de calidad de los archivos originales (sin concatenar) desde mazorka con FastQC:

    $ mkdir rep_origfiles


    #PBS -N FastQC_cat
    #PBS -l nodes=1:ppn=8,vmem=4gb,walltime=120:00:00
    #PBS -q default
    #PBS -V
    #PBS -o fastqc_cat.out
    #PBS -e fastqc_cat.err


    module load FastQC/0.11.2


    cd $PBS_O_WORKDIR


    cd /LUSTRE/usuario/smata/BBRB/fastq/cat_files


    for i in  *gz

    do


    fastqc /LUSTRE/usuario/smata/BBRB/fastq/cat_files/$i -o /LUSTRE/usuario/smata/BBRB/FASTQC/rep_catfiles


    done



Y tambien se realizo el analisis de calidad con los archivos concatenados:

    $ mkdir rep_catfiles


    #PBS -N FastQC_cat
    #PBS -l nodes=1:ppn=8,vmem=4gb,walltime=120:00:00
    #PBS -q default
    #PBS -V
    #PBS -o fastqc_cat.out
    #PBS -e fastqc_cat.err


    module load FastQC/0.11.2


    cd $PBS_O_WORKDIR


    cd /LUSTRE/usuario/smata/BBRB/fastq/cat_files


    for i in  *gz

    do


    fastqc /LUSTRE/usuario/smata/BBRB/fastq/cat_files/$i -o /LUSTRE/usuario/smata/BBRB/FASTQC/rep_catfiles


    done



Los resultados del analisis de *FASTQC* de los archivos originales y concatenados se descargaron en la computadora desde mazorka hacia una carpeta local, usando el comando *rsync*:


    $ rsync -av smata@10.10.10.240:/LUSTRE/usuario/smata/BBRB/FASTQC/rep_catfiles ./
    $ rsync -av smata@10.10.10.240:/LUSTRE/usuario/smata/BBRB/FASTQC/rep_origfiles ./


Luego se procedio a realizar un reporte general de los resultados ejecutando *MultiQC* en la carpeta donde se descargaron los archivos:

    $ multiqc ./


#### *Viernes 25 de octubre de 2019*


Se proceso con Trimmomatic las librerias de BBRB, con sus respectivos archivos R1 y R2. El script se corrio de la siguiente manera:


    #PBS -N trimmo_C1
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=20gb,walltime=1:00:00
    #PBS -V
    #PBS -o trimmo_C1.out
    #PBS -e trimmo_C1.err

    module load Trimmomatic/0.32

    cd $PBS_O_WORKDIR


    java -jar $TRIMMOMATIC PE -threads 2 /LUSTRE/usuario/smata/BBRB/fastq/orig_files/C1_BBRB_S255_L001_R1_001.fastq.gz /LUSTRE/usuario/smata/BBRB/fastq/orig_files/C1_BBRB_S255_L001_R2_001.fastq.gz Trimmed_C1_BBRB_S255_FP.fastq.gz Trimmed_C1_BBRB_S255_FU.fastq.gz Trimmed_C1_BBRB_S255_RP.fastq.gz Trimmed_C1_BBRB_S255_RU.fastq.gz ILLUMINACLIP:TruSeq3-PE-2.fa:2:30:10 SLIDINGWINDOW:4:15 MINLEN:60




Se uso esa misma plantilla para los demas archivos R1 y R2 de las demas muestras (C2, C3, L1, L2 y L3), usando los mismos parametros.

Los parametros usados fueron los siguientes:


     PE -> Indica que las librerias utilizadas son "paired-end".
    -threads 2 -> Indica que se usaron dos hilos del servidor para correr el programa.
    ILLUMINACLIP:TrueSeq3-PE-2.fa:2:30:10 -> Los adaptadores usados para remove que son TrueSeq PE y se usaron parametros por default.
    SLINDINGWINDOW:4:20 -> En una ventana de 4 bases remover aquellas bases cuyo promedio de calidad decae por debajo de 20.
    MINLEN:60 -> Mantener lecturas con un tamano minimo de 60 pb.
