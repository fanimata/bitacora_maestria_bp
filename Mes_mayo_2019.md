# **Bitácora: Mayo**



#### *Martes 7 de mayo de 2019*


Se corrió *featureCounts* desde línea de comando en Mazorka, usando el programa subread. Se generó una tabla de cuentas a partir de archivos
de alineamiento bam de *C. reinhardtii* generados con *HISAT2* y el archivo de coordenadas generado con *Stringtie* (el archivo concatenado). 
El *script* utilizado para correr *featureCounts* fue el siguiente:


    #PBS -N subread
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=120:00:00
    #PBS -V
    #PBS -o counts.out
    #PBS -e counts.err


    module load subread/1.5.1

    cd $PBS_O_WORKDIR


    cd /LUSTRE/usuario/smata/chlamy/hisat2/sam_to_bam


    featureCounts -T 4 -p -C -a /LUSTRE/usuario/smata/chlamy/stringtie_ensam/merged_gtfs/st_merged_chlamy.gtf AgNPs_12h_1.bam AgNPs_12h_2.bam 
    AgNPs_2h_1.bam AgNPs_2h_2.bam CK_12h.bam CK_2h.bam -o /LUSTRE/usuario/smata/chlamy/counts/chlamy_counts.txt


Los argumentos utilizados para correr el *script* fueron los siguientes:


    -T 4: Indica el número de threads a usar, en este caso fue 4.
    -p: Indica que los alineamientos provienen de librerías paired-end.
    -C: Indica que se excluyen fragmentos quiméricos.
    -a: Se refiere a la anotación usada como input, en este caso fue el archivo gtf concatenado a partir de varios archivos gtf
    generados con Stringtie.
    -o: Corresponde al archivo de salida en formato txt.
    NOTA: Los alineamientos (input) en formato bam son reconocidos automáticamente por el programa.
    
    
    
#### *Lunes 20 de mayo de 2019*


Debido a que los archivos de las secuencias de Chlamy AgNPs 12 h y 2h son réplicas técnicas se realizó la separación de los archivos usando *sratoolkit* y los archivos
R1 y R2 de estos fueron concatenados. Los archivos CK 12h y 2h también se separaron usando la misma herramienta. El *script* utilizado para 
ello fue el siguiente:

    #PBS -N sra_to_fastq
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=120:00:00
    #PBS -V
    #PBS -o sra_2_fastq.out
    #PBS -e sra_2_fastq.err

    module load sratoolkit/2.8.2

    cd $PBS_O_WORKDIR

    for sra in *sra

    do

    clean=$(echo $sra | sed 's/\.sra//')

    fastq-dump --split-files --gzip $sra

    done


Las muestras AgNPs de las secuencias de Chlamy de 12h y de 2h presetaban cada uno 2 replicados y cada replicado se dividió el dos, resultando de la siguiente
manera:

    * AgNPs_12h_1_1.fastq.gz
    * AgNPs_12h_1_2.fastq.gz
    * AgNPs_12h_2_1.fastq.gz
    * AgNPs_12h_2_2.fastq.gz
    * AgNPs_2h_1_1.fastq.gz
    * AgNPs_2h_1_2.fastq.gz
    * AgNPs_2h_2_1.fastq.gz
    * AgNPs_2h_2_2.fastq.gz
    
    
De estas se concatenó las secuencias R1 y R2:

    $ cat AgNPs_12h_1_1.fastq.gz AgNPs_12h_1_2.fastq.gz > AgNPs_12h_1.fastq.gz
    $ cat AgNPs_12h_2_1.fastq.gz AgNPs_12h_2_2.fastq.gz > AgNPs_12h_2.fastq.gz
    $ cat AgNPs_2h_1_1.fastq.gz AgNPs_2h_1_2.fastq.gz > AgNPs_2h_1.fastq.gz
    $ cat AgNPs_2h_2_1.fastq.gz AgNPs_2h_2_2.fastq.gz > AgNPs_2h_2.fastq.gz

Las muestras CK de las secuencias de Chlamy de 12h y 2h se dividieron unicamente en dos cada uno, resultando de la siguiente manera:

    * CK_12h_1.fastq.gz
    * CK_12h_2.fastq.gz
    * CK_2h_1.fastq.gz
    * CK_2h_2.fastq.gz
    
Al final se continuó trabajando con un total de 8 secuencias.

Se corrió un análisis de calidad usando *FastQC* de los 8 archivos *fastq*:


    #PBS -N FastQC
    #PBS -l nodes=1:ppn=8,vmem=4gb,walltime=120:00:00
    #PBS -q default
    #PBS -V
    #PBS -o fastqc_1.out
    #PBS -e fastqc_1.err

    cd $PBS_O_WORKDIR

    module load FastQC/0.11.2

    cd /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq_splited

    for i in  *gz

    do

    fastqc /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq_splited/$i -o /LUSTRE/usuario/smata/chlamy/fastqc_before

    done

Luego se corrió *MultiQC* para obtener un reporte general del resultado del análisis con *FastQC*. Para ellos los archivos se pasaron a una carpeta 
local en la computadora y se corrió *MultiQC* desde la terminal de la computadora en la carpeta donde se encontraban los reportes del *FastQC* 
descargados:

    $ multiqc ./
    

#### *Martes 21 de mayo de 2019*


Se procedió a procesar las 8 secuencias usando *Trimmomatic* desde mazorka con las siguientes especificaciones:
NOTA: El siguiente *script* se muestra de manera completa para las muestras *AgNPs_12h_1* y *AgNPs_12h_2*.

    
    #PBS -N trimmo_Ag12
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=20gb,walltime=1:00:00
    #PBS -V
    #PBS -o trimmo_Ag12_1.out
    #PBS -e trimmo_Ag12_1.err

    
    module load Trimmomatic/0.32


    cd $PBS_O_WORKDIR

    java -jar $TRIMMOMATIC PE -threads 2 /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq_splited/AgNPs_12h_1.fastq.gz 
    /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq_splited/AgNPs_12h_2.fastq.gz Trimmed_AgNPs_12h_FP.fastq.gz 
    Trimmed_AgNPs_12h_FU.fastq.gz Trimmed_AgNPs_12h_RP.fastq.gz Trimmed_AgNPs_12h_RU.fastq.gz ILLUMINACLIP:TruSeq3-PE-2.fa:2:30:10 
    HEADCROP:5 SLIDINGWINDOW:4:15  MINLEN:60


Para las demás muestras el *script* de *java* usando fue el siguiente:

    Muestra AgNps_2h_1 y AgNps_2h_2

    java -jar $TRIMMOMATIC PE -threads 2 /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq_splited/AgNPs_2h_1.fastq.gz 
    /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq_splited/AgNPs_2h_2.fastq.gz Trimmed_AgNPs_2h_FP.fastq.gz 
    Trimmed_AgNPs_2h_FU.fastq.gz Trimmed_AgNPs_2h_RP.fastq.gz Trimmed_AgNPs_2h_RU.fastq.gz ILLUMINACLIP:TruSeq3-PE-2.fa:2:30:10 
    HEADCROP:5 SLIDINGWINDOW:4:15 MINLEN:60


    Muestra CK_12h_1 y CK_12h_2

    java -jar $TRIMMOMATIC PE -threads 2 /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq_splited/CK_12h_1.fastq.gz 
    /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq_splited/CK_12h_2.fastq.gz Trimmed_CK_12h_FP.fastq.gz 
    Trimmed_CK_12h_FU.fastq.gz Trimmed_CK_12h_RP.fastq.gz Trimmed_CK_12h_RU.fastq.gz ILLUMINACLIP:TruSeq3-PE-2.fa:2:30:10 
    HEADCROP:5 SLIDINGWINDOW:4:15 MINLEN:60


    Muestra CK_2h_1 y CK_2h_2

    java -jar $TRIMMOMATIC PE -threads 2 /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq_splited/CK_2h_1.fastq.gz 
    /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq_splited/CK_2h_2.fastq.gz Trimmed_CK_2h_FP.fastq.gz 
    Trimmed_CK_2h_FU.fastq.gz Trimmed_CK_2h_RP.fastq.gz Trimmed_CK_2h_RU.fastq.gz ILLUMINACLIP:TruSeq3-PE-2.fa:2:30:10 
    HEADCROP:5 SLIDINGWINDOW:4:15 MINLEN:60

    

Para cada procesamiento se generó un 4 archivos trimeados (2 FP y 2 RP) y en total fueron 16 archivos, los cuales se analizaron con *FastQC*:

    #PBS -N FastQC
    #PBS -l nodes=1:ppn=8,vmem=4gb,walltime=120:00:00
    #PBS -q default
    #PBS -V
    #PBS -o QC_trim_files.out
    #PBS -e QC_trim_files.err

    cd $PBS_O_WORKDIR

    module load FastQC/0.11.2

    cd /LUSTRE/usuario/smata/chlamy/trimmo/agnps12

    for i in  *gz
    
    do

    fastqc /LUSTRE/usuario/smata/chlamy/trimmo/agnps12/$i -o /LUSTRE/usuario/smata/chlamy/fastqc_after

    done


    cd /LUSTRE/usuario/smata/chlamy/trimmo/agnps2

    for i in  *gz

    do

    fastqc /LUSTRE/usuario/smata/chlamy/trimmo/agnps2/$i -o /LUSTRE/usuario/smata/chlamy/fastqc_after

    done


    cd /LUSTRE/usuario/smata/chlamy/trimmo/ck12

    for i in  *gz

    do

    fastqc /LUSTRE/usuario/smata/chlamy/trimmo/ck12/$i -o /LUSTRE/usuario/smata/chlamy/fastqc_after

    done


    cd /LUSTRE/usuario/smata/chlamy/trimmo/ck2

    for i in  *gz

    do

    fastqc /LUSTRE/usuario/smata/chlamy/trimmo/ck2/$i -o /LUSTRE/usuario/smata/chlamy/fastqc_after
    
    
Nuevamente se corrió *MultiQC* para obtener un reporte general del resultado del análisis con *FastQC* de las secuencias luego de procesarlas. Los
archivos se pasaron a una carpeta local en la computadora y se corrió *MultiQC* desde la terminal en la carpeta donde se encontraban los reportes 
descargados:

    $ multiqc ./
    

Una vez procesados los arhivos y analizada su calidad se procedió a realizar un nuevo alineamiento con *HISAT2* desde Mazorka usand un índice generado
anteriormente (*chlamy_index*). Los archivos seleccionados
para el alineamiento fueron *forward-paired (FP)* y *reverse-paired (RP)*, para cada secuencia (tratamiento con AgNPs de 12h y 2h, y controles de 12h y 2h).

    
    #PBS -N hisat2_align
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=20gb,walltime=120:00:00
    #PBS -V
    #PBS -o hisat2_align.out
    #PBS -e hisat2_align.err

    module load hisat2/2.1.0

    cd $PBS_O_WORKDIR


    cd /LUSTRE/usuario/smata/chlamy/trimmo/agnps12

    hisat2 -p 4 --dta -x /LUSTRE/usuario/smata/chlamy/hisat2/index/chlamy_index --fr -1 /LUSTRE/usuario/smata/chlamy/trimmo/agnps12/
    Trimmed_AgNPs_12h_FP.fastq.gz -2 /LUSTRE/usuario/smata/chlamy/trimmo/agnps12/Trimmed_AgNPs_12h_RP.fastq.gz 
    -S /LUSTRE/usuario/smata/chlamy/hisat2/align/AgNPs_12h.sam --summary-file /LUSTRE/usuario/smata/chlamy/hisat2/align/AgNPs_12h_sum.txt


    cd /LUSTRE/usuario/smata/chlamy/trimmo/agnps2

    hisat2 -p 4 --dta -x /LUSTRE/usuario/smata/chlamy/hisat2/index/chlamy_index --fr -1 /LUSTRE/usuario/smata/chlamy/trimmo/agnps2/
    Trimmed_AgNPs_2h_FP.fastq.gz -2 /LUSTRE/usuario/smata/chlamy/trimmo/agnps2/Trimmed_AgNPs_2h_RP.fastq.gz 
    -S /LUSTRE/usuario/smata/chlamy/hisat2/align/AgNPs_2h.sam --summary-file /LUSTRE/usuario/smata/chlamy/hisat2/align/AgNPs_2h_sum.txt


    cd /LUSTRE/usuario/smata/chlamy/trimmo/ck12

    hisat2 -p 4 --dta -x /LUSTRE/usuario/smata/chlamy/hisat2/index/chlamy_index --fr -1 /LUSTRE/usuario/smata/chlamy/trimmo/ck12/
    Trimmed_CK_12h_FP.fastq.gz -2 /LUSTRE/usuario/smata/chlamy/trimmo/ck12/Trimmed_CK_12h_RP.fastq.gz 
    -S /LUSTRE/usuario/smata/chlamy/hisat2/align/CK_12h.sam --summary-file /LUSTRE/usuario/smata/chlamy/hisat2/align/CK_12h_sum.txt


    cd /LUSTRE/usuario/smata/chlamy/trimmo/ck2

    hisat2 -p 4 --dta -x /LUSTRE/usuario/smata/chlamy/hisat2/index/chlamy_index --fr -1 /LUSTRE/usuario/smata/chlamy/trimmo/ck2/
    Trimmed_CK_2h_FP.fastq.gz -2 /LUSTRE/usuario/smata/chlamy/trimmo/ck2/Trimmed_CK_2h_RP.fastq.gz 
    -S /LUSTRE/usuario/smata/chlamy/hisat2/align/CK_2h.sam --summary-file /LUSTRE/usuario/smata/chlamy/hisat2/align/CK_2h_sum.txt
 

Seguidamente se ordenaron los archivos *sam* resutantes del alineamiento y se convirtieron a formato *bam* en un mismo *script* usando *Samtools*:

    
    #PBS -N samtools
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=20gb,walltime=120:00:00
    #PBS -V
    #PBS -o convert_sam.out
    #PBS -e convert_sam.err


    module load samtools/1.9 

    cd $PBS_O_WORKDIR


    samtools sort -@ 8 -o /LUSTRE/usuario/smata/chlamy/hisat2/sam_to_bam/AgNPs_12h.bam /LUSTRE/usuario/smata/chlamy/hisat2/align/AgNPs_12h.sam

    samtools sort -@ 8 -o /LUSTRE/usuario/smata/chlamy/hisat2/sam_to_bam/AgNPs_2h.bam /LUSTRE/usuario/smata/chlamy/hisat2/align/AgNPs_2h.sam

    samtools sort -@ 8 -o /LUSTRE/usuario/smata/chlamy/hisat2/sam_to_bam/CK_12h.bam /LUSTRE/usuario/smata/chlamy/hisat2/align/CK_12h.sam 

    samtools sort -@ 8 -o /LUSTRE/usuario/smata/chlamy/hisat2/sam_to_bam/CK_2h.bam /LUSTRE/usuario/smata/chlamy/hisat2/align/CK_2h.sam



#### *Miércoles 22 de mayo de 2019*


Se realizó un emsamble de transcriptoma de Chlamy usandos los archivos *bam* del alineamiento con *HISAT2*. El ensamble se realizó con *Stringtie*
en mazorka:

    
    #PBS -N stringtie_ensam
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=20gb,walltime=120:00:00
    #PBS -V
    #PBS -o st_chlamy_ensam.out
    #PBS -e st_chlamy_ensam.err


    module load stringtie/1.3.4d

    cd $PBS_O_WORKDIR


    stringtie -p 4 -G /LUSTRE/usuario/smata/chlamy/datos_genoma/GCF_000002595.1_v3.0_genomic.gtf /LUSTRE/usuario/smata/chlamy/hisat2/sam_to_bam/
    AgNPs_12h.bam -l AgNPs_12h -o /LUSTRE/usuario/smata/chlamy/stringtie_ensam/AgNPs_12h.gtf -A /LUSTRE/usuario/smata/chlamy/stringtie_ensam/
    AgNPs_12h_geneabun.tab


    stringtie -p 4 -G /LUSTRE/usuario/smata/chlamy/datos_genoma/GCF_000002595.1_v3.0_genomic.gtf /LUSTRE/usuario/smata/chlamy/hisat2/sam_to_bam/
    AgNPs_2h.bam -l AgNPs_2h -o /LUSTRE/usuario/smata/chlamy/stringtie_ensam/AgNPs_2h.gtf -A /LUSTRE/usuario/smata/chlamy/stringtie_ensam/
    AgNPs_2h_geneabun.tab


    stringtie -p 4 -G /LUSTRE/usuario/smata/chlamy/datos_genoma/GCF_000002595.1_v3.0_genomic.gtf /LUSTRE/usuario/smata/chlamy/hisat2/sam_to_bam/
    CK_12h.bam -l CK_12h -o /LUSTRE/usuario/smata/chlamy/stringtie_ensam/CK_12h.gtf -A /LUSTRE/usuario/smata/chlamy/stringtie_ensam/
    CK_12h_geneabun.tab


    stringtie -p 4 -G /LUSTRE/usuario/smata/chlamy/datos_genoma/GCF_000002595.1_v3.0_genomic.gtf /LUSTRE/usuario/smata/chlamy/hisat2/sam_to_bam/
    CK_2h.bam -l CK_2h -o /LUSTRE/usuario/smata/chlamy/stringtie_ensam/CK_2h.gtf -A /LUSTRE/usuario/smata/chlamy/stringtie_ensam/
    CK_2h_geneabun.tab
    
    
Los archivos de coordenadas *gtf* generados en el ensamble se unieron usando la opción -merge de *Stringtie* y una lista con los nombres y localización
de los archivos *gtf*:


    #PBS -N stringtie_merge
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=00:30:00
    #PBS -V
    #PBS -o merged.out
    #PBS -e merged.err


    module load stringtie/1.3.4d

    cd $PBS_O_WORKDIR


    stringtie --merge -p 8 -G /LUSTRE/usuario/smata/chlamy/datos_genoma/GCF_000002595.1_v3.0_genomic.gtf /LUSTRE/usuario/smata/chlamy/
    stringtie_ensam/merged_gtfs/mergelist.txt -o ./st_merged_chlamy.gtf


El archivo *mergelist.txt* correspondiente a la lista de los archivos a usar como *input* contenía lo siguiente:

    /LUSTRE/usuario/smata/chlamy/stringtie_ensam/AgNPs_12h.gtf  
    /LUSTRE/usuario/smata/chlamy/stringtie_ensam/AgNPs_2h.gtf  
    /LUSTRE/usuario/smata/chlamy/stringtie_ensam/CK_12h.gtf
    /LUSTRE/usuario/smata/chlamy/stringtie_ensam/CK_2h.gtf
    

Se comparó el archivo *gtf* final con un archivo de anotación (*gtf*) ya existente para *C. reinhardtii* de tal manera que se pudiera determinar
si se había detectado transcritos en el ensamble del transcriptoma que realizados, que anteriormente no se hubieran anotado en la microalga:


    #PBS -N gffcompare
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=00:30:00
    #PBS -V
    #PBS -o compare.out
    #PBS -e compare.err


    module load gffcompare/0.10.4

    cd $PBS_O_WORKDIR


    gffcompare -R -V -r /LUSTRE/usuario/smata/chlamy/datos_genoma/GCF_000002595.1_v3.0_genomic.gtf -o compare /LUSTRE/usuario/smata/chlamy/stringtie_ensam/merged_gtfs/st_merged_chlamy.gtf


NOTA: El archivo de anotación ya reportado para Chlamy que se usó fue *GCF_000002595.1_v3.0_genomic.gtf*.

  
Finalmente se corrió nuevamente el programa *featurecounts* en mazorka para genera la tabla de cuentas que se usaría para hacer el análisis de expresión 
diferencial en *R*:

    
    #PBS -N subread
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=120:00:00
    #PBS -V
    #PBS -o counts.out
    #PBS -e counts.err


    module load subread/1.5.1

    cd $PBS_O_WORKDIR


    cd /LUSTRE/usuario/smata/chlamy/hisat2/sam_to_bam


    featureCounts -T 4 -p -C -a /LUSTRE/usuario/smata/chlamy/stringtie_ensam/merged_gtfs/st_merged_chlamy.gtf AgNPs_12h.bam AgNPs_2h.bam 
    CK_12h.bam CK_2h.bam -o /LUSTRE/usuario/smata/chlamy/counts/chlamy_counts.txt


