# **Bitácora: Marzo**



## Práctica de Análisis transcriptómico 
---


#### *Martes 5 de marzo de 2019*



**Análisis de calidad de secuencias**


Para la práctica se utilizaron datos de secuenciación de *Schizosaccharomyces pombe*, los cuale fueron descargados en formato comprimido desde una carpeta de google drive, el comando utilizados para esto fue:

    $ tar -xvf /export/storage/nnb/t2/FastQC_Short.tar.gz

El archivo se descomprimió en una carpeta local y se revisó la carpeta descomprimida, usando el siguiente *script*:

    $ tar -xvf /home/fani/Documentos/I_Semestre_2019/Tesis/Tutorial_transcriptomica/
    FastQC_Short.tar.gz
    $ cd FastQC_Short
    $ ls

    Partial_SRR2467141.fastq 
    Partial_SRR2467142.fastq 
    Partial_SRR2467143.fastq 
    Partial_SRR2467144.fastq 
    Partial_SRR2467145.fastq 
    Partial_SRR2467146.fastq
    Partial_SRR2467147.fastq
    Partial_SRR2467148.fastq
    Partial_SRR2467149.fastq
    Partial_SRR2467150.fastq
    Partial_SRR2467151.fastq
    
Se revisó los archivos de secuenciación de Illumina usando el comando:

    $ head -n "archivo.fastq"

    NOTA: Los archivos de Illumina generalmente presentan el siguiente formato:
    <nombre de la muestra>_<secuencia identificadora (barcode)>_L<línea (3 dígitos)>
    _R<número de lectura (read)>_<número del set (3 dígitos)>.fastq.gz
    
Una vez revisada las secuencias se procedió a realizar el análisis de calidad con FastQC, para lo cual se creó una carpeta nueva:

    $ mkdir QUAL

Para poder correr el análisis de calidad de cada uno de los archivos en formato *"fastq"*, se aplicó un ciclo for a cada uno de estos dentro de la carpeta FastQC_Short:

    $ for i in $(ls *fastq); do echo $i; done
    
    
    NOTA: El ciclo for ("for loop") nos permite ejecutar un comando para cada arhivo de
    manera secuencial, por lo que es muy últil cuando se poseen muchos arhivos.
    
En donde cada archivo fue nombrado como "$i"

    $ fastQC -O /home/fani/Documentos/I_Semestre_2019/Tesis/Tutorial_transcriptoma/
    FastQC_Short/QUAL/ $i

En donde el argumento (*flag*) *-O* sifinica el directorio de salida.
Una vez finalizado el resultado se revió la carpeta QUAL, en donde se observó archivos con formato "html" y "zip":

    $ cd QUAL
    $ ls
    Partial_SRR2467151_fastqc.html
    Partial_SRR2467151_fastqc.zip

Se revisó los archivos "html" en el buscador y la siguiente información que despliega:

    1) Basic Statistics
    2) Per base sequence quality
    3) Per tile sequence quiality
    4) Per sequence quality scores
    5) Per base sequence content
    6) Per sequence GC content
    7) Per base N content
    8) Sequence Length Distribution
    9) Sequence Duplication Levels
    10) Overrrepresented sequences
    11) Adapter Content
    12) Kmer Content
    
Con todo esto se observó que la distribución de GC es muy similar a la distribución teórica, que hay pocas bases ambiguas, que todas las secuencias tienen el mismo tamaño, que hay pocas secuencias sobre representadas y otros.
Se descomprimió además los archivos en formato "zip" y se revisó las carpetas:
    
    $ unzip Partial_SRR2467151_fastqc.zip
    $ cd Partial_SRR2467151_fastqc
    $ ls
    
Ahí dentro se observó un reporte del análisis en texto plano, el cual se revisó con el siguiente comando:

    $ more summary.txt


**Limpieza de las secuencias**


Para llevar a cabo la limpieza de los archivos "fastq" se utilizó el programa *Trimmomatic* basado en Java, y posee opciones de filtrado para lecturas individuales o emparejadas (single-end y paired-end)
Se le pidió a *Trimmomatic* que recortara lo siguiente:

    1) Los adaptadores Illumina TruSeq single-end (SE) del extremo 3'.
    2) Los primeros 10 nucleótidos del extremos 5'.
    3) Bases de mala calidad usando una ventana de 4 nulceótidos con un promedio mínimo
    de Phred de 15.
    4) Secuencias de longitud menor a 60 nucleótidos.
    
Antes de correr el programa se descargó el archivo de los adaptadores Tru-Seq SE en la carpeta FastQC_Short:

    $ wget https://raw.githubusercontent.com/timflutre/trimmomatic/master/adapters/
    TruSeq3-SE.fa ./

Luego se procedió a correr *Trimmomatic* usando el siguiente *script*:

    $ for i in $(ls *fastq); do echo $i; done
    $ trimmomatic SE $i Trimmed_$i ILLUMINACLIP:TruSeq3-SE.fa:2:30:10 HEADCROP:10 
    SLIDINGWINDOW:4:15 MINLEN:60

Una vez finalizado el análisis se revisó la carpeta y los archivos generados:

    $ ls
    $ head -n 12 Trimmed_Partial_SRR2467151.fastq
    
Una vez recortadas las secuencias "fastq" se corrió nuevamente un análisis de calidad usando FastQC:

    $ for i in $(ls Trimmed*); do echo $i; done
    $ fastqc -O ./QUAL/ $i
    
Se revisó los archivos "html" generados en este nuevo análisis y se comparó la calidad de las seuencias antes y después de la corrida con *Trimmomatic*.



#### *Jueves 7 de marzo de 2019*


**Ensamblaje *de novo* de transcriptomas**


    Notas importantes antes de proceder al ensamblaje
        
    ¿Qué es un k-mer? 
    Todas las posibles subsecuencias de tamaño "k" que existen en una secuencia. 
    El número de k'mer en una secuencias es:
    L - k + 1  en donde:
    L= Longitud de la secuencia
    K= Tamaño de la secuencia
    
    Teoría de grafos de *Bruijin*: 
    Se puede encontrar la supercadena circular más corta que posea todas las"subcadenas" posibles de tamaño "k" de un alfabeto determinado.
    
    ¿Cómo se aplica la teoría de grafos al ensamblaje?
    **Paso 1:** Se toma las secuencias y se rompen en pedacitos de tamaño "k".
    **Paso 2:** Se construye un grafo de *Bruijin* usando esas piezas.
    **Paso 3:** Se reconstruye la secuecia original buscando los "caminos".
    
Para realizar esta práctica se creó una carpeta nueva y dentro de esta se descargó varios archivos en formato "fastq" con el comando "wget":

    $ mkdir FASTQ_Complete
    $ cd FASTQ_Complete
    $ wget https://liz-fernandez.github.io/Talleres_Bioinfo_Cuernavaca_17-2/datasets/
    Sp_ds.left.fq.gz 
    $ wget https://liz-fernandez.github.io/Talleres_Bioinfo_Cuernavaca_17-2/datasets/
    Sp_ds.right.fq.gz
    $ wget https://liz-fernandez.github.io/Talleres_Bioinfo_Cuernavaca_17-2/datasets/
    Sp_hs.right.fq.gz 
    $ wget https://liz-fernandez.github.io/Talleres_Bioinfo_Cuernavaca_17-2/datasets/
    Sp_hs.left.fq.gz   
    $ wget https://liz-fernandez.github.io/Talleres_Bioinfo_Cuernavaca_17-2/datasets/
    Sp_plat.left.fq.gz
    $ wget https://liz-fernandez.github.io/Talleres_Bioinfo_Cuernavaca_17-2/datasets/
    Sp_plat.right.fq.gz
    $ wget https://liz-fernandez.github.io/Talleres_Bioinfo_Cuernavaca_17-2/datasets/
    Sp_log.left.fq.gz  
    $ wget https://liz-fernandez.github.io/Talleres_Bioinfo_Cuernavaca_17-2/datasets/
    Sp_log.right.fq.gz
    
Se revisó los ocho archivos mediante un ciclo for:

    $ for fastq in S*fq.gz ; do echo $fastq; zcat $fastq | head ; wait ; done

Mediante el cuál se pudo observar las primeras 10 líneas de cada una de las secuencias.
Asumiendo que es estos datos anteriormente ya se les realizó un filtrado por calidad, se procedió a realizar el ensamblaje de novo con *Trinity*
Para poder utilizar el programa se instaló primeramente de manera local en la computadora mediante Bioconda (un canal especialido en software bioinformático), para lo cual anteriormente Bioconda debía estar instalado en la computadora.

    $ conda install trinity
    
Una vez instalado se empezó a correr *Trinity* con un comando genérico:

    $ Trinity --seqType fq --SS_lib_type RF \ --left Sp_log.left.fq.gz Sp_hs.left.fq.gz
    \ --right Sp_log.right.fq.gz Sp_hs.right.fq.gz \ --CPU 2 --max_memory 1G 

En donde las opciones/argumentos (*flags*) utilizados son los más esenciales para correr el programa, y estas fueron las significan lo siguiente:

    * --seqType: Indica que el archivo de entrada que se está usando es en formato 
    "fastq".
    * --SS_lib_type RF: Indica que la librería fue construída usando lecturas pares 
    con orientación RF (*reverse-forward*).
    * --left: Indica lecturas del lado izquierdo (o R1).
    * --right: Indica lecturas del lado derecho (o R2).
    * --CPU 2: Indica que se va a utilizar 2 CPUs.
    * --max_memory 1G: Indica que la memoria RAM máxima a usar es de 1 GB.

Una vez que la corrida finalizó se revisó el resultado que corresponde a secuencias en formato "fasta":

    $ head trinity_out_dir/Trinity.fasta
    

**Análisis de la estadística del trancriptoma ensamblado *de novo***

Dentro de *Trinity* existe un programa que permite capturar las estadísticas del emsablaje, el cuál se corrió de la siguiente manera:

    $ TrinityStats.pl trinity_out_dir/Trinity.fasta
    
En donde despliega el siguiente resultado:

    ################################
    ## Counts of transcripts, etc.
    ################################
    Total trinity 'genes':	367
    Total trinity transcripts:	373
    Percent GC: 39.20

    ########################################
    Stats based on ALL transcript contigs:
    ########################################

	Contig N10: 2581
	Contig N20: 2016
	Contig N30: 1596
	Contig N40: 1375
	Contig N50: 1106

	Median contig length: 427
	Average contig: 701.63
	Total assembled bases: 261709


    #####################################################
    ## Stats based on ONLY LONGEST ISOFORM per 'GENE':
    #####################################################

	Contig N10: 2576
	Contig N20: 2016
	Contig N30: 1594
	Contig N40: 1369
	Contig N50: 1090

	Median contig length: 406
	Average contig: 695.00
	Total assembled bases: 255064

El tipo de resultados que despliega corresponde a:

    * El número de genes y transcritos que fueron ensamblados.
    * El porcentaje de GC.
    * Las estadísticas sobre el tamaño medio de los *contigs*.
    * El número de bases ensambladas.
    * Las estadísticas sobre el tamaño medio de la isoforma más larga de cada gen.

    NOTA: Es muy importante el valor del *contig* N40 y N50, en donde el primero indica
    un tamaño de *contig* del 40% y el segundo indica un tamaño del 50% (*contig* medio).
    Esta medida nos ayuda a basarnos en el *contig* más largo y a observar como aumenta 
    la longitud en todos los transcritos.
    
    
#### *Jueves 14 de marzo de 2019*


**Continuación de la práctica: Ensamblaje *de novo* de transcriptomas**

Teniendo el transcriptoma de *S. pombe* ensamblado *de novo* se procedió a  a realizar un blast de las primeras 5 secuencias para identificar a cuál microorganismo corresponden en *NCBI Blast*. Por lo que se realizó la opción de *blast* de nucleótidos a proteínas.

    >TRINITY_DN179_c0_g1_i1 len=1779 path=[0:0-1778]
    GCAACGTTCTGTTTTGGGAAATAGCGAAATGTCTAGCCAAGTTAGTTATTTATCGACTCGTGGTGGATCTTCAAATTTTTC
    TTTTGAAGAGGCTGTGCTTAAGGGTTTGGCTAATGATGGAGGATTGTTTATTCCATCAGAAATCCCTCAGCTACCTAGCGG
    ATGGATTGAGGCTTGGAAGGACAAATCCTTCCCTGAAATTGCCTTTGAGGTAATGTCTTTGTACATTCCTCGCTCTGAAAT
    TTCAGCTGATGAATTGAAAAAATTGGTTGATCGCTCTTACTCTACCTTCCGCCACCCAGAGACTACTCCTCTAAAATCATT
    AAAGAACGGTCTCAATGTACTTGAGCTCTTCCATGGTCCTACTTTTGCGTTCAAAGATGTTGCTTTGCAATTTCTTGGTAA
    CCTCTTTGAATTTTTCCTTACTCGTAAGAACGGAAACAAACCTGAAGACGAACGTGATCATCTGACCGTGGTTGGTGCCAC
    TAGTGGTGACACTGGTAGTGCAGCTATCTATGGCTTACGTGGCAAGAAGGACGTTTCAGTTTTTATCTTATTTCCCAATGG
    ACGTGTTTCCCCTATCCAAGAAGCTCAGATGACTACAGTCACAGATCCTAACGTTCACTGTATCACTGTGAACGGCGTCTT
    TGACGATTGTCAAGATCTTGTTAAACAAATTTTTGGCGATGTTGAGTTTAATAAGAAGCATCATATTGGTGCCGTTAATTC
    TATTAACTGGGCTCGTATTCTCTCTCAAATTACCTATTACTTATATTCTTATCTTTCCGTTTACAAGCAAGGAAAGGCTGA
    CGATGTTCGTTTCATCGTTCCCACCGGTAATTTTGGTGATATTCTCGCTGGTTACTACGCCAAGCGTATGGGCTTACCTAC
    TAAACAACTTGTTATTGCTACTAACGAGAATGATATTTTGAATCGTTTCTTCAAGACTGGTCGTTATGAAAAAGCTGATTC
    TACTCAAGTTTCGCCCAGTGGACCTATTTCCGCCAAAGAAACTTACTCTCCTGCAATGGACATTTTGGTTTCTTCTAACTT
    CGAGCGTTACTTATGGTATCTTGCATTAGCCACTGAGGCTCCTAATCATACTCCTGCTGAGGCCTCGGAAATTTTGTCTCG
    CTGGATGAATGAATTCAAACGTGATGGAACAGTTACTGTCCGTCCCGAGGTTTTGGAAGCCGCTAGACGTGATTTCGTGAG
    TGAACGTGTTAGTAATGATGAAACCATTGATGCTATTAAAAAAATCTATGAAAGTGATCATTATATCATCGACCCACACAC
    TGCTGTTGGCGTCGAAACTGGTCTTCGTTGCTTAGAGAAGACTAAGGACCAGGATATTACCTATATTTGCCTTTCCACAGC
    TCATCCTGCTAAGTTTGACAAAGCAGTAAACCTTGCTCTTTCTTCTTATAGTGATTATAACTTTAACACACAAGTATTACC
    GATAGAGTTTGATGGCCTCTTGGATGAGGAGCGCACCTGCATTTTCTCAGGAAAACCCAATATTGACATTCTTAAACAAAT
    TATTGAAGTTACTCTCAGCAGAGAGAAAGCTTAGAGGATTTAGGGATACCATTGTTACCGTTACTTTGTTTCTTTTCAAAA
    AAAAAATAAAAAAATCTGATTCCCGGTATTTTTTAGTTATATAGAAGTTGACTTTACATATCCATAGTATTAATTCGTTTT
    TGGAGTCGTCAAAATTTTTGAGGTTTAAGCTTAATCTTTAGCAAGACACTTTCATCCATTCTTTTGTTTATTTTTTTC
    
    >TRINITY_DN119_c0_g1_i1 len=1388 path=[0:0-1387]
    GTGTTATTAGGACTATTCCTCCAAATAGTCTTGATCTTATGCATTCAGTCAGCGTAAGTGATATTTGGCCCCTTAAAGTTAA
    TTATGAAACAAGTATTCCATCTAAGGTGTATGCAATTGGCTCAGAAATACCTGTTAACATTACTTTGTATCCTTTATTAAAA
    GGCCTTGATGTAGGAAAAGTGACTTTAGTTCTTAAAGAATACTGTACGCTTTTTATTACTTCCAAGGCCTATTCTTCTACAT
    GTAGAAAGGAGTTTAAGCGTGCATTAGTAAAGAAGACCATTCCGGGATTACCAATGGTGGATGATTATTGGCAAGACCAAAT
    TATGGTAAAGATTCCAGATTCCCTTGGCGAATGTACTCAAGACTGTGACCTAAATTGTATCCGAGTCCATCACAAACTAAGG
    TTGTCCATTTCGTTATTGAACCCTGATGGCCATGTATCAGAACTACGGAATTCTTTACCTTTAAGTCTTGTTATATCCCCAG
    TGATGTTTGGTGCTCGACCCACTGAAGGAGTATTCACTGGTGATCACAATTCCTATGTGAATGAAAATATTCTTCCAAGTTA
    CGACAAGCATGTATTTGATGTATTGTGGGATGGCATACCCTCGGAAAATCCACAGTTGCAAAGCGGGTTTACTACTCCTAAC
    TTAAGTCGTAGGAATTCTTCCGATTTCGGTCCAAACAGTCCAGTGAATATTCATTCTAATCCAGTTCCTATTTCTGGACAAC
    AACCGTCTTCTCCCGCTTCAAATTCCAATGCCAATTTTTTCTTTGGAAGCTCTCCACAGTCTATGTCTAGTGAGCAGACAGA
    TATGATGTCGCCAATAACTTCTCCATTAGCACCTTTTTCAGGAGTTACGCGGAGAGCCGCTAGGACGAGAGCTAACAGTGCC
    TCTTCTGTTTTCAATTCTCAGTTGCAACCATTACAAACGGATTTGCTTTCACCACTTCCTTCTCCAACCAGCAGTAATTCGA
    GGTTACCTCGTGTACGTAGCGCATGTACCCTTAATGTACAGGAATTGAGCAAAATTCCTCCGTACTACGAAGCTCACAGTGC
    GTTTACTAATGTCTTGCCGCTTGATGGGCTTCCCCGTTATGAAGAAGCTACAAGACCTTCAAGTCCTACTGAGTCTGTCGAG
    ATTCCTAGTAATACGACGACTATAGCGCCATCTCCTGTACCCACTATCATCGCACCAGCGCTTCCTTCAACTCCTGCGCCTC
    CGCTTCCTTCCCATCCTATGGCAACTAGGAAAAGTCTTTCTTCTACAAATCTCGTTCGAAGGGGAGTTCGATGAGCGTCTGT
    TAAAGTGGTTTATTTTCATTCCCTATGATCTAAGATCATTAATTTTAATGATTTTAAAAGTAAATTGGGTTTCAAG
    
    >TRINITY_DN119_c0_g2_i1 len=621 path=[0:0-620]
    ATTCAGTCCCGCAACCGTTTTGGTTGAAAATTGCTACATTCCTTCCGATTTTTTGGATTCAAATCTGTTCGTAGATATTCAG
    AGATACAAAATCTGGTACAGCACCAGAACTTATTATGAATGAATAAGATTAAAGGTGGCCGGCACAAAAGCCCCGTAAAGCT
    CTTTGAAGTTCGGCTTTACAATGCAGAGGCTAATGTGATTGTGCTATATGGAAACTCGATGGACACATCGGCACGATTATCA
    GGTATCGTAGTTCTTTCTGTATCTTCCCCCATTCGGGTAAAGAATATTAAATTACGGCTAAGTGGCCGTTCTTTCGTTTGTT
    GGGCTGATGAGTCCCGTCATGCATCGCCTGGTAACAGAATTCGACGTCAAGTTGTACAAATTCTGGATAAAAGCTGGTCTTT
    CCTTGCTCCCAACGAGTCAGCTAAAGTAATTGACCAAGGAAACTACGAGTATCCTTTTTATTACGAGCTCCCTCCAGATATT
    CCTGATTCAATTGAAGGAATTCCAGGATGTCATATTATCTATACACTCACTGCTTCTTTGGAGCGGGCAACTCAGCCTCCTA
    CCAATTTAGAAACTGCTCTGCAGTTTCGTGTTATTAGGACTATTCCC
    
    >TRINITY_DN123_c0_g1_i1 len=205 path=[0:0-204]
    GACTTTTGCATTCTGGGATGTGGATCAAGAATATCCTGTACAAGTCCGAAATTTTGTCGATTCAAACATACACACGTATACC
    CCCATGCAGAGAAATCCTCCTAAGACGGAGCTTGAGCCCATTAGGTCTATGAGGTGGTGTTGTTGTGAAGACCCAACAGTTT
    CATTTATTTTGATGTTAGGTGGTTTGCCGAAAGAAGCACCG
    
    >TRINITY_DN139_c0_g1_i1 len=207 path=[0:0-206]
    GTGAAAAGTGAATTAAACCATCCCATCTGAGCTTTGGCACGATTAATGACTAAAGTGCTAGCTGCAAAATCAACGTCCACGG
    AGATGGATTGTAATCTGTATGGATCATCAGAATCAATGATAATACGCCCCAGACAATATCCTGTTGTGCAGCCTTCTGTGTA
    ATTGATACTGAGTGTGACATTGAACTCGAATGCGGTATTGCTT

Como resultado se encontró que en su mayoría son secuencias de la enzima Treonina sintasa de varias especies de *Schizosaccharomyces pombe*.

Como anteriormente se realizó el ensamblaje sin filtrar las secuencias ya que se asumió que lo primero ya se había realizado, por lo que se procedió ahora a realizar reensamblaje filtrando por calidad usando *trimmomatic* desde *Trinity* de la siguiente manera:

    $ Trinity --trimmomatic --seqType fq --SS_lib_type RF --max_memory 1G --left 
    Sp_log.left.fq.gz Sp_hs.left.fq.gz Sp_ds.left.fq.gz Sp_plat.left.fq.gz --right 
    Sp_log.right.fq.gz Sp_hs.right.fq.gz Sp_ds.right.fq.gz Sp_plat.right.fq.gz --CPU 2
    --output /home/fani/Documentos/I_Semestre_2019/Tesis/Tutorial_transcriptomica/
    FASTQ_Complete/trinity_files_trimmo

Una vez reensamblado el genoma de *S. pombe* se corrió nuevamente un *blast* en *NCBI* de nucleótidos a proteínas.
Se obtuvo como resultado que la proteína corresponde a la subunidad *Cnd3* de un complejo de condensina presente en especies de *Schizosaccharomyces*.



#### *Viernes 15 de marzo de 2019*


**Alineamiento de lecturas**

Para poder realizar esta práctica de alineamiento se necesitaron los resultados del reensamblaje completo del transcriptoma de *S. pombe* y los archivos "fastq" de las lecturas filtradas con *trimmomatic* en *Trinity*.
Para empezar a trabajar se creó una carpeta llamada MAP y se descargó allí dentro la secuencia del genoma de *Shizosaccharomyces pombe*:

    $ mkdir MAP
    $ cd MAP
    $ wget https://liz-fernandez.github.io/Talleres_Bioinfo_Cuernavaca_17/datasets/
    genome/Sp_genome.fa ./

Posteriormente se verificó que los datos que se iban a usar eran correctos, para el ejercicio sólo se usaron las lecturas que conservaron sus pares luego de haber sido filtradas con *trimmomatic*. Los datos se revisaron sin necesidad de ser descomprimidos:

    $ for file in /home/fani/Documentos/I_Semestre_2019/Tesis/Tutorial_transcriptomica
    /FASTQ_Complete/trinity_files_trimmo/*.P.qtrim.gz ; do echo $file; zcat $file | 
    head ; done

Una vez realizadas las lecturas se procedió a realizar el mapeo usando *Bowtie2* a través de *TopHat*. Para realizar eso primeramente se creó un índice de *bowtie2* para el genoma de la levadura.

    $ bowtie2-build Sp_genome.fa Sp_genome
    
Se revisó los archivos generados en la carpeta MAP luego de generar el índice:

    $ ls
    Sp_genome.1.bt2        
    Sp_genome.2.bt2              
    Sp_genome.3.bt2       
    Sp_genome.4.bt2           
    Sp_genome.rev.1.bt2          
    Sp_genome.rev.2.bt2
    
Para los archivos "fastq" filtrados se creó un *hiperlinks* en el directorio local (carpeta MAP):

    $ ln -s /home/fani/Documentos/I_Semestre_2019/Tesis/Tutorial_transcriptomica/
    FASTQ_Complete/trinity_files_trimmo/*.P.qtrim.gz .
    

#### *Domingo 17 de marzo de 2019*


**Continuación de la práctica: Alineamiento de lecturas**

Una vez creado el índice se procedió a mapear con *TopHat*, pero antes se instaló en la computadora a través de *Bioconda*:

    $ conda install tophat
    
    NOTA: TopHat se encarga de mapear lecturas a una referencia (el genoma en este caso)
    incluyendo lecturas empalmadas que servirán como evidencia para eventos de empalme 
    (*splicing*).

El mapeo con *TopHat* se realizó de la siguiente usando el siguiente *script*:

    $ tophat2 -I 300 -i 20 Sp_genome \ Sp_log.left.fq.gz.P.qtrim.gz,
    Sp_hs.left.fq.gz.P.qtrim.gz,Sp_ds.left.fq.gz.P.qtrim.gz,
    Sp_plat.left.fq.gz.P.qtrim.gz \ Sp_log.right.fq.gz.P.qtrim.gz,
    Sp_hs.right.fq.gz.P.qtrim.gz,Sp_ds.right.fq.gz.P.qtrim.gz,
    Sp_plat.right.fq.gz.P.qtrim.gz

El formato de los archivos de salida (*output*) es en formato bam, el cuál es una versión comprimida de los archivos en formato sam.

    NOTA: *"El formato SAM"*
    Es un formato de texto plano que permite guardar datos de secuenciación en formato 
    ASCII delimitado por tabulaciones, y este último es un código de caracteres basado 
    en el alfabeto latino. El formato SAM está compuesto por:
    1) Encabezado: Empieza con el caracter @ seguido de uno de los códigos de 12 letras 
    usados para características de alineamientos, y cada línea está delimitada por 
    tabulaciones.
    2) Alineamiento: Contiene la siguiente información:
        * QNAME: Nombre de la referencia que sirve para agrupar alineamientos que están 
        juntos.
        * FLAG: Información que describe el alineamiento (si es múltiple, si los 
        fragmentos se alinearon bien, si la referencia de la cadena es inversa, etc.).
        * RNAME: Nombre de la secuencia de referencia.
        * POS: Posición del alineamiento izquierdo.
        * MAPQ: Calidad del alineamiento.
        * CIGAR: Se refiere a la cadena CIGAR, la cuál codifica para cada base y la 
        caraterística de cada una en el alineamiento.
        * RNEXT: Nombre de referencia del par o la siguiente lectura.
        * PNEXT: Posición del par o la siguiente lectura.
        * TLEN: Longitud del alineamiento.
        * SEQ: La secuencia de prueba del alineamiento.
        *QUAL: La calidad de la lectura.
        *TAGS: Información adicional.
        
Se procedió a revisar el archivo BAM usando el siguiente *script*:

    $ samtools view tophat_out/accepted_hits.bam | head
    
    61G9EAAXX100520:5:109:1498:818	161	genome	126	50	67M	=	360	286	
    GACTTGCTTATAAAGCCATCACTGGACAAACGGTAGCGATAAAAGTACACCCCCTTTCTTTTTTTTT	
    GGGGGGFGGGFGGGGGGFGGFFGFFFEFGGEFFAEFFFEEEDFEBCDFDC@E.A<CC?BC?C;?C??	
    AS:i:0	XN:i:0	XM:i:0	XO:i:0	XG:i:0	NM:i:0	MD:Z:67	YT:Z:UU	NH:i:1
    61G9EAAXX100520:5:113:17645:11983	161	genome	140	50	68M	=	461	389	
    GCCATCACTGGACAAACGGTAGCGATAAAAGTACACCCCCTTTCTTTTTTTTTCTGCTGTTTTTTCTG	
    GGGDGGGFFDBDGGG=DD?@FFFFBEGDGGAAFFDFGGGGD5FBCFAG>GGGGFGABEDDEGGDA6>8	
    AS:i:0	XN:i:0	XM:i:0	XO:i:0	XG:i:0	NM:i:0	MD:Z:68	YT:Z:UU	NH:i:1
    61G9EAAXX100520:5:111:12701:2062	161	genome	155	50	68M	=	484	387	
    ACGGTAGCGATAAAAGTACACCCCCTTTCTTTTTTTTTCTGCTGTTTTTTCTGCCTTTCTCTCTCTTT	
    FGGEFFGGG?GGGFGGCGFFEFGGEGGGGGGGGGDGGG=C/@?=A@DB><3>:CCBEECC=CA:?AAA	
    AS:i:0	XN:i:0	XM:i:0	XO:i:0	XG:i:0	NM:i:0	MD:Z:68	YT:Z:UU	NH:i:1
    61G9EAAXX100520:5:13:8118:19656	161	genome	157	50	47M	=	469	376	
    GGTAGCGATAAAAGTACACCCCCTTTCTTTTTTTTTCTGCTGTTTTTEECDEDFFFFDG>
    GFGGGDGGGGGFABGGFGGGGGC767@?=BBCEC	AS:i:0	XN:i:0	XM:i:0	XO:i:0	
    XG:i:0	NM:i:0	MD:Z:47	YT:Z:UU	NH:i:1
    61G9EAAXX100520:5:11:4190:7637	161	genome	162	50	67M	=	475	381	
    CGATAAAAGTACACCCCCTTTCTTTTTTTTTCTGCTGTTTTTTCTGCCTTTCTCTCTCTTTGTTTTC	
    EFDGFGGGGGFGEFGGF@GGFGGGGGGGGGFGGEGG=EGGGFC@88CDEEC5=>?4>>BCFDFGGC?	
    AS:i:0	XN:i:0	XM:i:0	XO:i:0	XG:i:0	NM:i:0	MD:Z:67	YT:Z:UU	NH:i:1
    61G9EAAXX100520:5:112:3468:17262	161	genome	173	50	68M	=	352	247	
    CACCCCCTTTCTTTTTTTTTCTGCTGTTTTTTCTGCCTTTCTCTCTCTTTGTTTTCATTTCTTTTTTT	
    GGGGGGGDGFGDGGEGGGGGGFEGGGFGGGGGFBDFGGGGEDGGEGFGGGGFEEGFGGGGGGGFACEC	
    AS:i:0	XN:i:0	XM:i:0	XO:i:0	XG:i:0	NM:i:0	MD:Z:68	YT:Z:UU	NH:i:1
    61G9EAAXX100520:5:102:2982:1585	161	genome	179	50	68M	=	521	409	
    CTTTCTTTTTTTTTCTGCTGTTTTTTCTGCCTTTCTCTCTCTTTGTTTTCATTTCTTTTTTTTCTGTT	
    GGGGGGGGGGGGGFFGGGGGGEGGGDGGGGGFGGFFEGGGDGGEFGGFGDEG?GFFGGGEGGDDA/A:	
    AS:i:0	XN:i:0	XM:i:0	XO:i:0	XG:i:0	NM:i:0	MD:Z:68	YT:Z:UU	NH:i:1
    61G9EAAXX100520:5:114:4503:16988	161	genome	188	50	68M	=	460	340	
    TTTTTCTGCTGTTTTTTCTGCCTTTCTCTCTCTTTGTTTTCATTTCTTTTTTTTCTGTTCTTTTTTTT	
    GGGGGGGGGGGGGGGGGGGGGFGGGGGGGGGGGGFGEGGGGBGGDEGGDGGGGC8DFGEFGGDEGFGG	
    AS:i:0	XN:i:0	XM:i:0	XO:i:0	XG:i:0	NM:i:0	MD:Z:68	YT:Z:UU	NH:i:1
    61G9EAAXX100520:5:10:17966:13959	161	genome	195	50	68M	=	421	294	
    GCTGTTTTTTCTGCCTTTCTCTCTCTTTGTTTTCATTTCTTTTTTTTCTGTTCTTTTTTTTTTTTAAT	
    GGGFGGGGGGGGGGGGGGGGGGGFGGGGGFGGEGGGGGGGGGGGFGFGGFEGFGGGGGGGGGGGG'@D	
    AS:i:0	XN:i:0	XM:i:0	XO:i:0	XG:i:0	NM:i:0	MD:Z:68	YT:Z:UU	NH:i:1
    61G9EAAXX100520:5:118:16080:8660	161	genome	195	50	68M	=	408	281	
    GCTGTTTTTTCTGCCTTTCTCTCTCTTTGTTTTCATTTCTTTTTTTTCTGTTCTTTTTTTTTTTTAAT	
    GGFGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGFGFGGGGGGGGGGGGGGDGGGGGGGGGGFA?=?	
    AS:i:0	XN:i:0	XM:i:0	XO:i:0	XG:i:0	NM:i:0	MD:Z:68	YT:Z:UU	NH:i:1



#### *Martes 19 de marzo de 2019*


**Ensamble guiado del transcriptoma de *S. pombe***


    NOTA: Este tipo de ensamblaje consiste en tomar pequeñas lecturas de RNA-seq y 
    convertirlas a transcritos completos usando un genoma como referencia.
    
Para esta práctica se usaron las mismas lecturas descargadas para el ensamblaje *de novo* y para empezar se creó una carpeta dentro del directorio "FASTQ_Complete".

    $ cd FASTQ_Complete
    $ mkdir CUFFLINKS
    $ cd CUFFLINKS
    
Se generó un acceso directo para el archivo del genoma de *S. pombe* y las lecturas:

    $ ln -s ../MAPPING/Sp_genome.fa
    $ ln -s ../Sp_*fq.gz .
    
Posteriormente se usó *Bowtie2* para crear un índice a partir del genoma de *S. pombe* y se realizó el alineamiento con *TopHat*:

    $ bowtie2-build Sp_genome.fa Sp_genome
    $ tophat2 -I 1000 -i 20 --library-type fr-firststrand -o tophat.Sp_ds.dir 
    Sp_genome Sp_ds.left.fq.gz Sp_ds.right.fq.gz
        
Los argumentos utilizados para correr el *TopHat* fueron los siguientes:

    * -I 1000: Indica el tamaño máximo del intron.
    * -i 20: Indica el tamaño mínimo del intron.
    * --library-tyoe fr-firststrand: Indica que la libería fue contruída usando lecturas
    en pares (*paired-end*) en la orientación RF (*reverse-forward*).
    * -o tophat.Sp_ds.dir: Indica el directorio donde se desea guardar los resultados 
    (*output*).
    * Sp_ds.left.fq.gz: Indica lecturas del lado izquierdo (o R2).
    * Sp_ds.right.fq.gz: Indica lecturas del lado derecho (o R2).
    
Una vez finalizado el alineamiento se renombró el archivo *output* de tal manera que concuerde con la muestra analizada y se creó un índice usando *samtools* para poder visualizar posteriormente en otra práctica:

    $ mv tophat.Sp_ds.dir/accepted_hits.bam tophat.Sp_ds.dir/Sp_ds.bam
    $ samtools index tophat.Sp_ds.dir/Sp_ds.bam
    
Posteriormente se procedió a reconstruir los transcritos para la muestra que se estuvo trabajando (Sp_ds), usando *Cufflinks*, para lo cuál es necesario instalar el programa:

    $ conda install cufflinks
    $ cufflinks --overlap-radius 1 \ --library-type fr-firststrand \ 
    -o cufflinks.Sp_ds.dir tophat.Sp_ds.dir/Sp_ds.bam

Una vez finalizada la reconstrucción se renombró el archivo de salida de *cufflinks* para la muetsra Sp_ds:

    $ mv cufflinks.Sp_ds.dir/transcripts.gtf cufflinks.Sp_ds.dir/Sp_ds.transcripts.gtf
    
Luego se repitió los mismo para las muestras restantes:

######**Para la muestra *Sp_log*:**
    
    $ tophat2 -I 1000 -i 20 --library-type fr-firststrand -o tophat.Sp_log.dir 
    Sp_genome Sp_log.left.fq.gz Sp_log.right.fq.gz
    
    $ mv tophat.Sp_log.dir/accepted_hits.bam tophat.Sp_log.dir/Sp_log.bam

    $ samtools index tophat.Sp_log.dir/Sp_log.bam

    $ cufflinks --overlap-radius 1 \ --library-type fr-firststrand \ 
    -o cufflinks.Sp_log.dir tophat.Sp_log.dir/Sp_log.bam

    $ mv cufflinks.Sp_log.dir/transcripts.gtf 
    cufflinks.Sp_log.dir/Sp_log.transcripts.gt
    
######**Para la muestra *Sp_hs*:**  

    $ tophat2 -I 1000 -i 20 --library-type fr-firststrand -o tophat.Sp_hs.dir 
    Sp_genome Sp_hs.left.fq.gz Sp_hs.right.fq.gz

    $ mv tophat.Sp_hs.dir/accepted_hits.bam tophat.Sp_hs.dir/Sp_hs.bam

    $ samtools index tophat.Sp_hs.dir/Sp_hs.bam

    $ cufflinks --overlap-radius 1 \ --library-type fr-firststrand \
    -o cufflinks.Sp_hs.dir tophat.Sp_hs.dir/Sp_hs.bam

    $ mv cufflinks.Sp_hs.dir/transcripts.gtf 
    cufflinks.Sp_hs.dir/Sp_hs.transcripts.gtf

     
######**Para la muestra *Sp_plat*:**

    $ tophat2 -I 1000 -i 20 --library-type fr-firststrand -o tophat.Sp_plat.dir 
    Sp_genome Sp_plat.left.fq.gz Sp_plat.right.fq.gz

    $ mv tophat.Sp_plat.dir/accepted_hits.bam tophat.Sp_plat.dir/Sp_plat.bam

    $ samtools index tophat.Sp_plat.dir/Sp_plat.bam

    $ cufflinks --overlap-radius 1 \ --library-type fr-firststrand \
    -o cufflinks.Sp_plat.dir tophat.Sp_plat.dir/Sp_plat.bam

    $ mv cufflinks.Sp_plat.dir/transcripts.gtf 
    cufflinks.Sp_plat.dir/Sp_plat.transcripts.gtf
    
Es importante tomar en cuenta que los comandos se corren de manera secuencial, es decir, hay que esperar a que el anterior termine para continuar con el siguiente:
Finalmente se procedió a crear un archivo maestro con coordenadas gtf que se utilizará para visualizar el transcriptoma. El archivo se creó de la siguiente manera:

    $ echo cufflinks.Sp_ds.dir/Sp_ds.transcripts.gtf > assemblies.txt
    $ echo cufflinks.Sp_hs.dir/Sp_hs.transcripts.gtf >> assemblies.txt
    $ echo cufflinks.Sp_log.dir/Sp_log.transcripts.gtf >> assemblies.txt
    $ echo cufflinks.Sp_plat.dir/Sp_plat.transcripts.gtf >> assemblies.txt 

La operación se realizó para cada una de las muestras (*ds*, *log*, *has* y *plat*).
Una vez creado el archivo se verificó que incluyera el nombre de todos los archivos gtf:
 
    $ cat assemblies.txt
    cufflinks.Sp_ds.dir/Sp_ds.transcripts.gtf
    cufflinks.Sp_hs.dir/Sp_hs.transcripts.gtf
    cufflinks.Sp_log.dir/Sp_log.transcripts.gtf
    cufflinks.Sp_plat.dir/Sp_plat.transcripts.gtf
    
Ya que el archivo de coordenadas llamado *"assemblies.txt"* posee todos los archivos de las muestras, se unieron en uno sólo, usando la opción *cuffmerged* de *cufflinks*:

    $ cuffmerge -s Sp_genome.fa assemblies.txt
    
Se verifó la creación del archivo concatenado "merged.gtf" que se guardó en la carpeta que fue creada con el nombre de "merged_asm":

    $ head merged_asm/merged.gtf
    
Una vez teniendo el archivo "merged.gtf" se visualizó en el programa *IGV*.
El programa *IGV* se corrió desde la terminal con el comando:

    $ ./igv.sh
    
En caso de no poder correrlo se debe cambiar la configuración para la protección contra escritura con *chmod* o hacer lo siguiente antes de correrlo:

    $ chmod a+x igv.sh
