# **Bit�cora: Agosto**



#### *Jueves 29 de agosto de 2019*


Desde la �ltima vez que se trabajo en la identificaci�n de lncRNAs se dejo ah� porque no se pudo continuar esto porque hac�a falta un archivo de genes codificantes de prote�nas tanto para Chlamy como para BBRB. Por lo que se procedi� primero a buscar nuevamente esos archivos para continuar los an�lisis.

Se intento de nuevo hacer el match del archivo de bed12 de lncRNAs de BBRB y el archivo de coordenadas gff3 de BBRB en formato bed12.

Se corrio de nuevo con el archivo "Bbrauniishowa_gene.bed12" que es el archivo "Bbrauniishowav1.1.gene.bed12" renombrado (todo esto esta en la carpeta ".../chlamy/CPC/gtf_to_bed12"


    $ awk 'FNR==NR{a[$4];next}($1 in a){print}' /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/BBRB_Tim/nc_genes_BBRB /home/fani/
	Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Bbrauniishowav1.1.gene_exons.bed12 > BBRB_ncrnas.bed12

NOTA: NO FUNCIONO, POR LO QUE SE DECIDI� MEJOR CONTINUAR CON LA IDENTIFICACI�N DE *lncRNAs* DE CHLAMY.