# **Bitacora: Septiembre**



#### *Martes 3 de septiembre de 2019*


## Continuaci�n de la identificaci�n de *lncRNAs* de Chlamy

Se us� el archivo de coordenasas en formato *gtf* de Chlamy (uno que se hab�a obtenido hace mucho a partir de una conversi�n de un archivo formato 
*gff3*), el archivo se filtr� para obtener todos los genes codificantes de la siguiente manera:
	
	$ grep grep "CDS" Chlamydomonas_reinhardtii_v5.5.43.gtf > Chlamydomonas_reinhardtii_CDS.gtf

El archivo *gtf* se convirti� a *bed12* de la siguiente manera:

	$ ./gtfToGenePred /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Chlamydomonas_reinhardtii_CDS.gtf 
	/home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Chlamydomonas_reinhardtii_CDS.genePred
	$ ./genePredToBed /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Chlamydomonas_reinhardtii_CDS.genePred 
	/home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Chlamydomonas_reinhardtii_CDS.bed12
	

Se clasific� *lncRNAs* superpuestos con genes codificantes intersetando la lista de *lncRNAs* de Chlamy con la lista de genes que codifican para 
prote�nas de la microalga, esto usando el programa *bedtools*, usando ambos archivos en formato *bed12* (primeramente estaban en formato *gtf* y 
luego se convirtieron a *bed12*).

	$ bedtools intersect -a /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/chlamy_ncrnas.bed12 
	-b /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Chlamydomonas_reinhardtii_CDS.bed12 
	> /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/lncrnas_found/chlamy_lnc_overlap.bed12


NOTA: El archivo que se gener� estaba vac�o, se revisar� despu�s.



#### *Mi�rcoles 4 de septiembre*

Parece que no hay lncRNAs suerpuestos con genes codificantes, pero si *lncRNAs* interg�nicos, ya que se corri� el *script* para intersectar *lncRNAs* 
interg�nicos y el *output* mostr� resultados, a diferencia del anterior:

	$ bedtools intersect -a /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/chlamy_ncrnas.bed12 
	-b /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Chlamydomonas_reinhardtii_CDS.bed12 
	-v > /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/lncrnas_found/chlamy_linc.bed12
	
	$ wc -l chlamy_linc.bed12  
	
NOTA: El archivo de salida mostr� un total de 26 *lncRNAs* que presuntamente son interg�nicos.


Se opto por clasificar los transcritos interg�nicos filtrando el archivo de *lincRNAs* de chlamy obtenido del *script* anterior, usando una 
ventana de 500 nt:

Para filtrar los que caen dentro de la ventana de 500 nt en el extremo 5' o el 3'  de la regi�n codificante se us� el siguiente *script*:

	$ bedtools window -a /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/lncrnas_found/chlamy_linc.bed12 
	-b /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Chlamydomonas_reinhardtii_CDS.bed12 -w 500 
	> chlamy_nearw500.bed12

	$ less chlamy_nearw500.bed12
	
NOTA: El archivo *chlamy_nearw500.bed12* se encontr� vac�o.

Para filtrar los que no caen dentro de esa ventana que son considerados interg�nicos se us� el siguente *script*:

	$ bedtools window -a /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/lncrnas_found/chlamy_linc.bed12 
	-b /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Chlamydomonas_reinhardtii_CDS.bed12 -w 500 
	-v > chlamy_lincw500.bed12

	$ wc -l chlamy_lincw500.bed12  

NOTA: El archivo de salida *chlamy_lincw500.bed12* mostr� un total de 26 *lincRNAs* identificados para Chlamy.


## Continuaci�n de la identificaci�n de *lncRNAs* de BBRB

Se hizo un *match* con la tabla de *lncRNAs* de BBRB obtenidos del analisis con *CPC* y el archivo de anotaci�n en formato *gtf* de genes de la 
microalga, tomado de la carpeta de drive "B race contigs" compartida por el Dr. Tim. El archivo descargado fue en formato *gff3* y se covirti�
primero a formato *gtf* y luego a *bed12*:

Conversi�n de *gff3* a *bed12*:

	$ gffread Bbrauniishowav1.1.gene.gff3 -T -o Bbrauniishowav1.1.gene.gtf
	$ ./gtfToGenePred /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Bbrauniishowav1.1.gene.gtf 
	/home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Bbrauniishowav_genes.genePred
	$ ./genePredToBed /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Bbrauniishowav_genes.genePred 
	/home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Bbrauniishowav_genes.bed12

*Match* entre tabla de *lncRNAs* y archivo de coordenadas de BBRB:

	$ awk 'FNR==NR{a[$1];next}($4 in a){print}' /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/BBRB_Tim/nc_genes_BBRB 
	/home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Bbrauniishowav_genes.bed12 > BBRB_ncrnas.bed12


NOTA: El archivo de bed12 generado del *match* est� vac�o y la raz�n es porque no coincide el ID de las secuencias de la tabla de *lncRNAs* de 
BBRB con el ID de las secuencias del archivo *bed12* del transcriptoma de la microalga.


#### *Martes 10 de septiembre*


Se us� un *script* de *perl* obtenido de (http://bioops.info/2012/11/intron-size-gff3-perl/) para obtener el tam�o de los intrones a partir de 
un arhivo *gff3* de coordenadas del genoma de Chlamy, de tal manera que se pueda usar el archivo de salida como plantilla para clasificar *lncRNAs*
intr�nicos.

Primero se convirti� el archivo *gff3* a formato *gtf* y luego a *bed12*:

	$ ./gtfToGenePred /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Chlamydomonas_reinhardtii_v5.5.43.gtf 
	/home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Chlamydomonas_reinhardtii_v5.5.43.genePred
	$ ./genePredToBed /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Chlamydomonas_reinhardtii_v5.5.43.genePred 
	/home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Chlamydomonas_reinhardtii_v5.5.43.bed

Luego se obtuvo el tama�o de los intrones con el *script* de *perl*:

	$ perl get_intron_from_bed12.pl /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Chlamydomonas_reinhardtii_v5.5.43.bed 
	> /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/intron_sizes.gff3


El script de perl usado fue el siguiente:


	#!/usr/bin/perl
	use strict;
	use warnings;

	# usage: perl get_intron_size.pl gff3_file >output

	my $input=$ARGV[0];
	my ($eachline,@exon);
	my $first=0;
	open (IN, "<$input") or die ("no such file!");
	while(defined($eachline=<IN>)){
	  if($eachline=~/\tmRNA\t/){
		$first++;
		if($first != 1){
		  print_intron(@exon);
		  @exon=();
		}
	  }elsif($eachline=~/\tCDS\t/){
		my @eachline=split(/\t/,$eachline);
		push (@exon, $eachline[3],$eachline[4]);
	  }
	}
	print_intron(@exon);

	sub print_intron{
	  my (@exon)=@_;
	  if(scalar(@exon)>2){
		my @ordered_exon=sort {$a<=>$b} @exon;
		for (my $i=1;$i<=scalar(@ordered_exon)-3;$i=$i+2){
		  my $each_intron_size=$ordered_exon[$i+1]-$ordered_exon[$i]-1;
		  print "$each_intron_size\t";
		}
	  }else{print "0";}
	  print "\n";
	}


El output en formato *gtf* se convirti� a *bed12*:

	$ ./gtfToGenePred /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/intron_sizes.gtf 
	/home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/intron_sizes.genePred
	$ ./genePredToBed /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/intron_sizes.genePred 
	/home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/intron_sizes.bed12


Se realiz� la intersecci�n para obtener *lncRNAs* intr�nicos, con el siguiente *script*:

	$ bedtools intersect -a /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/intron_sizesbed12 
	-b /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/chlamy_ncrnas.bed12 
	-f 1 -r > /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/lncrnas_found/chlamy_intron_lnc.bed12

NOTA: El archivo de salida se envi� a la carpeta *"lncrnas_found"* y el archivo se revis� pero no conten�a nada dentro. No funcion�!!



#### *Martes 17 de septiembre de 2019*


Con el *script* usado anteriormente no se obtuvo el resultado esperado, por lo que se us� otro *script* modificado de *perl* para identificar *lncRNAs*
intr�nicos. El *script* fue proporcionado por el maestro Irving.


	$ perl get_intron_gtf_from_bed.pl /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Chlamydomonas_reinhardtii_v5.5.43.bed 
	> /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/intron_size.gtf
 
 
El nuevo *script* de *perl* que se us� fue el siguiente (https://github.com/riverlee/IntronGTF/blob/master/get_intron_gtf_from_bed.pl):


	#!/usr/bin/env perl
	###################################
	# Author: Jiang (River) Li
	# Email:  riverlee2008@gmail.com
	# Date:   Sun Mar 31 16:52:45 2013
	###################################
	use strict;
	use warnings;

	my ($inbed,$outgtf) = @ARGV;
	my $usage="perl $0 gene.bed intron.gft\n";

	die $usage if (! $inbed || ! $outgtf || ! -e $inbed);

	# Bed file has 12 columns, the 4th columns is the gene name.
	# Since gene has multiple isoforms, so for these genes, we take the longest transcript with most exons 
	# to stand for the gene.

	#1) load bed 
	my %bed; #key are 4th column
	open(IN,$inbed) or die $!;
	my $totalline=0;
	while(<IN>){
		s/\r|\n//g;
		$totalline++;
		my @array = split "\t";
		push @{$bed{$array[3]}},[@array];
	}
	close IN;
	print "Total Line:   $totalline\n";
	print "Unique Genes: ",scalar(keys %bed),"\n";

	# Loop each gene and print out the gtf
	open(OUT,">$outgtf") or die $!;
	foreach my $gene(sort keys %bed){
		my $arrayref = $bed{$gene};
		my $strand = "+";
		my $start;
		my $end;

		my @array = get_longest_transcript_with_most_exons($arrayref);
		if($array[9] == 1){ #blockcount
			print "Gene '$array[3]' has no intron\n";
		}else{
			my @blocksizes = split ",", $array[10]; 
			my @blockstarts = split ",",$array[11];
			if($array[5] eq "+"){  #strand
				for(my $i=0;$i<$#blocksizes;$i++){
					my $intron_start = $array[1]+$blockstarts[$i]+$blocksizes[$i]+1;
					my $intron_end   = $array[1]+$blockstarts[$i+1];
					my $ii=$i+1;
					my $group = "gene_id \"$array[3]\"; transcript_id \"$array[3]\"; exon_number $ii\n";
					print OUT join "\t",($array[0],"intronGTF","exon",$intron_start,$intron_end,$array[4],$array[5],
											 0,$group);
				}
			}else{
				for(my $i=0;$i<$#blocksizes;$i++){
					my $intron_start = $array[1]+$blockstarts[$i]+$blocksizes[$i]+1;
					my $intron_end   = $array[1]+$blockstarts[$i+1];
					my $ii=$#blocksizes-$i;
					my $group = "gene_id \"$array[3]\"; transcript_id \"$array[3]\"; exon_number $ii\n";
					print OUT join "\t",($array[0],"intronGTF","exon",$intron_start,$intron_end,$array[4],$array[5],
											 0,$group);
				}
			}
		}
	}


	sub get_longest_transcript_with_most_exons{
		my ($ref) = @_;
		my $wanted_index=0;
		my $len=0;
		my $exons=0;
		for(my $i=0;$i<@{$ref};$i++){
			my $thislen=$ref->[$i]->[2]-$ref->[$i]->[1];
			if($thislen>$len){
				$wanted_index=$i;
				$len=$thislen;
				$exons=$ref->[$i]->[9];
			}elsif($thislen == $len){
				if($ref->[$i]->[9]>$exons){
					$exons=$ref->[$i]->[9];
					$wanted_index=$i;
				}
			}
		}
		return @{$ref->[$wanted_index]};
	}



NOTA: El archivo de salida estaba vac�o, indicando que no se detectaron intrones. 
Se observ� que el archivo *bed12* perdi� informaci�n a tr�ves de la conversi�n de *gtf* a este, debido a que un archivo *gff3* contempla
transcritos y regiones UTR's, mientras que el archivo *bed12* solo contempla los transcritos.

Como estrategia para intentar resolver el problema del *output* vac�o se cambi� el nombre de los cromosomas en la columna 1 del archivo *bed* 
de la anotaci�n del genoma de Chlamy, de tal manera que los nombres de los cromosomas estuvieran de manera consecutiva 
(https://serverfault.com/questions/319527/find-and-replace-text-with-sequence-of-numbers):

	$ awk 'BEGIN {count=1}; {if ($1 ~ /1/) {sub(/1/,"chr"(count++));print} else {print} }' 
	/home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Chlamydomonas_reinhardtii_v5.5.43.bed 
	> /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Chlamy_chrom_v5.5.43.bed

Se corri� de nuevo el *script* nuevo de *perl* usando el archivo *bed* con los cromosomas de Chlamy corregidos, llamado *Chlamy_chrom_v5.5.43.bed*:

	$ perl get_intron_gtf_from_bed.pl /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Chlamy_chrom_v5.5.43.bed 
	> /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/intron_size_new.gtf


#### *Jueves 19 de septiembre*

Se convirti� el archivo *gtf* *intron_new_size* con el tama�o de los intrones de Chlamy a formato *bed12*:


	$ ./gtfToGenePred /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/intron_size_new.gtf 
	/home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/intron_size_new.genePred
	$ ./genePredToBed /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/intron_size_new.genePred 
	/home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/intron_size_new.bed12


Usando el archivo *intron_size.bed12* se procedi� a identificar los *lncRNAs* intr�nicos con el siguiente *script*:


	$ bedtools intersect -a /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/intron_size_new.bed12 
	-b /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/chlamy_ncrnas.bed12 
	-f 1 -r > /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/lncrnas_found/chlamy_intron_lnc.bed12


NOTA: En la terminal se imprimi� el siguiente mensaje al correr el script

	WARNING: File /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/intron_size_new.bed12 has inconsistent naming convention for record:
	chr84767	4222	5444	transcript:PNW69870	0	-	5444	5444	0	1	1222,	0,

	WARNING: File /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/intron_size_new.bed12 has inconsistent naming convention for record:
	chr84767	4222	5444	transcript:PNW69870	0	-	5444	5444	0	1	1222,	0,

Y se observ� que el archivo de salida *chlamy_intron_lnc.bed12* se encontraba vac�o.


#### *Domingo 29 de septiembre*


Se envio de nuevo de correr los blastxy blastp con el mismo script ya usado en mazorka, sin embargo se observo que una de las corridas con el 
*blastp* se aborto dando el siguente error:

	terminate called after throwing an instance of 'ncbi::CSeqDBException'
	what():  NCBI C++ Exception:
	T0 "/home/coremake/release_build/build/PrepareRelease_Linux64-Centos_JSID_01_350334_130.14.22.10_9008__PrepareRelease_Linux64-Centos_
	1481139955/c++/compilers/unix/../../src/objtools/blast/seqdb_reader/seqdbatlas.cpp", line 1352: Error: BLASTDB::ncbi::CRegionMap::MapFile()
	- CSeqDBAtlas::MapFile: allocation failed for 900951970 bytes.

	/var/lib/torque/mom_priv/jobs/192237.mazorka.SC: l�nea 15: 28378 Abortado                
	blastp -query transcript_chlamy10000.pep -db /LUSTRE/storage/data/secuencias/NR/nr -num_threads 8 -max_target_seqs 1 -outfmt 6 
	> blastpep_chlamy_10000.outmt6

Por lo que se espero a conversar con Luisa para ver que tipo de error podria ser o preguntarle a Aracely.



#### *Lunes 30 de septiembre*


Con Luisa se vio el error del blast y se llego a la conclusion de que podria ser el input o un error en mazorka, por lo que se le pregunto a 
Aracely y ella realizo las correcciones pertinentes en el script del archivo que dio error, recomendando que corriera los demas blast con esa 
correccion. El script base nuevo fue el siguiente:


	#PBS -N BLASTpep_10000
	#PBS -q default
	#PBS -l nodes=1:ppn=8,vmem=18gb,walltime=999:00:00
	#PBS -o blastpep_10000.out
	#PBS -e blastpep_10000.err


	module load ncbi-blast+/2.6.0


	cd $PBS_O_WORKDIR


	blastp -query transcript_chlamy10000.pep -db /data/secuencias/NR/nr -num_threads 8 -max_target_seqs 1 -outfmt 6 
	-out blastpep_chlamy_10000.outmt6


NOTA: Lo que cambio con respecto al script anterior fue lo siguiente.

	1) wmem=18gb
	2) Se quito esto: #PBS -V
	3) Y en el output en ves de poner esto "> blastpep_chlamy_20000.outmt6" se puso esto "-out blastpep_chlamy_10000.outmt6"


Tomando en cuenta estos cambios se corrigio los demas scripts para las demas corridas de blast, lo que vario con los demas archivos fue lo 
siguiente:

	1) El nombre de los archivos de error de acuerdo al nombre del archivo y tipo de blast (blastx o blastp)
	2) El nombre del input (transcript_chlamy0, transcript_chlamy10000 o transcript_chlamy20000) y formato de 
	acuerdo al tipo de blast (".fasta" para blastx y ".pep" pata blastp).
	3) El nombre del output de acuerdo al nombre del archivo entrante y tipo de blast.
	
	
Las 6 corridas de *blast* se dejaron procesando en mazorka.


