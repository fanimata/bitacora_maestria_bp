# **Bitácora: Junio**



#### *Lunes 3 de junio de 2019*

Se continuó con en análisis en *R studio*, realizando los constrastes para la comparación entre los distintos tipos de condiciones de crecimiento 
de *C. reinhardtii*:

##  Contrastes entre grupos en una misma variable

Se realizó un contraste entre 2h y 12h en la variable *"time"*

    > resMFTime <- results(ds2MF, contrast = c("time", "2h", "12h"), alpha = 0.05)
    > head(resMFTime)
    > summary(resMFTime)
    > sum(resMFTime$padj < 0.05, na.rm = TRUE)

Se graficaron los resultados del constraste anterior:

    > plotMA(resMFTime, main = "Contrast 2h vs 12h times", ylim = c(-10, 10))

Se realizó otro contraste entre control y Ag dentro de la variables *condition*:

    > resMFCond <- results(ds2MF, contrast = c("condition", "control", "Ag"), alpha = 0.05)
    > head(resMFCond)
    > summary(resMFCond)
    > sum(resMFCond$padj < 0.05, na.rm = TRUE)

Se graficó también los resultados de dichos constrastes:

    > plotMA(resMFCond, main = "Constrast control vs Ag conditions", ylim = c(-10, 10))


##  Interacciones entre variables diferentes

En el objeto *ds2MF* se creó un grupo nuevo que incluyera las combinaciones entre las variables *time* y *condition*:

    > ds2MF$group <- factor(paste0(ds2MF$time, ds2MF$condition))

Se realizó una nueva matriz de diseño que incluyera todas las variables:
   
    > design(ds2MF) <- ~ group

Se aplicó la función *DESeq* al nuevo diseño:

    > ds2MF_all <- DESeq(ds2MF, test = "LRT", reduced = ~ 1, full = design(ds2MF), fitType = "local")
    > resultsNames(ds2MF_all)


##   Contrastes a partir de interacciones nuevas

Una vez realizadas las interacciones se utilizaron para generar nuevos contrastes entre las variables.

Se creó un constraste entre las muestras control y Ag con respecto a un tiempo de 2h, creando un objeto llamado *res2h*:

    > res2h <- results(ds2MF_all, contrast = c("group", "2hcontrol", "2hAg"), alpha = 0.05)
    > head(res2h)
    > summary(res2h)
    > sum(res2h$padj < 0.05, na.rm = TRUE)

Los resultados de *res2h* se este contraste nuevo se graficaron usando *plotMA*:

    > plotMA(res2h, main = "Contrast between control and Ag in 2h time", ylim = c(-10, 10))

Se hizo un contraste entre las muestras control y Ag pero esta vez con respecto a un tiempo de 12h y para ello se creó el objeto llamado *res12h*:

    > res12h <- results(ds2MF_all, contrast = c("group", "12hcontrol", "12hAg"), alpha = 0.05)
    > head(res12h)
    > summary(res12h)
    > sum(res12h$padj < 0.05, na.rm = TRUE)

Lo resultados de *res12h* fueron graficados:

    > plotMA(res12h, main = "Contrast between control and Ag in 12h time", ylim = c(-10, 10))

Otro contraste fue realizado entre los tiempos de 2h y 12h para la condición *control*, creando el objeto *resCtrl*:

    > resCtrl <- results(ds2MF_all, contrast = c("group", "2hcontrol", "12hcontrol"), alpha = 0.05)
    > head(resCtrl)
    > summary(resCtrl)
    > sum(resCtrl$padj < 0.05, na.rm = TRUE)

Lo resultados de *resCtrl* se graficaron:

    > plotMA(resCtrl, main = "Contrast between 2h and 12h in control condition", ylim = c(-10, 10))

Finalmente se realizó un contrate entre los tiempos 2h y 12h para la condición *Ag*:

    > resAg <- results(ds2MF_all, contrast = c("group", "2hAg", "12hAg"), alpha = 0.05)
    > head(resAg)
    > summary(resAg)
    > sum(resAg$padj < 0.05, na.rm = TRUE)

Los resultados del contraste anterior se graficaron a partir del objeto *resAg*:

    > plotMA(resAg, main = "Contrast between 2h and 12h in Ag condition", ylim = c(-10, 10))


#### *Viernes 7 de junio de 2019*


## Filtrado de genes

Se realizó un filtrado de genes que cuya expresión fuera significativa, esto con respecto al *p-value* ajustado:

    > sigT <- resMFTime[which(resMFTime$padj < 0.01), ]
    > sigC <- resMFCond[which(resMFCond$padj < 0.01), ]
    > sig2h <- res2h[which(res2h$padj < 0.01), ]
    > sig12h <- res12h[which(res12h$padj < 0.01), ]
    > sigCrtl <- resCtrl[which(resCtrl$padj < 0.01), ]
    > sigAg <- resAg[which(resAg$padj < 0.01), ]

Se realizó una búsqueda de los genes más expresados (genes top) ordenando los resultados del filtrado y generando tablas nuevas para cada contraste:

    > sigT <- sigT[order(sigT$padj), ]
    > sigC <- sigC[order(sigC$padj), ]
    > sig2h <- sig2h[order(sig2h$padj), ]
    > sig12h <- sig12h[order(sig12h$padj), ]
    > sigCrtl <- sigCrtl[order(sigCrtl$padj), ]
    > sigAg <- sigAg[order(sigAg$padj), ]

Se convirtió en un *data frame* cada tabla de cada contraste:

    > sigT <- as.data.frame(sigT)
    > sigC <- as.data.frame(sigC)
    > sig2h <- as.data.frame(sig2h)
    > sig12h <- as.data.frame(sig12h)
    > sigCrtl <- as.data.frame(sigCrtl)
    > sigAg <- as.data.frame(sigAg)


Se creó un *data frame* nuevo para cada contraste con valores de *log2 FC* y *p-value* ajustado:


Para el contraste *sigT*

    > stats_sigT <- sigT[, c("log2FoldChange", "padj")]
    > degVolcano(stats_sigT, title = "Volcano plot of log2 FC and padj from sigT contrast",
           point.colour = "green3", point.outline.colour = "green3")

Para el contraste *sigC*

    > stats_sigC <- sigC[, c("log2FoldChange", "padj")]
    > degVolcano(stats_sigC, title = "Volcano plot of log2 FC and padj from sigC contrast",
           point.colour = "green3", point.outline.colour = "green3")

Para el contraste *sig2h*

    > stats_sig2h <- sig2h[, c("log2FoldChange", "padj")]
    > degVolcano(stats_sig2h, title = "Volcano plot of log2 FC and padj from sig2h contrast",
           point.colour = "green3", point.outline.colour = "green3")

Para el contraste *sig12h*

    > stats_sig12h <- sig12h[, c("log2FoldChange", "padj")]
    > degVolcano(stats_sig12h, title = "Volcano plot of log2 FC and padj from sig2h contrast",
           point.colour = "green3", point.outline.colour = "green3")

Para el contraste *sigCtrl*

    > stats_sigCtrl <- sigCtrl[, c("log2FoldChange", "padj")]
    > degVolcano(stats_sigCtrl, title = "Volcano plot of log2 FC and padj from sig2h contrast",
           point.colour = "green3", point.outline.colour = "green3")

Para el contraste *sigAg*

    > stats_sigAg <- sigAg[, c("log2FoldChange", "padj")]
    > degVolcano(stats_sigAg, title = "Volcano plot of log2 FC and padj from sig2h contrast",
           point.colour = "green3", point.outline.colour = "green3")

Posteriormente se unió las columnas correspondientes a los valores de *log2 FC* de cada contraste y se hizo un nuevo *data frame* de los 6 diferentes
contrastes, con e nombre de *logFC*:

    > logFC <- cbind(sigT$log2FoldChange, sigC$log2FoldChange, sig2h$log2FoldChange,
               sig12h$log2FoldChange, sigCrtl$log2FoldChange, sigAg$log2FoldChange)

Se le cambió el nombre a las filas y a las columnas de acuerdo al tipo de contraste en *logFC*:

    > rownames(logFC) <- rownames(sigT)
    > colnames(logFC) <- c("sigT", "sigC", "sig2h", "sig12h", "sigCtrl", "sigAg")
    > head(logFC)
    > sapply(logFC, class)
    > sapply(logFC, mode)

Se convirtió *logFC* en un *data frame*: 

    > logFC <- as.data.frame(logFC)

NOTA: Quedó pendiente graficar los valores de *log2 FC* de cada contraste!


#### *Lunes 17 de junio de 2019*


##  Creación de annotación

Debido a que no se encontró un archivo de anotación funcional de *C. reinhardtii* que pudiera ser usado para hacer el análisis de enriquecimiento
funcional en *R sutudio* se procedió a generar una anotación para la microalga. Para ello primero se cargaron las librerías necesarias:

    > library(AnnotationDbi)
    > library(org.At.tair.db) 
    
NOTA: Debido a que no hay un paquete de anotación disponible en *R studio* para *C. renihardtii* se tuvo que usar el paquete *org.At.tair.db* de 
*A. thaliana*:

Se exploró el archivo de *or.At.tair.db*:

    > columns(org.At.tair.db)

Se pegó la información de cada *data frame* de los contrastes para el análisis:

Para *sigT*

    > sigT$symbol <- mapIds(org.At.tair.db, keys = row.names(sigT), column = "SYMBOL", 
                      keytype = "TAIR", multiVals = "first")


##### NOTA: Debido a que esto no funcionó se dejó a un lado para avanzar mientras tanto con la identificación de lncRNAs!



#### *Martes 18 de junio de 2019*



## Identificación de lncRNAs por el Método descrito por Cabili *et al.* (2011)


Se comenzó a trabajar en la identificación de lncRNAs a partir del transcriptoma ensamblado de *C. reinhardtii* con *Stringtie*, usando 
método de Cabili *et al.* (2011):

Se paso a una carpeta local en la computadora los archivos de st_merged_chlamy y el genoma de esta que se encontraban en mazorka:
	
	$ rsync -av smata@mazorka.langebio.cinvestav.mx:/LUSTRE/usuario/smata/chlamy/stringtie_ensam/merged_gtfs/st_merged_chlamy .

	$ rsync -av smata@mazorka.langebio.cinvestav.mx:/LUSTRE/usuario/smata/chlamy/datos_genoma/GCF_000002595.1_v3.0_genomic.fna .


A su vez se instaló *gffread* con bioconda desde la terminal para obtener un archivo fasta de las secuencias de transcritos a partir del 
archivo *gtf* del transcriptoma de Chlamy ensamblado con *Stringtie* y el genoma de la microalga en formato *fasta*:

	$ conda install gffread

	$ gffread -w transcript_chlamy.fasta -g GCF_000002595.1_v3.0_genomic.fna st_merged_chlamy.gtf

Para el *script* anteriormente los argumentos usados fueron los siguentes:

    -w: Indica el archivo de salida que queremos generar de las secuencias de los transcritos en formato *fasta*.
    -g: Indica uno de los archivos de entrada que corresponde a la secuencia del genoma del organismo en formato *fasta*.
    Y aunque no se indica como *flags* uno de los archivos de entrada es la anotación del transcriptoma generado en formato *gtf*
    del organismo a trabajar, en este caso para Chlamy fue el archivo *st_merged_chlamy.gtf*.
    

Se instalo *Trinotate* con *Bioconda* para desde la computadora para utilizar la base de datos *SQLite* de este:

	$ conda install trinotate

Se descargaron también las bases de datos de *Uniprot*, *Pfam* y *SQLite* (de *Trinotate*) en una carpeta en la computadora y se descomprimieron:

	$ wget https://data.broadinstitute.org/Trinity/Trinotate_v3_RESOURCES/uniprot_sprot.pep.gz
	$ gunzip uniprot_sprot.pep.gz

	$ wget https://data.broadinstitute.org/Trinity/Trinotate_v3_RESOURCES/Pfam-A.hmm.gz
	$ gunzip Pfam-A.hmm.gz

	$ wget https://data.broadinstitute.org/Trinity/Trinotate_v3_RESOURCES/Trinotate_v3.sqlite.gz -O Trinotate.sqlite.gz
	$ gunzip Trinotate.sqlite.gz

Se instalo *Transdecoder* con *Bioconda* para enocntrar regiones codificantes entre los transcritos encontrados en el transcriptoma de Chlamy que se 
ensambló. Con este programa se extrajo los marcos abiertos de lectura de cada secuencia de transcrito:

	$ conda install transdecoder

	$ TransDecoder.LongOrfs -t transcript_chlamy.fasta
	$ more transcript_chlamy.fasta.transdecoder.pep

	$ TransDecoder.Predict -t transcript_chlamy.fasta
	$ more transcript_chlamy.fasta.transdecoder.gff3

NOTA: Al correr estos *scripts* se logra identificar los ORFs mas largos encontrados en los transcritos de Chlamy.

Por otra parte se ejecutó *makeblastdb*, una herramienta de línea de comandos para crear base de datos de *BLAST*, para realizar la anotación: 


	$ makeblastdb -in uniprot_sprot.pep -dbtype prot



#### *Miercoles 19 de junio*


## Continuación del Método de Cabili *et al.* (2011)


Se paso a mazorka el archivo Pfam-A.hmm descargado en la computadora y el archivo transcript_chlamy.fasta.transdecoder.pep generado con *Transdecoder* 
para correr alli los siguientes programas.
	

Se usó el archivo *Pfam-A.hmm* para correr con *hmmer* desde mazorka:


    #PBS -N hmmer
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=20:00:00
    #PBS -V
    #PBS -o hmmpress.out
    #PBS -e hmmpress.err


    module load hmmer/3.1b2

    cd $PBS_O_WORKDIR


    hmmpress Pfam-A.hmm



Se generó una anotación cargando los modulos *hmmer*, *SignalP* y *tmhmm* desde mazorka: 


    #PBS -N anotacion_2
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=20:00:00
    #PBS -V
    #PBS -o anotacion_2.out
    #PBS -e anotacion_2.err


    module load hmmer/3.1b2
    module load SignalP/4.1
    module load tmhmm/2.0c


    cd $PBS_O_WORKDIR


    hmmscan --cpu 2 --domtblout TrinotatePFAM.out Pfam-A.hmm transcript_chlamy.fasta.transdecoder.pep > pfam.log &

    signalp -f short -n signalp.out transcript_chlamy.fasta.transdecoder.pep > signalp.log &

    tmhmm --short < transcript_chlamy.fasta.transdecoder.pep > tmhmm.out &


Por otra parte en paralelo se revisó el archivo *transcript_chlamy.fasta* para saber la cantidad de secuencias que contenía:


	$ awk '/>/ { count++ } END { print count }' transcript_chlamy.fasta    ----> Son 27 725 secuencias


Este archivo se dividio en varios partes para correr por separado un *blast* de cada uno, esto debido a que no era posible correr el archivo con 27 mil
secuencias:

	$ awk 'BEGIN {n_seq=0;} /^>/ {if(n_seq%10000==0){file=sprintf("transcript_chlamy%d.pep",n_seq);} print >> file; n_seq++; next;} 
    { print >> file; }' < transcript_chlamy.fasta

NOTA: El archivo *transcript_chlamy.fasta* se dividió en tres:
    
    a) trasncript_chlamy0.fa ---> Con 10 000 secuencias
	b) transcript_chlamy10000.fa ---> Con 10 000 secuencias
	c) transcript_chlamy20000.fa ---> Con 7 725 secuencias


Se corrio un blast con las secuencias divididas en los tres archivos, desde mazorka, por lo que los archivos fasta de los transcritos se copiaron al
servidor:

NOTA: Se realizó un *blast* de nucleótidos (*blastx*) y un *blast* de proteínas (*blastp*) por aparte para cada archivo fasta, por lo que se tuvo
6 corridas en total.


BLAST de nucleótidos:

    #PBS -N anBLASTx_1
    #PBS -q default
    #PBS -l nodes=1:ppn=8,vmem=8gb,walltime=120:00:00
    #PBS -V
    #PBS -o anblastx_1.out
    #PBS -e anblastx_1.err


    module load ncbi-blast+/2.6.0


    cd $PBS_O_WORKDIR


    blastx -query transcript_chlamy0.fa -db /LUSTRE/storage/data/secuencias/NR/nr -num_threads 8 -max_target_seqs 1 -outfmt 6 
    > blastx_chlamy_0.outmt6


BLAST de proteínas


    #PBS -N anBLASTp_1
    #PBS -q default
    #PBS -l nodes=1:ppn=8,vmem=8gb,walltime=120:00:00
    #PBS -V
    #PBS -o anblastp_1.out
    #PBS -e anblastp_1.err


    module load ncbi-blast+/2.6.0


    cd $PBS_O_WORKDIR


    blastp -query transcript_chlamy0.fa -db /LUSTRE/storage/data/secuencias/NR/nr -num_threads 8 -max_target_seqs 1 -outfmt 6 
    > blastx_chlamy_0.outmt6


NOTA: Se corrió también los dos *script* anteriores (el de nucleótidos y el de proteínas) para los archivos *transcript_chlamy10000.fa* y 
*transcript_chlamy20000.fa*. El archivo *output* cambio segun el nombre del archivo *input*.


## Identificaciòn de lncRNAs usando CPC


De manera paralela se corrió el programa *CPC* en mazorka para filtrar por longitud las secuencias de los transcritos de Chlamy y de los de la 
microalga *Botryococcus braunii* Raza B (el archivo de BBRB se obtuvo de los datos del transcriptoma generado por el Dr. Tim en Texas A&M, que fue 
compartido por *Google Drive*):


Para C. reinhardtii:


    #PBS -N CPC_chlamy
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=20:00:00
    #PBS -V
    #PBS -o cpc_chlamy.out
    #PBS -e cpc_chlamy.err


    module load cpc/0.9.r2


    cd $PBS_O_WORKDIR


    run_predict.sh transcript_chlamy.fasta result_in_table cpc result_evidences


Para B. braunii Raza B


    #PBS -N CPC_BBRB
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=120:00:00
    #PBS -V
    #PBS -o cpc_BBRB.out
    #PBS -e cpc_BBRB.err


    module load cpc/0.9.r2


    cd $PBS_O_WORKDIR


    run_predict.sh Botryococcus_braunii_Race_B_Showa.transcriptome.fasta result_in_table cpc result_evidences



El archivo *output* *result_in_table* de ambas microalgas se pasó a la computadora y se filtro con el comando *grep* para obtener genes mayores a 
200 nucleótidos y con clasificacion de *"noncoding"*:


    $ grep 'noncoding' result_in_table_chlamy > chlamy
	$ awk '{if($2 >= 200){print}}' chlamy > nc_genes_chlamy
	
	$ grep 'noncoding' result_in_table_BBRB > BBRB
	$ awk '{if($2 >= 200){print}}' BBRB > nc_genes_BBRB


Se contó el numero de líneas (genes) antes y después de procesar los archivos para verificar la reducción de datos y contabilizar los genes que 
fueron filtrados:

	$ wc -l result_in_table_chlamy ----> 27 725
	$ wc -l nc_genes_chlamy  ----> 11 081

	$ wc -l result_in_table_BBRB ----> 304 074
	$ wc -l nc_genes_BBRB ----> 259 628


NOTA: Los datos en "results_in_table..." corresponden a todos los genes mientras que el archivo "nc_genes..." corresponde a genes no codificantes 
con una longitud mayor o igual a 200 nt, es decir, el filtrado para obtener los *lncRNAs*.


Se quería realizar un *match* entre la tabla de resultados filtrada para cada microalga y el archivo de coordenadas en formato *gtf* de los ensambles de sus 
transcriptomas, pero para el caso de BBRB el archivo de coordenadas se encontraba en formato *gff*, por lo que primero se tuvo que convertir a 
formato *gtf*:

 
	$ gffread Bbrauniishowav1.1.gene_exons.gff3 -T -o Bbrauniishowav1.1.gene_exons.gtf


#### *Martes 25 de junio*


## Continuación del *blastx* y *blastp* (*Pipeline* Cabili *et al.* 2011)


Los *BLAST* de nucleótidos y proteínas se abortaron para agregar más tiempo de corrida y corregir unos detalles en el *script* del *blastp* 
(el nombre del *input*). 

El input usado para el *blastp* fue el *output* de transdecoder *.pep*, que se dividio en tres archivos al igual que con el archivo *.fasta*, y
se hizo eso de la siguiente manera:

a) Primero se reviso el archivo *.pep* para saber cuantas secuencias tenia:


	$ awk '/>/ { count++ } END { print count }' transcript_chlamy.fasta.transdecoder.pep   ----> Son 22 565 secuencias


b) Y segundo se dividio en tres archivos:

	$ awk 'BEGIN {n_seq=0;} /^>/ {if(n_seq%10000==0){file=sprintf("transcript_chlamy%d.pep",n_seq);} print >> file; n_seq++; next;} 
    { print >> file; }' < transcript_chlamy.fasta.transdecoder.pep


NOTA: El tamano de cada archivo fue el siguiente:

    a) trasncript_chlamy0.pep ---> 10 000 secuencias
	b) transcript_chlamy10000.pep ---> 10 000 secuencias
	c) transcript_chlamy20000.pep ---> 2 565 secuencias



Una vez teniendo los archivos de entrada adecuados el script con las correcciones se corrio, el cual fue el siguiente:


    #PBS -N anBLASTpep_1
    #PBS -q default
    #PBS -l nodes=1:ppn=8,vmem=8gb,walltime=999:00:00
    #PBS -V
    #PBS -o anblastpep_1.out
    #PBS -e anblastpep_1.err


    module load ncbi-blast+/2.6.0


    cd $PBS_O_WORKDIR


    blastp -query transcript_chlamy0.pep -db /LUSTRE/storage/data/secuencias/NR/nr -num_threads 8 -max_target_seqs 1 -outfmt 6 > blastpep_chlamy_0.outmt6


 ## Continuación de la identificación de *lncrnas* con el método de CPC

Una vez teniendo los archivos *gtf* de las dos microalgas se procedió a convertirlos a formato bed12 con *bedtools*:

Primero se descargaron los scripts de *UCSC* en una carpeta local y se hicieron ejecutables para la conversión:

	$ wget http://hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64/gtfToGenePred .
	$ wget http://hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64/genePredToBed .
	$ chmod +x gtfToGenePred genePredToBed

Posteriormente se convirtio los archivos *gtf* a formato *genepred*:

	$ ./gtfToGenePred /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/match/st_merged_chlamy.gtf st_merged_chlamy.genePred
	$ ./gtfToGenePred /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/match/Bbrauniishowav1.1.gene_exons.gtf Bbrauniishowav1.1.gene_exons.genePred

Finalmente se convirtió el archivo *genepred* a formato *bed12*:

	$ ./genePredToBed st_merged_chlamy.genePred st_merged_chlamy.bed12
	$ ./genePredToBed Bbrauniishowav1.1.gene_exons.genePred Bbrauniishowav1.1.gene_exons.bed12


Con los archivos en formato *bed12* se realizó el *match* de la siguiente manera:

	$ awk 'FNR==NR{a[$1];next}($4 in a){print}' /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/Chlamy/nc_genes_chlamy 
    /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/st_merged_chlamy.bed12 > chlamy_ncrnas.bed12

		$ awk 'FNR==NR{a[$1];next}($4 in a){print}' /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/BBRB_Tim/nc_genes_BBRB 
    /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Bbrauniishowav1.1.gene_exons.bed12 > BBRB_ncrnas.bed12


El archivo de BBRB no mostró contenido, por lo que no funcionó el *match* entonces se descargó otro archiv de coordenadas desde la carpeta 
del drive compartida por el Dr. Tim, el archivo fue *Bbrauniishowav1.1.gene.gff3*. Este se convirtio a gtf y luego a bed12 y con el comando *awk* 
se probo hacer el nuevo *match* contra la tabla de lncRNAs filtrados de BBRB:

	$ gffread Bbrauniishowav1.1.gene.gff3 -T -o Bbrauniishowav1.1.gene.gtf
	$ ./gtfToGenePred /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/match/Bbrauniishowav1.1.gene.gtf Bbrauniishowav1.1.gene.genePred
	$ ./genePredToBed Bbrauniishowav1.1.gene.genePred Bbrauniishowav1.1.gene.bed12
	$ mv Bbrauniishowav1.1.gene.bed12 Bbrauniishowa_gene.bed12 <------- Se le cambio el nombre para mas facil
	$ awk 'FNR==NR{a[$1];next}($4 in a){print}' /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/BBRB_Tim/nc_genes_BBRB /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Bbrauniishowa_gene.bed12 > BBRB_ncrnas_2.bed12

NOTA: Tampoco funciono realizar el *match* con este nuevo archivo.


#### *Jueves 27 de junio*


Ya que el de *match* con el archivo *bed12* de BBRB no funcionó se continuó trabajando con los datos de Chlamy. 

Se descargo archivo un archivo *gff3* de Chlamy desde el Ensembl, este se convirtio a *gtf* y luego a *bed12* (esto para tener un archivo de anotación 
de genes codificantes para proteinas de Chlamy). El archivo se descargó en la carpeta local "Descargas" y luego se pasó a una carpeta llamada 
"gtfto_bed12" ya que ahí se encuentran los ejecutables para la conversión de *gtf* a *bed12*:

	$ gffread Chlamydomonas_reinhardtii.Chlamydomonas_reinhardtii_v5.5.43.gff3 -T -o Chlamydomonas_reinhardtii.Chlamydomonas_reinhardtii_v5.5.43.gtf
	
    $ mv Chlamydomonas_reinhardtii.Chlamydomonas_reinhardtii_v5.5.43.gtf /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/
    Chlamydomonas_reinhardtii_v5.5.43.gtf

	$ ./gtfToGenePred /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Chlamydomonas_reinhardtii_v5.5.43.gtf 
    Chlamydomonas_reinhardtii_v5.5.43.genePred
	
    $ ./genePredToBed /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/Chlamydomonas_reinhardtii_v5.5.43.genePred 
    Chlamydomonas_reinhardtii_v5.5.43.bed12


Una vez convertido a *bed12* el archivo se filtró para obtener sólo las secuencias que codifican para proteínas, el *output* se envió a la carpeta
"bedtools". Sin embargo, tampoco funcionó el filtrado.

	$ grep 'protein' Chlamydomonas_reinhardtii_v5.5.43.bed12 > Chlamydomonas_reinhardtii_filt.bed12