# **Bitacora: Octubre**



#### *Martes 1 de octubre de 2019*


## Repeticion del match para la identificacion de tipos de lncRNAs


Se intento convertir de nuevo el archivo st_merged_chlamy.gtf a formato *bed12* usando esta vez un *script* de *python* que primero se hizo 
ejecutable con *chmod*:

	$ chmod +x gtf2bed_st.py
	$ ./gtf2bed_st.py st_merged_chlamy.gtf > prueba_CR.bed12


El script usado de *phyton* fue el siguiente:


	#!/usr/bin/env python3
	'''
	gtf2bed.py converts GTF file to BED file.
	Usage: gtf2bed.py {OPTIONS} [.GTF file]

	History
		Nov.5th 2012:
			1. Allow conversion from general GTF files (instead of only Cufflinks supports).
			2. If multiple identical transcript_id exist, transcript_id will be appended a string like "_DUP#" to separate.
	'''

	import sys;
	import re;

	if len(sys.argv)<2:
	  print('This script converts .GTF into .BED annotations.\n');
	  print('Usage: gtf2bed {OPTIONS} [.GTF file]\n');
	  print('Options:');
	  print('-c color\tSpecify the color of the track. This is a RGB value represented as "r,g,b". Default 255,0,0 (red)');
	  print('\nNote:');
	  print('1\tOnly "exon" and "transcript" are recognized in the feature field (3rd field).');
	  print('2\tIn the attribute list of .GTF file, the script tries to find "gene_id", "transcript_id" and "FPKM" attribute, and convert them as name and score field in .BED file.'); 
	  print('Author: Wei Li (li.david.wei AT gmail.com)');
	  sys.exit();

	color='255,0,0'


	for i in range(len(sys.argv)):
	  if sys.argv[i]=='-c':
		color=sys.argv[i+1];


	allids={};

	def printbedline(estart,eend,field,nline):
	  try:
		estp=estart[0]-1;
		eedp=eend[-1];
		# use regular expression to get transcript_id, gene_id and expression level
		geneid=re.findall(r'gene_id \"([\w\.]+)\"',field[8])
		transid=re.findall(r'transcript_id \"([\w\.]+)\"',field[8])
		fpkmval=re.findall(r'FPKM \"([\d\.]+)\"',field[8])
		if len(geneid)==0:
		  print('Warning: no gene_id field ',file=sys.stderr);
		else:
		  geneid=geneid[0];
		if len(transid)==0:
		  print('Warning: no transcript_id field',file=sys.stderr);
		  transid='Trans_'+str(nline);
		else:
		  transid=transid[0];
		if transid in allids.keys():
		  transid2=transid+'_DUP'+str(allids[transid]);
		  allids[transid]=allids[transid]+1;
		  transid=transid2;
		else:
		  allids[transid]=1;
		if len(fpkmval)==0:
		  #print('Warning: no FPKM field',file=sys.stderr);
		  fpkmval='100';
		else:
		  fpkmval=fpkmval[0];
		fpkmint=round(float(fpkmval));
		print(field[0]+'\t'+str(estp)+'\t'+str(eedp)+'\t'+transid+'\t'+str(fpkmint)+'\t'+field[6]+'\t'+str(estp)+'\t'+str(eedp)+'\t'+color+'\t'+str(len(estart))+'\t',end='');
		seglen=[eend[i]-estart[i]+1 for i in range(len(estart))];
		segstart=[estart[i]-estart[0] for i in range(len(estart))];
		strl=str(seglen[0]);
		for i in range(1,len(seglen)):
		  strl+=','+str(seglen[i]);
		strs=str(segstart[0]);
		for i in range(1,len(segstart)):
		  strs+=','+str(segstart[i]);
		print(strl+'\t'+strs);
	  except ValueError:
		print('Error: non-number fields at line '+str(nline),file=sys.stderr);

	estart=[];
	eend=[];
	# read lines one to one
	nline=0;
	prevfield=[];
	prevtransid='';
	for lines in open(sys.argv[-1]):
	  field=lines.strip().split('\t');
	  nline=nline+1;
	  if len(field)<9:
		print('Error: the GTF should has at least 9 fields at line '+str(nline),file=sys.stderr);
		continue;
	  if field[1]!='StringTie':
		pass;
		#print('Warning: the second field is expected to be \'StringTie\' at line '+str(nline),file=sys.stderr);
	  if field[2]!='exon' and field[2] !='transcript':
		#print('Error: the third filed is expected to be \'exon\' or \'transcript\' at line '+str(nline),file=sys.stderr);
		continue;
	  transid=re.findall(r'transcript_id \"([\w\.]+)\"',field[8]);
	  if len(transid)>0:
		transid=transid[0];
	  else:
		transid='';
	  if field[2]=='transcript' or (prevtransid != '' and transid!='' and transid != prevtransid):
		#print('prev:'+prevtransid+', current:'+transid);
		# A new transcript record, write
		if len(estart)!=0:
		  printbedline(estart,eend,prevfield,nline);
		estart=[];
		eend=[];
	  prevfield=field;
	  prevtransid=transid;
	  if field[2]=='exon':
		try:  
		  est=int(field[3]);
		  eed=int(field[4]);
		  estart+=[est];
		  eend+=[eed];
		except ValueError:
		  print('Error: non-number fields at line '+str(nline),file=sys.stderr);
	# the last record
	if len(estart)!=0:
	  printbedline(estart,eend,field,nline);
	#!/usr/bin/env python3
	'''
	gtf2bed.py converts GTF file to BED file.
	Usage: gtf2bed.py {OPTIONS} [.GTF file]

	History
		Nov.5th 2012:
			1. Allow conversion from general GTF files (instead of only Cufflinks supports).
			2. If multiple identical transcript_id exist, transcript_id will be appended a string like "_DUP#" to separate.
	'''

	import sys;
	import re;

	if len(sys.argv)<2:
	  print('This script converts .GTF into .BED annotations.\n');
	  print('Usage: gtf2bed {OPTIONS} [.GTF file]\n');
	  print('Options:');
	  print('-c color\tSpecify the color of the track. This is a RGB value represented as "r,g,b". Default 255,0,0 (red)');
	  print('\nNote:');
	  print('1\tOnly "exon" and "transcript" are recognized in the feature field (3rd field).');
	  print('2\tIn the attribute list of .GTF file, the script tries to find "gene_id", "transcript_id" and "FPKM" attribute, and convert them as name and score field in .BED file.'); 
	  print('Author: Wei Li (li.david.wei AT gmail.com)');
	  sys.exit();

	color='255,0,0'


	for i in range(len(sys.argv)):
	  if sys.argv[i]=='-c':
		color=sys.argv[i+1];


	allids={};

	def printbedline(estart,eend,field,nline):
	  try:
		estp=estart[0]-1;
		eedp=eend[-1];
		# use regular expression to get transcript_id, gene_id and expression level
		geneid=re.findall(r'gene_id \"([\w\.]+)\"',field[8])
		transid=re.findall(r'transcript_id \"([\w\.]+)\"',field[8])
		fpkmval=re.findall(r'FPKM \"([\d\.]+)\"',field[8])
		if len(geneid)==0:
		  print('Warning: no gene_id field ',file=sys.stderr);
		else:
		  geneid=geneid[0];
		if len(transid)==0:
		  print('Warning: no transcript_id field',file=sys.stderr);
		  transid='Trans_'+str(nline);
		else:
		  transid=transid[0];
		if transid in allids.keys():
		  transid2=transid+'_DUP'+str(allids[transid]);
		  allids[transid]=allids[transid]+1;
		  transid=transid2;
		else:
		  allids[transid]=1;
		if len(fpkmval)==0:
		  #print('Warning: no FPKM field',file=sys.stderr);
		  fpkmval='100';
		else:
		  fpkmval=fpkmval[0];
		fpkmint=round(float(fpkmval));
		print(field[0]+'\t'+str(estp)+'\t'+str(eedp)+'\t'+transid+'\t'+str(fpkmint)+'\t'+field[6]+'\t'+str(estp)+'\t'+str(eedp)+'\t'+color+'\t'+str(len(estart))+'\t',end='');
		seglen=[eend[i]-estart[i]+1 for i in range(len(estart))];
		segstart=[estart[i]-estart[0] for i in range(len(estart))];
		strl=str(seglen[0]);
		for i in range(1,len(seglen)):
		  strl+=','+str(seglen[i]);
		strs=str(segstart[0]);
		for i in range(1,len(segstart)):
		  strs+=','+str(segstart[i]);
		print(strl+'\t'+strs);
	  except ValueError:
		print('Error: non-number fields at line '+str(nline),file=sys.stderr);

	estart=[];
	eend=[];
	# read lines one to one
	nline=0;
	prevfield=[];
	prevtransid='';
	for lines in open(sys.argv[-1]):
	  field=lines.strip().split('\t');
	  nline=nline+1;
	  if len(field)<9:
		print('Error: the GTF should has at least 9 fields at line '+str(nline),file=sys.stderr);
		continue;
	  if field[1]!='StringTie':
		pass;
		#print('Warning: the second field is expected to be \'StringTie\' at line '+str(nline),file=sys.stderr);
	  if field[2]!='exon' and field[2] !='transcript':
		#print('Error: the third filed is expected to be \'exon\' or \'transcript\' at line '+str(nline),file=sys.stderr);
		continue;
	  transid=re.findall(r'transcript_id \"([\w\.]+)\"',field[8]);
	  if len(transid)>0:
		transid=transid[0];
	  else:
		transid='';
	  if field[2]=='transcript' or (prevtransid != '' and transid!='' and transid != prevtransid):
		#print('prev:'+prevtransid+', current:'+transid);
		# A new transcript record, write
		if len(estart)!=0:
		  printbedline(estart,eend,prevfield,nline);
		estart=[];
		eend=[];
	  prevfield=field;
	  prevtransid=transid;
	  if field[2]=='exon':
		try:  
		  est=int(field[3]);
		  eed=int(field[4]);
		  estart+=[est];
		  eend+=[eed];
		except ValueError:
		  print('Error: non-number fields at line '+str(nline),file=sys.stderr);
	# the last record
	if len(estart)!=0:
	  printbedline(estart,eend,field,nline);


NOTAS: 

	1) Este script dice que es para gtf de Cufflinks pero funciono para stringtie, el script original tenia en dos lineas el nombre de
	"Cufflink" pero se lo cambie por "StringTie".
	2) Funciona aunque hay un error que dice lo siguiente:

	Error: the GTF should has at least 9 fields at line 1
	Error: the GTF should has at least 9 fields at line 2


A pesar del mensaje de error, se genero un archivo *output* en formato *bed12*, del cual fue revisado su numero de columnas y las accesiones que 
tenia:

	$ awk '{print NF}' prueba_CR.bed12 | sort -nu | tail -n 1    ------> Tiene  12 columnas

	$ cut -f1 prueba_CR.bed12 | sort | uniq -c | head 

	26 NC_001638.1
	131 NC_005353.1
	2 NW_001842467.1
	1 NW_001842468.1
    1 NW_001842470.1
    2 NW_001842474.1
    2 NW_001842476.1
    1 NW_001842477.1
    3 NW_001842479.1
    1 NW_001842483.1


Se renombro el archivo *"prueba_CR.bed12"*:

	$ mv prueba_CR.bed12 ./chlamy_st.bed12
	

NOTA: 

	Se observo que no solo presentaba las accesiones de la mitocondria y del cloroplasto, NC_001638.1 y NC_005353.1, sino que tambien
	las del nucleo (empiezan con la nomenclatura "NW").


Luego de esto se procedio a hacer de nuevo un *match* entre el archivo *bed12* y la tabla de ncRNAs obtenida con *CPC*:


	$ awk 'FNR==NR{a[$1];next}($4 in a){print}' /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/Chlamy/nc_genes_chlamy 
	/home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/chlamy_st.bed12 > chlamy_ncrnas_new.bed12



#### *Miercoles 2 de octubre de 2019*


## Nuevas intersecciones para identificar tipos de lncRNAs en *C. reinhardtii*


Para poder hacer la interseccion del archivo de coordenadas de referencia y el archivo *bed12* generado del nuevo *match* se decidio cambiar el 
archivo de coordenadas por uno que fuese la misma version del genoma que se uso para hacer el ensamble guiado del transcriptoma, el cual
corresponde a este: 

	GCF_000002595.1_v3.0_genomic.gff   ------> Descargado de la base de datos del NCBI


Este archivo fue convertido a formato gtf usando el siguiente script:

	$ gffread GCF_000002595.1_v3.0_genomic.gff -T -o GCF_000002595.1_v3.0_genomic.gtf


Del archivo se filtro todas aquellas secuencias correspondientes al *CDS*:

	$ grep "CDS" /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/GCF_000002595.1_v3.0_genomic.gtf 
	> GCF_000002595.1_v3.0_genomic_CDS.gtf


Se tuvo que convertir el archivo de coordenadas nuevo de formato gtf a bed12:

	$ ./gtfToGenePred /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/GCF_000002595.1_v3.0_genomic_CDS.gtf 
	GCF_000002595.1_v3.0_genomic_CDS.genePred


NOTA: Luego de correr el script de genePred la terminal arrojo un mensaje que fue el siguiente:

	invalid gffGroup detected on line: NC_005353.1	RefSeq	CDS	69520	71506	0.000000	-	1	gene_id "gene14405"; 
	transcript_id "gene14405"; 
	GFF/GTF group gene14405 on NC_005353.1+, this line is on NC_005353.1-, all group members must be on same seq and strand


Y realizando un "grep" en el output el resultado obtenido es que la secuencia no se encontraba en el archivo

 	$ grep "gene14405" "69520" /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/GCF_000002595.1_v3.0_genomic_CDS.genePred

	grep: 69520: No existe el archivo o el directorio


Y contando la cantidad de items y su repeticion en la columna 9 del archivo gtf con grep se encontro esto:

	$ cut -f9 GCF_000002595.1_v3.0_genomic_CDS.gtf | sort | uniq -c


Que no estaba el id del gen:

	NC_005353.1	RefSeq	CDS	69520	71506	.	-	1	transcript_id "gene14405"; gene_id "gene14405"; gene_name "psaA";


A pesar de eso se procedio a continuar con la conversion a bed12:

	$ ./genePredToBed /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/GCF_000002595.1_v3.0_genomic_CDS.genePred 
	GCF_000002595.1_v3.0_genomic_CDS.bed12


En el archivo bed12 de referencia se busco los ID's correspondientes a la mitocondria y cloroplasto para asegurarnos que este archivo 
lo tenia al igual que el de chlamy_ncrnas_new.bed12:


	1) De la mitocrondria:

	$ grep "NC_001638.1" /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/GCF_000002595.1_v3.0_genomic_CDS.bed12

	NC_001638.1	544	1690	gene14354	0	-	544	1690	0	1	1146,	0,
	NC_001638.1	1712	3044	gene14355	0	-	1712	3044	0	1	1332,	0,
	NC_001638.1	3319	4960	gene14356	0	-	3319	4960	0	1	1641,	0,
	NC_001638.1	5041	6559	gene14357	0	+	5041	6559	0	1	1518,	0,
	NC_001638.1	6706	7855	gene14358	0	+	6706	7855	0	1	1149,	0,
	NC_001638.1	7952	8441	gene14359	0	+	7952	8441	0	1	489,	0,
	NC_001638.1	10345	11224	gene14367	0	+	10345	11224	0	1	879,	0,
	NC_001638.1	11567	12674	gene14370	0	+	11567	12674	0	1	1107,	0,


	2) Del cloroplasto:

	$ grep "NC_005353.1" /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/GCF_000002595.1_v3.0_genomic_CDS.bed12

	NC_005353.1	2897	3851	gene14379	0	+	2897	3851	0	1	954,	0,
	NC_005353.1	6381	6864	gene14380	0	+	6381	6864	0	1	483,	0,
	NC_005353.1	8913	10977	gene14382	0	+	8913	10977	0	1	2064,	0,
	NC_005353.1	11841	11982	gene14384	0	+	11841	11982	0	1	141,	0,
	NC_005353.1	12708	13965	gene14385	0	+	12708	13965	0	1	1257,	0,
	NC_005353.1	15845	16184	gene14390	0	-	15845	16184	0	1	339,	0,
	NC_005353.1	17635	19210	gene14393	0	-	17635	19210	0	1	1575,	0,
	NC_005353.1	20006	20654	gene14395	0	-	20006	20654	0	1	648,	0,
	NC_005353.1	21285	22167	gene14396	0	+	21285	22167	0	1	882,	0,
	NC_005353.1	22631	22745	gene14397	0	+	22631	22745	0	1	114,	0,
	NC_005353.1	23946	24234	gene14398	0	+	23946	24234	0	1	288,	0,
	NC_005353.1	24960	25797	gene14399	0	+	24960	25797	0	1	837,	0,
	NC_005353.1	26582	26861	gene14400	0	+	26582	26861	0	1	279,	0,
	NC_005353.1	27993	28404	gene14401	0	+	27993	28404	0	1	411,	0,
	NC_005353.1	28921	29290	gene14402	0	+	28921	29290	0	1	369,	0,
	NC_005353.1	30124	30664	gene14403	0	+	30124	30664	0	1	540,	0,
	NC_005353.1	31650	32064	gene14404	0	+	31650	32064	0	1	414,	0,


Una vez teniendo el archivo de referencia en formato bed12 se procedio a identificar lncRNAS por tipos intersectando los dos archivos 
usando bedtools:


NOTA: Cada output se ira a una nueva carpeta llamada "lncrnas_types"


a) lncRNAs que sobrelapan genes codificantes:

	$ bedtools intersect -a /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/chlamy_ncrnas_new.bed12 
	-b /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/GCF_000002595.1_v3.0_genomic_CDS.bed12 
	> /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/lncrnas_types/overlap_chlamy_CPC.bed12


b) lncRNAs en sentido con respecto al gen codificante:

	$ bedtools intersect -a /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/chlamy_ncrnas_new.bed12 
	-b /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/GCF_000002595.1_v3.0_genomic_CDS.bed12 -f 0.1 -wa -s 
	> /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/lncrnas_types/sense_chlamy_CPC.bed12


c) lncRNAs antisentido con respecto al gen codificante:

	$ bedtools intersect -a /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/chlamy_ncrnas_new.bed12 
	-b /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/GCF_000002595.1_v3.0_genomic_CDS.bed12 -f 0.1 -wa -S 
	> /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/lncrnas_types/antisense_chlamy_CPC.bed12


NOTA: Los lncRNAs sentido y antisentido son aquellos donde el 10% sobrelapa en sentido positivo de 5' a 3' (en sentido) o negativo de 3' a 5' 
(antisentido) de la region codificante del ADN.


d) lncRNAs intergenicos:

	$ bedtools intersect -a /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/chlamy_ncrnas_new.bed12 
	-b /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/GCF_000002595.1_v3.0_genomic_CDS.bed12 -v 
	> /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/lncrnas_types/intergenic_chlamy_CPC.bed12


e) lncRNAs intronicos:

e.1) Para el caso de los lncRNAs intronicos es necesario primero obtener un archivo ".bed12" de coordenadas de los intrones, por lo que se us\F3 
un script de perl obtenido de (http://bioops.info/2012/11/intron-size-gff3-perl/) para obtener el tamano de los intrones a partir del archivo ".gtf" de coordenadas del genoma de Chlamy (GCF_000002595.1_v3.0_genomic.gtf) que fue convertido a formato ".bed12".

	$ ./gtfToGenePred /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/GCF_000002595.1_v3.0_genomic.gtf 
	/home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/GCF_000002595.1_v3.0_genomic.genePred


NOTA: Como resultado la terminal arrojo el mismo error anteriormente encontrado con el gen 14405:

	invalid gffGroup detected on line: NC_005353.1	RefSeq	CDS	69520	71506	0.000000	-	1	gene_id "gene14405"; 	
	transcript_id "gene14405"; 
	GFF/GTF group gene14405 on NC_005353.1+, this line is on NC_005353.1-, all group members must be on same seq and strand


	$ ./genePredToBed /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/GCF_000002595.1_v3.0_genomic.genePred 
	/home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/GCF_000002595.1_v3.0_genomic.bed12


e.2) Una vez teniendo el archivo en formato ".bed12" se procedio a obtener las posiciones de los intrones con el script de python:

	$ perl get_intron_gtf_from_bed.pl /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/GCF_000002595.1_v3.0_genomic.bed12 
	> /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/intron_GCF_chlamy.gtf
 

## Pendiente de hacer porque el archivo gtf esta vacio


e.3) El arhivo "intron_GCF_chlamy.gtf" se convirtio a formato ".bed12" para hacer la interseccion:

	$ ./gtfToGenePred /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/intron_GCF_chlamy.gtf
	/home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/intron_GCF_chlamy.genePred
	
	$ ./genePredToBed /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/intron_GCF_chlamy.genePred 
	/home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/intron_GCF_chlamy.bed12

	
e.4) Se verifico que el archivo bed12 tuviera las 12 columnas:

	$ awk '{print NF}' GCF_000002595.1_v3.0_genomic.bed12 | sort -nu | tail -n 1      ------> Tiene 12 columnas!


e.5) Se realizo la interseccion con bedtools para obtener los lncRNAs intronicos de C. reinhardtii:

	$ bedtools intersect -a /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/chlamy_ncrnas_new.bed12 
	-b /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/gtf_to_bed12/intron_GCF_chlamy.bed12 -f 1 -r -wa 
	> /home/fani/Documentos/I_Semestre_2019/Tesis/chlamy/CPC/lncrnas_types/intronic_chlamy_CPC.bed12


NOTA: Los argumentos usados para cada uno de las intersecciones con bedtools fueron los siguientes:

	-a: Se refiere al archivo "A" que en este caso es en formato ".bed12".
	-b: Se refiere al archivo "B" que en este caso es en formato ".bed12".
	-f: Indica el minimo sobrelape requerido como una fraccion del archivo "A". El valor por defecto es 1x10^-9.
	-wa: Le indica al programa que escriba la entrada original en el archivo "A" para cada sobrelape encontrado.
	-s: Este argumento fuerza la direccionalidad del transcrito, es decir, solo reporta en el archivo "B" "hits" que sobrelapan 
	en el archivo "A", en la misma hebra. Por defecto los sobrelapes son reportados sin que sean respecto a la hebra.
	-S: Indica que se requiere de una direccionalidad diferente, es decir, solo reporta en el archivo "B" "hits" que sobrelapan 
	el archivo "A" en la hebra opuesta. Al igual que con "-s" los sobrelapes son reportados sin que sean respecto a la hebra.
	-v: Le indica al programa que solamente reporte en el archivo "A" aquellas entradas que no tienen sobrelape en el archivo "B" 
	y este es restringido por el argumento "-f" y "-r".
	-r: Indica al programa que la interseccion requiere de una fraccion de sobrelape sea reciproca en el archivo "A" y el "B", 
	es decir, si -f es 1 (como en este caso) y se usa el argumento -r, se requiere que el archivo "B" sobrelape en un 100% al 
	archivo "A" y viceversa. 
	