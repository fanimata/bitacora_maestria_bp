# **Bitácora: Enero, 2020**


#### *Lunes 6 de enero de 2020*


#### Identificacion de lncRNAs con el metodo de CPC

Se inicio la identificacion de lncRNAs con el metodo de CPC, usando el transcriptoma de B. braunii Raza B ensamblado con StringTie y con Trinity, ambos en formato fasta.
Para ello se convirtio el transcriptoma ensamblado con StringTie de formato gtf a fasta.

    $ gffread -w BBRB_v2_trans_annotated.fasta -g Bbraunii_genome_502_v2.0.fa BBRB_v2_trans_annotated.gtf
    FASTA index file Bbraunii_genome_502_v2.0.fa.fai created.

El archivo en formato fasta y el index se copiaron a la una carpeta en mazorka (la carpeta merged_gtf). Antes de correr los scripts se creo una carpeta llamada "cpc/".

Se corrio un script de CPC para ambos archivos en formato fasta:


    *Para el transcriptoma ensamblado guiado con StringTie:*

    #PBS -N CPC_st
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=120:00:00
    #PBS -V
    #PBS -o cpc_BBRB2_st.out
    #PBS -e cpc_BBRB2_st.err


    module load cpc/0.9.r2


    cd $PBS_O_WORKDIR


    run_predict.sh /LUSTRE/usuario/smata/BBRB/stringtie_en/compare_gtf/BBRB_v2_trans_annotated.fasta result_in_table cpc result_evidences


    *Para en transcriptoma ensamblado de novo con Trinity:*


    #PBS -N CPC_trin
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=120:00:00
    #PBS -V
    #PBS -o cpc_BBRB_trin.out
    #PBS -e cpc_BBRB_trin.err


    module load cpc/0.9.r2


    cd $PBS_O_WORKDIR


    run_predict.sh /LUSTRE/usuario/smata/BBRB/transcriptome_trinity_2_8_3/trinity_all/BBRB_novo_transcriptome.fasta result_in_table cpc result_evidences

    
    NOTA: Dio error.

    /data/software/cpc-0.9-r2/bin/run_predict.sh: línea 132: cpc/ff.fa: No existe el fichero o el directorio
    /data/software/cpc-0.9-r2/bin/run_predict.sh: línea 146: cpc/ff.lsv: No existe el fichero o el directorio
    /data/software/cpc-0.9-r2/bin/run_predict.sh: línea 142: cpc/blastx.lsv: No existe el fichero o el directorio
    /data/software/cpc-0.9-r2/bin/run_predict.sh: línea 149: cpc/test.lsv: No existe el fichero o el directorio
    /data/software/cpc-0.9-r2/bin/run_predict.sh: línea 150: cpc/test.lsv.scaled: No existe el fichero o el directorio
    /data/software/cpc-0.9-r2/bin/run_predict.sh: línea 157: cpc/test.svm0.stdout: No existe el fichero o el directorio
    cat: cpc/test.svm0.predict: No existe el fichero o el directorio
    /data/software/cpc-0.9-r2/bin/run_predict.sh: línea 171: cpc/blastx.index: No existe el fichero o el directorio
    cat: cpc/blastx.feat: No existe el fichero o el directorio
    Can't open BLAST TABLE file (cpc/blastx.table): No existe el fichero o el directorio at /data/software/cpc-0.9-r2/bin/generate_plot_features.pl line 18.



Mientras tanto se prosiguio identificando lncRNAs pero con el metodo de Cabili et al. (2011).


#### Identificacion de lncRNAs con el metodo de Cabili et al. (2011)


Se copiaron a una carpeta local en la computadora los archivos .fasta de ambos transcriptomas. 
Primero se identifico regiones codificantes con Transdecoder en ambos transcriptomas. Los comandos se corrieron en una carpeta local llamada "/Cabili/Transdecoder" y cada archivo fue revisado.

    NOTA: TransDecoder.LongOrfs es para extraer los ORFs de los transcritos y TransDecoder.Predict para predecir las regiones codificantes

    
    *Para el transcriptoma ensamblado guiado con StringTie (el anotado con gffcompare):*

    $ TransDecoder.LongOrfs -t /home/fani/Documentos/I_Semestre_2019/Tesis/BBRB_analisis_bioinfo/ensam/st_BBRB/BBRB_v2_trans_annotated.fasta
    NOTA: El total de trascritos a examinar fueron 37359
    $ TransDecoder.Predict -t /home/fani/Documentos/I_Semestre_2019/Tesis/BBRB_analisis_bioinfo/ensam/st_BBRB/BBRB_v2_trans_annotated.fasta
    $ head BBRB_v2_trans_annotated.fasta.transdecoder.pep

    >Bobra.0001s0001.1.v2.1.p1 GENE.Bobra.0001s0001.1.v2.1~~Bobra.0001s0001.1.v2.1.p1  ORF type:complete len:224 (+),score=20.97 Bobra.0001s0001.1.v2.1:1-672(+)
    MEGTKVDRADLFMRKMQDKITLARKCLRAAQDRQTAYANRHRREAIYQEGDYVLLSTRAA
    PTLTSNRFSKLQPRHVGPFRVMAVTPDNVNFVTLDLPDRLDIHPRINVCYLKPYWTREEA
    QSRGYCPPKVITKADGTTHLEWVVDALVDHRLLHDKDGFSYHFEDGTPFLEYLVKWAGHG
    ATEDSWESQEAFETHPGLVNTYHRLQGLAPPIWDQVALRDTFP*
    >Bobra.0001s0002.1.v2.1.p1 GENE.Bobra.0001s0002.1.v2.1~~Bobra.0001s0002.1.v2.1.p1  ORF type:complete len:523 (+),score=37.89 Bobra.0001s0002.1.v2.1:269-1837(+)
    MEAQIQIRHNAQEVQDYVRDLYKWETDLKRKEKGKHEEPPHQARPDGTSCCDLQERDCLT
    RPVVSDCAEGTSRSTKKTRRDADLPVQTLKIGSESAAEHTYEYARNKWDKFDVDAALAAL
    SDDSDTETKKNPVKGNDDLRNGEKLEAHVREVHRKSIVQQPKVENSSQAPIRTRRDTQYA
    CPMTADEWKAEGNSLFKSGRFEDAITAYSRSIELGGGSVVYANRAMALLKVNDNHRAEED

    $ head BBRB_v2_trans_annotated.fasta.transdecoder.gff3

    Bobra.0001s0001.1.v2.1	transdecoder	gene	1	672	.	+	.	ID=GENE.Bobra.0001s0001.1.v2.1~~Bobra.0001s0001.1.v2.1.p1;Name=ORF%20type%3Acomplete%20len%3A224%20%28%2B%29%2Cscore%3D20.97
    Bobra.0001s0001.1.v2.1	transdecoder	mRNA	1	672	.	+	.	ID=Bobra.0001s0001.1.v2.1.p1;Parent=GENE.Bobra.0001s0001.1.v2.1~~Bobra.0001s0001.1.v2.1.p1;Name=ORF%20type%3Acomplete%20len%3A224%20%28%2B%29%2Cscore%3D20.97
    Bobra.0001s0001.1.v2.1	transdecoder	exon	1	672	.	+	.	ID=Bobra.0001s0001.1.v2.1.p1.exon1;Parent=Bobra.0001s0001.1.v2.1.p1
    Bobra.0001s0001.1.v2.1	transdecoder	CDS	1	672	.	+	0	ID=cds.Bobra.0001s0001.1.v2.1.p1;Parent=Bobra.0001s0001.1.v2.1.p1

    Bobra.0001s0002.1.v2.1	transdecoder	gene	1	2066	.	+	.	ID=GENE.Bobra.0001s0002.1.v2.1~~Bobra.0001s0002.1.v2.1.p1;Name=ORF%20type%3Acomplete%20len%3A523%20%28%2B%29%2Cscore%3D37.89
    Bobra.0001s0002.1.v2.1	transdecoder	mRNA	1	2066	.	+	.	ID=Bobra.0001s0002.1.v2.1.p1;Parent=GENE.Bobra.0001s0002.1.v2.1~~Bobra.0001s0002.1.v2.1.p1;Name=ORF%20type%3Acomplete%20len%3A523%20%28%2B%29%2Cscore%3D37.89
    Bobra.0001s0002.1.v2.1	transdecoder	five_prime_UTR	1	268	.	+	.	ID=Bobra.0001s0002.1.v2.1.p1.utr5p1;Parent=Bobra.0001s0002.1.v2.1.p1
    Bobra.0001s0002.1.v2.1	transdecoder	exon	1	2066	.	+	.	ID=Bobra.0001s0002.1.v2.1.p1.exon1;Parent=Bobra.0001s0002.1.v2.1.p1
    Bobra.0001s0002.1.v2.1	transdecoder	CDS	269	1837	.	+	0	ID=cds.Bobra.0001s0002.1.v2.1.p1;Parent=Bobra.0001s0002.1.v2.1.p1


    $ head BBRB_v2_trans_annotated.fasta.transdecoder.bed

    track name='BBRB_v2_trans_annotated.fasta.transdecoder.gff3'
    Bobra.0001s0001.1.v2.1	0	672	ID=Bobra.0001s0001.1.v2.1.p1;GENE.Bobra.0001s0001.1.v2.1~~Bobra.0001s0001.1.v2.1.p1;ORF_type:complete_len:224_(+),score=20.97	0	+	0	672	0	1	672	0
    Bobra.0001s0002.1.v2.1	0	2066	ID=Bobra.0001s0002.1.v2.1.p1;GENE.Bobra.0001s0002.1.v2.1~~Bobra.0001s0002.1.v2.1.p1;ORF_type:complete_len:523_(+),score=37.89	0	+	268	1837	0	1	2066	0
    Bobra.0001s0003.1.v2.1	0	849	ID=Bobra.0001s0003.1.v2.1.p1;GENE.Bobra.0001s0003.1.v2.1~~Bobra.0001s0003.1.v2.1.p1;ORF_type:5prime_partial_len:231_(+),score=15.14	0	+	1	694	0	1	849	0
    Bobra.0001s0004.1.v2.1	0	813	ID=Bobra.0001s0004.1.v2.1.p1;GENE.Bobra.0001s0004.1.v2.1~~Bobra.0001s0004.1.v2.1.p1;ORF_type:complete_len:188_(+),score=20.47	0	+	200	764	0	1	813	0
    Bobra.0001s0005.1.v2.1	0	756	ID=Bobra.0001s0005.1.v2.1.p1;GENE.Bobra.0001s0005.1.v2.1~~Bobra.0001s0005.1.v2.1.p1;ORF_type:complete_len:252_(+),score=12.92	0	+	0	756	0	1	756	0
    Bobra.0001s0007.1.v2.1	0	390	ID=Bobra.0001s0007.1.v2.1.p1;GENE.Bobra.0001s0007.1.v2.1~~Bobra.0001s0007.1.v2.1.p1;ORF_type:complete_len:130_(+),score=0.31	0	+	0	390	0	1	390	0
    Bobra.0001s0008.1.v2.1	0	1826	ID=Bobra.0001s0008.1.v2.1.p3;GENE.Bobra.0001s0008.1.v2.1~~Bobra.0001s0008.1.v2.1.p3;ORF_type:3prime_partial_len:147_(+),score=4.53	0	+	1387	1825	0	1	1826	0
    Bobra.0001s0008.1.v2.1	0	1826	ID=Bobra.0001s0008.1.v2.1.p1;GENE.Bobra.0001s0008.1.v2.1~~Bobra.0001s0008.1.v2.1.p1;ORF_type:complete_len:276_(+),score=16.49	0	+	436	1264	0	1	1826	0
    Bobra.0001s0009.1.v2.1	0	1476	ID=Bobra.0001s0009.1.v2.1.p1;GENE.Bobra.0001s0009.1.v2.1~~Bobra.0001s0009.1.v2.1.p1;ORF_type:complete_len:178_(+),score=8.31	0	+	0	534	0	1	1476	0


    $ head BBRB_v2_trans_annotated.fasta.transdecoder.cds

    >Bobra.0001s0001.1.v2.1.p1 GENE.Bobra.0001s0001.1.v2.1~~Bobra.0001s0001.1.v2.1.p1  ORF type:complete len:224 (+),score=20.97 Bobra.0001s0001.1.v2.1:1-672(+)
    ATGGAGGGCACTAAGGTGGATAGGGCTGATCTCTTCATGAGAAAAATGCAAGACAAAATC
    ACCCTTGCAAGGAAGTGTTTGCGTGCAGCTCAAGACCGTCAGACGGCCTATGCCAACCGG
    CATAGGAGGGAGGCCATCTACCAGGAAGGGGACTATGTCCTCCTTTCAACCCGTGCGGCC
    CCCACGCTAACTTCAAACAGATTCAGCAAGCTGCAACCCAGGCACGTGGGACCCTTCAGG
    GTCATGGCGGTCACGCCCGACAACGTTAACTTCGTCACACTGGATCTTCCGGACCGGCTG
    GACATCCACCCCCGCATAAATGTCTGCTATTTGAAGCCATACTGGACACGTGAGGAGGCG
    CAGTCGCGTGGATACTGCCCCCCAAAAGTCATCACCAAAGCGGATGGCACCACTCACCTT
    GAGTGGGTAGTGGATGCACTAGTCGATCACCGGCTGTTGCACGACAAGGATGGGTTCTCC
    TACCATTTTGAGGATGGCACACCCTTTCTCGAGTACCTTGTAAAGTGGGCAGGCCATGGG


    *Para el transcriptoma ensamblado de novo con Trinity:*

    $ TransDecoder.LongOrfs -t /home/fani/Documentos/I_Semestre_2019/Tesis/BBRB_analisis_bioinfo/ensam/trinity/BBRB_novo_transcriptome.fasta
    NOTA: El total de trascritos a examinar fueron 129078
    $ TransDecoder.Predict -t /home/fani/Documentos/I_Semestre_2019/Tesis/BBRB_analisis_bioinfo/ensam/trinity/BBRB_novo_transcriptome.fasta
    $ head BBRB_novo_transcriptome.fasta.transdecoder.pep

    >TRINITY_DN0_c0_g1_i1.p1 TRINITY_DN0_c0_g1~~TRINITY_DN0_c0_g1_i1.p1  ORF type:complete len:562 (-),score=88.29 TRINITY_DN0_c0_g1_i1:277-1962(-)
    MLPITDAVRQLQVQSITILKVWIICAVAWLLWARCIALISWARIDIGIRKSGIRRGPAFW
    HALSGASSRQLHRVLLEWVEQYGPVFFLRLGPVHVVVVSDMRIAAEVLRPSGALDKGGHY
    DAVAQMTSEKGYHNLVSSPSNARWRMIRKTVAPAFSMISLKQAFPNMQKVMARTVAHLRD
    SGSANGVVNMTRVLECQGMDVIGHVGFGTSMDGFESLKPGFQGLDKVALALACADELRKR
    LRNPFRKYFFWSKDVKDGKQVFATFHRAMRELLEQMREAEERGKLAPDCIAGRLLQLRDP
    STGKPLHDDMIHSEFGAMFLAGFETMSHALAWVLYCVSQSPEVEAKLLAELASLGLLASS
    GNPNPRQVEWDDLPQLIYTDAVIKETLRMYPPVGQGTSREALKDVVLGGKYKIPKGTRVW
    LPIIPVQMSEALWENPTAFKPERFLQEGAELATQSPPSPNTPSTSYPGRDLDDEARHSTG
    KKYMPFSDGPRNCVGMPLAKIAMTATLATVLAHFSFQLAPEMGGPEGVRRSEHFHTTLGV

    $ head BBRB_novo_transcriptome.fasta.transdecoder.gff3

    TRINITY_DN0_c0_g1_i1	transdecoder	gene	1	2395	.	-	.	ID=TRINITY_DN0_c0_g1~~TRINITY_DN0_c0_g1_i1.p1;Name=ORF%20type%3Acomplete%20len%3A562%20%28-%29%2Cscore%3D88.29
    TRINITY_DN0_c0_g1_i1	transdecoder	mRNA	1	2395	.	-	.	ID=TRINITY_DN0_c0_g1_i1.p1;Parent=TRINITY_DN0_c0_g1~~TRINITY_DN0_c0_g1_i1.p1;Name=ORF%20type%3Acomplete%20len%3A562%20%28-%29%2Cscore%3D88.29
    TRINITY_DN0_c0_g1_i1	transdecoder	five_prime_UTR	1963	2395	.	-	.	ID=TRINITY_DN0_c0_g1_i1.p1.utr5p1;Parent=TRINITY_DN0_c0_g1_i1.p1
    TRINITY_DN0_c0_g1_i1	transdecoder	exon	1	2395	.	-	.	ID=TRINITY_DN0_c0_g1_i1.p1.exon1;Parent=TRINITY_DN0_c0_g1_i1.p1
    TRINITY_DN0_c0_g1_i1	transdecoder	CDS	277	1962	.	-	0	ID=cds.TRINITY_DN0_c0_g1_i1.p1;Parent=TRINITY_DN0_c0_g1_i1.p1
    TRINITY_DN0_c0_g1_i1	transdecoder	three_prime_UTR	1	276	.	-	.	ID=TRINITY_DN0_c0_g1_i1.p1.utr3p1;Parent=TRINITY_DN0_c0_g1_i1.p1

    TRINITY_DN0_c0_g1_i2	transdecoder	gene	1	3226	.	+	.	ID=TRINITY_DN0_c0_g1~~TRINITY_DN0_c0_g1_i2.p2;Name=ORF%20type%3Acomplete%20len%3A135%20%28%2B%29%2Cscore%3D10.45
    TRINITY_DN0_c0_g1_i2	transdecoder	mRNA	1	3226	.	+	.	ID=TRINITY_DN0_c0_g1_i2.p2;Parent=TRINITY_DN0_c0_g1~~TRINITY_DN0_c0_g1_i2.p2;Name=ORF%20type%3Acomplete%20len%3A135%20%28%2B%29%2Cscore%3D10.45
    TRINITY_DN0_c0_g1_i2	transdecoder	five_prime_UTR	1	2494	.	+	.	ID=TRINITY_DN0_c0_g1_i2.p2.utr5p1;Parent=TRINITY_DN0_c0_g1_i2.p2


    $ head BBRB_novo_transcriptome.fasta.transdecoder.bed

    track name='BBRB_novo_transcriptome.fasta.transdecoder.gff3'
    TRINITY_DN0_c0_g1_i1	0	2395	ID=TRINITY_DN0_c0_g1_i1.p1;TRINITY_DN0_c0_g1~~TRINITY_DN0_c0_g1_i1.p1;ORF_type:complete_len:562_(-),score=88.29	0	-	276	1962	0	1	2395	0
    TRINITY_DN0_c0_g1_i2	0	3226	ID=TRINITY_DN0_c0_g1_i2.p2;TRINITY_DN0_c0_g1~~TRINITY_DN0_c0_g1_i2.p2;ORF_type:complete_len:135_(+),score=10.45	0	+	2494	2899	0	1	3226	0
    TRINITY_DN0_c0_g1_i3	0	4390	ID=TRINITY_DN0_c0_g1_i3.p1;TRINITY_DN0_c0_g1~~TRINITY_DN0_c0_g1_i3.p1;ORF_type:complete_len:561_(-),score=88.35	0	-	2274	3957	0	1	4390	0
    TRINITY_DN0_c0_g1_i4	0	4572	ID=TRINITY_DN0_c0_g1_i4.p1;TRINITY_DN0_c0_g1~~TRINITY_DN0_c0_g1_i4.p1;ORF_type:complete_len:559_(-),score=86.91	0	-	2442	4119	0	1	4572	0
    TRINITY_DN10001_c0_g2_i1	0	857	ID=TRINITY_DN10001_c0_g2_i1.p1;TRINITY_DN10001_c0_g2~~TRINITY_DN10001_c0_g2_i1.p1;ORF_type:complete_len:145_(-),score=20.23	0	-	104	539	0	1	857	0
    TRINITY_DN10001_c0_g3_i1	0	960	ID=TRINITY_DN10001_c0_g3_i1.p1;TRINITY_DN10001_c0_g3~~TRINITY_DN10001_c0_g3_i1.p1;ORF_type:complete_len:145_(-),score=19.94	0	-	351	786	0	1	960	0
    TRINITY_DN10002_c0_g1_i1	0	840	ID=TRINITY_DN10002_c0_g1_i1.p1;TRINITY_DN10002_c0_g1~~TRINITY_DN10002_c0_g1_i1.p1;ORF_type:complete_len:104_(-),score=2.21	0	-	293	605	0	1	840	0
    TRINITY_DN10003_c0_g1_i1	0	925	ID=TRINITY_DN10003_c0_g1_i1.p1;TRINITY_DN10003_c0_g1~~TRINITY_DN10003_c0_g1_i1.p1;ORF_type:3prime_partial_len:270_(+),score=-2.96	0	+	118	925	0	1	925	0
    TRINITY_DN10004_c0_g1_i1	0	375	ID=TRINITY_DN10004_c0_g1_i1.p1;TRINITY_DN10004_c0_g1~~TRINITY_DN10004_c0_g1_i1.p1;ORF_type:5prime_partial_len:113_(-),score=24.14	0	-	35	374	0	1	375	0


    $ head BBRB_novo_transcriptome.fasta.transdecoder.cds

    >TRINITY_DN0_c0_g1_i1.p1 TRINITY_DN0_c0_g1~~TRINITY_DN0_c0_g1_i1.p1  ORF type:complete len:562 (-),score=88.29 TRINITY_DN0_c0_g1_i1:277-1962(-)
    ATGTTACCAATCACAGACGCTGTCCGGCAACTCCAGGTCCAGAGCATCACGATTTTGAAA
    GTTTGGATCATCTGTGCCGTTGCCTGGCTTCTTTGGGCCCGTTGTATCGCTCTTATCTCA
    TGGGCCCGAATTGACATCGGCATCAGAAAGAGTGGCATAAGAAGAGGACCAGCCTTCTGG
    CATGCTTTGTCAGGGGCGTCATCACGACAACTGCATCGTGTCCTGCTTGAATGGGTGGAG
    CAATATGGACCCGTGTTCTTTTTACGTCTTGGGCCTGTGCATGTTGTGGTAGTCTCAGAC
    ATGAGAATTGCAGCAGAAGTGCTACGCCCGTCAGGTGCCCTTGATAAGGGGGGTCACTAC
    GACGCTGTTGCACAGATGACCAGCGAAAAGGGTTATCACAACTTGGTTTCTTCTCCCTCT
    AACGCACGCTGGCGCATGATCAGAAAAACGGTTGCACCGGCCTTCAGCATGATCAGCTTG
    AAGCAAGCCTTCCCAAACATGCAGAAGGTCATGGCACGCACAGTGGCTCATCTGAGAGAC


A partir de este punto se continuo trabajando solamente con los datos del transcriptoma guiado (el anotado con gffcompare).


#### Anotacion con bases de datos

Se descargo la base de datos Pfam (wget https://data.broadinstitute.org/Trinity/Trinotate_v3_RESOURCES/Pfam-A.hmm.gz) y el archivo descomprimido se copio a una carpeta de Mazorka ("/BBRB/Cabili/annotation") y alli se corrio un script de hmmer (hmmpress_script_BBRB.sh) usando esa base de datos:

    #PBS -N hmmer_BBRB
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=20:00:00
    #PBS -V
    #PBS -o hmmpress_BBRB.out
    #PBS -e hmmpress_BBRB.err


    module load hmmer/3.1b2

    cd $PBS_O_WORKDIR


    hmmpress Pfam-A.hmm
 

Se copio a la carpeta de Mazorka anteriormente mencionada, el archivo "BBRB_v2_trans_annotated.fasta.transdecoder.pep"
Y se corrio en Mazorka una anotacion con las bases de datos Pfam, SignalP y tmhmm:

    #PBS -N hmmscan_pfam_BBRB_st
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=20:00:00
    #PBS -V
    #PBS -o hmmscan_pfam_BBRB_st.out
    #PBS -e hmmscan_pfam_BBRB_st.err

    module load hmmer/3.1b2

    cd $PBS_O_WORKDIR

    hmmscan --cpu 2 --domtblout TrinotatePFAM_st.out Pfam-A.hmm BBRB_v2_trans_annotated.fasta.transdecoder.pep > pfam_st.log



De forma paralela se corrio BLAT en mazorka para anotar el transcriptoma ensamblado de novo.

    NOTA: El archivo de salida en formato ".psl" correspondera a una tabla que sera convertida posteriormente a formato ".gtf" y que se usara para realizar la tabla de cuentas para el analisis de expresion diferencial.


    #PBS -N BLAT_BBRB_trin
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=20:00:00
    #PBS -V
    #PBS -o BLAT_BBRB_trin.out
    #PBS -e BLAT_BBRB_trin.err

    module load blat/36

    blat -t=dna /LUSTRE/usuario/smata/BBRB/genome_data/Bbraunii_genome_502_v2.0.fa -q=dna /LUSTRE/usuario/smata/BBRB/transcriptome_trinity_2_8_3/trinity_all/BBRB_novo_transcriptome.fasta -maxIntron=60000 -fine -out=psl BBRB_novo_transcriptome.psl


#### *Martes 7 de enero de 2020*


Se continuo corriendo los modulos de SignalP y tmhmm en Mazorka, para la prediccion de regiones de peptido senal en los transcritos y predecir si estos transcritos codifican para helices transmembranales, respectivamente.


    *Para la prediccion de peptido senal con SignalP:*

    #PBS -N signalp_BBRB_st
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=20:00:00
    #PBS -V
    #PBS -o signalp_BBRB_st.out
    #PBS -e signalp_pfam_BBRB_st.err

   
    module load SignalP/4.1
   

    cd $PBS_O_WORKDIR

    signalp -f short -n signalp_st.out BBRB_v2_trans_annotated.fasta.transdecoder.pep > signalp_st.log &

   
    *Para la prediccion de helices transmembranales con tmhmm:*

    #PBS -N tmhmm_BBRB_st
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=20:00:00
    #PBS -V
    #PBS -o tmhmm_BBRB_st.out
    #PBS -e tmhmm_BBRB_st.err

    
    module load tmhmm/2.0c


    cd $PBS_O_WORKDIR


    tmhmm --short < BBRB_v2_trans_annotated.fasta.transdecoder.pep > 	t 


Se corrio un alineamiento con BLASTP y BLASTX usando ncbi-blast+ en Mazorka, de la secuencia de nucleotidos y aminoacidos de los transcritos.


    *Para el alineamiento de nucleotidos:*

    #PBS -N blastx_st
    #PBS -q default
    #PBS -l nodes=1:ppn=8,vmem=18gb,walltime=999:00:00
    #PBS -o blastx_st.out
    #PBS -e blastx_st.err

    
    module load ncbi-blast+/2.6.0


    cd $PBS_O_WORKDIR


    blastx -query /LUSTRE/usuario/smata/BBRB/stringtie_en/compare_gtf/BBRB_v2_trans_annotated.fasta -db /data/secuencias/NR/nr -num_threads 8 -max_target_seqs 1 -outfmt 6 -out blastx_BBRB_st.outmt6


    *Para el alineamiento de proteinas:*

    #PBS -N blastp_st
    #PBS -q default
    #PBS -l nodes=1:ppn=8,vmem=18gb,walltime=999:00:00
    #PBS -o blastp_st.out
    #PBS -e blastp_st.err


    module load ncbi-blast+/2.6.0


    cd $PBS_O_WORKDIR


    blastp -query /LUSTRE/usuario/smata/BBRB/Cabili/annotation/BBRB_v2_trans_annotated.fasta.transdecoder.pep -db /data/secuencias/NR/nr -num_threads 8 -max_target_seqs 1 -outfmt 6 -out blastp_BBRB_st.outmt6


#### *Miercoles 8 de enero de 2020*


Se corrio nuevamente el script para el modulo de CPC en Mazorka con algunas correcciones


    *Usando como input el transcriptoma de BBRB ensamblado con StringTie:*     

    #PBS -N CPC_BBRB2
    #PBS -q default
    #PBS -l nodes=1:ppn=1,mem=20gb,vmem=30gb,walltime=120:00:00
    #PBS -V
    #PBS -o cpc_BBRB2.out
    #PBS -e cpc_BBRB2.err

    module load cpc/0.9.r2
    module load ncbi-blast/2.2.26

    cd $PBS_O_WORKDIR


    run_predict.sh /LUSTRE/usuario/smata/BBRB/stringtie_en/compare_gtf/BBRB_v2_trans_annotated.fasta result_in_table cpc result_evidences


    
    *Usando como input el transcriptma de BBRB ensamblado con Trinity:*

    #PBS -N CPC_trin
    #PBS -q default
    #PBS -l nodes=1:ppn=1,mem=20gb,vmem=30gb,walltime=120:00:00
    #PBS -V
    #PBS -o cpc_BBRB_trin.out
    #PBS -e cpc_BBRB_trin.err

    module load cpc/0.9.r2
    module load ncbi-blast/2.2.26

    cd $PBS_O_WORKDIR


    run_predict.sh /LUSTRE/usuario/smata/BBRB/transcriptome_trinity_2_8_3/trinity_all/BBRB_novo_transcriptome.fasta result_in_table cpc result_evidences



#### *Jueves 9 de enero de 2020*


Se copio un script de perl (http://eugenes.org/gmod/tandy/blat2gff.pl) para convertir el archivo de alineamiento ".psl" a formato ".gff", obtenido al alinear el transcriptoma BBRB (ensamble de Trinity) con el genoma de BBRB v2. Se hizo ejecutable el archivo script, este se corrio y posteriormente se convirtio el archivo ".gff" a formato ".gtf":

    $ chmod +x blat2gff.pl
    $ ./blat2gff.pl < BBRB_novo_transcriptome.psl > BBRB_novo_transcriptome.gff

Se uso linea de comandos en la terminal de Linux, desde una carpeta local donde se encontraba el archivo y el script ejecutable.

Se continuo con el analisis de expression diferencial en R.



#### *Lunes 13 de enero de 2020*


Se dividio el archivo del transcriptoma ensamblado en formato fasta en archivos de 10 000 transcritos cada uno, usando el comando "awk":

    $ awk 'BEGIN {n_seq=0;} /^>/ {if(n_seq%10000==0){file=sprintf("BBRB_v2_trans_annotated%d.fasta",n_seq);} print >> file; n_seq++; next;} 
{ print >> file; }' < BBRB_v2_trans_annotated.fasta


Se verifico el tamano de cada archivo con el siguiente comando

    $ awk '/>/ { count++ } END { print count }' BBRB_v2_trans_annotated0.fasta     -----> 10 000 transcritos
    $ awk '/>/ { count++ } END { print count }' BBRB_v2_trans_annotated10000.fasta    -----> 10 000 transcritos
    $ awk '/>/ { count++ } END { print count }' BBRB_v2_trans_annotated20000.fasta    -----> 10 000 transcritos
    $ awk '/>/ { count++ } END { print count }' BBRB_v2_trans_annotated30000.fasta    -----> 7 359 transcritos

Por lo tanto se mando a correr de nuevo el CPC pero en 4 scripts por separado, correspondientes a cada archivo y se aumento la cantidad de horas de corrida. Cada script se corrio en una carpeta separada.


    *Se uso un script base y se modifico segun el nombre del input, y este fue el siguiente:*

    #PBS -N CPC0_BBRB2
    #PBS -q default
    #PBS -l nodes=1:ppn=1,mem=20gb,vmem=30gb,walltime=999:00:00
    #PBS -V
    #PBS -o cpc_BBRB2.out
    #PBS -e cpc_BBRB2.err


    module load cpc/0.9.r2
    module load ncbi-blast/2.2.26

    cd $PBS_O_WORKDIR


    run_predict.sh ./BBRB_v2_trans_annotated0.fasta result_in_table cpc result_evidences


Se corrio de nuevo el script de BLAT porque en el flag -q es rna y no dna!!


    #PBS -N BLAT_BBRB_trin
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=20:00:00
    #PBS -V
    #PBS -o BLAT_BBRB_trin.out
    #PBS -e BLAT_BBRB_trin.err

    module load blat/36

    blat -t=dna /LUSTRE/usuario/smata/BBRB/genome_data/Bbraunii_genome_502_v2.0.fa -q=rna /LUSTRE/usuario/smata/BBRB/transcriptome_trinity_2_8_3/trinity_all/BBRB_novo_transcriptome.fasta -maxIntron=60000 -fine -out=psl BBRB_novo_transcriptome.psl
 

Y se corrigio el script "blat2gff.pl", estas lineas:

    my $matchpart="match_part"
    my $source="BLAT"

Se cambio por esta:

    my $matchpart="exon"
    my $source="Trinity"

Para correr el script cuando termine de correr BLAT en Mazorka


#### *Martes 14 de enero de 2020*


Una vez finalizada la corrida de BLAT se convirtio el archivo de formato .psl a .gff usando el script modificado:

    $ ./blat2gff.pl < BBRB_novo_transcriptome.psl > BBRB_novo_transcriptome.gff

Se reviso con el visualizador de minmap, junto con el genoma de la microalga, los resultados del alineamiento de las lecturas (librerias en fastqc) contra el archivo gtf reportado, el gtf reportado y el sccafold_1 del los archivos gtf de cada transcriptoma.


#### *Viernes 17 de enero de 2020*


Se corrio gffcompare en mazorka en bash, para anotar el archivo gff del transcriptoma de novo usando el archivo gtf reportado del la version 2 de genoma de BBRB.

   
    $ module load gffcompare/0.10.4

    $ gffcompare -V -r /LUSTRE/usuario/smata/BBRB/genome_data/Bbraunii_502_v2.1.gene.anot.gtf -o compare /LUSTRE/usuario/smata/BBRB/transcriptome_trinity_2_8_3/trinity_all/BBRB_novo_transcriptome.gff
    $ 6170317 query transcripts found.

Los resultados de la anotacion con gffcompare fueron los siguientes:

    #= Summary for dataset: /LUSTRE/usuario/smata/BBRB/stringtie_en/merged_gtf/BBRB_v2_transcriptome.gtf 
    #     Query mRNAs :   37359 in   21771 loci  (30574 multi-exon transcripts)
    #            (7141 multi-transcript loci, ~1.7 transcripts per locus)
    # Reference mRNAs :   23678 in   20035 loci  (19101 multi-exon)
    # Super-loci w/ reference transcripts:    17319
    #-----------------| Sensitivity | Precision  |
    Base level:   100.0     |    76.6    |
    Exon level:   100.0     |    80.1    |
    Intron level:    99.9     |    86.3    |
    Intron chain level:   100.0     |    62.5    |
    Transcript level:   100.0     |    63.4    |
    Locus level:   100.0     |    84.0    |

    Matching intron chains:   19101
    Matching transcripts:   23678
    Matching loci:   20035

    Missed exons:       0/113878	(  0.0%)
    Novel exons:   13956/145808	(  9.6%)
    Missed introns:      83/91554	(  0.1%)
    Novel introns:    9326/105941	(  8.8%)
    Missed loci:       0/20035	(  0.0%)
    Novel loci:    3287/21771	( 15.1%)

    Total union super-loci across all input datasets: 21771 
    37359 out of 37359 consensus transcripts written in gffcmp.annotated.gtf (0 discarded as redundant)


Se decidio generar en Mazorka la tabla de cuentas del transcriptoma de novo ensamblado con Trinity, usando el archivo gff anotado con gffcompare y los alineamientos en formato ".bam" de las librerias de RNA-seq contra el genoma v2 de BBRB, usando Hisat2.


    #PBS -N featureCounts_v2
    #PBS -q default
    #PBS -l nodes=1:ppn=4,vmem=4gb,walltime=01:00:00
    #PBS -V
    #PBS -o fcounts_v2.out
    #PBS -e fcounts_v2.err

    module load subread/1.5.1

    cd $PBS_O_WORKDIR

    cd /LUSTRE/usuario/smata/BBRB/hisat2_align/align/align_v2_genome

    featureCounts -T 4 -s 1 -p -t exon -g gene_id -a /LUSTRE/usuario/smata/BBRB/transcriptome_trinity_2_8_3/compare_and_counts/BBRB_novo_transcriptome_annotated.gtf CBBRB_1_v2.bam CBBRB_2_v2.bam CBBRB_3_v2.bam LBBRB_1_v2.bam LBBRB_2_v2.bam LBBRB_3_v2.bam -o /LUSTRE/usuario/smata/BBRB/featurecounts/BBRB_trinity_counts.txt


Se cargo la tabla de cuentas del transcriptoma de novo en R, y se realizo el analisis de expresion diferencial con DESeq2.



#### *Martes 21 de enero de 2020*


Se extrajo la lista de ID's de los transcritos que resultaron con dominios proteicos del output de la corrida del hmmscan junto con la base de datos de Pfam, usando el transcriptoma guiado

    $ awk '{print $4}' TrinotatePFAM_st.out > list_hmmscan.out

Se extrajo una lista de los ID's de los transcritos del gtf del ensamble guiado de BBRB y se guardo en un archivo llamado "all_ts_list"

    $ cut -f9  /home/fani/Documents/Tesis_BP/BBRB_analysis/ensam/guided/BBRB_v2_trans_annotated.gtf | perl -pe 's/\;/\n/g' | grep transcript_id | cut -f2 -d' '  | sort -V | uniq > all_ts_novo_list

La lista de ID's del output de hmmscan (TrinotatePFAM_st.out) se copio en otro archivo con otro nombre, se ordenó y se eliminaron los ID's redundantes, y se guardó en un archivo nuevo llamado "mapped_ts_list"

    $ cp list_hmmscan.out exclude_list
    $ sort -V exclude_list | uniq > mapped_ts_list

Revisamos la cantidad de líneas de cada archivo llamado algo con "list"

    $ wc -l *list

    37359 all_ts_list # Archivo con ID's no redundantes del gtf del ensamble guiado
    165821 exclude_list # Es la lista que contiene todos los ID's que extraje del output del hmmscan (contiene redundantes)
    25065 mapped_ts_list Lista de ID's no redundantes del output de hmmscan
    228245 total

Eliminamos las " " de los ID's de "all_ts_list"

    $ perl -pi -e 's/\"//g' all_ts_list 
    $ less all_ts_list 

Para ver lo que era diferente entre los ID's del gtf del ensamble y los ID's del output del hmmscan, ahi vimos lo del la "p#".

    $ diff all_ts_list mapped_ts_list | less -S
    $ head all_ts_list mapped_ts_list 

Aqui supimos que el ID no hacía match por lo que revisamos el archivo y vimoss que al final del ID se agrego "p#" en el output del hmmscan (Pfam), la "p#" no estaba presente en el gtf del transcriptoma

    $ grep "Bobra.0001s0001.1.v2.1.p1" /home/fani/Documents/Tesis_BP/BBRB_analysis/ensam/guided/BBRB_v2_trans_annotated.gtf 

Quitando "p#" supimos que el ID sí se encontraba en el gtf del transcriptoma

    $ grep "Bobra.0001s0001.1.v2.1" /home/fani/Documents/Tesis_BP/BBRB_analysis/ensam/guided/BBRB_v2_trans_annotated.gtf 

Cambiamos el nombre de mapped_ts_list a exclude_nr_list 

    $ mv mapped_ts_list exclude_nr_list
    $ less exclude_nr_list 

Eliminamos el "p#" de los ID's

    $ perl -pe 's/\.p[0-9].*\n/\n/' exclude_nr_list | less

Para decirle que ejecutara el script en el mismo input usamos la opción -pi (in situ)

    $ perl -pi -e 's/\.p[0-9].*\n/\n/' exclude_nr_list 

Ordenamos los ID's, elimanos aquellos redundantes y el resultado se guardó en un output del mismo nombre

    $ echo "$(sort -V exclude_nr_list | uniq )" > exclude_nr_list 

Revisamos cuantos ID's quedaron luego de eliminar los redundantes

    $ wc -l exclude_nr_list # 21 879

Para ver lo que era diferente entre los ID's del gtf del ensamble y los ID's del output del hmmscan (sin ID's redundantes)

    $ diff exclude_nr_list all_ts_list | head

Buscamos un ID en ambas listas de ID's

    $ grep Bobra.0001s0006.1.v2.1 exclude_nr_list all_ts_list 

Para ver de nuevo lo que era diferente, a la vez buscamos con grep ">", le pedimos que cortara la columna #2, ordenara los ID's, eliminara redundantes y guardara los resultados en un nuevo archivo llamado "include_nr_list"

    $ diff exclude_nr_list all_ts_list | grep \> | cut -f2 -d' ' | sort -V | uniq > include_nr_list

Contamos la cantidad de líneas en los archivos finales

    $ wc -l *list

    37359 all_ts_list # Archivo con ID's no redundantes del gtf del ensamble guiado
    165821 exclude_list # Lista original de ID's del output de hmmscan
    21879 exclude_nr_list # Lista de ID's no redundantes del output de hmmscan, antes llamado "mapped_ts_list"
    15480 include_nr_list # Lista de ID's no redundantes que queríamos conservar
    240539 total

Y ahora si usamos el archivo que contenía los ID's que queríamos conservar para hacer match con el gtf del ensamble y el output es el archivo filtrado con nombre "hmmscan_filt_st.gtf", creamos una nueva carpeta llamada "filtered_files" y movimos el archivo a esa carpeta, finalmente verificamos la cantidad de transcritos que conservamos.

    $ grep -wFf include_nr_list /home/fani/Documents/Tesis_BP/BBRB_analysis/ensam/guided/BBRB_v2_trans_annotated.gtf > filter_hmmscan_st.gtf
    $ wc -l filter_hmmscan_st.gtf

    62775 filter_hmmscan_st.gtf

Revisamos cuántos transcritos eran inicialmente en el gtf del ensamble

    $ wc -l BBRB_v2_trans_annotated.gtf 

    259516 BBRB_v2_trans_annotated.gtf  # Por lo que eliminamos 196 741 transcritos


Se realizo la resta de la columna $5 y $4 y se (+1 para evitar que se excluyan transcritos que si pueden ser lncRNAs) y se mantuvieron aquellos cuyo resultados fueron >= 200 nt

    $ awk 'BEGIN{FS="\t"}{if($3=="transcript"){print $AF}}' filter_hmmscan_st.gtf | awk 'BEGIN{FS="\t"}{if( ($5-$4)+1 >= 200){print $AF}}' | less -S
    $ awk 'BEGIN{FS="\t"}{if($3=="transcript"){print $AF}}' filter_hmmscan_st.gtf | awk 'BEGIN{FS="\t"}{if( ($5-$4)+1 >= 200){print $AF}}' | wc -l # Al final no quedamos con 16 274 luego del filtro 2

El resultado se guardo en un archivo nuevo 

    $ awk 'BEGIN{FS="\t"}{if($3=="transcript"){print $AF}}' filter_hmmscan_st.gtf | awk 'BEGIN{FS="\t"}{if( ($5-$4)+1 >= 200){print $AF}}' > filter_200nt_st.gtf



#### Pipeline de Cabili con transcriptoma de novo


De manera paralela se inició la corrida del hmmscan en conjunto con la base de datos del Pfam en mazorka, usando como input el archivo ".pep" del transcriptoma de novo de BBRB.


    #PBS -N hmmscan_pfam_BBRB_trin
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=240:00:00
    #PBS -V
    #PBS -o hmmscan_pfam_BBRB_trin.out
    #PBS -e hmmscan_pfam_BBRB_trin.err


    module load hmmer/3.1b2


    cd $PBS_O_WORKDIR


    hmmscan --cpu 2 --domtblout TrinotatePFAM_trin.out Pfam-A.hmm BBRB_novo_transcriptome.fasta.transdecoder.pep > pfam_trin.log



#### *Miercoles 22 de enero de 2020*

Se extrajo la lista de ID's de los transcritos que resultaron con dominios proteicos del output de la corrida del hmmscan junto con la base de datos de Pfam, usando el transcriptoma de novo, se sortearon de una vez y se eliminaron ID's redundantes

    $ grep -v ^$ TrinotatePFAM_trin.out | grep -v ^# | awk '{print $4}' | sort -V | uniq > list_hmmscan_trin.out.nr

    $ head list_hmmscan_trin.out.nr

    TRINITY_DN0_c0_g1_i1.p1
    TRINITY_DN0_c0_g1_i3.p1
    TRINITY_DN0_c0_g1_i4.p1
    TRINITY_DN1_c0_g3_i1.p1
    TRINITY_DN2_c0_g1_i1.p1
    TRINITY_DN2_c1_g1_i1.p1
    TRINITY_DN2_c1_g1_i2.p1
    TRINITY_DN2_c1_g1_i2.p2
    TRINITY_DN2_c1_g2_i1.p1
    TRINITY_DN2_c1_g2_i2.p1


Se extrajo una lista de los ID's de los transcritos del gtf del ensamble de novo de BBRB y se guardo en un archivo llamado "all_ts_novo_list" y eliminamos las "" de los ID's

    $ cut -f9  /home/fani/Documents/Tesis_BP/BBRB_analysis/ensam/novo/BBRB_novo_transcriptome_annotated.gtf | perl -pe 's/\;/\n/g' | grep transcript_id | cut -f2 -d' ' | sort -V | uniq | perl -pe 's/\"//g' > all_ts_novo_list 
    $ perl -pi -e 's/\"//g' all_ts_novo_list 
    $ head all_ts_novo_list 
 
    TRINITY_DN0_c0_g1_i1_mid1
    TRINITY_DN0_c0_g1_i1_mid2
    TRINITY_DN0_c0_g1_i1_mid3
    TRINITY_DN0_c0_g1_i1_mid4
    TRINITY_DN0_c0_g1_i1_mid5
    TRINITY_DN0_c0_g1_i1_mid6
    TRINITY_DN0_c0_g1_i1_mid7
    TRINITY_DN0_c0_g1_i1_mid8
    TRINITY_DN0_c0_g1_i1_mid9
    TRINITY_DN0_c0_g1_i1_mid10


Revisamos la cantidad de líneas de cada archivo llamado algo con "list"

    $ wc -l *list*

    6170317 all_ts_novo_list # Archivo con ID's no redundantes del gtf del ensamble guiado
    39427 list_hmmscan_trin.out.nr # Es la lista que contiene todos los ID's que extraje del output del hmmscan sin redundantes
    6209744 total

Para ver lo que era diferente entre los ID's del gtf del ensamble y los ID's del output del hmmscan, ahi vimos lo del "_mid#" y el "p#".

    $ diff all_ts_novo_list list_hmmscan_trin.out.nr | less -S
    $ head all_ts_novo_list list_hmmscan_trin.out.nr

    ==> all_ts_novo_list <==
    TRINITY_DN0_c0_g1_i1_mid1
    TRINITY_DN0_c0_g1_i1_mid2
    TRINITY_DN0_c0_g1_i1_mid3
    TRINITY_DN0_c0_g1_i1_mid4
    TRINITY_DN0_c0_g1_i1_mid5
    TRINITY_DN0_c0_g1_i1_mid6
    TRINITY_DN0_c0_g1_i1_mid7
    TRINITY_DN0_c0_g1_i1_mid8
    TRINITY_DN0_c0_g1_i1_mid9
    TRINITY_DN0_c0_g1_i1_mid10

    ==> list_hmmscan_trin.out.nr <==
    TRINITY_DN0_c0_g1_i1.p1
    TRINITY_DN0_c0_g1_i3.p1
    TRINITY_DN0_c0_g1_i4.p1
    TRINITY_DN1_c0_g3_i1.p1
    TRINITY_DN2_c0_g1_i1.p1
    TRINITY_DN2_c1_g1_i1.p1
    TRINITY_DN2_c1_g1_i2.p1
    TRINITY_DN2_c1_g1_i2.p2
    TRINITY_DN2_c1_g2_i1.p1
    TRINITY_DN2_c1_g2_i2.p1


Se tuvo que remover la parte que decia "_mid#" del archivo "all_ts_novo_list" y una vez verificado con "head" se ejecuto el script y se sobre escribio el resultado en el mismo archivo

    $ perl -pe 's/_mid[0-9].*\n/\n/' all_ts_novo_list > all_ts_novo_list_clean
    $ head all_ts_novo_list_clean

    TRINITY_DN0_c0_g1_i1
    TRINITY_DN0_c0_g1_i1
    TRINITY_DN0_c0_g1_i1
    TRINITY_DN0_c0_g1_i1
    TRINITY_DN0_c0_g1_i1
    TRINITY_DN0_c0_g1_i1
    TRINITY_DN0_c0_g1_i1
    TRINITY_DN0_c0_g1_i1
    TRINITY_DN0_c0_g1_i1
    TRINITY_DN0_c0_g1_i1

Por otra parte se tuvo que remover el "p#" del archivo "list_hmmscan_trin.out.nr"

    $ perl -pe 's/\.p[0-9].*\n/\n/' list_hmmscan_trin.out.nr > list_hmmscan_trin.out.nr_clean
    $ head  list_hmmscan_trin.out.nr_clean

    TRINITY_DN0_c0_g1_i1
    TRINITY_DN0_c0_g1_i3
    TRINITY_DN0_c0_g1_i4
    TRINITY_DN1_c0_g3_i1
    TRINITY_DN2_c0_g1_i1
    TRINITY_DN2_c1_g1_i1
    TRINITY_DN2_c1_g1_i2
    TRINITY_DN2_c1_g1_i2
    TRINITY_DN2_c1_g2_i1
    TRINITY_DN2_c1_g2_i2


Ordenamos los ID's, elimanos aquellos redundantes y el resultado se guardó en un output con el nombre exclude_trin_nr_list

    $ wc -l list_hmmscan_trin.out.nr_clean # Antes de eliminar redundantes: 39 427
    $ echo "$(sort -V list_hmmscan_trin.out.nr_clean | uniq )" > list_hmmscan_trin.out.nr_clean
    $ wc -l list_hmmscan_trin.out.nr_clean # Despues de eliminar redundantes: 35 660
     
    $ wc -l all_ts_novo_list_clean # Antes de eliminar redundantes: 6 170 317 
    $ echo "$(sort -V all_ts_novo_list_clean | uniq )" > all_ts_novo_list_clean 
    $ wc -l all_ts_novo_list_clean # Despues de eliminar redundantes: 85 871

Para ver lo que era diferente entre los ID's del gtf del ensamble y los ID's del output del hmmscan (sin ID's redundantes "new"), se hizo lo siguiente

    $ diff list_hmmscan_trin.out.nr_clean all_ts_novo_list_clean | head

    1a2
    > TRINITY_DN0_c0_g1_i2
    3a5,14
    > TRINITY_DN0_c0_g2_i1
    > TRINITY_DN1_c0_g1_i1
    > TRINITY_DN1_c0_g1_i3
    > TRINITY_DN1_c0_g1_i5
    > TRINITY_DN1_c0_g1_i6
    > TRINITY_DN1_c0_g1_i7
    > TRINITY_DN1_c0_g1_i8

Ahora que sabemos que tenemos el simbolo ">" y que tenemos que eliminarlo, en una misma linea de comando se ejecuta la accion y ademas conservamos todas aquellas diferencias en ambos archivos (ojo el orden de los archivos es importante)
Guardamos los resultados en un archivo nuevo llamado "include_list"

    $ diff list_hmmscan_trin.out.nr_clean all_ts_novo_list_clean | grep \> | awk '{print $2}' | sort -V | uniq > include_list
    $ wc -l include_list #  Son 63 799

Tomando en cuenta que nuestros ID's del gtf del ensamble de novo tienen al final un "_mid#", editamos el "include_list" para agregarle "_mid" y generamos un archivo nuevo para no perder el original

    $ perl -pe 's/$/_mid/' include_list > include_list_dirt
    $ head include_list_dirt

    TRINITY_DN0_c0_g1_i2_mid
    TRINITY_DN0_c0_g2_i1_mid
    TRINITY_DN1_c0_g1_i1_mid
    TRINITY_DN1_c0_g1_i3_mid
    TRINITY_DN1_c0_g1_i5_mid
    TRINITY_DN1_c0_g1_i6_mid
    TRINITY_DN1_c0_g1_i7_mid
    TRINITY_DN1_c0_g1_i8_mid
    TRINITY_DN1_c0_g1_i10_mid
    TRINITY_DN1_c0_g1_i11_mid

Ejecutamos con perl un script para ver cuantos transcritos nos quedamos al final del filtrado

    $ grep -cFf include_list_dirt BBRB_novo_transcriptome_annotated.gtf # Son 9 964 001
    $ wc -l BBRB_novo_transcriptome_annotated.gtf # En el transcriptoma tenemos 13 937 136, eliminamos un total de 3 973 135 transcritos

Ejecutamos de nuevo perl y guardamos los resultados en un archivo nuevo (el archivo final filtrado)

    $ grep -Ff include_list_dirt BBRB_novo_transcriptome_annotated.gtf > filter_hmmscan_trin.gtf
    $ head filter_hmmscan_trin.gtf

    scaffold_1	Trinity	transcript	1193	1340	.	+	.	transcript_id "TRINITY_DN5493_c0_g2_i3_mid1"; gene_id "XLOC_000001"; gene_name "Bobra.0001s0001.v2.1"; cmp_ref "Bobra.0001s0001.1.v2.1"; class_code "o"; tss_id "TSS1";
    scaffold_1	Trinity	exon	1193	1340	.	+	.	transcript_id "TRINITY_DN5493_c0_g2_i3_mid1"; gene_id "XLOC_000001"; exon_number "1";
    scaffold_1	Trinity	transcript	2236	2609	.	+	.	transcript_id "TRINITY_DN93_c1_g1_i8_mid2"; gene_id "XLOC_000002"; contained_in "TRINITY_DN93_c1_g1_i4_mid2"; class_code "u"; tss_id "TSS2";
    scaffold_1	Trinity	exon	2236	2507	.	+	.	transcript_id "TRINITY_DN93_c1_g1_i8_mid2"; gene_id "XLOC_000002"; exon_number "1";
    scaffold_1	Trinity	exon	2515	2609	.	+	.	transcript_id "TRINITY_DN93_c1_g1_i8_mid2"; gene_id "XLOC_000002"; exon_number "2";
    scaffold_1	Trinity	transcript	2236	2609	.	+	.	transcript_id "TRINITY_DN93_c1_g1_i4_mid2"; gene_id "XLOC_000002"; class_code "u"; tss_id "TSS2";
    scaffold_1	Trinity	exon	2236	2507	.	+	.	transcript_id "TRINITY_DN93_c1_g1_i4_mid2"; gene_id "XLOC_000002"; exon_number "1";
    scaffold_1	Trinity	exon	2515	2609	.	+	.	transcript_id "TRINITY_DN93_c1_g1_i4_mid2"; gene_id "XLOC_000002"; exon_number "2";
    scaffold_1	Trinity	transcript	2237	2313	.	+	.	transcript_id "TRINITY_DN816_c1_g1_i4_mid3"; gene_id "XLOC_000002"; gene_name "Bobra.0001s0001.v2.1"; contained_in "TRINITY_DN93_c1_g1_i2_mid2"; cmp_ref "Bobra.0001s0001.1.v2.1"; class_code "p"; tss_id "TSS2";
    scaffold_1	Trinity	transcript	2237	2549	.	+	.	transcript_id "TRINITY_DN93_c1_g1_i2_mid2"; gene_id "XLOC_000002"; contained_in "TRINITY_DN93_c1_g1_i7_mid2"; class_code "u"; tss_id "TSS2";


Contamos la cantidad de líneas en los archivos finales

    $ wc -l *list*

    6170317 all_ts_novo_list # Archivo con ID's no redundantes del gtf del ensamble guiado
    85871 all_ts_novo_list_clean # # Despues de haber quitado las "_mid#" en los ID's
    261512 list_hmmscan_trin.out # Archivo original con ID's del output de hmmscan/Pfam
    39427 list_hmmscan_trin.out.nr # Archivo con ID's no redundantes del output de hmmscan/Pfam
    35660 list_hmmscan_trin.out.nr_clean # Despues de haber quitado las "p#" en los ID's
    63799 include_list # La lista de los ID's que queremos conservar
    63799 include_list_dirt # La lista de ID's que queremos conservar pero modificada con "_mid"
    6458873 total

Posteriormente se procedio a hacer el segundo filtro de la longitud de secuencia
Se tomaron todos las lineas pertenecientes a transcritos y se revisaron primero

    $ awk 'BEGIN{FS="\t"}{if($3=="transcript"){print $AF}}' filter_hmmscan_trin.gtf | head

    scaffold_1	Trinity	transcript	1193	1340	.	+	.	transcript_id "TRINITY_DN5493_c0_g2_i3_mid1"; gene_id "XLOC_000001"; gene_name "Bobra.0001s0001.v2.1"; cmp_ref "Bobra.0001s0001.1.v2.1"; class_code "o"; tss_id "TSS1";
    scaffold_1	Trinity	transcript	2236	2609	.	+	.	transcript_id "TRINITY_DN93_c1_g1_i8_mid2"; gene_id "XLOC_000002"; contained_in "TRINITY_DN93_c1_g1_i4_mid2"; class_code "u"; tss_id "TSS2";
    scaffold_1	Trinity	transcript	2236	2609	.	+	.	transcript_id "TRINITY_DN93_c1_g1_i4_mid2"; gene_id "XLOC_000002"; class_code "u"; tss_id "TSS2";
    scaffold_1	Trinity	transcript	2237	2313	.	+	.	transcript_id "TRINITY_DN816_c1_g1_i4_mid3"; gene_id "XLOC_000002"; gene_name "Bobra.0001s0001.v2.1"; contained_in "TRINITY_DN93_c1_g1_i2_mid2"; cmp_ref "Bobra.0001s0001.1.v2.1"; class_code "p"; tss_id "TSS2";
    scaffold_1	Trinity	transcript	2237	2549	.	+	.	transcript_id "TRINITY_DN93_c1_g1_i2_mid2"; gene_id "XLOC_000002"; contained_in "TRINITY_DN93_c1_g1_i7_mid2"; class_code "u"; tss_id "TSS2";
    scaffold_1	Trinity	transcript	2237	2609	.	+	.	transcript_id "TRINITY_DN93_c1_g1_i7_mid2"; gene_id "XLOC_000002"; contained_in "TRINITY_DN93_c1_g1_i4_mid2"; class_code "u"; tss_id "TSS2";
    scaffold_1	Trinity	transcript	2284	2650	.	+	.	transcript_id "TRINITY_DN7179_c0_g1_i3_mid1"; gene_id "XLOC_000002"; gene_name "Bobra.0001s0001.v2.1"; contained_in "TRINITY_DN7179_c0_g1_i5_mid1"; cmp_ref "Bobra.0001s0001.1.v2.1"; class_code "p"; tss_id "TSS2";
    scaffold_1	Trinity	transcript	2284	2671	.	+	.	transcript_id "TRINITY_DN7179_c0_g1_i5_mid1"; gene_id "XLOC_000002"; gene_name "Bobra.0001s0001.v2.1"; contained_in "TRINITY_DN7179_c0_g1_i8_mid1"; cmp_ref "Bobra.0001s0001.1.v2.1"; class_code "p"; tss_id "TSS2";
    scaffold_1	Trinity	transcript	2284	2720	.	+	.	transcript_id "TRINITY_DN7179_c0_g1_i8_mid1"; gene_id "XLOC_000002"; gene_name "Bobra.0001s0001.v2.1"; contained_in "TRINITY_DN7179_c0_g1_i7_mid1"; cmp_ref "Bobra.0001s0001.1.v2.1"; class_code "p"; tss_id "TSS2";
    scaffold_1	Trinity	transcript	2284	2720	.	+	.	transcript_id "TRINITY_DN7179_c0_g1_i7_mid1"; gene_id "XLOC_000002"; gene_name "Bobra.0001s0001.v2.1"; contained_in "TRINITY_DN7179_c0_g1_i1_mid1"; cmp_ref "Bobra.0001s0001.1.v2.1"; class_code "p"; tss_id "TSS2";


Se realizo la resta de la columna $5 y $4 y se (+1 para evitar que se excluyan transcritos que si pueden ser lncRNAs) y se mantuvieron aquellos cuyo resultados fueron >= 200 nt

    $ awk 'BEGIN{FS="\t"}{if($3=="transcript"){print $AF}}' filter_hmmscan_trin.gtf | awk 'BEGIN{FS="\t"}{if( ($5-$4)+1 >= 200){print $AF}}' | less -S
    $ awk 'BEGIN{FS="\t"}{if($3=="transcript"){print $AF}}' filter_hmmscan_trin.gtf | awk 'BEGIN{FS="\t"}{if( ($5-$4)+1 >= 200){print $AF}}' | wc -l # Al final nos quedamos con 664 020 luego del filtro #2

El resultado se guardo en un archivo nuevo 

    $ awk 'BEGIN{FS="\t"}{if($3=="transcript"){print $AF}}' filter_hmmscan_trin.gtf | awk 'BEGIN{FS="\t"}{if( ($5-$4)+1 >= 200){print $AF}}' > filter_200nt_trin.gtf


Posteriormente se convirtio a formato fasta el archivo filter_200nt_trin.gtf y filter_200nt_st.gtf, para correr getorf en Mazorka y realizar el tercer filtro del pipeline de Cabili.

    $ gffread -w filter_200nt_trin.fasta -g Bbraunii_genome_502_v2.0.fa filter_200nt_trin.gtf
    $ gffread -w filter_200nt_st.fasta -g Bbraunii_genome_502_v2.0.fa filter_200nt_st.gtf

    #PBS -N getorf_st
    #PBS -q default
    #PBS -l nodes=1:ppn=8,vmem=20gb,mem=20gb,walltime=240:00:00
    #PBS -V
    #PBS -o getorf_st.out
    #PBS -e getorf_st.err

    module load EMBOSS/6.6.0

    cd $PBS_O_WORKDIR

    getorf -minsize 225 -find 1 filter_200nt_st.fasta filter_getorf_st.fasta


    NOTA: Se uso -find 1 para traduccion de regiones entre codon de inicio y de paro


    #PBS -N getorf_trin
    #PBS -q default
    #PBS -l nodes=1:ppn=8,vmem=20gb,mem=20gb,walltime=24:00:00
    #PBS -V
    #PBS -o getorf_trin.out
    #PBS -e getorf_trin.err

    module load EMBOSS/6.6.0

    cd $PBS_O_WORKDIR

    getorf -minsize 225 -find 1 filter_200nt_trin.fasta filter_getorf_trin.fasta


#### *Jueves 23 de enero de 2020*


Se creo un archivo conteniendo la lista de ID's del archivo output_getorf_st.fasta (output de getorf) 

    $ grep -E ">" output_getorf_st.fasta | sed 's/>//g; s/\[[^]]*\]//g' > getorf_list_st_dirt # Son 387 984 
    $ less -S getorf_list_st_dirt 
    $ head getorf_list_st_dirt
    
    MSTRG.29.1_1  
    MSTRG.29.1_2  
    MSTRG.29.1_3  
    MSTRG.29.1_4  
    MSTRG.29.1_5  
    MSTRG.29.1_6  
    MSTRG.29.1_7  
    MSTRG.29.1_8  
    MSTRG.29.1_9  
    MSTRG.29.1_10 

El archivo se separo en dos para poder remover caracteres de los ID's fijados por StringTie y los ID's de transcritos ya anotados (gtf de referencia)

    Primero se aislo un solo archivo unicamente conteniendo los ID's que iniciaban con "MSTRG" y se edito para remover el caracter "_#" al final de cada ID, obtenerlos ordenados y no redundantes

    $ grep -E "MSTRG*" getorf_list_st_dirt | perl -pe 's/_.*\n/\n/' > MSTRG_orflist_st  # Son 244 023
    $ sort -V MSTRG_orflist_st | uniq > MSTRG_orflist_st.nr # Son 6 738
    $ head MSTRG_orflist_st.nr

    MSTRG.6.1
    MSTRG.8.1
    MSTRG.11.1
    MSTRG.13.1
    MSTRG.18.1
    MSTRG.20.1
    MSTRG.29.1
    MSTRG.31.1
    MSTRG.37.1
    MSTRG.45.1


    Luego del archivo original se removio todos los ID's que iniciaban con "MSTRG" para quedarnos solo con los que iniciaban con "Bobra.", este archivo fue editado tambien 

    $ sed '/MSTRG/d' getorf_list_st_dirt | sed -e 's/([^(^)]*)//g' > BOBRall_orflist_st # 143 961
    $ head BOBRall_orflist_st

    Bobra.00010007.1.v2.s1_1  
    Bobra.0001s0007.1.v2.1_2   
    Bobra.0001s0009.1.v2.1_1  
    Bobra.0001s0009.1.v2.1_2  
    Bobra.0001s0009.1.v2.1_3  
    Bobra.0001s0009.1.v2.1_4   
    Bobra.0001s0012.1.v2.1_1  
    Bobra.0001s0012.1.v2.1_2  
    Bobra.0001s0012.1.v2.1_3  
    Bobra.0001s0012.1.v2.1_4   

    $ tail BOBRall_orflist_st

    Bobra.9_2s0123.1.v2.1_31   
    Bobra.9_2s0123.1.v2.1_32   
    Bobra.9_2s0123.1.v2.1_33   
    Bobra.9_2s0123.1.v2.1_34   
    Bobra.9_2s0123.1.v2.1_35   
    Bobra.9_2s0123.1.v2.1_36   
    Bobra.9_2s0123.1.v2.1_37   
    Bobra.9_2s0123.1.v2.1_38   
    Bobra.9_2s0123.1.v2.1_39   
    Bobra.9_2s0123.1.v2.1_40 

    Como debiamos eliminar el caracter "_#" se procedio a verificar algun patron que todos los ID's compatieran y era "v2.1", por lo que se removio esa parte de los ID's para el eliminar con esto el "_#" y se agrego nuevamente el "v2.1"

    $ sed 's/v2.1_.*//' BOBRall_orflist_st | awk '{print $0"v2.1"}' | sort -V | uniq > BOBRall_orflist_st.nr  # Son 8 892
    $ head BOBRall_orflist_st.nr

    Bobra.0001s0007.1.v2.1
    Bobra.0001s0009.1.v2.1
    Bobra.0001s0012.1.v2.1
    Bobra.0001s0015.1.v2.1
    Bobra.0001s0016.2.v2.1
    Bobra.0001s0016.3.v2.1
    Bobra.0001s0019.1.v2.1
    Bobra.0002s0004.1.v2.1
    Bobra.0002s0006.1.v2.1
    Bobra.0002s0007.1.v2.1

    Ambos archivos se concatenaron para hacer una sola lista

    $ cat MSTRG_orflist_st.nr BOBRall_orflist_st.nr > include_getorf_list_st.nr # 15 630 (la suma exacta de MSTRG_orflist_st.nr y BOBRall_orflist_st.nr) 
 
    NOTA: Esta lista de ID's corresponde a los que vamos a conservar mas bien (por haber usado el *flag -minsize* al correr *getorf* en Mazorka)


Una vez teniendo nuestra lista de incluidos ya pudimos hacer el filtrado en el archivo filter_200nt_st.gtf, obtenido en el filtro anterior

    $ grep -Ff include_getorf_list_st.nr filter_200nt_st.gtf > filter_getorf_st.gtf # Quedaron 15 744


#### *Jueves 23 de enero de 2020*

    
Se detecto un error que pprovenia del gtf del ensamble, por lo que se procedio a correr de nuevo el ensamble de stringtie, se creo una carpeta llamada "new_v2_ensam" dentro de la carpeta de stringtie en Mazorka

    $ infoseq BBRB_v2_transcriptome.fasta:MSTRG.29.1    # Son 873 nt en este transcrito      ----->      archivo fasta del transcritoma ensamblado
    $ perl -pe 'if("\>"){s/\n/\t/g};s/\n//g;s/\>/\n\>/g' filter_200nt_st.fasta | grep "MSTRG\.29\.1" | cut -f1 --complement | perl -pe 's/\t//g' | wc -c    # Son 873 nt en este transcrito   -----> archivo gtf del transcriptoma anotado con gffcompare
    $ grep -w "MSTRG\.29\.1" BBRB_v2_trans_annotated.gtf | grep exon | awk 'BEGIN{FS="\t"}{print ($5-$4)+1}'   # 86+787 = 873

Vimos que el transcritos conservaba la misma longitud pero dos cosas:
    1) Cuando se convertio el archivo "filter_200nt_st.gtf" a formato fasta con gffread, aparecian regiones de GAPS en las secuencias de los transcritos, por esa razon el output del getorf salia con muchas "X"
    2) Vimos que en el gtf del transcriptoma original estaba el ID "MSTRG.29.1" con hasta 3 exones 

Se corrio el siguiente script para el ensamble guiado, agregando la anotacion (gtf) de referencia del genoma como input:

    #PBS -N st_ensam_BBRB_v2
    #PBS -q default
    #PBS -l nodes=1:ppn=8,mem=20gb,vmem=20gb,walltime=120:00:00
    #PBS -V
    #PBS -o new_ensam_BBRB_v2.out
    #PBS -e new_ensam_BBRB_v2.err


    module load stringtie/1.3.4d

    cd $PBS_O_WORKDIR

    file_list=/LUSTRE/usuario/smata/BBRB/hisat2_align/align/align_v2_genome/all_bam_BBRB_v2
    for condition in C L
    do
      for replica in BBRB_1 BBRB_2 BBRB_3
      do
        bam_files=$(awk -v replica="${condition}${replica}" 'BEGIN{FS="\t"}{if($2==replica){print $3}}' $file_list)
        stringtie -p 8 /LUSTRE/usuario/smata/BBRB/hisat2_align/align/align_v2_genome/$bam_files -l ${condition}${replica}_v2 -G /LUSTRE/usuario/smata/BBRB/genome_data/Bbraunii_502_v2.1.gene.anot.gtf -o /LUSTRE/usuario/smata/BBRB/stringtie_en/${condition}${replica}_v2.gtf -A /LUSTRE/usuario/smata/BBRB/stringtie_en/${condition}${replica}_v2_geneab.tab
      done
    done


Se corrio tambien el StrinTie merge para unir todos los gtf's de las librerias en un solo gtf:

    #PBS -N stringtie_merge_v2
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=00:30:00
    #PBS -V
    #PBS -o merged_gtf_BBRB_v2.out
    #PBS -e merged_gtf_BBRB_v2.err

    module load stringtie/1.3.4d

    cd $PBS_O_WORKDIR

    stringtie --merge -p 8 /LUSTRE/usuario/smata/BBRB/stringtie_en/new_ensam_v2/mergelist_BBRB_v2.txt -G /LUSTRE/usuario/smata/BBRB/genome_data/Bbraunii_502_v2.1.gene.anot.gtf -o /LUSTRE/usuario/smata/BBRB/stringtie_en/new_ensam_v2/BBRB_v2_transcriptome.gtf

El mergelist_BBRB_v2.txt corresponde a la lista de los archivos gtf de cada libreria con su *path*

    /LUSTRE/usuario/smata/BBRB/stringtie_en/new_ensam_v2/CBBRB_1_v2.gtf
    /LUSTRE/usuario/smata/BBRB/stringtie_en/new_ensam_v2/CBBRB_2_v2.gtf
    /LUSTRE/usuario/smata/BBRB/stringtie_en/new_ensam_v2/CBBRB_3_v2.gtf
    /LUSTRE/usuario/smata/BBRB/stringtie_en/new_ensam_v2/LBBRB_1_v2.gtf
    /LUSTRE/usuario/smata/BBRB/stringtie_en/new_ensam_v2/LBBRB_2_v2.gtf
    /LUSTRE/usuario/smata/BBRB/stringtie_en/new_ensam_v2/LBBRB_3_v2.gtf

Se realizo la comparacion entre la anotacion del genoma y la anotacion del transcriptoma con *gffcompare*:

    #PBS -N gffcompare
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=00:30:00
    #PBS -V
    #PBS -o compare_v2.out
    #PBS -e compare_v2.err

    module load gffcompare/0.10.4

    cd $PBS_O_WORKDIR

    gffcompare -r /LUSTRE/usuario/smata/BBRB/genome_data/Bbraunii_502_v2.1.gene.anot.gtf /LUSTRE/usuario/smata/BBRB/stringtie_en/new_ensam_v2/BBRB_v2_transcriptome.gtf  

El output "gffcmp.annotated.gtf" se renombro como "BBRB_v2_transcriptome.annotated.gtf 

Y se revisaron las estadisticas de la comparacion:

    #= Summary for dataset: /LUSTRE/usuario/smata/BBRB/stringtie_en/new_ensam_v2/BBRB_v2_transcriptome.gtf 
    #     Query mRNAs :   36987 in   21909 loci  (30340 multi-exon transcripts)
    #            (7227 multi-transcript loci, ~1.7 transcripts per locus)
    # Reference mRNAs :   23678 in   20035 loci  (19101 multi-exon)
    # Super-loci w/ reference transcripts:    17739
    #-----------------| Sensitivity | Precision  |
            Base level:   100.0     |    81.0    |
            Exon level:   100.0     |    81.0    |
          Intron level:    99.9     |    86.6    |
    Intron chain level:   100.0     |    63.0    |
      Transcript level:   100.0     |    64.0    |
           Locus level:   100.0     |    84.9    |

         Matching intron chains:   19101
           Matching transcripts:   23678
                  Matching loci:   20035

              Missed exons:       0/113878	(  0.0%)
               Novel exons:   12933/143361	(  9.0%)
            Missed introns:      75/91554	(  0.1%)
             Novel introns:    8807/105584	(  8.3%)
               Missed loci:       0/20035	(  0.0%)
                Novel loci:    3129/21909	( 14.3%)

     Total union super-loci across all input datasets: 21909 
    36987 out of 36987 consensus transcripts written in gffcmp.annotated.gtf (0 discarded as redundant)


#### *Sabado 25 de enero de 2020*


### Filtro 1: Eliminar secuencias codificantes ("CDS")

Se extrajo todas aquellas secuencias CDS del archivo de anotacion del genoma v2 de BBRB y se edito para tener solamente los IDs no redundantes de estas secuencias

    $ grep 'CDS' Bbraunii_502_v2.1.gene.anot.gtf > CDS_v2_gene.anot.gtf
    $ cut -f9 CDS_v2_gene.anot.gtf | perl -pe 's/\;/\n/g' | grep transcript_id | cut -f2 -d' ' | perl -pi -e 's/\"//g' > CDS_IDs_gene_anot
    $ head CDS_IDs_gene_anot

    Bobra.0001s0001.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0003.1.v2.1
    
    $ wc -l CDS_IDs_gene_anot # Son 126 319 secuencias CDS

    $ sort -V CDS_IDs_gene_anot | uniq > CDS_IDs_gene_anot_nr
    $ head CDS_IDs_gene_anot_nr

    Bobra.0001s0001.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0003.1.v2.1
    Bobra.0001s0004.1.v2.1
    Bobra.0001s0005.1.v2.1
    Bobra.0001s0006.1.v2.1
    Bobra.0001s0007.1.v2.1
    Bobra.0001s0008.1.v2.1
    Bobra.0001s0009.1.v2.1
    Bobra.0001s0010.1.v2.1

    $ wc -l CDS_IDs_gene_anot_nr # Son 23 685


Se extrajo los IDs de las secuencias del archivo de anotacion del transcriptoma guiado de BBRB

    $ cut -f9 BBRB_v2_transcriptome.annotated.gtf | perl -pe 's/\;/\n/g' | grep transcript_id | cut -f2 -d' ' | perl -pi -e 's/\"//g' > all_trans_ids_st
    $ head all_trans_ids_st

    Bobra.0001s0001.1.v2.1
    Bobra.0001s0001.1.v2.1
    MSTRG.31.2
    MSTRG.31.2
    MSTRG.31.2
    MSTRG.31.2
    MSTRG.31.2
    MSTRG.31.1
    MSTRG.31.1
    MSTRG.31.1

    $ wc -l all_trans_ids_st # Son 253 365
    $ sort -V all_trans_ids_st | uniq > all_trans_ids_st_nr
    $ head all_trans_ids_st_nr

    Bobra.0001s0001.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0003.1.v2.1
    Bobra.0001s0004.1.v2.1
    Bobra.0001s0005.1.v2.1
    Bobra.0001s0006.1.v2.1
    Bobra.0001s0007.1.v2.1
    Bobra.0001s0008.1.v2.1
    Bobra.0001s0009.1.v2.1
    Bobra.0001s0010.1.v2.1

    $ wc -l all_trans_ids_st_nr # Son 36 987


Se compararon ambas listas de IDs para ver que era diferente entre amas y conservar esos IDs diferentes en otro archivo

    $ diff CDS_IDs_gene_anot_nr all_trans_ids_st_nr | grep \> | cut -f2 -d' ' | sort -V | uniq > include_filter1_list
    $ head include_filter1_list

    MSTRG.1.1
    MSTRG.2.6
    MSTRG.3.1
    MSTRG.3.2
    MSTRG.3.3
    MSTRG.6.1
    MSTRG.7.1
    MSTRG.8.1
    MSTRG.9.1
    MSTRG.10.1

    $ wc -l include_filter1_list # Son 13306

Una vez teniendo la lista de IDs que queremos conservar se compara contra el gtf del emsamble guiado de BBRB y se mantiene todo lo que es igual en un archivo nuevo

    $ grep -wFf include_filter1_list BBRB_v2_transcriptome.annotated.gtf > filter1_CDS_st.gtf
    $ head filter1_CDS_st.gtf

    scaffold_1	StringTie	transcript	48764	50381	.	+	.	transcript_id "MSTRG.31.2"; gene_id "MSTRG.31"; gene_name "Bobra.0001s0003.v2.1"; xloc "XLOC_000002"; cmp_ref "Bobra.0001s0003.1.v2.1"; class_code "j"; tss_id "TSS2";
    scaffold_1	StringTie	exon	48764	48950	.	+	.	transcript_id "MSTRG.31.2"; gene_id "MSTRG.31"; exon_number "1";
    scaffold_1	StringTie	exon	49035	49105	.	+	.	transcript_id "MSTRG.31.2"; gene_id "MSTRG.31"; exon_number "2";
    scaffold_1	StringTie	exon	49283	50049	.	+	.	transcript_id "MSTRG.31.2"; gene_id "MSTRG.31"; exon_number "3";
    scaffold_1	StringTie	exon	50143	50381	.	+	.	transcript_id "MSTRG.31.2"; gene_id "MSTRG.31"; exon_number "4";
    scaffold_1	StringTie	transcript	48764	50381	.	+	.	transcript_id "MSTRG.31.1"; gene_id "MSTRG.31"; gene_name "Bobra.0001s0003.v2.1"; xloc "XLOC_000002"; cmp_ref "Bobra.0001s0003.1.v2.1"; class_code "j"; tss_id "TSS2";
    scaffold_1	StringTie	exon	48764	48950	.	+	.	transcript_id "MSTRG.31.1"; gene_id "MSTRG.31"; exon_number "1";
    scaffold_1	StringTie	exon	49035	49105	.	+	.	transcript_id "MSTRG.31.1"; gene_id "MSTRG.31"; exon_number "2";
    scaffold_1	StringTie	exon	49870	50381	.	+	.	transcript_id "MSTRG.31.1"; gene_id "MSTRG.31"; exon_number "3";
    scaffold_1	StringTie	transcript	48770	50381	.	+	.	transcript_id "MSTRG.31.5"; gene_id "MSTRG.31"; gene_name "Bobra.0001s0003.v2.1"; xloc "XLOC_000002"; cmp_ref "Bobra.0001s0003.1.v2.1"; class_code "j"; tss_id "TSS2";
    
    $ wc -l BBRB_v2_transcriptome.annotated.gtf # 253 365
    $ wc -l filter1_CDS_st.gtf # Son 94 820, por lo que eliminamos 158 545 transcritos del transcriptoma guiado de BBRB


### Filtro 2: Eliminar secuencias con lonitud mayor o igual a 200 nt

Se tomo el archivo del primer filtro, es decir, "filter1_CDS_st.gtf" y se removieron de este todos aquellos transcritos cuya longitud fueran mayor o igual a los 200 nt

Tomando en cuenta que las columnas $4 y $5 corresponden a las coordenadas de inicio y fin de los transcritos, se procedio a hacer una resta (+1 para evitar que se excluyan transcritos que si pueden ser lncRNAs) entre ambas para sacar el valor de la longitud.

    $ awk 'BEGIN{FS="\t"}{if($3=="transcript"){print $AF}}' filter1_CDS_st.gtf | awk 'BEGIN{FS="\t"}{if( ($5-$4)+1 >= 200){print $AF}}' | less -S
    $ awk 'BEGIN{FS="\t"}{if($3=="transcript"){print $AF}}' filter1_CDS_st.gtf | awk 'BEGIN{FS="\t"}{if( ($5-$4)+1 >= 200){print $AF}}' > filter2_200nt_st.gtf
    $ head filter2_200nt_st.gtf

    scaffold_1	StringTie	transcript	48764	50381	.	+	.	transcript_id "MSTRG.31.2"; gene_id "MSTRG.31"; gene_name "Bobra.0001s0003.v2.1"; xloc "XLOC_000002"; cmp_ref "Bobra.0001s0003.1.v2.1"; class_code "j"; tss_id "TSS2";
    scaffold_1	StringTie	transcript	48764	50381	.	+	.	transcript_id "MSTRG.31.1"; gene_id "MSTRG.31"; gene_name "Bobra.0001s0003.v2.1"; xloc "XLOC_000002"; cmp_ref "Bobra.0001s0003.1.v2.1"; class_code "j"; tss_id "TSS2";
    scaffold_1	StringTie	transcript	48770	50381	.	+	.	transcript_id "MSTRG.31.5"; gene_id "MSTRG.31"; gene_name "Bobra.0001s0003.v2.1"; xloc "XLOC_000002"; cmp_ref "Bobra.0001s0003.1.v2.1"; class_code "j"; tss_id "TSS2";
    scaffold_1	StringTie	transcript	48770	50381	.	+	.	transcript_id "MSTRG.31.4"; gene_id "MSTRG.31"; gene_name "Bobra.0001s0003.v2.1"; xloc "XLOC_000002"; cmp_ref "Bobra.0001s0003.1.v2.1"; class_code "j"; tss_id "TSS2";
    scaffold_1	StringTie	transcript	201212	204971	.	+	.	transcript_id "MSTRG.2.6"; gene_id "MSTRG.2"; gene_name "Bobra.0001s0016.v2.1"; xloc "XLOC_000009"; cmp_ref "Bobra.0001s0016.7.v2.1"; class_code "j"; tss_id "TSS9";
    scaffold_1	StringTie	transcript	266693	267578	.	+	.	transcript_id "MSTRG.9.1"; gene_id "MSTRG.9"; xloc "XLOC_000013"; class_code "u"; tss_id "TSS13";
    scaffold_1	StringTie	transcript	50140	51615	.	-	.	transcript_id "MSTRG.32.1"; gene_id "MSTRG.32"; gene_name "Bobra.0001s0004.v2.1"; xloc "XLOC_000015"; cmp_ref "Bobra.0001s0004.1.v2.1"; class_code "k"; tss_id "TSS15";
    scaffold_1	StringTie	transcript	50489	51615	.	-	.	transcript_id "Bobra.0001s0004.1.v2.1"; gene_id "MSTRG.32"; gene_name "Bobra.0001s0004.v2.1"; xloc "XLOC_000015"; ref_gene_id "Bobra.0001s0004.v2.1"; contained_in "MSTRG.32.1"; cmp_ref "Bobra.0001s0004.1.v2.1"; class_code "="; tss_id "TSS15";
    scaffold_1	StringTie	transcript	204591	207829	.	-	.	transcript_id "MSTRG.3.1"; gene_id "MSTRG.3"; gene_name "Bobra.0001s0017.v2.1"; xloc "XLOC_000021"; cmp_ref "Bobra.0001s0017.1.v2.1"; class_code "j"; tss_id "TSS21";
    scaffold_1	StringTie	transcript	204600	214352	.	-	.	transcript_id "MSTRG.3.2"; gene_id "MSTRG.3"; gene_name "Bobra.0001s0017.v2.1"; xloc "XLOC_000021"; cmp_ref "Bobra.0001s0017.1.v2.1"; class_code "k"; tss_id "TSS22";

    $ wc -l filter2_200nt_st.gtf # Son 15 421, por lo que se removieron 79 399 (tomando en cuenta que en el paso anterior conservamos 94 820


### Filtro 3: Eliminar secuencias con ORF mayores o iguales a 75 aa (225 nt)

Se convirtio a formato fasta el archivo gtf del filtro 2 para correr en Mazorka getorf del modulo de EMBOSS.

    $ gffread -w filter2_200nt_st.fasta -g Bbraunii_genome_502_v2.0.fa filter2_200nt_st.gtf
    $ head filter2_200nt_st.fasta

    >MSTRG.31.1
    TGGTCAAGAGATCTGTGCTATCATTAGCGCAGACGAACCTAAAGAGTTACCGTCAGATGAACAAATATGT
    GAATTTAGGTGTGAACTCAACCATTCCAATTTGAACATGGCGAAGGCCACCAGTTTAAAAGAGGCCATAG
    CGAACTTCGAGAAGGCCAAAGGCACCAGCGCTGCAGAAGCAGAAAAGGTCGTGCTTTGCGCAAAGTTTAC
    ATAGTCAAACAGACATGAAGAACCAAATTGTTTACTGACATCCATCATTGTTACGTATCAGGTCGAGCTT
    ATGGCGCAGGTGCCACCCATCGAGAAGTTAGATGCGGCTCTTTCGAGCTTAAAGTCGTGCAGGTAATTCC
    TTACCGCTGTATTTGTTTCTCTGCCGATTATTATTCTCTTATGATTGGCGCCTGAAAGGCCTATAGAGTC
    TTAAGTAGTATTTTGTTTGGCCAGTACTGTTTGGGGAGGCAGCAACGACACCTAGAAAGGTATAAAATTG
    TTCACTTAGCCGTGACAAATCTGTTGCAGACACTTATCCTTATCCACCAACAACATCGAGAAGATTAGTA
    ATTTGGCAGGCTTGGATAACTTGCGGATATTGTCACTTGGCCGGAATCAGATCAAAAAGCTTGAAAATTT

Se verifico la presencia de *gaps*  (N) en el archivo fasta.

    $ awk '/[NNN]/' RS='>' filter2_200nt_st.fasta > N_list_filter2  

Si se encontraron secuencias cuyas N eran bastantes y se extrajeron los IDs de estas.

    $ grep 'MSTRG' N_list_filter2 | sort -V | uniq > MSTRG_with_N_list  
    $ wc -l MSTRG_with_N_list # Son 1 572
    $ grep 'Bobra' N_list_filter2 | sort -V | uniq > Bobra_with_N_list
    $ wc -l grep Bobra_with_N_list # Son 47 
    $ cat MSTRG_with_N_list Bobra_with_N_list > N_list_filter2_id
    $ wc -l N_list_filter2_id # Son 1619
    $ head N_list_filter2_id

    MSTRG.79.2
    MSTRG.79.4
    MSTRG.86.1
    MSTRG.86.2
    MSTRG.90.1
    MSTRG.90.2
    MSTRG.90.3
    MSTRG.90.4
    MSTRG.90.6
    MSTRG.91.1

Revisamos en el archivo fasta del transcriptoma guiado de BBRB, aquellos IDs cuyas secuencias tuvieran gaps (En la carpeta del ensamble guiado).

    $ awk '/[NNN]/' RS='>' BBRB_v2_transcriptome.annotated.fasta > N_list_trans_st
    $ grep 'MSTRG' N_list_trans_st | sort -V | uniq > MSTRG_N_list_st 
    $ wc -l MSTRG_N_list_st  # Son 113
    $ grep 'Bobra' N_list_trans_st | sort -V | uniq > Bobra_N_list_st
    $ wc -l grep Bobra_N_list_st # Son 38
    $ cat MSTRG_N_list_st Bobra_N_list_st > N_list_id_st
    $ wc -l N_list_id_st # Son 151
    $ head N_list_id_st

    MSTRG.129.1
    MSTRG.368.1
    MSTRG.391.2
    MSTRG.699.1
    MSTRG.918.1
    MSTRG.1030.1
    MSTRG.1197.1
    MSTRG.1413.6
    MSTRG.1536.1
    MSTRG.2287.1



#### *Lunes 27 de enero de 2020*


Se reviso con la herramienta *infoseq* de EMBOSS el archivo gtf del ensamble del transcriptoma. 

    $ infoseq BBRB_v2_transcriptome.annotated.fasta -only -name -type -length > transcripts_list
    $ head transcripts_list

    Name                      Length 
    Bobra.0001s0001.1.v2.1    672    
    Bobra.0001s0002.1.v2.1    2066   
    Bobra.0001s0003.1.v2.1    849    
    MSTRG.31.1                770    
    MSTRG.31.2                1264   
    MSTRG.31.4                1351   
    MSTRG.31.5                671    
    MSTRG.32.1                963    
    Bobra.0001s0004.1.v2.1    813   

Se comparo la lista de transcritos generada con el archivo gtf del ensamble, usando especificamente el tamaño para determinar cuales eran los transcritos maduros en el archivo gtf, los cuales se debian remover, debido a que generaban el problema de los *gaps* al convertir el archivo gtf a fasta
Por ejemplo se tomo como referencia uno de los transcritos de la lista de infoseq, "Bobra.0001s0002.1.v2.1" y se reviso este ID en el archivo de coordenadas.

    $ grep -w 'Bobra.0001s0002.1.v2.1' BBRB_v2_transcriptome.annotated.gtf

    scaffold_1	StringTie	transcript	44525	48780	.	-	.	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "MSTRG.30"; gene_name "Bobra.0001s0002.v2.1"; xloc "XLOC_000014"; ref_gene_id "Bobra.0001s0002.v2.1"; cmp_ref "Bobra.0001s0002.1.v2.1"; class_code "="; tss_id "TSS14";
    scaffold_1	StringTie	exon	44525	44791	.	-	.	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "MSTRG.30"; exon_number "1";
    scaffold_1	StringTie	exon	45095	45304	.	-	.	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "MSTRG.30"; exon_number "2";
    scaffold_1	StringTie	exon	45727	46044	.	-	.	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "MSTRG.30"; exon_number "3";
    scaffold_1	StringTie	exon	46232	46395	.	-	.	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "MSTRG.30"; exon_number "4";
    scaffold_1	StringTie	exon	46996	47205	.	-	.	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "MSTRG.30"; exon_number "5";
    scaffold_1	StringTie	exon	47585	47727	.	-	.	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "MSTRG.30"; exon_number "6";
    scaffold_1	StringTie	exon	47937	48255	.	-	.	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "MSTRG.30"; exon_number "7";
    scaffold_1	StringTie	exon	48346	48780	.	-	.	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "MSTRG.30"; exon_number "8";
 
Se observo que este transcrito presenta un total de 8 exones, se sumo su longitud (2066), la cual concuerda con el valor generado por *infoseq*. Por lo que se llego a la conclusion de que la linea con el identificador "transcript" es la responsable de generar regiones de *gaps* (N's) en la secuencia cuando el archivo gtf es convertido a fasta, debido a que el archivo gtf solo cuenta con
exones y el transcrito completo, no los intrones, por lo que al hacer la conversion de formato *gffread* resuelve las regiones correspondientes a intrones como *gaps*, con base a esto se decidio seleccionar solamente aquellas
secuencias con el identificador "exon" en la columna #3 del archivo gtf del ensamble guiado de BBRB.

    $ grep 'exon' BBRB_v2_transcriptome.annotated.gtf > BBRB_v2_transcriptome.annotated.exon.gtf
    $ head BBRB_v2_transcriptome.annotated.exon.gtf

    scaffold_1	phytozomev12	exon	581	1252	.	+	.	transcript_id "Bobra.0001s0001.1.v2.1"; gene_id "Bobra.0001s0001.v2.1"; exon_number "1";
    scaffold_1	StringTie	exon	48764	48950	.	+	.	transcript_id "MSTRG.31.2"; gene_id "MSTRG.31"; exon_number "1";
    scaffold_1	StringTie	exon	49035	49105	.	+	.	transcript_id "MSTRG.31.2"; gene_id "MSTRG.31"; exon_number "2";
    scaffold_1	StringTie	exon	49283	50049	.	+	.	transcript_id "MSTRG.31.2"; gene_id "MSTRG.31"; exon_number "3";
    scaffold_1	StringTie	exon	50143	50381	.	+	.	transcript_id "MSTRG.31.2"; gene_id "MSTRG.31"; exon_number "4";
    scaffold_1	StringTie	exon	48764	48950	.	+	.	transcript_id "MSTRG.31.1"; gene_id "MSTRG.31"; exon_number "1";
    scaffold_1	StringTie	exon	49035	49105	.	+	.	transcript_id "MSTRG.31.1"; gene_id "MSTRG.31"; exon_number "2";
    scaffold_1	StringTie	exon	49870	50381	.	+	.	transcript_id "MSTRG.31.1"; gene_id "MSTRG.31"; exon_number "3";
    scaffold_1	StringTie	exon	48764	48950	.	+	.	transcript_id "Bobra.0001s0003.1.v2.1"; gene_id "MSTRG.31"; exon_number "1";
    scaffold_1	StringTie	exon	49035	49105	.	+	.	transcript_id "Bobra.0001s0003.1.v2.1"; gene_id "MSTRG.31"; exon_number "2";

    $ wc -l BBRB_v2_transcriptome.annotated.exon.gtf  # Son en total 216 378

Partiendo de este archivo se procedio a realizar el pipeline de Cabili para identificacion de transcritos con el potencial de ser lncRNAs


## Pipeline de Cabili para identificacion de lnRNAs en el transcriptoma guiado de BBRB

### Filtro 1: Eliminar secuencias codificantes ("CDS")

Se extrajo todas aquellas secuencias CDS del archivo de anotacion del genoma v2 de BBRB y se edito para tener solamente los IDs no redundantes de estas secuencias

    $ grep 'CDS' Bbraunii_502_v2.1.gene.anot.gtf > CDS_v2_gene.anot.gtf
    $ cut -f9 CDS_v2_gene.anot.gtf | perl -pe 's/\;/\n/g' | grep transcript_id | cut -f2 -d' ' | perl -pi -e 's/\"//g' > CDS_IDs_gene_anot
    $ head CDS_IDs_gene_anot

    Bobra.0001s0001.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0003.1.v2.1
    
    $ wc -l CDS_IDs_gene_anot # Son 126 319 secuencias CDS

    $ sort -V CDS_IDs_gene_anot | uniq > CDS_IDs_v2_gene_anot_nr
    $ head CDS_IDs_v2_gene_anot_nr

    Bobra.0001s0001.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0003.1.v2.1
    Bobra.0001s0004.1.v2.1
    Bobra.0001s0005.1.v2.1
    Bobra.0001s0006.1.v2.1
    Bobra.0001s0007.1.v2.1
    Bobra.0001s0008.1.v2.1
    Bobra.0001s0009.1.v2.1
    Bobra.0001s0010.1.v2.1

    $ wc -l CDS_IDs_v2_gene_anot_nr # Son 23 685


Se extrajo los IDs de las secuencias del archivo de anotacion del transcriptoma guiado de BBRB

    $ cut -f9 BBRB_v2_transcriptome.annotated.exon.gtf | perl -pe 's/\;/\n/g' | grep transcript_id | cut -f2 -d' ' | perl -pi -e 's/\"//g' > all_trans_ids_st
    $ head all_trans_ids_st

    Bobra.0001s0001.1.v2.1
    MSTRG.31.2
    MSTRG.31.2
    MSTRG.31.2
    MSTRG.31.2
    MSTRG.31.1
    MSTRG.31.1
    MSTRG.31.1
    Bobra.0001s0003.1.v2.1
    Bobra.0001s0003.1.v2.1


    $ wc -l all_trans_ids_st # Son 216 378
    $ sort -V all_trans_ids_st | uniq > all_trans_ids_st_nr
    $ head all_trans_ids_st_nr

    Bobra.0001s0001.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0003.1.v2.1
    Bobra.0001s0004.1.v2.1
    Bobra.0001s0005.1.v2.1
    Bobra.0001s0006.1.v2.1
    Bobra.0001s0007.1.v2.1
    Bobra.0001s0008.1.v2.1
    Bobra.0001s0009.1.v2.1
    Bobra.0001s0010.1.v2.1

    $ wc -l all_trans_ids_st_nr # Son 36 987


Se compararon ambas listas de IDs para ver que era diferente entre ambas y conservar esos IDs diferentes en otro archivo

    $ diff CDS_IDs_v2_gene_anot_nr all_trans_ids_st_nr | grep \> | cut -f2 -d' ' | sort -V | uniq > include_filter1_list
    $ head include_filter1_list

    MSTRG.1.1
    MSTRG.2.6
    MSTRG.3.1
    MSTRG.3.2
    MSTRG.3.3
    MSTRG.6.1
    MSTRG.7.1
    MSTRG.8.1
    MSTRG.9.1
    MSTRG.10.1


    $ wc -l include_filter1_list # Son 13 306

Una vez teniendo la lista de IDs que queremos conservar se compara contra el gtf del emsamble guiado de BBRB y se mantiene todo lo que es igual en un archivo nuevo

    $ grep -wFf include_filter1_list BBRB_v2_transcriptome.annotated.exon.gtf > filter1_CDS_st.gtf
    $ head filter1_CDS_st.gtf

    scaffold_1	StringTie	exon	48764	48950	.	+	.	transcript_id "MSTRG.31.2"; gene_id "MSTRG.31"; exon_number "1";
    scaffold_1	StringTie	exon	49035	49105	.	+	.	transcript_id "MSTRG.31.2"; gene_id "MSTRG.31"; exon_number "2";
    scaffold_1	StringTie	exon	49283	50049	.	+	.	transcript_id "MSTRG.31.2"; gene_id "MSTRG.31"; exon_number "3";
    scaffold_1	StringTie	exon	50143	50381	.	+	.	transcript_id "MSTRG.31.2"; gene_id "MSTRG.31"; exon_number "4";
    scaffold_1	StringTie	exon	48764	48950	.	+	.	transcript_id "MSTRG.31.1"; gene_id "MSTRG.31"; exon_number "1";
    scaffold_1	StringTie	exon	49035	49105	.	+	.	transcript_id "MSTRG.31.1"; gene_id "MSTRG.31"; exon_number "2";
    scaffold_1	StringTie	exon	49870	50381	.	+	.	transcript_id "MSTRG.31.1"; gene_id "MSTRG.31"; exon_number "3";
    scaffold_1	StringTie	exon	48770	48950	.	+	.	transcript_id "MSTRG.31.5"; gene_id "MSTRG.31"; exon_number "1";
    scaffold_1	StringTie	exon	49035	49105	.	+	.	transcript_id "MSTRG.31.5"; gene_id "MSTRG.31"; exon_number "2";
    scaffold_1	StringTie	exon	49870	50049	.	+	.	transcript_id "MSTRG.31.5"; gene_id "MSTRG.31"; exon_number "3";
    
    $ wc -l BBRB_v2_transcriptome.annotated.gtf # 216 378
    $ wc -l filter1_CDS_st.gtf # Son 79 333, por lo que eliminamos 137 045 transcritos del transcriptoma guiado de BBRB


### Filtro 2: Eliminar secuencias con longitud mayor o igual a 200 nt

Se tomo el archivo del primer filtro, es decir, "filter1_CDS_st.gtf" y se removieron de este todos aquellos transcritos cuya longitud fueran mayor o igual a los 200 nt

Tomando en cuenta que las columnas $4 y $5 corresponden a las coordenadas de inicio y fin de los transcritos, se procedio a hacer una resta (+1 para evitar que se excluyan transcritos que si pueden ser lncRNAs) entre ambas para sacar el valor de la longitud.

    $ awk 'BEGIN{FS="\t"}{if( ($5-$4)+1 >= 200){print $AF}}' filter1_CDS_st.gtf | less -S
    $ awk 'BEGIN{FS="\t"}{if( ($5-$4)+1 >= 200){print $AF}}' filter1_CDS_st.gtf > filter2_200nt_st.gtf
    $ head filter2_200nt_st.gtf

    scaffold_1	StringTie	exon	49283	50049	.	+	.	transcript_id "MSTRG.31.2"; gene_id "MSTRG.31"; exon_number "3";
    scaffold_1	StringTie	exon	50143	50381	.	+	.	transcript_id "MSTRG.31.2"; gene_id "MSTRG.31"; exon_number "4";
    scaffold_1	StringTie	exon	49870	50381	.	+	.	transcript_id "MSTRG.31.1"; gene_id "MSTRG.31"; exon_number "3";
    scaffold_1	StringTie	exon	50143	50381	.	+	.	transcript_id "MSTRG.31.5"; gene_id "MSTRG.31"; exon_number "4";
    scaffold_1	StringTie	exon	49283	50381	.	+	.	transcript_id "MSTRG.31.4"; gene_id "MSTRG.31"; exon_number "3";
    scaffold_1	StringTie	exon	201212	201838	.	+	.	transcript_id "MSTRG.2.6"; gene_id "MSTRG.2"; exon_number "1";
    scaffold_1	StringTie	exon	202274	202520	.	+	.	transcript_id "MSTRG.2.6"; gene_id "MSTRG.2"; exon_number "2";
    scaffold_1	StringTie	exon	202848	203477	.	+	.	transcript_id "MSTRG.2.6"; gene_id "MSTRG.2"; exon_number "3";
    scaffold_1	StringTie	exon	203769	204971	.	+	.	transcript_id "MSTRG.2.6"; gene_id "MSTRG.2"; exon_number "4";
    scaffold_1	StringTie	exon	266693	267058	.	+	.	transcript_id "MSTRG.9.1"; gene_id "MSTRG.9"; exon_number "1";

    $ wc -l filter2_200nt_st.gtf # Son 35 024, por lo que se removieron 44 309 


### Filtro 3: Eliminar secuencias con ORF mayores o iguales a 75 aa (225 nt)

Se convirtio a formato fasta el archivo gtf del filtro 2 para correr en Mazorka getorf del modulo de EMBOSS.

    $ gffread -w filter2_200nt_st.fasta -g Bbraunii_genome_502_v2.0.fa filter2_200nt_st.gtf
    $ head filter2_200nt_st.fasta

    >MSTRG.31.2
    ACACTTATCCTTATCCACCAACAACATCGAGAAGATTAGTAATTTGGCAGGCTTGGATAACTTGCGGATA
    TTGTCACTTGGCCGGAATCAGATCAAAAAGCTTGAAAATTTAGACCCACTCGCAGACAATCTCGAAGAGC
    TTTGGATTTCTTACAACTTGCTAGAAAAACTAGTACGTATCACGGGCCTCAAAGAGTCCACTCATTGACA
    AAGATGAAAGCATTCTTGGTTGGCTTGACGGCCGAAATCTTGCTCAAGTATCCACAGTGACGAAGTAGAT
    GTTTCTAAACTGCTACTTGCAGTTTCAATAGTATCCCAAGCTTTCCTTAGCAATCTTAACAGGAACAGCT
    AGCATAATGGTGATAACTCTGCATGCATGCAAGGAAGCCAACGTCTGCAAGCGTCCAAAGTCTTAGGCCT
    TATTAAGGGGCACTCCACCATTTGACCAGACGCTGAGAAAATTAATGAAGATTGGGAGTCCCCTCGTACA
    GTAAGCTTAAAACCGATGTCCAAACGTCCAGCCAGTTTGGGCTGCACTGTTGAAGTCCTACAGCTGGCAC
    TAAAACAAATGAATACATGTGCCGCAGGTGGGGATAGAAAAGCTGGTAAACCTCAGGGTCCTTTTTGCCA


Se verifico la presencia de *gaps* (N) en el archivo fasta.

    $ awk '/[NNN]/' RS='>' filter2_200nt_st.fasta > N_list_filter2  

Se corrio getorf en mazorka y se vio la presencia de X pero en menor cantidad

    $ awk '/[X]/' RS='>' output_getorf_st.fasta > X_list_filter3 



#### *Martes 28 de enero de 2020*


Se corrio nuevamente el modulo de featureCounts en Mazorka para generar la tabla de cuentas del ensamble guiado del transcriptoma de BBRB.

    #PBS -N featureCounts_v2
    #PBS -q default
    #PBS -l nodes=1:ppn=4,vmem=4gb,walltime=120:00:00
    #PBS -V
    #PBS -o fcounts_v2.out
    #PBS -e fcounts_v2.err

    module load subread/1.5.1

    cd $PBS_O_WORKDIR

    cd /LUSTRE/usuario/smata/BBRB/hisat2_align/align/align_v2_genome

    featureCounts -T 4 -s 1 -p -t exon -g gene_id -a /LUSTRE/usuario/smata/BBRB/stringtie_en/new_ensam_v2/BBRB_v2_transcriptome.annotated.gtf CBBRB_1_v2.bam CBBRB_2_v2.bam CBBRB_3_v2.bam LBBRB_1_v2.bam LBBRB_2_v2.bam LBBRB_3_v2.bam -o /LUSTRE/usuario/smata/BBRB/featurecounts/BBRB_st_counts.txt


El resumen de la corrida fue el siguiente:

    Status	                   CBBRB_1_v2.bam   CBBRB_2_v2.bam   CBBRB_3_v2.bam   LBBRB_1_v2.bam   LBBRB_2_v2.bam   LBBRB_3_v2.bam
    Assigned	                   5189280	    5633248	     5181311	      5767648	       5081999	        5955344
    Unassigned_Ambiguity	   43711	    48386	     43030	      49798	       43315	        54021
    Unassigned_MultiMapping	   2192105	    2782274	     1978880	      2923497	       3295498	        3247041
    Unassigned_NoFeatures	   12562648	    14223213	     13073166	      15668454         15173870	        13818239
    Unassigned_Unmapped	           1056688	    1045860	     939975	      1132276	       1330728	        1027130
    Unassigned_MappingQuality	   0	            0	             0	              0	               0	        0
    Unassigned_FragmentLength	   0	            0	             0                0	               0                0
    Unassigned_Chimera	           0	            0	             0	              0	               0	        0
    Unassigned_Secondary	   0	            0	             0	              0                0                0
    Unassigned_Nonjunction	   0	            0	             0                0	               0                0
    Unassigned_Duplicate	   0	            0	             0	              0	               0	        0



#### *Miercoles 29 de enero de 2020*


Se convertio el archivo gtf del ensamble guiado a formato bed12 para poder trabajarlo para los filtros del *pipeline* de Cabili y correr *CPC*, usando scripts de esta fuente: 
https://gist.github.com/gireeshkbogu/f478ad8495dca56545746cd391615b93

Los scripts se descargaron en la carpeta "fani"

    $ wget http://hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64/gtfToGenePred
    $ wget http://hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64/genePredToBed
    $ chmod +x gtfToGenePred genePredToBed

Primero se convirtio el archivo de gtf a GenePred y este a Bed12 en la carpeta del ensamble guiado

   $ /home/fani/gtfToGenePred BBRB_v2_transcriptome.annotated.gtf BBRB_v2_transcriptome.annotated.genePred
   $ /home/fani/genePredToBed BBRB_v2_transcriptome.annotated.genePred BBRB_v2_transcriptome.annotated.bed12

   $ /home/fani/gtfToGenePred BBRB_v2_transcriptome.annotated.exon.gtf  BBRB_v2_transcriptome.annotated.exon.genePred
   $ /home/fani/genePredToBed BBRB_v2_transcriptome.annotated.exon.genePred BBRB_v2_transcriptome.annotated.exon.bed12




#### *Jueves 30 de enero de 2020*


### Filtro 1 Para CPC y Cabili: Eliminar genes codificantes


Se extrajo todas aquellas secuencias CDS del archivo de anotacion del genoma v2 de BBRB y se edito para tener solamente los IDs no redundantes de estas secuencias

    $ grep 'CDS' Bbraunii_502_v2.1.gene.anot.gtf > CDS_v2_gene.anot.gtf
    $ head CDS_v2_gene.anot.gtf

    scaffold_1	phytozomev12	CDS	581	1252	.	+	0	transcript_id "Bobra.0001s0001.1.v2.1"; gene_id "Bobra.0001s0001.v2.1";
    scaffold_1	phytozomev12	CDS	44754	44791	.	-	2	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "Bobra.0001s0002.v2.1";
    scaffold_1	phytozomev12	CDS	45095	45304	.	-	2	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "Bobra.0001s0002.v2.1";
    scaffold_1	phytozomev12	CDS	45727	46044	.	-	2	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "Bobra.0001s0002.v2.1";
    scaffold_1	phytozomev12	CDS	46232	46395	.	-	1	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "Bobra.0001s0002.v2.1";
    scaffold_1	phytozomev12	CDS	46996	47205	.	-	1	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "Bobra.0001s0002.v2.1";
    scaffold_1	phytozomev12	CDS	47585	47727	.	-	0	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "Bobra.0001s0002.v2.1";
    scaffold_1	phytozomev12	CDS	47937	48255	.	-	1	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "Bobra.0001s0002.v2.1";
    scaffold_1	phytozomev12	CDS	48346	48512	.	-	0	transcript_id "Bobra.0001s0002.1.v2.1"; gene_id "Bobra.0001s0002.v2.1";
    scaffold_1	phytozomev12	CDS	48870	48950	.	+	0	transcript_id "Bobra.0001s0003.1.v2.1"; gene_id "Bobra.0001s0003.v2.1";


    $ cut -f9 CDS_v2_gene.anot.gtf | perl -pe 's/\;/\n/g' | grep transcript_id | cut -f2 -d' ' > CDS_IDs_gene_anot
    $ perl -pi -e 's/\"//g' CDS_IDs_gene_anot 
    $ head CDS_IDs_gene_anot

    Bobra.0001s0001.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0003.1.v2.1

    
    $ wc -l CDS_IDs_gene_anot # Son 126 319 secuencias CDS

    $ sort -V CDS_IDs_gene_anot | uniq > CDS_IDs_v2_gene_anot_nr
    $ head CDS_IDs_v2_gene_anot_nr

    Bobra.0001s0001.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0003.1.v2.1
    Bobra.0001s0004.1.v2.1
    Bobra.0001s0005.1.v2.1
    Bobra.0001s0006.1.v2.1
    Bobra.0001s0007.1.v2.1
    Bobra.0001s0008.1.v2.1
    Bobra.0001s0009.1.v2.1
    Bobra.0001s0010.1.v2.1


    $ wc -l CDS_IDs_v2_gene_anot_nr # Son 23 685


Se extrajo los IDs de las secuencias del archivo de anotacion del transcriptoma guiado de BBRB, usando como input el archivo bed12 que contiene los transcritos maduros

    $ cut -f4 BBRB_v2_transcriptome.annotated.bed12 > all_trans_ids_st
    $ head all_trans_ids_st

    Bobra.0001s0001.1.v2.1
    MSTRG.31.2
    MSTRG.31.1
    Bobra.0001s0003.1.v2.1
    MSTRG.31.5
    MSTRG.31.4
    Bobra.0001s0006.1.v2.1
    Bobra.0001s0007.1.v2.1
    Bobra.0001s0008.1.v2.1
    Bobra.0001s0009.1.v2.1

    $ wc -l all_trans_ids_st # Son 36 987
    $ sort -V all_trans_ids_st | uniq > all_trans_ids_st_nr
    $ head all_trans_ids_st_nr

    Bobra.0001s0001.1.v2.1
    Bobra.0001s0002.1.v2.1
    Bobra.0001s0003.1.v2.1
    Bobra.0001s0004.1.v2.1
    Bobra.0001s0005.1.v2.1
    Bobra.0001s0006.1.v2.1
    Bobra.0001s0007.1.v2.1
    Bobra.0001s0008.1.v2.1
    Bobra.0001s0009.1.v2.1
    Bobra.0001s0010.1.v2.1

    $ wc -l all_trans_ids_st_nr # Son 36 987


Se compararon ambas listas de IDs para ver que era diferente entre ambas y conservar esos IDs diferentes en otro archivo

    $ diff CDS_IDs_v2_gene_anot_nr all_trans_ids_st_nr | grep \> | cut -f2 -d' ' | sort -V | uniq > include_filter_list
    $ head include_filter_list

    MSTRG.1.1
    MSTRG.2.6
    MSTRG.3.1
    MSTRG.3.2
    MSTRG.3.3
    MSTRG.6.1
    MSTRG.7.1
    MSTRG.8.1
    MSTRG.9.1
    MSTRG.10.1


    $ wc -l include_filter_list # Son 13 306

Una vez teniendo la lista de IDs que queremos conservar se compara contra el gtf del emsamble guiado de BBRB y se mantiene todo lo que es igual en un archivo nuevo

    $ grep -wFf include_filter_list BBRB_v2_transcriptome.annotated.bed12 > filter_CDS_st.bed12
    $ head filter_CDS_st.bed12

    scaffold_1	48763	50381	MSTRG.31.2	0	+	50381	50381	0	4	187,71,767,239,	0,271,519,1379,
    scaffold_1	48763	50381	MSTRG.31.1	0	+	50381	50381	0	3	187,71,512,	0,271,1106,
    scaffold_1	48769	50381	MSTRG.31.5	0	+	50381	50381	0	4	181,71,180,239,	0,265,1100,1373,
    scaffold_1	48769	50381	MSTRG.31.4	0	+	50381	50381	0	3	181,71,1099,	0,265,513,
    scaffold_1	201211	204971	MSTRG.2.6	0	+	204971	204971	0	4	627,247,630,1203,	0,1062,1636,2557,
    scaffold_1	266692	267578	MSTRG.9.1	0	+	267578	267578	0	2	366,38,	0,848,
    scaffold_1	50139	51615	MSTRG.32.1	0	-	51615	51615	0	6	150,129,79,120,120,365,	0,349,573,720,918,1111,
    scaffold_1	204590	207829	MSTRG.3.1	0	-	207829	207829	0	3	105,2191,211,	0,325,3028,
    scaffold_1	204599	214352	MSTRG.3.2	0	-	214352	214352	0	9	112,2191,189,128,200,157,162,127,101,	0,316,5727,6312,7888,8600,8950,9190,9652,
    scaffold_1	205015	227402	MSTRG.3.3	0	-	227402	227402	0	18	2091,160,128,200,157,228,145,155,137,180,143,218,140,192,83,146,84,431,	0,5311,5896,7472,8184,8534,9079,13525
    
    $ wc -l BBRB_v2_transcriptome.annotated.bed12 # 36 987
    $ wc -l filter_CDS_st.bed12 # Son 13 306, por lo que eliminamos 23 681 transcritos del transcriptoma guiado de BBRB



### Filtro 2 Para CPC y Cabili: Eliminar secuencias incompletas


Se convirtio el archivo bed12 del filtrado de los CDS a formato fasta para correr CPC, usando bedtools

    $ bedtools getfasta -fi Bbraunii_genome_502_v2.0.fa -bed filter_CDS_st.bed12 -name -fo filter_CDS_st.fasta 

Se reviso la presencia de N's es el archivo fasta y se removió aquellos transcritos que no estuvieran bien anotados (secuencias incompletas).

    $ awk '/[NNN]/' RS='>' filter_CDS_st.fasta > filter_CDS_st_with_N.fasta

Se extrajo todos los ID's del archivo fasta con N's para removerlos del fasta original

    $ grep 'MSTRG*' filter_CDS_st_with_N.fasta | perl -pe 's/::.*\n/\n/' > N_list_CDS_filter
    $ head N_list_CDS_filter 

    MSTRG.79.2
    MSTRG.79.4
    MSTRG.86.1
    MSTRG.86.2
    MSTRG.91.1
    MSTRG.129.1
    MSTRG.90.2
    MSTRG.90.4
    MSTRG.90.3
    MSTRG.90.1

    $ wc -l N_list_CDS_filter # Son 1572

Se extrajo a su vez todos los ID's del archivo fasta original

    $ grep 'MSTRG*' filter_CDS_st.fasta | perl -pe 's/::.*\n/\n/' | sed 's/>//g' > list_CDS_filter
    $ head list_CDS_filter

    MSTRG.31.2
    MSTRG.31.1
    MSTRG.31.5
    MSTRG.31.4
    MSTRG.2.6
    MSTRG.9.1
    MSTRG.32.1
    MSTRG.3.1
    MSTRG.3.2
    MSTRG.3.3

Compararmos entre ambas lineas para ver que es diferente

    $ diff N_list_CDS_filter list_CDS_filter | grep \> | cut -f2 -d' ' | sort -V | uniq > include_list_CDS_filter
    $ head include_list_CDS_filter

    MSTRG.1.1
    MSTRG.2.6
    MSTRG.3.1
    MSTRG.3.2
    MSTRG.3.3
    MSTRG.6.1
    MSTRG.7.1
    MSTRG.8.1
    MSTRG.9.1
    MSTRG.10.1

Ahora se hace filtrado para conservar los ID's del "include_list_CDS_filter"

    $ grep -wFf include_list_CDS_filter filter_CDS_st.bed12 > filter_CDS_st_NoN.bed12
    $ wc -l filter_CDS_st_NoN.bed12 # Luego de remover ID's con secuencias incompletas nos quedaron 11 734, por lo que se removieron 

Se convirtio el archivo nuevo de bed12 a fasta y se reviso si habia N's

    $ bedtools getfasta -fi Bbraunii_genome_502_v2.0.fa -bed filter_CDS_st_NoN.bed12 -name -fo filter_CDS_st_NoN.fasta 
    $ awk '/[NNN]/' RS='>' filter_CDS_st_NoN.fasta  # No tiene N's las secuencias del archivo



### Identificación de lncRNAs con el método de CPC


El archivo "filter_CDS_st_NoN.fasta" se usó para correr en CPC en Mazorka. Este archivo es el filtrado sin CDS y sin transcritos con secuencias incompletas

    #PBS -N CPC_st_NoN
    #PBS -q default
    #PBS -l nodes=1:ppn=8,mem=20gb,vmem=30gb,walltime=999:00:00
    #PBS -V
    #PBS -o cpc_BBRB_st_NoN.out
    #PBS -e cpc_BBRB_st_NoN.err

    module load cpc/0.9.r2
    module load ncbi-blast/2.2.26

    cd $PBS_O_WORKDIR

    run_predict.sh ./filter_CDS_st_NoN.fasta result_in_table cpc result_evidences



### Identificación de lncRNAs con el método de Cabili et al. (2011)


### Filtro 3: Eliminar secuencias con longitud mayor o igual a 200 nt


Se tomo el archivo del segundo filtro, es decir, "filter_CDS_st_NoN.bed12" y se conservaron de este todos aquellos transcritos cuya longitud fueran mayor o igual a los 200 nt

Tomando en cuenta que las columnas $2 y $3 corresponden a las coordenadas de inicio y fin de los transcritos en el archivo bed12, se procedio a hacer una resta (+1 para evitar que se excluyan 
transcritos que si pueden ser lncRNAs) entre ambas para sacar el valor de la longitud.

    $ awk 'BEGIN{FS="\t"}{if( ($3-$2)+1 >= 200){print $AF}}' filter_CDS_st_NoN.bed12 | less -S
    $ awk 'BEGIN{FS="\t"}{if( ($3-$2)+1 >= 200){print $AF}}' filter_CDS_st_NoN.bed12 > filter_200nt_st_NoN.bed12
    $ head filter_200nt_st_NoN.bed12

    scaffold_1	48763	50381	MSTRG.31.2	0	+	50381	50381	0	4	187,71,767,239,	0,271,519,1379,
    scaffold_1	48763	50381	MSTRG.31.1	0	+	50381	50381	0	3	187,71,512,	0,271,1106,
    scaffold_1	48769	50381	MSTRG.31.5	0	+	50381	50381	0	4	181,71,180,239,	0,265,1100,1373,
    scaffold_1	48769	50381	MSTRG.31.4	0	+	50381	50381	0	3	181,71,1099,	0,265,513,
    scaffold_1	201211	204971	MSTRG.2.6	0	+	204971	204971	0	4	627,247,630,1203,	0,1062,1636,2557,
    scaffold_1	266692	267578	MSTRG.9.1	0	+	267578	267578	0	2	366,38,	0,848,
    scaffold_1	50139	51615	MSTRG.32.1	0	-	51615	51615	0	6	150,129,79,120,120,365,	0,349,573,720,918,1111,
    scaffold_1	204590	207829	MSTRG.3.1	0	-	207829	207829	0	3	105,2191,211,	0,325,3028,
    scaffold_1	204599	214352	MSTRG.3.2	0	-	214352	214352	0	9	112,2191,189,128,200,157,162,127,101,	0,316,5727,6312,7888,8600,8950,9190,9652,
    scaffold_1	205015	227402	MSTRG.3.3	0	-	227402	227402	0	18	2091,160,128,200,157,228,145,155,137,180,143,218,140,192,83,146,84,431,	0,5311,5896,7472,8184,8534,9079,13525
    
    $ wc -l filter_200nt_st.gtf # Son 11 734, por lo que no se removieron transcritos

Para verificar eso se calculo la longitud de los transcritos del archivo fasta con *infoseq*, pero primero se elimino del archivo fasta los "::" en cada linea del ID

    $ perl -pe 's/::.*\n/\n/' filter_CDS_st_NoN.fasta > prueba.fasta
    $ mv prueba.fasta ./filter_CDS_st_NoN.fasta
    $ infoseq filter_CDS_st_NoN.fasta -only -name -type -length > transcripts_NoN_list
    $ head transcripts_NoN_list

    Name           Type  Length 
    MSTRG.31.2     N     1618   
    MSTRG.31.1     N     1618   
    MSTRG.31.5     N     1612   
    MSTRG.31.4     N     1612   
    MSTRG.2.6      N     3760   
    MSTRG.9.1      N     886    
    MSTRG.32.1     N     1476   
    MSTRG.3.1      N     3239   
    MSTRG.3.2      N     9753   


Todos los transcritos presentaban una longitud mayor o igual a 200 nt, por lo que se siguieron trabajando todos



#### *Viernes 31 de enero de 2020*


### Filtro 4: Conservar transcritos con ORF <= a 75 aa

El archivo fasta "filter_CDS_st_NoN.fasta", se copio con el nombre de "filter_200nt_st_NoN.fasta" dentro de la carpeta del getorf de los filtros de Cabili d eMazorka y se corrio el modulo de *getorf*

    #PBS -N getorf_st_NoN
    #PBS -q default
    #PBS -l nodes=1:ppn=8,vmem=20gb,mem=20gb,walltime=24:00:00
    #PBS -V
    #PBS -o getorf_st_NoN.out
    #PBS -e getorf_st_NoN.err

    module load EMBOSS/6.6.0

    cd $PBS_O_WORKDIR

    getorf -minsize 225 -find 0 filter_200nt_st_NoN.fasta output_getorf_st_NoN.fasta


El archivo de salida del *getorf* se reviso para ver la presencia de X's en las secuencias de aminoacidos

    $ head output_getorf_st_NoN.fasta

    >MSTRG.31.2_1 [1092 - 1322] 
    IHVPQVGIEKLVNLRVLFASNNKINSWSDIERLSALDKLEDLLLVGNPIYNEYKDRAATSEYRIEVIPQILHTLKSP
    >MSTRG.31.2_2 [1548 - 1315] (REVERSE SENSE) 
    LLITHHAGRSRCQYCLVCAKRVKLTVSYFKPFWLLLSHPHRLGCHPTSAIWEGVSTPVEKKKSLSGAGCAPGAHLMVI
    >MSTRG.31.2_3 [1131 - 871] (REVERSE SENSE) 
    GLPAFLSPPAAHVFICFSASCRTSTVQPKLAGRLDIGFKLTVRGDSQSSLIFSASGQMVECPLIRPKTLDACRRWLPCMHAELSPLC
   

    $ awk '/[X]/' RS='>' output_getorf_st_NoN.fasta | head # No se encontraron X's en las secuencias, se procedio trabajando el archivo

Se extrajo una lista de ID's del archivo fasta, correspondientes a los ID's que queremos conservar en el archivo bed12

    $ grep -E ">" output_getorf_st_NoN.fasta | sed 's/>//g; s/\[[^]]*\]//g' > getorf_NoN_list
    $ head getorf_NoN_list

    MSTRG.31.2_1  
    MSTRG.31.2_2  (REVERSE SENSE) 
    MSTRG.31.2_3  (REVERSE SENSE) 
    MSTRG.31.2_4  (REVERSE SENSE) 
    MSTRG.31.1_1  
    MSTRG.31.1_2  (REVERSE SENSE) 
    MSTRG.31.1_3  (REVERSE SENSE) 
    MSTRG.31.1_4  (REVERSE SENSE) 
    MSTRG.31.5_1  
    MSTRG.31.5_2  (REVERSE SENSE) 

    
    $ sed -e 's/([^(^)].*)//g' getorf_NoN_list | perl -pe 's/_.*\n/\n/' | sort -V | uniq > getorf_NoN_list_nr
    $ head getorf_NoN_list_nr

    MSTRG.1.1
    MSTRG.2.6
    MSTRG.3.1
    MSTRG.3.2
    MSTRG.3.3
    MSTRG.6.1
    MSTRG.7.1
    MSTRG.8.1
    MSTRG.9.1
    MSTRG.10.1


Se comparó esa lista de ID's con el archivo bed12 para ver que ID's son iguales, los cuales queremos conservar

    $ grep -wFf getorf_NoN_list_nr filter_200nt_st_NoN.bed12 > filter_getorf_st_NoN.bed12
    $ wc -l filter_getorf_st_NoN.bed12 # Nos quedamos con 11 298 transcritos, removimos 436


Se convirto el archivo bed12 de este filtro en formato fasta y se edito para dejar solamente los ID's de los transcritos

    $ bedtools getfasta -fi /home/fani/Documents/Tesis_BP/BBRB_analysis/genome_data_v2/assembly/Bbraunii_genome_502_v2.0.fa -bed filter_getorf_st_NoN.bed12 -name -fo filter_getorf_st_NoN.fasta
    $ perl -pe 's/::.*\n/\n/' filter_getorf_st_NoN.fasta > prueba.fasta
    $ mv prueba.fasta ./filter_getorf_st_NoN.fasta

El archivo se paso a una carpeta llamada "hmmscan"