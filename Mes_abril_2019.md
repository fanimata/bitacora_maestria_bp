# **Bit�cora: Abril**




#### *Lunes 1 de abril de 2019*


Se descarg� un archivo en formato *txt* con informaci�n sobre el nombre de las accesiones de los archivos de secuenciaci�n RNA-seq de 
*Chamydomonas reinhardtii* la base de datos *GEO* del siguiente link: https://www.ncbi.nlm.nih.gov/sra?term=SRP155962.

El archivo *txt* descargado se renombr� como *SraAccList_chlamy.txt* y este se subi� de la computadora a una carpeta en el cluster de alto 
rendimiento Mazorka.

    $ rsync -av /home/fani/Descargas/SraAccList_chlamy.txt smata@10.10.10.240:/LUSTRE/usuario/smata/
    chlamy/datos_transcriptoma

Desde Mazorka se descarg� los archivos de secuenciaci�n en formato *sra* usando la herramienta *sratoolkit*:

    #PBS -N sra_download
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=120:00:00
    #PBS -V
    #PBS -o sra.out
    #PBS -e sra.err

    module load sratoolkit/2.8.2

    cd $PBS_O_WORKDIR

    sras=$(cat SraAccList_chlamy.txt)

    for sra in $sras

    do

    wget ftp://ftp-trace.ncbi.nih.gov/sra/sra-instant/reads/ByRun/sra/SRR/SRR763/$sra/$sra.sra ./

    done
    

La idea de usar un archivo *txt* con las accesiones era para mediante este decirle al programa cuales archivos deb�a descargar 
espec�ficamente desde el link que se indic� en el *script*. El total de archivos descargados fue de 6:

 
    1) SRR7632607
    2) SRR7632608
    3) SRR7632609
    4) SRR7632610
    5) SRR7632611
    6) SRR7632612
	
	
	


#### *Martes 2 de abril de 2019*


Los archivos *sra* se convirtieron a formato *fastq* usando *sratoolkit*, usando el siguiente *script*:

    #PBS -N sra_to_fastq
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=120:00:00
    #PBS -V
    #PBS -o sra_2_fastq.out
    #PBS -e sra_2_fastq.err

    module load sratoolkit/2.8.2

    cd $PBS_O_WORKDIR

    for sra in *sra

    do

    clean=$(echo $sra | sed 's/\.sra//')

    fastq-dump --gzip $sra

    done


En el *script* anterior se le indic� al programa mediante un *ciclo for* que para cada uno de los archivos *sra* se realizara su conversi�n
en formato *fatsq* con el argumento *fastq-dump* y se comprimieran de una vez usando el argumento *--gzip*. El imperativo *clean* le indic� 
a *sratoolkit* que en los nuevos archivos mantuviera el nombre originar pero que eliminara la terminaci�n *".sra"*.

Una vez convertido los archivos se procedi� a realizar un an�lisis de calidad de estos usando *FastQC*:
   
    
    #PBS -N FastQC
    #PBS -l nodes=1:ppn=8,vmem=4gb,walltime=120:00:00
    #PBS -q default
    #PBS -V
    #PBS -o fastqc_1.out
    #PBS -e fastqc_1.err

    module load FastQC/0.11.2  
    
    cd $PBS_O_WORKDIR

    cd /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq

    for i in  *gz

    do

    fastqc /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq/$i -o /LUSTRE/usuario/smata/chlamy/fastqc_before

    done




#### *Mi�rcoles 3 de abril de 2019*


Debido a que la secuenciaci�n de los archivos de *C. reinhardtii* fue *paired-end* se repiti� la conversi�n de los archivos *sra* a *fastq*
para poder indicarle al *sratoolkit* que separara cada muestra (archivo *fastq*) en dos, por lo que unicamente se agreg� en el *script* del 
ejecutable, el argumento *--split-files*:


    #PBS -N sra_to_fastq
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=120:00:00
    #PBS -V
    #PBS -o sra_2_fastq.out
    #PBS -e sra_2_fastq.err

    module load sratoolkit/2.8.2

    cd $PBS_O_WORKDIR

    for sra in *sra

    do

    clean=$(echo $sra | sed 's/\.sra//')

    fastq-dump --split-files --gzip $sra

    done
	

Cada uno de los archivos *fastq* fue renombrado de la siguiente manera de acuerdo a su n�mero de accesi�n en *GEO*:


    1) SRR7632607 --> CK_2h_1.fastq.gz y CK_2h_2.fastq.gz
    2) SRR7632608 --> CK_12h_1.fastq.gz y  CK_12h_2.fastq.gz 
    3) SRR7632609 --> AgNPs_2h_1_1.fastq.gz y AgNPs_2h_1_2.fastq.gz
    4) SRR7632610 --> AgNPs_2h_2_1.fastq.gz y AgNPs_2h_2_2.fastq.gz
    5) SRR7632611 --> AgNPs_12h_1_1.fastq.gz y AgNPs_12h_1_2.fastq.gz
    6) SRR7632612 --> AgNPs_12h_2_1.fastq.gz y AgNPs_12h_2_2.fastq.gz


En el momento de la conversi�n los 6 archivos *sra* descargados se separaron en 12 y de estos se realiz� nuevamente un an�lisis de calidad, 
usando el *script* de *FastQC* anteriormente utilizado.
Los archivos *html* y *fastqc.zip* generados en la corrida fueron descargados a la computadora mediante el siguiente script:


    $ rsync -av smata@10.10.10.240:/LUSTRE/usuario/smata/chlamy/fastqc_before .
	
	
El "." indic� que los archivos se descargaron en la carpeta actual (donde nos encontrabamos en ese momento). En esa misma carpeta se corri� 
el *MultiQC* para poder visualizar graficamente la calidad de los 12 archivos *fastq* en un s�lo reporte, la corrida se realiz� sobre la 
carpeta donde se encontraban los archivos:


    $ multiqc .
	


#### *Mi�rcoles 10 de abril de 2019*


Debido a que se pensaba que era posible que las secuencias de *C. reinhardtii* pudieran tener contaminaci�n con secuencias de otros 
organismos, se decidi� utilizar el programar *Kraken* para poder determinarlo. *Kraken* se encarga de llevar a cabo la clasificaci�n 
filogen�tica de secuenciasusando bases de datos taxon�micas de diferentes organismos. Para poder correr el programa es necesario primero 
generar una base de datos, aunque se pueden utilizar una por defecto, sin embargo cuando uno desee realizar un an�lisis m�s exhaustivo se 
recomienda generar la base de datos con la informaci�n de los organismos que uno as� desee.
En Mazorka se procedi� a generar una base de datos para poder proveer la mayor cantidad de informaci�n taxon�mica para clasificar las 
secuencias de las librer�as de *C. rienhardtii* y con mayor precisi�n poder determinar si existe contaminaci�n con otras secuencias. 
El *script* que se utiliz� fue el siguiente:


    #PBS -N library_kraken2
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=20gb,walltime=120:00:00
    #PBS -V
    #PBS -o library.out
    #PBS -e library.err

    module load kraken/2.0.7

    cd $PBS_O_WORKDIR

    kraken2-build --db CHLAMY_TAX --download-taxonomy
    kraken2-build --db CHLAMY_TAX --download-library archaea
    kraken2-build --db CHLAMY_TAX --download-library bacteria
    kraken2-build --db CHLAMY_TAX --download-library fungi
    kraken2-build --db CHLAMY_TAX --download-library viral
    kraken2-build --db CHLAMY_TAX --download-library protozoa
    kraken2-build --db CHLAMY_TAX --download-library human
    kraken2-build --db CHLAMY_TAX --download-library plant
    kraken2-build --db CHLAMY_TAX --download-library plasmid
    kraken2-build --db CHLAMY_TAX --build --threads 32


Sin embargo, la corrida fue cancelada debido a que en el cluster ya existe una base de datos generada para correr *Kraken* y porque el 
generar una base de datos nueva representa un gran gasto de espacio de almacenamiento en el servidor. As� tambi�n se decidi� no realizar
un an�lisis con *Kraken*, se continu� trabando con las secuencias tal y como se encontraban.



#### *Lunes 22 de abril de 2019*


Se procedi� a realizar el procesamiento de las 12 secuencias *fastq* con *Trimmomatic*, tomando en cuenta los resultados obtenidos en el 
an�lisis de calidad con *FastQC*. 
Se corrieron 6 *scripts* los cuales correspondieron a las muestras con sus pares. El *script* utilizado para la muestra *AgNPs_12h_1* fue el 
siguiente:


    #PBS -N trimmo_Ag12_1
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=20gb,walltime=1:00:00
    #PBS -V
    #PBS -o trimmo_Ag12_1.out
    #PBS -e trimmo_Ag12_1.err

    module load Trimmomatic/0.32

    cd $PBS_O_WORKDIR

    
    java -jar $TRIMMOMATIC PE -threads 2 /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq/AgNPs_12h_1* 
    Trimmed_AgNPs_12h_1.fastq.gz ILLUMINACLIP:TruSeq3-PE-2.fa:2:30:10 HEADCROP:1 SLIDINGWINDOW:4:15  MINLEN:60


Ese mismo *script* se corri� para las 5 muestras restantes con sus pares. �nicamente se modific� el nombre del nombre, los nombres de los archivos 
*err* y *out*, y los nombres de las muestras. El ejecutable utilizado para cada muestra fue el siguiente:

    
    Para la muestra AgNPs_12h_2:

    java -jar $TRIMMOMATIC PE -threads 2 /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq/AgNPs_12h_2* 
    Trimmed_AgNPs_12h_1.fastq.gz ILLUMINACLIP:TruSeq3-PE-2.fa:2:30:10 HEADCROP:1 SLIDINGWINDOW:4:15  MINLEN:60
    
   
    Para la muestra AgNPs_2h_1:
    
    java -jar $TRIMMOMATIC PE -threads 2 /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq/AgNPs_2h_1* 
    Trimmed_AgNPs_12h_1.fastq.gz ILLUMINACLIP:TruSeq3-PE-2.fa:2:30:10 HEADCROP:1 SLIDINGWINDOW:4:15  MINLEN:60

    
    Para la muestra AgNPs_2h_2:

    java -jar $TRIMMOMATIC PE -threads 2 /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq/AgNPs_2h_2* 
    Trimmed_AgNPs_12h_1.fastq.gz ILLUMINACLIP:TruSeq3-PE-2.fa:2:30:10 HEADCROP:1 SLIDINGWINDOW:4:15  MINLEN:60
    
    
    Para la muestra CK_12h:
    
    java -jar $TRIMMOMATIC PE -threads 2 /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq/CK_12h*
    Trimmed_CK_12h.fastq.gz ILLUMINACLIP:TruSeq3-PE-2.fa:2:30:10 HEADCROP:1 SLIDINGWINDOW:4:15  MINLEN:60
    
    
    Para la muestra CK_2h:
    
    java -jar $TRIMMOMATIC PE -threads 2 /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq/CK_2h*
    Trimmed_CK_2h.fastq.gz ILLUMINACLIP:TruSeq3-PE-2.fa:2:30:10 HEADCROP:1 SLIDINGWINDOW:4:15  MINLEN:60
    
    
Con los argumentos utilizados se le dijo a *Trimmomatic* lo siguiente:

    ILLUMINACLIP:TruSeq3-PE-2.fa:2:30:10: Indica el tipo de adaptador a utilizar, en este caso para una corrida con secuencia *paired-end*
    y se us� por defecto las especificaciones 2:30:10.
    HEADCROP:5: Se le indic� que cortara 1 par de base al inicio de cada una de las secuencias.
    SLIDINGWINDOW:4:15: Empieza a escanear en una ventana de 4 en el extremos 5' y recorta la lectura cuando la calidad promedio cae bajo el 
    umbral, en este caso 15.
    MINLEN:60: Suelta la lectura si pertenece a una laongitud espec�fica, en este caso 60 pb.
    
    
Una vez recortadas las secuencias se procedi� a realizar el an�lisis de calidad de cada uno de los *outputs* de las corridad, en total fueron
24, el *script* usado para correr *FastQC* fue el siguiente:


    #PBS -N FastQC
    #PBS -l nodes=1:ppn=8,vmem=4gb,walltime=120:00:00
    #PBS -q default
    #PBS -V
    #PBS -o QC_trim_files.out
    #PBS -e QC_trim_files.err

    module load FastQC/0.11.2
    
    cd $PBS_O_WORKDIR


    cd /LUSTRE/usuario/smata/chlamy/trimmo/agnps12_1
    for i in  *gz
    do
    fastqc /LUSTRE/usuario/smata/chlamy/trimmo/agnps12_1/$i -o /LUSTRE/usuario/smata/chlamy/fastqc_after
    done


    cd /LUSTRE/usuario/smata/chlamy/trimmo/agnps12_2
    for i in  *gz
    do
    fastqc /LUSTRE/usuario/smata/chlamy/trimmo/agnps12_2/$i -o /LUSTRE/usuario/smata/chlamy/fastqc_after
    done

    
    cd /LUSTRE/usuario/smata/chlamy/trimmo/agnps2_1
    for i in  *gz
    do
    fastqc /LUSTRE/usuario/smata/chlamy/trimmo/agnps2_1/$i -o /LUSTRE/usuario/smata/chlamy/fastqc_after
    done


    cd /LUSTRE/usuario/smata/chlamy/trimmo/agnps2_2
    for i in  *gz
    do
    fastqc /LUSTRE/usuario/smata/chlamy/trimmo/agnps2_2/$i -o /LUSTRE/usuario/smata/chlamy/fastqc_after
    done


    cd /LUSTRE/usuario/smata/chlamy/trimmo/ck12
    for i in  *gz
    do
    fastqc /LUSTRE/usuario/smata/chlamy/trimmo/ck12/$i -o /LUSTRE/usuario/smata/chlamy/fastqc_after
    done

    
    cd /LUSTRE/usuario/smata/chlamy/trimmo/ck2 
    for i in  *gz
    do
    fastqc /LUSTRE/usuario/smata/chlamy/trimmo/ck2/$i -o /LUSTRE/usuario/smata/chlamy/fastqc_after
    done
    



#### *Martes 23 de abril de 2019*


Se descarg� en en la computadora el archivo de coordenadas del genoma de *C. rienhardtii* en formato *gff* desde la base de datos del *NCBI*,
en una carpeta llamda *datos_genoma*:


    $ wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/002/595/GCF_000002595.1_v3.0/GCF_000002595.1_v3.0_genomic.gff.gz


Una vez descargado se convirti� a formato *gft* con *gffread*, para lo cu�l �ste se instal� primero con *Bioconda*:


    $ conda install gffread
    $ gffread -E GCF_000002595.1_v3.0_genomic.gff -T -o GCF_000002595.1_v3.0_genomic.gtf
    
La opci�n -E le indica al programa que muestre cualquier advertencia en caso de que haya un potencial problema con el archivo *gff*. Cuando se
corri� el *script* el mensaje que apareci� fue el siguiente:

    GFF Warining: duplicate features ID gene 14405 (69520-71506) (discontinuous feature?)
    
La opci�n -T le indic� a *gffread* que realice la conversi�n al nuevo formato y el argumento -o se refiere al archivo de salida.



#### *Viernes 26 de abril de 2019*


Se corri� nuevamente el procesamiento con *Trimmomatic* de las secuencias, debido a que se detect� un problema en los archivos que se hab�an 
generado. Debido a que la secuenciaci�n fue *paired-end* cada ejecutable iba a tener especificado el nombres de las secuencias pares y cuatro 
archivos de salida (*output*) correspondientes a las muestras *forward-paired (FP9)*, *forward-unpaired (FU)*, *reverse-paired (RP)* y 
*reverse-unpaired (RU)*, lo cual no se hab�a la primera vez que se procesaron las secuencias y por esa raz�n se repiti� el an�lisis. As� tambi�n
se decidi� recortar 5 pb en el extremo 5' y no 1 pb como se hab�a realizado anteriormente.
El *script* utilizado para la muestra *AgNPs_12h_1* fue el siguiente:


    #PBS -N trimmo_Ag12_1
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=20gb,walltime=1:00:00
    #PBS -V
    #PBS -o trimmo_Ag12_1.out
    #PBS -e trimmo_Ag12_1.err

    module load Trimmomatic/0.32

    cd $PBS_O_WORKDIR

    
    java -jar $TRIMMOMATIC PE -threads 2 /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq/AgNPs_12h_1_1.fastq.gz
    /LUSTRE/usuario/smata//chlamy/datos_transcriptoma/chlamy_fastq/AgNPs_12h_1_2.fastq.gz Trimmed_AgNPs_12h_1_FP.fastq.gz 
    Trimmed_AgNPs_12h_1_FU.fastq.gz Trimmed_AgNPs_12h_1_RP.fastq.gz Trimmed_AgNPs_12h_1_RU.fastq.gz ILLUMINACLIP:TruSeq3-PE-2.fa:2:30:10 
    HEADCROP:5 SLIDINGWINDOW:4:15  MINLEN:60


As� tambi�n ese mismo script se utiliz� para las dem�s 5 muestras restantes con sus respectivos duplicados, en donde se modific� el nombre del trabajo,
el nombres de los archivos *err* y *out*, y el nombre de cada una de las muestras tal y como correspond�a.

    
    Para la muestra AgNPs_12h_2:

    java -jar $TRIMMOMATIC PE -threads 2 /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq/AgNPs_12h_2_1.fastq.gz
    /LUSTRE/usuario/smata//chlamy/datos_transcriptoma/chlamy_fastq/AgNPs_12h_2_2.fastq.gz Trimmed_AgNPs_12h_2_FP.fastq.gz 
    Trimmed_AgNPs_12h_2_FU.fastq.gz Trimmed_AgNPs_12h_2_RP.fastq.gz Trimmed_AgNPs_12h_2_RU.fastq.gz ILLUMINACLIP:TruSeq3-PE-2.fa:2:30:10 
    HEADCROP:5 SLIDINGWINDOW:4:15  MINLEN:60
    
   
    Para la muestra AgNPs_2h_1:
    
    java -jar $TRIMMOMATIC PE -threads 2 /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq/AgNPs_2h_1_1.fastq.gz
    /LUSTRE/usuario/smata//chlamy/datos_transcriptoma/chlamy_fastq/AgNPs_2h_1_2.fastq.gz Trimmed_AgNPs_2h_1_FP.fastq.gz 
    Trimmed_AgNPs_2h_1_FU.fastq.gz Trimmed_AgNPs_2h_1_RP.fastq.gz Trimmed_AgNPs_2h_1_RU.fastq.gz ILLUMINACLIP:TruSeq3-PE-2.fa:2:30:10 
    HEADCROP:5 SLIDINGWINDOW:4:15  MINLEN:60

    
    Para la muestra AgNPs_2h_2:

    java -jar $TRIMMOMATIC PE -threads 2 /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq/AgNPs_2h_2_1.fastq.gz
    /LUSTRE/usuario/smata//chlamy/datos_transcriptoma/chlamy_fastq/AgNPs_2h_2_2.fastq.gz Trimmed_AgNPs_2h_2_FP.fastq.gz 
    Trimmed_AgNPs_2h_2_FU.fastq.gz Trimmed_AgNPs_2h_2_RP.fastq.gz Trimmed_AgNPs_2h_2_RU.fastq.gz ILLUMINACLIP:TruSeq3-PE-2.fa:2:30:10 
    HEADCROP:5 SLIDINGWINDOW:4:15  MINLEN:60
    
    
    Para la muestra CK_12h:
    
    java -jar $TRIMMOMATIC PE -threads 2 /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq/CK_12h_1.fastq.gz /LUSTRE/usuario/
    smata/chlamy/datos_transcriptoma/chlamy_fastq/CK_12h_2.fastq.gz Trimmed_CK_12h_FP.fastq.gz Trimmed_CK_12h_FU.fastq.gz 
    Trimmed_CK_12h_RP.fastq.gz Trimmed_CK_12h_RU.fastq.gz ILLUMINACLIP:TruSeq3-PE-2.fa:2:30:10 HEADCROP:5 SLIDINGWINDOW:4:15 MINLEN:60
    
    
    Para la muestra CK_2h:
    
    java -jar $TRIMMOMATIC PE -threads 2 /LUSTRE/usuario/smata/chlamy/datos_transcriptoma/chlamy_fastq/CK_2h_1.fastq.gz /LUSTRE/usuario/
    smata/chlamy/datos_transcriptoma/chlamy_fastq/CK_2h_2.fastq.gz Trimmed_CK_2h_FP.fastq.gz Trimmed_CK_2h_FU.fastq.gz 
    Trimmed_CK_2h_RP.fastq.gz Trimmed_CK_2h_RU.fastq.gz ILLUMINACLIP:TruSeq3-PE-2.fa:2:30:10 HEADCROP:5 SLIDINGWINDOW:4:15 MINLEN:60
    
    
Se realiz� nuevamente un an�lisis de calidad con *FastQC* con el mismo *script* utilizado anteriormente. Los archivos se pasaron del servidor 
a la computadora para reviar los reportes en *html* del an�lisis y adem�s se corri� un an�lisis general de todas las secuencias con *MultiQC* 
en la misma carpeta donde se guardaron los archivos:


    $ rsync -av smata@10.10.10.240:/LUSTRE/usuario/smata/chlamy/fastqc_after .
    $ multiqc .


Se observ� que la calidad de las secuencias mejor�, sobre todo en el extremo 5' por las bases eliminadas y adem�s que se logr� remover los 
adaptadores por lo que ya no se encontraba entre las secuencias sobre representadas, sin embargo la cantidad de duplicados segu�a siendo alta
en comparaci�n a la cantidad de lecturas �nicas.

Una vez procesadas las secuencias se procedi� a trabajar con el programa *HISAT2* desde Mazorka.
Se utiliz� dicho programa para generar un �ndice para realizar el alineamiento de los archivos *fastq* *FP* y *RP* de cada muestra. El *script*
usado fue el siguiente:


    #PBS -N hisat2_index
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=1:00:00
    #PBS -V
    #PBS -o hisat2_index.out
    #PBS -e hisat2_index.err

    module load hisat2/2.1.0

    cd $PBS_O_WORKDIR

    hisat2-build -f /LUSTRE/usuario/smata/chlamy/datos_genoma/GCF_000002595.1_v3.0_genomic.fna chlamy_index
    
    


#### *Domingo 28 de abril de 2019*


Una vez generado el �ndice para *C. reinhardtii*, llamado *chlamy_index*, se procedi� a realizar el alineamiento de las lecturas con *HISAT2*:


    #PBS -N hisat2_align
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=20gb,walltime=120:00:00
    #PBS -V
    #PBS -o hisat2_align.out
    #PBS -e hisat2_align.err

    module load hisat2/2.1.0

    cd $PBS_O_WORKDIR


    cd /LUSTRE/usuario/smata/chlamy/trimmo/agnps12_1

    hisat2 -p 4 --dta -x /LUSTRE/usuario/smata/chlamy/hisat2/index/chlamy_index --fr -1 /LUSTRE/usuario/smata/chlamy/trimmo/agnps12_1/
    Trimmed_AgNPs_12h_1_FP.fastq.gz -2 /LUSTRE/usuario/smata/chlamy/trimmo/agnps12_1/Trimmed_AgNPs_12h_1_RP.fastq.gz -S /LUSTRE/usuario/
    smata/chlamy/hisat2/align/AgNPs_12h_1.sam --summary-file /LUSTRE/usuario/smata/chlamy/hisat2/align/AgNPs_12h_1_sum.txt


    cd /LUSTRE/usuario/smata/chlamy/trimmo/agnps12_2

    hisat2 -p 4 --dta -x /LUSTRE/usuario/smata/chlamy/hisat2/index/chlamy_index --fr -1 /LUSTRE/usuario/smata/chlamy/trimmo/agnps12_2/
    Trimmed_AgNPs_12h_2_FP.fastq.gz -2 /LUSTRE/usuario/smata/chlamy/trimmo/agnps12_2/Trimmed_AgNPs_12h_2_RP.fastq.gz -S /LUSTRE/usuario/
    smata/chlamy/hisat2/align/AgNPs_12h_2.sam --summary-file /LUSTRE/usuario/smata/chlamy/hisat2/align/AgNPs_12h_2_sum.txt


    cd /LUSTRE/usuario/smata/chlamy/trimmo/agnps2_1

    hisat2 -p 4 --dta -x /LUSTRE/usuario/smata/chlamy/hisat2/index/chlamy_index --fr -1 /LUSTRE/usuario/smata/chlamy/trimmo/agnps2_1/
    Trimmed_AgNPs_2h_1_FP.fastq.gz -2 /LUSTRE/usuario/smata/chlamy/trimmo/agnps2_1/Trimmed_AgNPs_2h_1_RP.fastq.gz -S /LUSTRE/usuario/
    smata/chlamy/hisat2/align/AgNPs_2h_1.sam --summary-file /LUSTRE/usuario/smata/chlamy/hisat2/align/AgNPs_2h_1_sum.txt


    cd /LUSTRE/usuario/smata/chlamy/trimmo/agnps2_2

    hisat2 -p 4 --dta -x /LUSTRE/usuario/smata/chlamy/hisat2/index/chlamy_index --fr -1 /LUSTRE/usuario/smata/chlamy/trimmo/agnps2_2/
    Trimmed_AgNPs_2h_2_FP.fastq.gz -2 /LUSTRE/usuario/smata/chlamy/trimmo/agnps2_2/Trimmed_AgNPs_2h_2_RP.fastq.gz -S /LUSTRE/usuario/
    smata/chlamy/hisat2/align/AgNPs_2h_2.sam --summary-file /LUSTRE/usuario/smata/chlamy/hisat2/align/AgNPs_2h_2_sum.txt


    cd /LUSTRE/usuario/smata/chlamy/trimmo/ck12

    hisat2 -p 4 --dta -x /LUSTRE/usuario/smata/chlamy/hisat2/index/chlamy_index --fr -1 /LUSTRE/usuario/smata/chlamy/trimmo/ck12/
    Trimmed_CK_12h_FP.fastq.gz -2 /LUSTRE/usuario/smata/chlamy/trimmo/ck12/Trimmed_CK_12h_RP.fastq.gz -S /LUSTRE/usuario/smata/chlamy/
    hisat2/align/CK_12h.sam --summary-file /LUSTRE/usuario/smata/chlamy/hisat2/align/CK_12h_sum.txt


    cd /LUSTRE/usuario/smata/chlamy/trimmo/ck2

    hisat2 -p 4 --dta -x /LUSTRE/usuario/smata/chlamy/hisat2/index/chlamy_index --fr -1 /LUSTRE/usuario/smata/chlamy/trimmo/ck2/
    Trimmed_CK_2h_FP.fastq.gz -2 /LUSTRE/usuario/smata/chlamy/trimmo/ck2/Trimmed_CK_2h_RP.fastq.gz -S /LUSTRE/usuario/smata/chlamy/
    hisat2/align/CK_2h.sam --summary-file /LUSTRE/usuario/smata/chlamy/hisat2/align/CK_2h_sum.txt


Para el alineamiento los argumentos utilizados fueron los siguientes:

    -p 4: Indica la cantidad de threads utilizados que en este caso fueron 4.
    --dta: Indica que reporte las alineaciones reportadas para ensambladores de transcripci�n.
    -x: Indica el �ndice a utilizar para el alineamiento, chlamy_index en este caso.
    --fr: Indica la direcci�n de las librer�as, forward-reverse.
    -1: Indica el primer archivo de entrada forward-paired en este caso.
    -2: Indica el segundo archivo de entrada reverse-paired en este caso.
    -S: Especifica el archivo de salida en formato sam.
    --summary-file: Permite obtener un archivo resumen del alineamiento.


Los archivos *sam* de salida fueron debidamente ordenadas y convertidos a formato comprimido *bam* utilizando *samtools* desde Mazorka:


    #PBS -N samtools
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=20gb,walltime=120:00:00
    #PBS -V
    #PBS -o convert_sam.out
    #PBS -e convert_sam.err

    module load samtools/1.9

    cd $PBS_O_WORKDIR

    samtools sort -@ 8 -o /LUSTRE/usuario/smata/chlamy/hisat2/sam_to_bam/AgNPs_12h_1.bam /LUSTRE/usuario/smata/chlamy/hisat2/align/
    AgNPs_12h_1.sam

    samtools sort -@ 8 -o /LUSTRE/usuario/smata/chlamy/hisat2/sam_to_bam/AgNPs_12h_2.bam /LUSTRE/usuario/smata/chlamy/hisat2/align/
    AgNPs_12h_2.sam

    samtools sort -@ 8 -o /LUSTRE/usuario/smata/chlamy/hisat2/sam_to_bam/AgNPs_2h_1.bam /LUSTRE/usuario/smata/chlamy/hisat2/align/
    AgNPs_2h_1.sam

    samtools sort -@ 8 -o /LUSTRE/usuario/smata/chlamy/hisat2/sam_to_bam/AgNPs_2h_2.bam /LUSTRE/usuario/smata/chlamy/hisat2/align/
    AgNPs_2h_2.sam

    samtools sort -@ 8 -o /LUSTRE/usuario/smata/chlamy/hisat2/sam_to_bam/CK_12h.bam /LUSTRE/usuario/smata/chlamy/hisat2/align/
    CK_12h.sam

    samtools sort -@ 8 -o /LUSTRE/usuario/smata/chlamy/hisat2/sam_to_bam/CK_2h.bam /LUSTRE/usuario/smata/chlamy/hisat2/align/
    CK_2h.sam
     
     
    
Para la corrida realizada con *samtools* los argumentos usados fueron los siguientes:


    sort: Indica al programa que ordene los archivos de acuerdo a la posici�n de las coordenadas a partir de del lado izquierdo.
    -@: Indica el n�mero de threads a utilizar, en este caso fueron 8.
    -o: Se refiere al archivo de salida, en este caso el archivo bam convertido.


Los archivos *bam* generados fueron usados para realizar el ensamblaje de *C. reinhardtii* usando el programa *Stringtie* desde Mazorka, el
*script* usado fue el siguiente:


    #PBS -N stringtie_ensam
    #PBS -q ensam
    #PBS -l nodes=1:ppn=1,vmem=20gb,walltime=120:00:00
    #PBS -V
    #PBS -o st_chlamy_ensam.out
    #PBS -e st_chlamy_ensam.err

    module load stringtie/1.3.4d

    cd $PBS_O_WORKDIR

    stringtie -p 8 -G /LUSTRE/usuario/smata/chlamy/datos_genoma/GCF_000002595.1_v3.0_genomic.gtf /LUSTRE/usuario/smata/chlamy/hisat2/
    sam_to_bam/AgNPs_12h_1.bam -l AgNPs_12h_1 -o /LUSTRE/usuario/smata/chlamy/stringtie_ensam/AgNPs_12h_1.gtf -A /LUSTRE/usuario/smata/
    chlamy/stringtie_ensam/AgNPs_12h_1_geneabun.tab

    stringtie -p 8 -G /LUSTRE/usuario/smata/chlamy/datos_genoma/GCF_000002595.1_v3.0_genomic.gtf /LUSTRE/usuario/smata/chlamy/hisat2/
    sam_to_bam/AgNPs_12h_2.bam -l AgNPs_12h_2 -o /LUSTRE/usuario/smata/chlamy/stringtie_ensam/AgNPs_12h_2.gtf -A /LUSTRE/usuario/smata/
    chlamy/stringtie_ensam/AgNPs_12h_2_geneabun.tab

    stringtie -p 8 -G /LUSTRE/usuario/smata/chlamy/datos_genoma/GCF_000002595.1_v3.0_genomic.gtf /LUSTRE/usuario/smata/chlamy/hisat2/
    sam_to_bam/AgNPs_2h_1.bam -l AgNPs_2h_1 -o /LUSTRE/usuario/smata/chlamy/stringtie_ensam/AgNPs_2h_1.gtf -A /LUSTRE/usuario/smata/
    chlamy/stringtie_ensam/AgNPs_2h_1_geneabun.tab

    stringtie -p 8 -G /LUSTRE/usuario/smata/chlamy/datos_genoma/GCF_000002595.1_v3.0_genomic.gtf /LUSTRE/usuario/smata/chlamy/hisat2/
    sam_to_bam/AgNPs_2h_2.bam -l AgNPs_2h_2 -o /LUSTRE/usuario/smata/chlamy/stringtie_ensam/AgNPs_2h_2.gtf -A /LUSTRE/usuario/smata/
    chlamy/stringtie_ensam/AgNPs_2h_2_geneabun.tab

    stringtie -p 8 -G /LUSTRE/usuario/smata/chlamy/datos_genoma/GCF_000002595.1_v3.0_genomic.gtf /LUSTRE/usuario/smata/chlamy/hisat2/
    sam_to_bam/CK_12h.bam -l CK_12h -o /LUSTRE/usuario/smata/chlamy/stringtie_ensam/CK_12h.gtf -A /LUSTRE/usuario/smata/chlamy/
    stringtie_ensam/CK_12h_geneabun.tab

    stringtie -p 8 -G /LUSTRE/usuario/smata/chlamy/datos_genoma/GCF_000002595.1_v3.0_genomic.gtf /LUSTRE/usuario/smata/chlamy/hisat2/
    sam_to_bam/CK_2h.bam -l CK_2h -o /LUSTRE/usuario/smata/chlamy/stringtie_ensam/CK_2h.gtf -A /LUSTRE/usuario/smata/chlamy/
    stringtie_ensam/CK_2h_geneabun.tab


A diferencia de las veces anteriores que se han corrido *scripts* en Mazorka en la cola *default* esta vez �ste se mand� a correr en la cola 
*ensam*. Los argumentos utilizados para realizar el ensamblaje con *Stringtie* fueron los siguientes:

   
    -p 8: Indica el n�mero de threads, en este caso 8.
    -G: Se refiere al archivo de referencia de la anotaci�n.
    -l: Indica el nombre del prefijo de los transcritos de salida.
    -o: Los archivos de salida para los transcritos ensamblados en formato gtf.
    -A: Indica que de un archivo de salid de la estimaci�n de la abundancia de genes.
    

Todos los archivos gtf generados del ensamble fueron concatenados en uno s�lo usando el argumentos *-merge* de *Stringtie*:


    #PBS -N stringtie_merge
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=00:30:00
    #PBS -V
    #PBS -o merged.out
    #PBS -e merged.err

    module load stringtie/1.3.4d

    cd $PBS_O_WORKDIR

    stringtie --merge -p 8 -G /LUSTRE/usuario/smata/chlamy/datos_genoma/GCF_000002595.1_v3.0_genomic.gtf /LUSTRE/usuario/smata/chlamy/
    stringtie_ensam/merged_gtfs/mergelist.txt -o ./st_merged_chlamy.gtf
    

Una vez unidos los archivos en uno s�lo se compar� este archivo generado del ensamble con el archivo gtf de referencia para el genoma de 
*C. reinhardtii*, usando *gffcompare* desde Mazorka:

    
    #PBS -N gffcompare
    #PBS -q default
    #PBS -l nodes=1:ppn=1,vmem=4gb,walltime=00:30:00
    #PBS -V
    #PBS -o compare.out
    #PBS -e compare.err

    module load gffcompare/0.10.4

    cd $PBS_O_WORKDIR

    gffcompare -R -V -r /LUSTRE/usuario/smata/chlamy/datos_genoma/GCF_000002595.1_v3.0_genomic.gtf -o compare /LUSTRE/usuario/smata/
    chlamy/stringtie_ensam/merged_gtfs/st_merged_chlamy.gtf 


Para el *script* anterior los argumentos utilizados fueron los siguientes:


    -r: El archivo de anotaci�n de referencia en formato gtf.
    -R: Para la opci�n -r, indica que considere solamente los transcritos de referencia que �nicamente sobrelapan alguno de los arhivos 
    de entrada.
    -V: Modo de procesamiento detallada, el cual indica mensajes de advertencia en caso de cualquier inconsistencia o potencial riesgo 
    encontrado mientras se leen los archivos gtf dados.
    -o: Se refiere al nombre del prefijo para todos los archivos de salida creados por gffcompare.
    Ademas del archivo de entrada correspondiente al de anotacion en formato gtf obtenido luego de hacer el "merged", en este caso 
    "st_merged_chlamy.gtf" 
